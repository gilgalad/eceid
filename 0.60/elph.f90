program elph

   !this program computes the current in a 1D wire with the geometry in the hamiltonian built in diagH.
   !it can simulate phonons that interact in real time with the OB driven current
   !REQUIREMENTS: the execution of diagH

   use modvar
   implicit none
   
   integer :: i,j
   type(global) :: g
   type(rhoEvo), allocatable :: rEvo(:) 
   type(oscillator), allocatable :: o(:)
   type(oscillatorEvo), allocatable :: oEvo(:,:) 
 
   allocate(g%Hel(nlev,nlev))
   allocate(g%Hprojections(nlev,nlev))
   allocate(g%HprojectionsTC(nlev,nlev))
   allocate(g%E(nlev))
   allocate(g%inj(nlev,nlev))       !OB injection term for a 0 temperature FD distribution

   !NEW
   allocate(rEvo(3))  !allocate entire type, DM at 3 times
   allocate(g%rdot(nlev,nlev))
   do j=1,3 !the density matrix in the past, present, future  
      allocate(rEvo(j)%rho(nlev,nlev))
   enddo

   if(nho .ne. 0) then
      allocate(o(nho)) 
      open(33, file='input.dat')
      do i=1,nho
         allocate(o(i)%F(nlev,nlev))
         allocate(o(i)%Fstore(nlev,nlev))
         allocate(o(i)%CCdot(nlev,nlev))
         allocate(o(i)%CSdot(nlev,nlev))
         allocate(o(i)%ACdot(nlev,nlev))
         allocate(o(i)%ASdot(nlev,nlev))
         allocate(o(i)%mu(nlev,nlev))
         read(33,*) o(i)%coup, o(i)%N, o(i)%om, o(i)%mass
         o(i)%om = o(i)%om/hb !so that the om is in eV/hb units = fs^-1
         o(i)%mass = o(i)%mass*amu !so that the oscillator mass is in amu units
      end do
      close(33)

      allocate(oEvo(nho,3)) !NEW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! I had to switch to 1,2,3.Hopefully when I complete the transition it will not be too confusing
      do i=1,nho
         !do j=1,3
            !allocate(oEvo(i,j)%CC(nlev,nlev))   !they are not allocatable anymore
            !allocate(oEvo(i,j)%CS(nlev,nlev))
            !allocate(oEvo(i,j)%AC(nlev,nlev))
            !allocate(oEvo(i,j)%AS(nlev,nlev))
         !enddo
         oEvo(i,2)%N = o(i)%N
      enddo

   else
      !just allocate a fake oscillator so that the subroutines don't crash
      allocate(o(1))
   endif

   open(34, file='phon.dat')
   open(35, file='phon2.dat')   
   open(22, file='phon3.dat') 
   open(36, file='bondcurrent.dat')
   open(37, file='energiaHO.dat')
   open(41, file='tracerho.dat')
   open(42, file='debug.dat')
   open(43, file='trMu.dat')
   open(66, file='varMu.dat')
   open(67, file='varMunorm.dat')
   open(68, file='purity.dat')   
   open(73, file='enel.dat')
   open(74, file='entrel.dat')
   open(47, file='entr2el.dat') !TOGLIMI  
   open(75, file='Tel.dat')
   open(57, file='T2el.dat')  !TOGLIMI
   open(76, file='enosc.dat')
   open(77, file='entrosc.dat')
   open(78, file='Tosc.dat')  
   open(79, file='totenergy.dat')
   open(80, file='avTel.dat')  !TOGLIMI
   open(81, file='avT2el.dat')  !TOGLIMI
   open(82, file='deltaE.dat')
   open(83, file='occupation.dat')

   call initialize(g,rEvo,o,oEvo)
   call leapfrog(g,rEvo,o,oEvo)


!-------------------END MAIN PROGRAM-------------------------


contains

!----------------------
!setting up the initial parameters and the step 0 of the time evolution
!----------------------
subroutine initialize(g,rEvo,o,oEvo)

   use modvar
   implicit none
   
   integer :: i,j,l,k,numel
   integer, dimension(nlev*nlev,2) :: temp
   complex(dp), dimension(nlev,nlev) :: buf,buf2
   type(global) :: g
   type(rhoEvo), dimension(:) :: rEvo
   type(oscillator), dimension(:) :: o
   type(oscillatorEvo), dimension(:,:) :: oEvo    

   !files where the initial hamiltonian and its GS DM are stored
   open(44, file='ham',form='unformatted')
   open(45, file='rhozero',form='unformatted')
    
   !initialize the electronic hamiltonian
   read(44) g%Hel,g%Hprojections,g%E
   !initialize the electronic density matrix
   read(45) rEvo(2)%rho
   close(44)
   close(45)
   if(openboundaries .ne. 0) then
      !initialize the injection term in the OB
      open(46, file='inj',form='unformatted')
      read(46) g%inj
      close(46)
   endif

   !find nonzero elements of the Hamiltonian and store their position, great optimization for matmul with sparse matrices
   call findnonZero(g%Hel,temp,numel)
   allocate(g%nonZeroH(numel,2))
   g%nonZeroH = temp(1:numel,:)
   !write(*,*) 'H',numel
    
   !it's real BUT I use it as complex because of the lapack function zgemm
   g%HprojectionsTC = conjg(transpose(g%Hprojections)) 
   !g%HprojectionsTC = (transpose(g%Hprojections))        
  
   !OB terms initialization. g%SigmaPlus
   buf = 0.0_dp  !this effectively is SigmaPlus
   do i= 1,NL
      buf(i,i) = -im*gam*0.5_dp
   enddo
   do i= NL+NC+1,nlev
      buf(i,i) = -im*gam*0.5_dp
   enddo
   call findnonZeroC(buf,temp,numel)
   allocate(g%nonZeroSigma(numel,2))  
   g%nonZeroSigma = temp(1:numel,:)
   allocate(g%SigmaPlus(numel))
   call buildSparseC(g%nonZeroSigma,buf,g%SigmaPlus) !!!!!!!!!!!!!!KEY NEW FUNCTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !NEW, creation of Hplus for the explicit evolution of CC/CS/AC/AS
   if(HplusSwitch == 1) then   
      buf2 = g%Hel + buf  !it was g%Hplus = g%Hel + g%SigmaPlus
   elseif(HplusSwitch == 2) then  
      buf2 = g%Hel
      do i=1,nlev
         buf2(i,i) = buf2(i,i) - im*epsE*0.5_dp
      enddo
   endif
   call findnonZeroC(buf2,temp,numel)  !careful that Hplus is complex, findnonzeroC is needed
   allocate(g%nonZeroHplus(numel,2))
   g%nonZeroHplus = temp(1:numel,:)
   allocate(g%Hplus(numel))
   call buildSparseC(g%nonZeroHplus,buf2,g%Hplus) !!!!!!!!!!!!!!KEY NEW FUNCTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   buf = conjg(transpose(buf2))
   call findnonZeroC(buf,temp,numel)  
   allocate(g%nonZeroHplusTC(numel,2))
   g%nonZeroHplusTC = temp(1:numel,:)
   allocate(g%HplusTC(numel))
   call buildSparseC(g%nonZeroHplusTC,buf,g%HplusTC) !!!!!!!!!!!!!!KEY NEW FUNCTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   g%avTel = inTel  !TOGLIMI
   g%avTel2 = inTel  !TOGLIMI

   !initialize the phonon variables
   if(nho .ne. 0) then
      open(62, file='coupHO',form='unformatted')

      do i=1,nho       
         !read the interaction built in diagH
         read(62) o(i)%F
         call findnonZero(o(i)%F,temp,numel)
         allocate(o(i)%nonZeroF(numel,2)) !labels of the nonzero elements of F
         o(i)%nonZeroF = temp(1:numel,:)
         !write(*,*) 'F',numel

         call FmatmulLrcc(o(i)%nonZeroF,o(i)%F,rEvo(2)%rho,buf) !CAREFUL about this, for specific rho it may be wrong and break the program
         call findnonZeroC(buf,temp,numel)  
         allocate(o(i)%nonZeroFrho(numel,2))
         o(i)%nonZeroFrho = temp(1:numel,:)

         o(i)%Fstore = o(i)%F   !trick for the linear turning on of F, the real F is stored in Fstore
         if(Tcoup > zero) then
            o(i)%F = 0.0_dp    !if there is a linear turning on of F, at the first time step it is zero
         endif      

         !NEW!!!!!!!!!!!!!!
         oEvo(i,2)%CC = 0.0_dp
         oEvo(i,2)%CS = 0.0_dp
         oEvo(i,2)%AC = 0.0_dp
         oEvo(i,2)%AS = 0.0_dp

         if( fixedN == 0 ) then  !normal evolution for N, isolated system
            call Npunto(o(i),oEvo(i,2)) 
         elseif( fixedN == 1 ) then  !N is fixed, we consider Ndot zero
            o(i)%Ndot = 0.0_dp
         endif
         call ABCDpunto(g, rEvo(2)%rho, o(i), oEvo(i,2) )
         !MIXED, careful, abcdpunto va riscritta
         oEvo(i,1)%N = oEvo(i,2)%N - (dt*o(i)%Ndot)
         oEvo(i,1)%CC = oEvo(i,2)%CC - (dt*o(i)%CCdot)
         oEvo(i,1)%CS = oEvo(i,2)%CS - (dt*o(i)%CSdot)
         oEvo(i,1)%AC = oEvo(i,2)%AC - (dt*o(i)%ACdot)
         oEvo(i,1)%AS = oEvo(i,2)%AS - (dt*o(i)%ASdot)
         oEvo(i,3)%N = oEvo(i,1)%N + (2.0_dp*dt*o(i)%Ndot)
         oEvo(i,3)%CC = oEvo(i,1)%CC + (2.0_dp*dt*o(i)%CCdot)
         oEvo(i,3)%CS = oEvo(i,1)%CS + (2.0_dp*dt*o(i)%CSdot)
         oEvo(i,3)%AC = oEvo(i,1)%AC + (2.0_dp*dt*o(i)%ACdot)
         oEvo(i,3)%AS = oEvo(i,1)%AS + (2.0_dp*dt*o(i)%ASdot)  
         
         !constants for mu, I store them so it's faster
         o(i)%c1mu = (im/(o(i)%mass * o(i)%om ))
         o(i)%c2mu = (1.0_dp/(o(i)%mass * o(i)%om))
         call mu(o(i), oEvo(i,2) )
      end do
   endif
   close(62)

   if(fixedRho == 0) then  !normal evolution for rho
      call rhopunto(g,rEvo(2)%rho,o,0.0_dp)
   elseif(fixedRho == 1) then  !rho is fixed to its initial condition
      g%rdot = 0.0_dp    
   endif  
   
   !backward Euler only for the first step
   rEvo(1)%rho = rEvo(2)%rho - (dt*g%rdot)
   rEvo(3)%rho = rEvo(1)%rho + (g%rdot*dt*2.0_dp)

end subroutine

!----------------------
!time evolution function, it evaluates also the observables
!----------------------
subroutine leapfrog(g,rEvo,o,oEvo)

   use OMP_LIB
   use MKL_SERVICE
   use modvar
   implicit none

   type(global) :: g
   type(rhoEvo), dimension(:) :: rEvo
   type(oscillator), dimension(:) :: o
   type(oscillatorEvo), dimension(:,:), target :: oEvo
   type(pointer), allocatable :: p(:,:)  
   integer :: i,nstep,j,dummy,num,k
   real(dp) :: time
   complex(dp) :: buffer
   complex(2*dp), dimension(5) :: OLD !I store here the old values for entropy and energy used for the time local temperature 

   !horrible workaround, NUM_THREADS didn't accept an argument 0, even if the cycle was not to get entered
   num = 1
   if(nho .ne. 0) then
      num = nho
      call OMP_SET_NUM_THREADS(nho)
   endif 
   nstep = int(T/dt)

   !NEW!!!!!!! initialize the pointers
   allocate(p(nho,3))
   do j=1,nho
      p(j,2)%ho => oEvo(j,2)
      p(j,3)%ho => oEvo(j,3)
      p(j,1)%ho => oEvo(j,1)
   enddo 
 
   do i=1,nstep
      time = dt*real(i,dp)

      !OBSERVABLES & DEBUG only once every obsfreq steps
      if(mod(i,obsfreq)==0) then
         !o(:)%N = oEvo(:,2)%N  !legacy notation, TOGLIMI
         do j=1,nho
            o(j)%N = p(j,2)%ho%N  !legacy notation, TOGLIMI
         enddo
         call observables(g,rEvo(2)%rho,o,time,OLD)
      endif
      !write(42,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time, real(p(2,1)%ho%N,dp),real(p(2,2)%ho%N,dp), real(p(2,3)%ho%N,dp), real(oEvo(2,2)%N,dp)
      
      !RENAMING
      !trasforming the new step into step 1 and proceding to the next time step
      !Effectively the following is for the following timestep
      rEvo(1)%rho = rEvo(2)%rho
      rEvo(2)%rho = rEvo(3)%rho

      if(nho .ne. 0) then
         !$OMP PARALLEL DEFAULT(shared),PRIVATE(j) NUM_THREADS(num)         
         !$OMP DO SCHEDULE(STATIC)
         do j=1,nho
            !Linear ramp for F
            if(Tcoup > zero) then
               if(time <= Tcoup) then
                  o(j)%F = o(j)%Fstore*(time)*(1.0_dp/Tcoup)  !after Tcoup, it should be just F, CHECK that it is
               endif 
            endif
               
            !intermediate way of doing it, until I fix the pointers
            !oEvo(j,1)%CC = oEvo(j,2)%CC
            !oEvo(j,1)%CS = oEvo(j,2)%CS
            !oEvo(j,1)%AC = oEvo(j,2)%AC
            !oEvo(j,1)%AS = oEvo(j,2)%AS
            !oEvo(j,1)%N = oEvo(j,2)%N
            !oEvo(j,2)%CC = oEvo(j,3)%CC
            !oEvo(j,2)%CS = oEvo(j,3)%CS
            !oEvo(j,2)%AC = oEvo(j,3)%AC
            !oEvo(j,2)%AS = oEvo(j,3)%AS
            !oEvo(j,2)%N = oEvo(j,3)%N

            !NEW, KEY POINT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if( associated( p(j,2)%ho, oEvo(j,2)) ) then
               p(j,2)%ho => oEvo(j,3)
               p(j,3)%ho => oEvo(j,1)
               p(j,1)%ho => oEvo(j,2)               
            elseif( associated( p(j,2)%ho, oEvo(j,3)) ) then
               p(j,2)%ho => oEvo(j,1)
               p(j,3)%ho => oEvo(j,2)
               p(j,1)%ho => oEvo(j,3) 
            elseif( associated( p(j,2)%ho, oEvo(j,1)) ) then
               p(j,2)%ho => oEvo(j,2)
               p(j,3)%ho => oEvo(j,3)
               p(j,1)%ho => oEvo(j,1) 
            endif

            !call ABCDpunto(g, rEvo(2)%rho, o(j), oEvo(j,2) ) 
            call ABCDpunto(g, rEvo(2)%rho, o(j), p(j,2)%ho ) 

            !IMPORTANT, mu and Npunto use A,B,C,D at the next step, CHECK WELL!!!!!!!!!!!!!!!!!
            !the following quantities are computed for the neext time step. Is mu not necessary for this time step?
            !or maybe not, it should be at the next because it appears in rhopunto for the next
            !comment better leapfrog, this whole section is uniform now            
            if( fixedN == 0 ) then  
               !call Npunto(o(j), oEvo(j,2))
               call Npunto(o(j), p(j,2)%ho) 
            elseif( fixedN == 1 ) then
               o(j)%Ndot = 0.0_dp
            endif
            !call mu(o(j), oEvo(j,2) )
            call mu(o(j), p(j,2)%ho )

            !EVOLUTION of the oscillator variables
            !EULER TRICK
            if(mod(i+1,trick)==0) then         
               !oEvo(j,3)%N  = oEvo(j,2)%N + (dt*o(j)%Ndot)
               !oEvo(j,3)%CC = oEvo(j,2)%CC + (dt*o(j)%CCdot)
               !oEvo(j,3)%CS = oEvo(j,2)%CS + (dt*o(j)%CSdot)
               !oEvo(j,3)%AC = oEvo(j,2)%AC + (dt*o(j)%ACdot)
               !oEvo(j,3)%AS = oEvo(j,2)%AS + (dt*o(j)%ASdot)
               p(j,3)%ho%N  = p(j,2)%ho%N + (dt*o(j)%Ndot)
               p(j,3)%ho%CC = p(j,2)%ho%CC + (dt*o(j)%CCdot)
               p(j,3)%ho%CS = p(j,2)%ho%CS + (dt*o(j)%CSdot)
               p(j,3)%ho%AC = p(j,2)%ho%AC + (dt*o(j)%ACdot)
               p(j,3)%ho%AS = p(j,2)%ho%AS + (dt*o(j)%ASdot)
            !NORMAL LEAPFROG
            else      
               !oEvo(j,3)%N  = oEvo(j,1)%N + (2.0_dp*dt*o(j)%Ndot)
               !oEvo(j,3)%CC = oEvo(j,1)%CC + (2.0_dp*dt*o(j)%CCdot)
               !oEvo(j,3)%CS = oEvo(j,1)%CS + (2.0_dp*dt*o(j)%CSdot)
               !oEvo(j,3)%AC = oEvo(j,1)%AC + (2.0_dp*dt*o(j)%ACdot)
               !oEvo(j,3)%AS = oEvo(j,1)%AS + (2.0_dp*dt*o(j)%ASdot)
               p(j,3)%ho%N  = p(j,1)%ho%N + (2.0_dp*dt*o(j)%Ndot)
               p(j,3)%ho%CC = p(j,1)%ho%CC + (2.0_dp*dt*o(j)%CCdot)
               p(j,3)%ho%CS = p(j,1)%ho%CS + (2.0_dp*dt*o(j)%CSdot)
               p(j,3)%ho%AC = p(j,1)%ho%AC + (2.0_dp*dt*o(j)%ACdot)
               p(j,3)%ho%AS = p(j,1)%ho%AS + (2.0_dp*dt*o(j)%ASdot)
            endif   
         enddo
         !$OMP END DO
         !$OMP END PARALLEL
      endif

      if(fixedRho == 0) then  !normal evolution for rho
         call rhopunto(g,rEvo(2)%rho,o,time+dt)
      elseif(fixedRho == 1) then  !rho is fixed to its initial condition
         g%rdot = 0.0_dp    
      endif          
      
      !EVOLUTION RHO
      !EULER TRICK
      if(mod(i+1,trick)==0) then         
         rEvo(3)%rho = rEvo(2)%rho + (dt*g%rdot)
      !NORMAL LEAPFROG
      else
         rEvo(3)%rho = rEvo(1)%rho + (g%rdot*dt*2.0_dp)
      endif
           
   enddo
   
end subroutine

!----------------------
!EVOLUTION subroutines
!----------------------

subroutine ABCDpunto(g,r,ol,olEvo)

   use modvar
   implicit none

   type(global) :: g
   type(oscillator) :: ol
   type(oscillatorEvo) :: olEvo
   complex(dp), dimension(:,:) :: r

   integer :: i,j
   complex(dp), dimension(nlev,nlev) :: temp,tempp,buf1,buf2
   complex(dp) :: traccia,alpha,beta

   !----
   call SmatmulL(g%nonZeroHplus,g%Hplus,olEvo%CC,temp)
   call SmatmulR(g%nonZeroHplusTC,olEvo%CC,g%HplusTC,tempp)
   !call FmatmulLrcc(g%nonZeroH,g%Hel,ol%CC1,temp)
   !call FmatmulRcrc(g%nonZeroH,ol%CC1,g%Hel,tempp)
   call FmatmulLrcc(ol%nonZeroF,ol%F,r,buf1)
   call FmatmulRcrc(ol%nonZeroF,r,ol%F,buf2)
   ol%CCdot = - (im/hb) * ( temp - tempp ) + ( ol%om * olEvo%CS ) + (olEvo%N + 0.5_dp)*(buf1 - buf2) !check the SIGN of omega, me and tchavdar is different because I use tau-t
                                                                              !maybe split the above into two, I don't know how slow it is to do everything in one line
                                                                              !possibly the EOM are different too, it's better to derive everything from scratch myself!!!!!!!!
                                                                              !For now I simulate EXACTLY what Tchavdar told me, then I rederive
   
   !----
   call SmatmulL(g%nonZeroHplus,g%Hplus,olEvo%CS,temp)
   call SmatmulR(g%nonZeroHplusTC,olEvo%CS,g%HplusTC,tempp)  
   !call FmatmulLrcc(g%nonZeroH,g%Hel,ol%CS1,temp)
   !call FmatmulRcrc(g%nonZeroH,ol%CS1,g%Hel,tempp)
   ol%CSdot = - (im/hb) * ( temp - tempp ) - ( ol%om * olEvo%CC )

   !----
   call SmatmulL(g%nonZeroHplus,g%Hplus,olEvo%AC,temp)
   call SmatmulR(g%nonZeroHplusTC,olEvo%AC,g%HplusTC,tempp)
   !call FmatmulLrcc(g%nonZeroH,g%Hel,ol%AC1,temp)
   !call FmatmulRcrc(g%nonZeroH,ol%AC1,g%Hel,tempp)
   !simil-Ehrenfest, the anticommutator is off
   if (Ehrenfest == 1) then
      ol%ACdot = - (im/hb) * ( temp - tempp ) + ( ol%om * olEvo%AS ) 
   else
      ol%ACdot = - (im/hb) * ( temp - tempp ) + ( ol%om * olEvo%AS ) + 0.5_dp*( buf1 + buf2 )
      !the HF approximation terms are added, they impose fermionic statistics for the electrons
      if(hf .ne. 0) then
         temp = 0.0_dp
         !ignore this term for now
         if(hf == 1) then     
            traccia = 0.0_dp      
            do i=1,nlev
               traccia = traccia + buf1(i,i)  !buf1 is computed above and it is F rho 
            enddo
            temp = traccia * r
         endif

         call FmatmulR(ol%nonZeroFrho,r,buf1,tempp) !faster than below
         !alpha = 1.0_dp
         !beta = 0.0_dp
         !call zgemm('N','N',nlev,nlev,nlev,alpha,r,nlev,buf1,nlev,beta,tempp,nlev)  
        
         ol%ACdot = ol%ACdot + temp - tempp      
      endif
   endif

   !---
   call SmatmulL(g%nonZeroHplus,g%Hplus,olEvo%AS,temp)
   call SmatmulR(g%nonZeroHplusTC,olEvo%AS,g%HplusTC,tempp)  
   !call FmatmulLrcc(g%nonZeroH,g%Hel,ol%AS1,temp)
   !call FmatmulRcrc(g%nonZeroH,ol%AS1,g%Hel,tempp)
   ol%ASdot = - (im/hb) * ( temp - tempp ) - ( ol%om * olEvo%AC )

end subroutine

!-------------

subroutine rhopunto(g,r,o,time)

   use modvar
   implicit none

   integer :: i,j
   type(global) :: g
   type(oscillator), dimension(:) :: o
   complex(dp), dimension (:,:) :: r
   complex(dp), dimension(nlev,nlev) :: OBbuffer,temp,tempp
   real(dp) :: time
   
   !it's a collective variable, depends on ALL the oscillators better to parallelize inside rather than outside
   !OPTIMIZED
   call FmatmulLrcc(g%nonZeroH,g%Hel,r,temp)
   call FmatmulRcrc(g%nonZeroH,r,g%Hel,tempp)
   g%rdot = - (im/hb) * ( temp - tempp )

   !NON OPTIMIZED
   !g%rdot = - (im/hb) * ( matmul(g%Hel,r) - matmul(r,g%Hel) )

   !eventually parallelizable for a lot of oscilators, use critical though, it is
   !an atomic udate I think
   if(nho .ne. 0) then
      do i=1,nho
         !OPTIMIZED
         call FmatmulLrcc(o(i)%nonZeroF,o(i)%F,o(i)%mu,temp)
         call FmatmulRcrc(o(i)%nonZeroF,o(i)%mu,o(i)%F,tempp)
         !NON OPTIMIZED
         !g%rdot =  g%rdot + (im/hb) * ( matmul(o(i)%F,o(i)%mu) - matmul(o(i)%mu,o(i)%F) ) 
         
         g%rdot =  g%rdot + (im/hb) * ( temp - tempp )      
      enddo
   endif

   !OB!!!!!!!!!!!
   if(openboundaries > 0) then
      !OPTIMIZED
      call SmatmulL(g%nonZeroSigma,g%SigmaPlus,r,temp)
      call SmatmulR(g%nonZeroSigma,r,-(g%SigmaPlus),tempp)
      OBbuffer = temp - tempp
      !NON OPTIMIZED
      !OBbuffer = matmul(g%SigmaPlus,r) - matmul(r,(-g%SigmaPlus)) 

      !normal OB after TOB
      if(time >= TOB) then
         g%rdot =  g%rdot - (im/hb)*(OBbuffer + g%inj) 
      !turning on he OB linearly
      else
         g%rdot =  g%rdot - (im/hb)*(OBbuffer + g%inj)*(time)*(1.0_dp/TOB) 
      endif

   endif

end subroutine

!-------------

subroutine Npunto(ol,olEvo)

   use modvar
   implicit none

   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL, it will be a vector of dimension 1
   type(oscillatorEvo) :: olEvo

   complex(dp) :: buf(nlev,nlev)
   complex(dp) :: s1,s2
   integer :: i
   complex(dp) :: tr1,tr2,tr3,alpha,beta
   
   call FmatmulLrcc(ol%nonZeroF,ol%F,olEvo%CS,buf)
   s1 = 0.0_dp
   do i=1,nlev
      s1 = s1 + buf(i,i)
   enddo
   s1 = (im/(ol%mass * ol%om * hb)) * s1 
  
   call FmatmulLrcc(ol%nonZeroF,ol%F,olEvo%AC,buf)
   s2 = 0.0_dp
   do i=1,nlev
      s2 = s2 + buf(i,i)
   enddo
   s2 = (1.0_dp/(ol%mass * ol%om *hb)) * s2

   ol%Ndot = s1 + s2

   !SPIN DEGENERACY for all the terms since they are traced over all the electrons
   ol%Ndot = ol%Ndot * 2.0_dp

end subroutine

!-------------

subroutine mu(ol,olEvo)

   use modvar
   implicit none

   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL
   type(oscillatorEvo) :: olEvo
   
   ol%mu = ol%c1mu * olEvo%CC   -   ol%c2mu * olEvo%AS

end subroutine

!----------------------
!OBSERVABLES subroutines
!----------------------

subroutine observables(g,r,o,time,OLD)

   use modvar
   implicit none

   type(global) :: g
   type(oscillator), dimension(:) :: o
   complex(dp), dimension(:,:) :: r
   complex(dp) :: en,debug,trace,EHO,EHO2,EHO3,EHO4,buffer,purity
   complex(dp), dimension(nlev,nlev) :: musq,rhosq
   complex(dp) :: Etot,Tel,Tel2,Tosc
   complex(2*dp) :: eel,entr,eosc,entrosc
   complex(2*dp) :: entr2,entr2old   !TOGLIMI
   complex(2*dp) :: eelold,entrold,eoscold,entroscold
   complex(2*dp), dimension(:) :: OLD
   real(dp) :: time,J1,J2
   integer :: nstep,j,k,l,flag,dummy,num

   !LAPACK
   complex(dp) :: alpha,beta

   !DEBUG on hermiticity
   !call checkHermite(o(1)%FcRho - o(1)%RhoFc,flag)
   !call checkHermite(o(1)%mu,flag)                  
   !call checksame(o(1)%FcRho, conjg(transpose(o(1)%RhoFc)),time,flag)
   !call checksame(o(1)%FsRho, conjg(transpose(o(1)%RhoFs)),time,flag)

   !bond current
   J1 = -aimag(g%Hel(oscL+2,oscL+3)*r(oscL+3,oscL+2))
   J2 = -aimag(g%Hel(siteJ,siteJ+1)*r(siteJ+1,siteJ))

   !energy of a couple of oscillators
   EHO = (o(1)%N + 0.5_dp)*hb*o(1)%om
   EHO2 = (o(2)%N + 0.5_dp)*hb*o(2)%om
   EHO3 = (o(3)%N + 0.5_dp)*hb*o(3)%om
   EHO4 = (o(4)%N + 0.5_dp)*hb*o(4)%om
   write(37,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time, real(EHO,dp), real(EHO2,dp), real(EHO3,dp), real(EHO4,dp)

   !Bondcurrent output
   write(36,*) time,microampere*J2
   !write(36,*) time,microampere*J1,microampere*J2

   !screen output
   write(*,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,microampere*J1,microampere*J2, real(EHO,dp), real(EHO2,dp)
   !write(*,'(f8.3,f11.6,f11.6,f11.6)') time,microampere*J1,microampere*J2,real(o(1)%N,dp)
   !write(*,'(f8.3,f11.6,f11.6,f11.6,i10)') time,microampere*J1,microampere*J2,real(o(1)%N,dp),flag

   !check of the trace of rho
   trace = 0.0_dp
   do k=1,nlev
      trace = trace + r(k,k)
   enddo
   write(41,*) time,trace

   !PURITY CHECK for rho. Is the system in a pure state and does it stay in it??
   !alpha = 1.0_dp
   !beta = 0.0_dp
   !call zgemm('N','N',nlev,nlev,nlev,alpha,r,nlev,r,nlev,beta,rhosq,nlev) 
   !trace = 0.0_dp
   !do k=1,nlev
   !   trace = trace + rhosq(k,k)
   !enddo
   !purity = 1.0_dp - trace/(nlev/2.0_dp)
   !write(68,*) time,real(purity,dp)
                
   if(dualtemperature == 1) then
      
      Eel = energy(g,r,o,1)   !electronic energy        
      write(73,*) time, real(Eel,2*dp)
      entr = entropy(g,r,o%N,1,time) !electronic entropy     
      write(74,*) time,real(entr,2*dp) 
      entr2 = entropy(g,r,o%N,3,time) !electronic entropy 2    
      write(47,*) time,real(entr2,2*dp) 

      eosc = energy(g,r,o,2)   !oscillator energy
      write(76,*) time,real(eosc,2*dp) 
      entrosc = entropy(g,r,o%N,2,time) !oscillator entropy
      write(77,*) time,real(entrosc,2*dp)

      !TOTAL energy
      write(79,*) time, real(Eel,dp)+real(eosc,dp)
      if(time==Tcoup) g%Ein = real(Eel,dp)+real(eosc,dp)   !sets the initial energy after the ramp

      if(i == 1) then  !temporary workaround
         OLD(1) = eel     !eelold
         OLD(2) = entr    !entrold
         OLD(3) = eosc    !eoscold
         OLD(4) = entrosc !entroscold
         OLD(5) = entr2   !entr2old  !TOGLIMI        
      endif
      !local names, the old values for these are stored in OLD which is preserved for any timestep
      eelold = OLD(1) 
      entrold = OLD(2)
      eoscold = OLD(3)
      entroscold = OLD(4)
      entr2old = OLD(5) !TOGLIMI

      !temperatures are evaluated only after some equilibration time
      if(time >= Tcoup) then   !put this initial time in the parameters eventually
         Tel = temperature(eel,eelold,entr,entrold,1,OLD,flag)
         if(flag==1) write(75,*) time,real(Tel,dp)    !print only if a temperature is calculated, flag controls it
         Tosc = temperature(eosc,eoscold,entrosc,entroscold,2,OLD,flag)
         if(flag==1) write(78,*) time,real(Tosc,dp)
         Tel2 = temperature(eel,eelold,entr2,entr2old,3,OLD,flag) !TOGLIMI
         if(flag==1) write(57,*) time,real(Tel2,dp)   !TOGLIMI

         !TOGLIMI        
         g%avTel = (1.0_dp - mixTel)*g%avTel + (mixTel*Tel)
         g%avTel2 = (1.0_dp - mixTel)*g%avTel2 + (mixTel*Tel2)
         write(80,*) time,real(g%avTel,dp)
         write(81,*) time,real(g%avTel2,dp)

         write(82,*) time,real(Eel,dp)+real(eosc,dp)-g%Ein
      endif   
        
   endif
         
   if(nho .ne. 0) then

      !write(34,*) time,real(o(1)%N,dp),aimag(o(1)%N)
      !write(34,*) time,real(o(1)%N,dp),real(o(2)%N)
      write(34,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,real(o(1)%N,dp),real(o(2)%N),real(o(3)%N),real(o(4)%N)
      write(35,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,real(o(5)%N,dp),real(o(6)%N),real(o(7)%N),real(o(8)%N)
      write(22,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,real(o(9)%N,dp),real(o(10)%N),real(o(11)%N),real(o(12)%N)
      !write(34,'(f9.3,f11.6,f11.6)') time,real(o(1)%N,dp),aimag(o(1)%N)
      !write(41,*) time,real(o(1)%Ndot,dp),aimag(o(1)%Ndot)

      !WARNING: ALL THEE FOLLOWING PART WRITES OBSERVABLES ONLY FOR OSCILLATOR 1
      !!!!!!!!!!!!!!!FIX IT FOR MULTIPLE OSCILLATORS, LIKE IN THE EINSTEIN CASEf

      !check of the trace of mu
      trace = 0.0_dp
      do k=1,nlev
         trace = trace + o(1)%mu(k,k)
      enddo
      write(43,*) time,real(trace,dp)
            
      !variance of mu
      buffer = trace*trace
      alpha = 1.0_dp
      beta = 0.0_dp
      call zgemm('N','N',nlev,nlev,nlev,alpha,o(1)%mu,nlev,o(1)%mu,nlev,beta,musq,nlev) 
      musq = musq - buffer 
      buffer = 0.0_dp
      do k=1,nlev
         buffer = buffer + musq(k,k)
      enddo
      buffer = sqrt(real(buffer,dp))
      write(66,*) time,real(buffer,dp)  !the variance of mu
      buffer = sqrt(real(buffer,dp))/real(trace,dp)
      write(67,*) time,real(buffer,dp)  !the variance of mu over its average value

   endif       


end subroutine

!----------------------

function energy(g,r,o,system)

   use modvar
   implicit none

   type(global) :: g
   type(oscillator), dimension(:) :: o
   complex(dp), dimension(:,:) :: r
   complex(dp), dimension(nlev,nlev) :: temp,energia
   integer, INTENT(IN) :: system  !if 1 electrons, if 2 ions
   complex(2*dp) :: energy,Eel,eosc
   integer :: j

   if(system == 1) then
      !electronic energy, missing the OB
      call FmatmulLrcc(g%nonZeroH,g%Hel,r,energia)
      if(nho .ne. 0) then
        do j=1,nho
           call FmatmulLrcc(o(j)%nonZeroF,o(j)%F,o(j)%mu,temp)
           energia = energia - temp
        enddo
      endif 
      !!!!!!!!!!CAREFUL
      !does it make sense to trace only over the central region?? I am ignoring the OB and the HF too
      Eel = 0.0_dp
      if(tracecentral == 1) then
         do j=NL+1,NL+NC           
            Eel = Eel + energia(j,j)
         enddo
      else
         do j=1,nlev
            Eel = Eel + energia(j,j)
         enddo       
      endif
      energy = 2.0_dp*Eel   !It's double because of the spin!!!!!!!!!!
      return
   
   elseif(system==2) then

      if(nho .ne. 0) then
         eosc = 0.0_dp               
         do j=1,nho
            eosc = eosc + (o(j)%N + 0.5_dp)*hb*o(j)%om
         enddo
      endif
      energy = eosc
      return

   endif
   
end function

!----------------------

function entropy(g,r,enne,system,time)

   use modvar
   implicit none

   type(global) :: g
   complex(dp), dimension(:,:) :: r
   complex(dp), dimension(:) :: enne
   complex(dp), dimension(nlev,nlev) :: temp
   integer, INTENT(IN) :: system  !if 1 electrons, if 2 ions, if 3 electrons in another way
   complex(dp), dimension(:,:), allocatable :: small,hpsmall,hpTCsmall
   complex(dp*2) :: entropy,entrosc,trace,uno,due
   integer :: i,j,dimen
   real(dp) :: time

   !LAPACK
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   real(dp), allocatable :: eigv(:)
   integer :: info

   if(system == 1) then  !electronic entropy, from the eigenvalues of rho in the TB basis and the Von Neumann entropy
      allocate(work(lwork))
      allocate(rwork(lrwork))
      allocate(iwork(liwork))
      if(tracecentral == 1) then
         dimen = NC
         allocate(eigv(NC))
         allocate(small(NC,NC))
         small = r((NL+1):(NL+NC),(NL+1):(NL+NC))
      else   
         dimen = nlev
         allocate(eigv(nlev))
         allocate(small(nlev,nlev))
         small = r     
      endif

      !call checkHermite(small,flag)
      call zheevd('N','U',dimen,small,dimen,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
      trace = 0.0_dp
      do j = 1,dimen
         
         !TOGLIMI
         !if(eigv(j) > 0.0_dp) then
         !   uno = abs(eigv(j))*log(abs(eigv(j)))
         !else
         !   uno = 0.0_dp
         !endif
         !if((1.0_dp - eigv(j)) > 0.0_dp) then
         !   due = abs(1.0_dp - eigv(j))*log(abs(1.0_dp - eigv(j)))  
         !else
         !   due = 0.0_dp
         !endif
               
         uno = abs(eigv(j))*log(abs(eigv(j))) 
         due = abs(1.0_dp - eigv(j))*log(abs(1.0_dp - eigv(j)))
         !TOGLIMI  
         !if((eigv(j) < 0.0_dp) .or. ((1.0_dp-eigv(j)) < 0.0_dp)) then
         !   write(42,'(i5,f11.6,f11.6,f11.6)') j,eigv(j),real(uno,dp),real(due,dp)
         !endif         
         trace = trace + uno + due

      enddo
      
      entropy = -kb*trace*2.0_dp   !SPIN!!!!!!!
      return
    
   elseif(system == 2) then  !oscillator entropy         
      if(nho .ne. 0) then
         entrosc = 0.0_dp           
         do j=1,nho
            entrosc = entrosc - enne(j)*log(real(enne(j),dp)) + (enne(j)+1)*log(real(enne(j),dp)+1)
         enddo
      endif
      
      entropy = kb*entrosc
      return

   elseif(system == 3) then  !TOGLIMI !electronic entropy from the diagonal elements of rho in the H basis  
      if(tracecentral == 1) then
         dimen = NC
         allocate(small(NC,NC))
         allocate(hpsmall(NC,NC))       !CAREFUL!!!!!!!!!!!!!!!!!!!!!!! Sarà giusto fare a pezzi così una matrice??????? NOOO!
         allocate(hpTCsmall(NC,NC))
         small = r((NL+1):(NL+NC),(NL+1):(NL+NC))
         hpsmall = g%Hprojections((NL+1):(NL+NC),(NL+1):(NL+NC))
         hpTCsmall = g%HprojectionsTC((NL+1):(NL+NC),(NL+1):(NL+NC))
         small = matmul(hpTCsmall,small) 
         small = matmul(small,hpsmall) 
      else   
         dimen = nlev
         allocate(small(nlev,nlev))
         small = r
         small = matmul(g%HprojectionsTC,small) !cambio di base, scrivo rho nella base dell'hamiltoniana
         small = matmul(small,g%Hprojections)   
      endif    
   
      trace = 0.0_dp      
      do j = 1,dimen
         trace = trace + abs(small(j,j))*log(abs(small(j,j))) + abs(1.0_dp - small(j,j))*log(abs(1.0_dp - small(j,j)))
      enddo 
      entropy = -kb*trace*2.0_dp !!!!!!!SPIN!!!!!!???

      !occupation
      i = int(time/dt)
      if(mod(i,obsfreq*obsfreq)==0) then
         do j = 1,dimen
            write(83,*) g%E(j), real(small(j,j),dp)
         enddo 
         write(83,*)
      endif
   
      return
  
   endif

end function

!----------------------

function temperature(enuno,endue,entropuno,entropdue,system,OLD,flag)

   use modvar
   implicit none
   
   complex(2*dp) :: enuno,endue,entropuno,entropdue
   complex(dp) :: temperature
   integer, INTENT(IN) :: system 
   complex(2*dp), dimension(:) :: OLD
   integer, INTENT(OUT) :: flag

   flag = 0
   if(abs(entropuno - entropdue) > zero) then            
      temperature = (enuno - endue) / (entropuno - entropdue)
      flag = 1
   endif

   if(system == 1) then !electrons update
      OLD(1) = enuno
      OLD(2) = entropuno
   elseif(system == 2) then !ions update
      OLD(3) = enuno
      OLD(4) = entropuno
   elseif(system == 3) then !TOGLIMI !electrons 2 update
      OLD(1) = enuno
      OLD(5) = entropuno
   endif

   return

end function

!----------------------
!efficient matrix multiplications when one matrix is sparse AND the matrix is stored
!----------------------

!-------------!efficient matmul Left CC->C

subroutine FmatmulL(noz,sparse,gen,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the left element is F or similar

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(i,j)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Left RC->C

subroutine FmatmulLrcc(noz,sparse,gen,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the left element is F or similar

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:,:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(i,j)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CC->C

subroutine FmatmulR(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right RR->R

subroutine FmatmulRrrr(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   real(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CR->C

subroutine FmatmulRcrc(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   real(dp), dimension(:,:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!----------------------
!efficient matrix multiplications when one matrix is sparse but the matrix IS NOT stored
!----------------------

!-------------!efficient matmul Left CC->C

subroutine SmatmulL(noz,sparse,gen,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(n)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Left RC->C

subroutine SmatmulLrcc(noz,sparse,gen,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(n)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CC->C

subroutine SmatmulR(noz,gen,sparse,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(n)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right RR->R

subroutine SmatmulRrrr(noz,gen,sparse,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:), INTENT(IN) :: sparse
   real(dp), dimension(:,:), INTENT(IN) :: gen
   real(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(n)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CR->C

subroutine SmatmulRcrc(noz,gen,sparse,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   real(dp), dimension(:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(n)
      enddo

   enddo
   
end subroutine

!-------------!sparse matrix storage of elements and their position

subroutine findnonZero(matrix,label,numel)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(OUT) :: label
   real(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: numel
   integer i,j,n,q,elementi
   
   n=1
   do i=1,nlev
      do j=1,nlev
         if(matrix(i,j) .ne. 0.0_dp) then
            label(n,1) = i
            label(n,2) = j
            n = n+1
            !write(*,*) i,j
         endif
      enddo
   enddo

   numel = n-1
   
end subroutine

!-------------!sparse matrix storage of elements and their position, COMPLEX input

subroutine findnonZeroC(matrix,label,numel)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(OUT) :: label
   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: numel
   integer i,j,n,q,elementi
   
   n=1
   do i=1,nlev
      do j=1,nlev
         if(abs(matrix(i,j)) .ne. 0.0_dp) then
            label(n,1) = i
            label(n,2) = j
            n = n+1
            !write(*,*) i,j
         endif
      enddo
   enddo

   numel = n-1
   
end subroutine

!-------------!sparse matrix builder, only the nonzero elements are stored; COMPLEX input and output

subroutine buildSparseC(nonzero,full,reduced)

   integer, dimension(:,:), INTENT(IN) :: nonzero
   complex(dp), dimension(:,:), INTENT(IN) :: full
   complex(dp), dimension(:), INTENT(OUT) :: reduced
   integer i,j,n,elementi
   
   elementi = size(nonzero,1)

   do n=1,elementi
      i = nonzero(n,1)
      j = nonzero(n,2)

      reduced(n) = full(i,j)
   enddo

end subroutine

!----------------------
!Check routines
!----------------------

!-------------!checks the hermiticity of a matrix

subroutine checkHermite(matrix,flag)
   use modvar
   implicit none

   !routine for checking the hermiticity of matrices

   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: flag
   integer i,j,n,q,elementi,enne
   
   flag = 0
   enne = size(matrix,1)
   do i=1,enne
      do j=1,enne
         if(abs(matrix(i,j) - conjg(matrix(j,i))) .ge. zero ) then
            flag = flag + 1
            write(42,*) i,j,matrix(i,j),conjg(matrix(j,i))
         endif
      enddo
   enddo
  
end subroutine

!-------------!checks if a matrix is symmetrical

subroutine checkSymm(matrix,flag)
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: flag
   integer i,j,n,q,elementi,enne
   
   flag = 0
   enne = size(matrix,1)
   do i=1,enne
      do j=1,enne
         if( abs(matrix(i,j) - matrix(j,i)) .ge. zero ) then
            flag = flag + 1
            write(42,*) i,j,matrix(i,j),conjg(matrix(j,i))
         endif
      enddo
   enddo
  
end subroutine

!-------------!checks if 2 matrices are identical
!maybe it's better I put some flexibility like a +- epsilon, for including the machine error

subroutine checkSame(matrix,matrix2,time,flag)
   use modvar
   implicit none

   !routine for checking if 2 complex matrices are the same
   !it's useful if I use 2 different methods to get them

   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   complex(dp), dimension(:,:), INTENT(IN) :: matrix2
   integer, INTENT(OUT) :: flag
   real(dp), INTENT(IN)  :: time
   integer i,j,n,q,elementi,enne
   
   flag = 0
   enne = size(matrix,1)
   do i=1,enne
      do j=1,enne
         if( abs(matrix(i,j) - matrix2(i,j)) .ge. zero ) then
            flag = flag + 1
            !write(42,*) i,j,matrix(i,j),matrix2(i,j)
         endif
      enddo
   enddo

   !if(flag .ne. 0) then
   !   write(*,*) 'At time',time,'the matrices do not coincide, check debug.dat'
   !   stop
   !endif
  
end subroutine

end program elph
