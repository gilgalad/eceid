module modvar
   
   implicit none

   !numerical and computational parameters (don't change them in general on the same machine)
   integer, parameter :: dp = selected_real_kind(12) !precision for reals and complex numbers    
   integer, parameter :: trick = 1000  !Euler trick, every trick step an Euler integration is performed instead of a leapfrog 
   integer, parameter :: obsfreq = 100  !it indicates how often the observables are evaluated. Every obsfreq steps, the observables are evaluated
   integer, parameter :: openboundaries = 0 !if 0 the OB are off, if 1 they are on,
                                            !if 2 they are on in the crippled Eunan's version (del will be forced to be gam/2)
   integer, parameter :: Ehrenfest = 0  !if 1, it turns off the anticommutator which relates to the electronic noise and reproduces a simil Ehrenfest
                                        !situation. hf doesn't mean anything in this case. If 0, the program runs normally. 
   integer, parameter :: hf = 2 !if 0 the Hartree-Fock approximation terms in mu and Ndot are off 
                                !if 1 they are on (they are of capital importance) 
                                !if 2 they are on with Tchavdar's suggestion to ignore their first part, the trace
   integer, parameter :: HplusSwitch = 1 !if 1, Hplus is H0 + SigmaPlus, if 2 it is H0 + i epsE I
   integer, parameter :: dualtemperature = 1 !if 1, it computes the entropy for both electrons and ions according to obsfreq (Lapack needed)
                                             !this allows to get a time local electronic and ionic temperature
   integer, parameter :: tracecentral = 0   !if 1 it traces the electronic energy only over the central region,
                                            !otherwise it traces it over the whole wire
   integer, parameter :: fixedN = 0 !if 1 it turns off Npunto and fixes the average N for the oscillators at their initial value
                                    !it is like an overdamped system that dissipates heat immediately
                                    !if 0 it lets N evolve normally, which means that the oscillators are isolated and don't dissipate at all
   integer, parameter :: fixedRho = 0  !if 1 it turns off rhopunto and fixes rho to its initial condition. It tests the frictionless
                                       !and pure noise energy transfer from hot electrons to the oscillators
   integer, parameter :: Telrho = 1   !if 1, the initial DM is chosen from the eigenstates of H, weighted by the FD distribution at T=inTel
                                      !if 0 the initial DM is just half a band filled from the eigenstates of H, the same as inTel=0K

   !SWITCHES for different systems and WIRE parameters
   integer, parameter :: nho = 24  !if 0, no oscillators (large speedup), if not, it's the number of independent oscillators
   integer, parameter :: geomwire = 5  !if 1 DIMER geometry
                                       !if 2, ATOM at the center of the wire
                                       !if 3, STM, a wire with a  weak bond in the middle
                                       !if 4, EINSTEIN, several oscillators in the middle of NC, nho must be even
                                       !if 5, OHM, several oscillators uniformly distributed in NC. If nho=0, just a perfect chain
   integer, parameter :: NL = 60    !number of sites in the Left lead
   integer, parameter :: NR = NL   !number of sites in the Right lead
   integer, parameter :: NC = 60    !number of sites in the center of the junction, 
                                    !careful that Eunan puts the dimer in the middle so it is equivalent to 20 +2
                                    !DIMER: it must be even
                                    !ATOM: it must be odd
                                    !STM: it must be even 
                                    !EINSTEIN: it must be even
                                    !OHM: it depends, careful to the disposition in diagH.f90 though
                                    !I tend to stay further away from the leads. It must be densityOhm*nho + 2 if density is even
                                    !or densityOhm*nho + 1 if it is odd
   integer, parameter :: oscL = NL + NC/2        !DIMER: position of the LEFT atom in the dimer
   integer, parameter :: oscR = NL + NC/2 + 1    !DIMER: position of the RIGHT atom in the dimer 
   integer, parameter :: atom = NL + NC/2 + 1    !ATOM: position of the single oscillating atom
   integer, parameter :: leftend = NL + NC/2     !STM: position of the atom on the left of the weak link
   integer, parameter :: Lfirst = NL + NC/2 - nho + 1    !EINSTEIN: position of the first oscillator in the central region, 
                                                         !the last one is on site Lfirst + 2*nho
   integer, parameter :: siteJ = NL+NC-1 !site where the current evaluated (and the relative +1 site on the right of it), on the right border of NC
   integer, parameter :: nlev = NL + NR + NC  !number of sites in the quatum wire

   !SIMULATION CONDITIONS 
   !times
   real(dp), parameter :: dt = 0.005_dp !time step in fs
   real(dp), parameter :: T = 20.00_dp !total simulation time in fs
   real(dp), parameter :: TOB = 200.000_dp !time in fs where the OB are turned on linearly, NOT the oscillator 
   real(dp), parameter :: Tcoup = 20.000_dp !time in fs where the coupling F is turned on linearly. Tel is evaluated after Tcoup.
   !energies
   real(dp), parameter :: onsite = 0.0_dp !onsite energy for the TB orbitals
   real(dp), parameter :: tchain = -1.0_dp !hopping parameter for the atoms in the leads L and R
   real(dp), parameter :: tcd = -0.5_dp !DIMER: hopping parameter for the atoms in the junction between LC and CR
   real(dp), parameter :: tdimer = -3.88_dp !DIMER: hopping parameter for the atoms in the central region C 
   real(dp), parameter :: onsiteAtom = 0.0_dp !ATOM: onsite energy of the atom, if high it is like a barrier   
   real(dp), parameter :: acar = 3.00 !ATOM: characteristic distance of the chain, in A
   real(dp), parameter :: middle = 0.3_dp !STM: weak coupling of the central bond
   real(dp), parameter :: epsE = 0.05_dp  !dissipation parameter in the wire hamiltonian H_0 (in eV)
   real(dp), parameter :: inTel = 10000.00_dp  !initial electronic temperature (in K). It enters the program only if Telrho=1
   real(dp), parameter :: mixTel = 1.0E-3_dp !DESCRIVI MEGLIO e DERIVA MEGLIO IN FUNZIONE DEL TEMPO DA SMOOTHARE, ADIMENSIONALE

   !The INITIAL CONDITIONS for the oscillators are in input.dat
   !every line corresponds to a different oscillator
   !the listed parameters are coupling, initial N, omega (in eV), mass (adimensional)

   !OB parameters
   real(dp), parameter :: Volt = 1.00_dp !difference of potential betwen the left and the right lead
   real(dp), parameter :: gam = 0.01_dp !wide band approximation parameter
   real(dp), parameter :: muL = Volt/2.0_dp !chemical potential in the LEFT lead
   real(dp), parameter :: muR = -Volt/2.0_dp !chemical potential in the RIGHT lead
   real(dp), parameter :: del = 0.00005_dp !probes decoherence approximation parameter 
   real(dp), parameter :: BI = -100000.0_dp !lower boundary of energy integral in the injection term

   !STEADY.F90, exact conduction for finite wires and Landauer (used ONLY there)
   integer, parameter :: scanon = 0  !if 1 it turns on the scan of the current for voltages from dV to Vmax
                                     !if 0 it evaluates one single value of the current for the OB parameters above
   real(dp), parameter :: dV = 0.01_dp  !increment voltage in the scan
   real(dp), parameter :: Vmax = 4.00_dp  !increment voltage in the scan
   integer, parameter :: integralLand = 1  !if 1 it turns on the Landauer integration of the Trasmission for the dimer system   
   real(dp), parameter :: dE = 0.0001_dp
 
   !UNIVERSAL CONSTANTS
   real(dp), parameter :: hb = 0.658211942_dp  !hbar in eV*fs
   real(dp), parameter :: mp = 104.3968511_dp  !proton mass in eV*fs^2/A^2
   real(dp), parameter :: amu = 103.642753  !atomic mass unit in eV*fs^2/A^2  (1/12 of the mass of carbon)
   real(dp), parameter :: pi =  3.141592653589793_dp
   real(dp), parameter :: kb = 8.6173324E-05  !Boltzmann constant in eV/K
   complex(dp), parameter :: im = (0.0_dp,1.0_dp) !i
   real(dp), parameter :: quantumConductance = 77.480917 ! muA/eV
   real(dp), parameter :: microampere = 973.6538812 ! muA/eV
   real(dp), parameter :: zero = 1.0E-20_dp  !when checking for hermiticity, I consider numbers below this as zero
                                            !I also use it for the temperature calculation using Delta entropy, if it is below this, I skip to the next time step
   real(dp), parameter :: eps = epsE/hb   !dissipation parameter in the wire hamiltonian H_0 (in fs^-1), don't touch it
                                          !(I had to put it here, after the definition of hb, otherwise the program wouldn't compile)

   !LAPACK PARAMETERS
   integer, parameter :: LWORK = 10000000 !MAX integer is 2147483647, but I get errors. It must be at least (2 nlev + nlev**2)
   integer, parameter :: LRWORK=20000000
   integer, parameter :: LIWORK=15000

  
   type global
      complex(dp), allocatable :: rdot(:,:),inj(:,:)       
      real(dp), allocatable :: Hel(:,:)      !electronic Hamiltonian
      complex(dp), allocatable :: Hprojections(:,:),HprojectionsTC(:,:)
      real(dp), allocatable :: E(:)
      complex(dp) :: avTel,avTel2   !TOGLIMI 
      real(dp) :: Ein 
      complex(dp), allocatable :: SigmaPlus(:),Hplus(:),HplusTC(:)  
      integer, allocatable :: nonZeroSigma(:,:), nonZeroH(:,:), nonZeroHplus(:,:), nonZeroHplusTC(:,:) !storage of labels created to optimize 
                              !the matrix multiplications since F is very sparse
                              !the 1 index runs 1:numNonZero, the second from 1:2 with 1 for rows and 2 for columns
                              !e.g. nonZeroF(5,1) contains the label of the row of the fifth non zero element of F
   end type

   !if the oscillators and these variables are independent
   type oscillator
      complex(dp) :: N, Ndot, c1mu,c2mu   !N is kept here only for the observables, in fact it is updated only every time observables is called
      complex(dp), allocatable :: CCdot(:,:), CSdot(:,:), ACdot(:,:), ASdot(:,:)
      complex(dp), allocatable :: mu(:,:)
      real(dp) :: om,mass,coup
      real(dp), allocatable :: F(:,:),Fstore(:,:)
      integer, allocatable :: nonZeroF(:,:),nonZeroFrho(:,:)
   end type

   !NEW!!!!!!!!!!!!!!!
   !evolution of the DM in the same style as the oscillators
   type rhoEvo
      complex(dp), allocatable :: rho(:,:)
   end type
   !explicit evolution of the oscillator variables
   type oscillatorEvo
      complex(dp) :: N
      complex(dp) :: CC(nlev,nlev), CS(nlev,nlev), AC(nlev,nlev), AS(nlev,nlev)  !I don't use them allocatable because of the pointer that doesn't work otherwise
   end type

   type pointer
      type(oscillatorEvo), pointer :: ho
   end type 

end module modvar
