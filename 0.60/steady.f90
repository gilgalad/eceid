program steady

   !This program finds an exact solution of the steady state current for a finite wire
   !it is standalone, it's not necessary, but adds more information to elph.
   !REQUIREMENTS: the execution of diagH

   use modvar
   implicit none

   !LAPACK VARIABLES   
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   integer :: info

   !SYSTEM VARIABLES (use allocatable for avoiding problems with openMP)
   real(dp), allocatable :: H(:,:) !hamiltonian of the system
   complex(dp), allocatable :: effe(:,:) !operatorial part in G- here (similar to diagH but without delta)
   complex(dp), allocatable :: lambda(:)
   real(dp), allocatable :: lambdaRe(:),lambdaIm(:),lambdaStarIm(:)    !ordered eigevalue vector
   complex(dp), allocatable :: evecL(:,:),evecR(:,:) !Left and right eigenvectors of effe
   complex(dp), allocatable :: unoL(:),dueL(:),unoStarL(:),dueStarL(:) !buffer terms to speed up the calculation
   complex(dp), allocatable :: unoR(:),dueR(:),unoStarR(:),dueStarR(:)

   !integral variables
   complex(dp), allocatable :: cL(:,:),cR(:,:)
   complex(dp) :: enint,buffer
   integer i,j,L,R,nstep,k
   real(dp) :: current,vi,mL,mR
   !Landauer variables   
   real(dp) :: integrand,lam,en,A,num,den,buf,res
   integer :: nintstep,s

   allocate(H(nlev,nlev))
   open(44, file='ham',form='unformatted')
   read(44) H
   close(44)

   allocate(effe(nlev,nlev))
   call buildF(effe,H)

   !diagonalize effe
   allocate(work(lwork))
   allocate(rwork(lrwork))
   allocate(lambda(nlev))
   allocate(evecL(nlev,nlev))
   allocate(evecR(nlev,nlev))
   call zgeev('V','V',nlev,effe,nlev,lambda,evecL,nlev,evecR,nlev, &
               work,LWORK,rwork,info)
   !write(*,*) info
   deallocate(work)
   deallocate(rwork)

   allocate(lambdaRe(nlev))
   allocate(lambdaIm(nlev))
   allocate(lambdaStarIm(nlev))
   lambdaRe = real(lambda,dp)
   lambdaIm = aimag(lambda)
   lambdaStarIm = -aimag(lambda)

   !fixed normalization
   do i=1,nlev   
      evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
      evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
   enddo

   !computation of the coefficient before starting with the integrals
   allocate(cL(nlev,nlev))
   allocate(cR(nlev,nlev))
   cL = 0.0_dp
   cR = 0.0_dp
   do i = 1,nlev
      do j = 1,nlev
         
         !in these evec, the TB basis need always to be on the left, while the eigenvectors of F basis on the right
         do L = 1,NL
            cL(i,j) = cL(i,j) + ( conjg(evecR(L,j)) * evecR(L,i)  )
         enddo
         do R = NL+NC+1,nlev
            cR(i,j) = cR(i,j) + ( conjg(evecR(R,j)) * evecR(R,i)  )
         enddo

         cL(i,j) = cL(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 
         cR(i,j) = cR(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 

      enddo
   enddo

   allocate(unoL(nlev))
   allocate(unoR(nlev))
   allocate(dueL(nlev))
   allocate(dueR(nlev))
   allocate(unoStarL(nlev))
   allocate(unoStarR(nlev))
   allocate(dueStarL(nlev))
   allocate(dueStarR(nlev))

   !scan over different voltages to plot the conduction curve
   if (scanon == 1) then
      open(19, file='finCond.dat')
      open(20, file='LanCond.dat')
      nstep = int(Vmax/dV)
      do k=1,nstep

         vi = k*dV
         mL = vi/2.0_dp 
         mR = -vi/2.0_dp     

         do i = 1,nlev
            unoL(i) = 0.5_dp*log( ((mL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
            dueL(i) = im*atan( ( mL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
            unoStarL(i) = 0.5_dp*log( ((mL - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
            dueStarL(i) = im*atan( ( mL - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
            
            unoR(i) = 0.5_dp*log( ((mR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
            dueR(i) = im*atan( ( mR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
            unoStarR(i) = 0.5_dp*log( ((mR - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
            dueStarR(i) = im*atan( ( mR - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
         enddo

         enint = 0.0_dp
         do i = 1,nlev
            do j = 1,nlev         
               enint = enint + cL(i,j) * (unoL(i) + dueL(i) - unoStarL(j) - dueStarL(j) ) *( 1.0_dp / (lambda(i)-conjg(lambda(j))) )      
               enint = enint + cR(i,j) * (unoR(i) + dueR(i) - unoStarR(j) - dueStarR(j) ) *( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
               !enint = enint * ( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
            enddo
         enddo
         enint = enint * (gam/(2.0_dp*pi)) 

         !the current for that voltage
         current = -microampere*aimag(H(siteJ,siteJ+1)*enint)
         write(*,*) vi,current
         write(19,*) vi,current

         !Landauer integration
         if(integralLand == 1) then
            
   
            if(geomwire == 1 ) then
               !DIMER
               lam = tcd/tchain - 1.0_dp
            elseif(geomwire == 2 ) then
               !ATOM
               lam = 0.0_dp
            endif
            A = tchain

            nintstep = int(vi/dE)
            res = 0.0_dp
            do s=0,nintstep
               En = -vi/2.0_dp + (s*dE)        

               num = quantumConductance * A**2 * (4.0_dp*A**2 - En**2) * (1.0_dp + lam)**4
               den = En**4 * lam**2 * (2.0_dp + lam)**2
               buf = (2.0_dp + lam * (2.0_dp + lam))
               den = den + A**4 * (2.0_dp + lam * (2.0_dp + lam)*buf)**2
               den = den - A**2 * En**2 * (1.0_dp + lam*(2.0_dp+lam)*buf*(1.0_dp+2.0_dp*lam*(2.0_dp+lam)))
               integrand = num / den

               if((s==0).or.(s==nintstep)) then
                  res = res + (integrand/2.0_dp)
               else
                  res = res + integrand
               endif
            enddo
            res = res*dE
            write(20,*) vi,res
         endif         
      
      enddo
   
   else
      do i = 1,nlev
         unoL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
         dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
         unoStarL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
         dueStarL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
            
         unoR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
         dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
         unoStarR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
         dueStarR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
      enddo

      enint = 0.0_dp
      do i = 1,nlev
         do j = 1,nlev         
            enint = enint + cL(i,j) * (unoL(i) + dueL(i) - unoStarL(j) - dueStarL(j) ) * ( 1.0_dp / (lambda(i)-conjg(lambda(j))) )      
            enint = enint + cR(i,j) * (unoR(i) + dueR(i) - unoStarR(j) - dueStarR(j) ) * ( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
            !enint = enint * ( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
         enddo
      enddo
      enint = enint * (gam/(2.0_dp*pi)) 

      !the current for that voltage
      current = -microampere*aimag(H(siteJ,siteJ+1)*enint)
      write(*,*)
      write(*,*) 'The exact current in a finite wire is' 
      write(*,*) current,'uA'

      !Landauer integration
      if(integralLand == 1) then
         
         if(geomwire == 1 ) then
            !DIMER
            lam = tcd/tchain - 1.0_dp
         elseif(geomwire == 2 ) then
            !ATOM
            lam = 0.0_dp
         endif
         A = tchain

         nintstep = int(Volt/dE)
         res = 0.0_dp
         do s=0,nintstep
            En = -Volt/2.0_dp + (s*dE)        

            num = quantumConductance * A**2 * (4.0_dp*A**2 - En**2) * (1.0_dp + lam)**4
            den = En**4 * lam**2 * (2.0_dp + lam)**2
            buf = (2.0_dp + lam * (2.0_dp + lam))
            den = den + A**4 * (2.0_dp + lam * (2.0_dp + lam)*buf)**2
            den = den - A**2 * En**2 * (1.0_dp + lam*(2.0_dp+lam)*buf*(1.0_dp+2.0_dp*lam*(2.0_dp+lam)))
            integrand = num / den

            if((s==0).or.(s==nintstep)) then
               res = res + (integrand/2.0_dp)
            else
               res = res + integrand
            endif
         enddo
         res = res*dE
         write(*,*)
         write(*,*) 'The Landauer current in a wire is' 
         write(*,*) res,'uA'
      endif

   endif
   

!-------------------------------------

contains

subroutine buildF(effe,H)

!this is the operatorial part in G- = H_S0 + Sigma- 

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: effe
   real(dp), dimension(:,:), INTENT(IN) :: H
   integer :: i,j

   effe = H

   do i= 1,NL
      effe(i,i) = effe(i,i) + im*0.5_dp*gam
   enddo
   do i= NL+NC+1,nlev
      effe(i,i) = effe(i,i) + im*0.5_dp*gam
   enddo

end subroutine buildF

end program steady
