program steady

   !This program finds an exact solution of the steady state current for a finite wire
   !it is standalone, it's not necessary, but adds more information to elph.
   !REQUIREMENTS: the execution of diagH

   use modvar
   implicit none

   !LAPACK VARIABLES   
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   integer :: info

   !SYSTEM VARIABLES (use allocatable for avoiding problems with openMP)
   real(dp), allocatable :: H(:,:) !hamiltonian of the system
   complex(dp), allocatable :: effe(:,:) !operatorial part in G- here (similar to diagH but without delta)
   complex(dp), allocatable :: effeOLD(:,:)
   complex(dp), allocatable :: effeOLDNC(:,:),effeNC(:,:),interNC(:,:)
   complex(dp), allocatable :: lambda(:)
   complex(dp), allocatable :: inter(:,:,:) !electron-phonon interaction matrices
   real(dp), allocatable :: lambdaRe(:),lambdaIm(:),lambdaStarIm(:),eigv(:)    !ordered eigevalue vector
   real(dp), allocatable :: ldos(:,:),TraE(:,:),store(:,:)
   complex(dp), allocatable :: evecL(:,:),evecR(:,:) !Left and right eigenvectors of effe
   complex(dp), allocatable :: unoL(:),dueL(:),unoStarL(:),dueStarL(:) !buffer terms to speed up the calculation
   complex(dp), allocatable :: unoR(:),dueR(:),unoStarR(:),dueStarR(:)
   complex(dp), allocatable :: egunoL(:),egdueL(:),egunostarL(:),egduestarL(:) !egun mode
   real(dp) :: egU,egD !upper and lower limit of egun injection
   complex(dp), allocatable :: effeL(:,:),effeR(:,:),effeC(:,:),effeCOLD(:,:)
   complex(dp), allocatable :: gelle(:),gerre(:),dielle(:),dierre(:) !GF at the left/right point of the leads, in energy
   complex(dp) :: tra,gcentro,gcentrostar

   !integral variables
   complex(dp), allocatable :: cL(:,:),cR(:,:)
   complex(dp) :: enint,buffer
   integer i,j,L,R,nstep,k,nstep2,n
   real(dp) :: current,vi,mL,mR,gamma
   !Landauer variables   
   real(dp) :: integrand,lam,en,A,num,den,buf,res,x
   integer :: nintstep,s

   allocate(H(nlev,nlev))
   open(44, file='ham',form='unformatted')
   read(44) H
   close(44)

   !choice of gamma
   if(openboundaries == 0) then
      gamma = gamFIN
   else
      gamma = max(gamFIN,gamOB)
   endif

   allocate(effe(nlev,nlev))
   call buildF(effe,H,gamma)

   if(scanon < 2) then !normal run with the static hamiltonian and its conduction

      !diagonalize effe
      allocate(work(lwork))
      allocate(rwork(lrwork))
      allocate(lambda(nlev))
      allocate(evecL(nlev,nlev))
      allocate(evecR(nlev,nlev))
      call zgeev('V','V',nlev,effe,nlev,lambda,evecL,nlev,evecR,nlev, &
                  work,LWORK,rwork,info)
      !write(*,*) info
      !deallocate(work)
      !deallocate(rwork)

      allocate(lambdaRe(nlev))
      allocate(lambdaIm(nlev))
      allocate(lambdaStarIm(nlev))
      lambdaRe = real(lambda,dp)
      lambdaIm = aimag(lambda)
      lambdaStarIm = -aimag(lambda)

      !fixed normalization
      do i=1,nlev   
         evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
         evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
      enddo

      !computation of the coefficient before starting with the integrals
      allocate(cL(nlev,nlev))
      allocate(cR(nlev,nlev))
      cL = 0.0_dp
      cR = 0.0_dp

      if(h2o == 0) then !normal case
         do i = 1,nlev
            do j = 1,nlev
             
               !in these evec, the TB basis need always to be on the left, while the eigenvectors of F basis on the right
               do L = 1,NL
                  cL(i,j) = cL(i,j) + ( conjg(evecR(L,j)) * evecR(L,i)  )
               enddo
               do R = NL+NC+1,nlev
                  cR(i,j) = cR(i,j) + ( conjg(evecR(R,j)) * evecR(R,i)  )
               enddo

               cL(i,j) = cL(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 
               cR(i,j) = cR(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 

            enddo
         enddo
      else
         do i = 1,nlev
            do j = 1,nlev
             
               !in these evec, the TB basis need always to be on the left, while the eigenvectors of F basis on the right
               do L = 1,NL-meterlead
                  cL(i,j) = cL(i,j) + ( conjg(evecR(L,j)) * evecR(L,i)  )
               enddo
               do R = NL+NC+1+meterlead,nlev
                  cR(i,j) = cR(i,j) + ( conjg(evecR(R,j)) * evecR(R,i)  )
               enddo

               cL(i,j) = cL(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 
               cR(i,j) = cR(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 

            enddo
         enddo
      endif

      allocate(unoL(nlev))
      allocate(unoR(nlev))
      allocate(dueL(nlev))
      allocate(dueR(nlev))
      allocate(unoStarL(nlev))
      allocate(unoStarR(nlev))
      allocate(dueStarL(nlev))
      allocate(dueStarR(nlev))
      
      !scan over different voltages to plot the conduction curve
      if (scanon == 1) then
         open(19, file='finCond.dat')
         open(20, file='LanCond.dat')
         nstep = int(Vmax/dV)
         do k=1,nstep

            vi = k*dV
            mL = chempot + vi/2.0_dp 
            mR = chempot - vi/2.0_dp     

            do i = 1,nlev
               unoL(i) = 0.5_dp*log( ((mL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
               dueL(i) = im*atan( ( mL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
               unoStarL(i) = 0.5_dp*log( ((mL - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
               dueStarL(i) = im*atan( ( mL - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
               
               unoR(i) = 0.5_dp*log( ((mR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
               dueR(i) = im*atan( ( mR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
               unoStarR(i) = 0.5_dp*log( ((mR - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
               dueStarR(i) = im*atan( ( mR - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
            enddo

            enint = 0.0_dp
            do i = 1,nlev
               do j = 1,nlev
                  !if((i.ne.nlev).and.(j.ne.nlev))then         
                  enint = enint + cL(i,j) * (unoL(i) + dueL(i) - unoStarL(j) - dueStarL(j) ) *( 1.0_dp / (lambda(i)-conjg(lambda(j))) )      
                  enint = enint + cR(i,j) * (unoR(i) + dueR(i) - unoStarR(j) - dueStarR(j) ) *( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
                  !enint = enint * ( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
                  !endif
               enddo
            enddo
            !write(*,*) lambda
            !pause
            enint = enint * (gamma/(2.0_dp*pi)) 

            !the current for that voltage
            current = -microampere*aimag(H(NL-1,NL)*enint)
            write(*,*) vi,current
            write(19,*) vi,current

            !Landauer integration
            if(integralLand == 1) then
               
      
               if(geomwire == 1 ) then
                  !DIMER
                  lam = tcd/tchain - 1.0_dp
               elseif(geomwire == 2 ) then
                  !ATOM
                  lam = 0.0_dp
               endif
               A = tchain

               nintstep = int(vi/dE)
               res = 0.0_dp
               do s=0,nintstep
                  En = -vi/2.0_dp + (s*dE)        

                  num = quantumConductance * A**2 * (4.0_dp*A**2 - En**2) * (1.0_dp + lam)**4
                  den = En**4 * lam**2 * (2.0_dp + lam)**2
                  buf = (2.0_dp + lam * (2.0_dp + lam))
                  den = den + A**4 * (2.0_dp + lam * (2.0_dp + lam)*buf)**2
                  den = den - A**2 * En**2 * (1.0_dp + lam*(2.0_dp+lam)*buf*(1.0_dp+2.0_dp*lam*(2.0_dp+lam)))
                  integrand = num / den

                  if((s==0).or.(s==nintstep)) then
                     res = res + (integrand/2.0_dp)
                  else
                     res = res + integrand
                  endif
               enddo
               res = res*dE
               write(20,*) vi,res
            endif         
         
         enddo
      
      else  !no scan in voltages
         do i = 1,nlev
            unoL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
            dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
            unoStarL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
            dueStarL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
               
            unoR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
            dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
            unoStarR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
            dueStarR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
         enddo

         if(openboundaries .eq. 11) then !electron gun mode
            allocate(egunoL(nlev))
            allocate(egdueL(nlev))
            allocate(egunostarL(nlev))
            allocate(egduestarL(nlev))
            !limits of injection of the electron
            egU = egE + eGW
            egD = egE - eGW
            do i = 1,nlev
               egunoL(i) = 0.5_dp*log( ((egU - lambdaRe(i))**2 + lambdaIm(i)**2)/((egD - lambdaRe(i))**2 + lambdaIm(i)**2) )       
               egdueL(i) = im*atan( ( egU - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( egD - lambdaRe(i) )/lambdaIm(i) )
               egunostarL(i) = 0.5_dp*log( ((egU - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((egD - lambdaRe(i))**2 + lambdaStarIm(i)**2) )       
               egduestarL(i) = im*atan( ( egU - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( egD - lambdaRe(i) )/lambdaStarIm(i) )
               unoL(i) = unoL(i) + egunoL(i)
               dueL(i) = dueL(i) + egdueL(i)
               unoStarL(i) = unoStarL(i) + egunoStarL(i)
               dueStarL(i) = dueStarL(i) + egdueStarL(i)
            enddo
         endif

         enint = 0.0_dp
         do i = 1,nlev
            do j = 1,nlev         
               enint = enint + cL(i,j) * (unoL(i) + dueL(i) - unoStarL(j) - dueStarL(j) ) * ( 1.0_dp / (lambda(i)-conjg(lambda(j))) )      
               enint = enint + cR(i,j) * (unoR(i) + dueR(i) - unoStarR(j) - dueStarR(j) ) * ( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
               !enint = enint * ( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
            enddo
         enddo
         enint = enint * (gamma/(2.0_dp*pi)) 

         !the current for that voltage
         current = -microampere*aimag(H(siteJ,siteJ+1)*enint)
         write(*,*)
         write(*,*) 'The exact current in a finite wire is' 
         write(*,*) current,'uA'

         !Landauer integration
         if(integralLand == 1) then
            
            if(geomwire == 1 ) then
               !DIMER
               lam = tcd/tchain - 1.0_dp
            elseif(geomwire == 2 ) then
               !ATOM
               lam = 0.0_dp
            endif
            A = tchain

            nintstep = int(Volt/dE)
            res = 0.0_dp
            do s=0,nintstep
               En = -Volt/2.0_dp + (s*dE)        

               num = quantumConductance * A**2 * (4.0_dp*A**2 - En**2) * (1.0_dp + lam)**4
               den = En**4 * lam**2 * (2.0_dp + lam)**2
               buf = (2.0_dp + lam * (2.0_dp + lam))
               den = den + A**4 * (2.0_dp + lam * (2.0_dp + lam)*buf)**2
               den = den - A**2 * En**2 * (1.0_dp + lam*(2.0_dp+lam)*buf*(1.0_dp+2.0_dp*lam*(2.0_dp+lam)))
               integrand = num / den

               if((s==0).or.(s==nintstep)) then
                  res = res + (integrand/2.0_dp)
               else
                  res = res + integrand
               endif
            enddo
            res = res*dE
            write(*,*)
            write(*,*) 'The Landauer current in a wire is' 
            write(*,*) res,'uA'
         endif

      endif

   elseif(scanon == 2) then  !I add an FX term to H so that I estimate an elastic phonon scattering, positional

      open(62, file='coupHO',form='unformatted')
      allocate(inter(nho,nlev,nlev))
      do i=1,nho
         read(62) inter(i,:,:)
      enddo  
      close(62)

      allocate(effeOLD(nlev,nlev))
      effeOLD = effe

      open(21, file='finCondX.dat')
      allocate(work(lwork))
      allocate(rwork(lrwork))
      allocate(lambda(nlev))
      allocate(lambdaRe(nlev))
      allocate(lambdaIm(nlev))
      allocate(lambdaStarIm(nlev))
      allocate(evecL(nlev,nlev))
      allocate(evecR(nlev,nlev))
      allocate(cL(nlev,nlev))
      allocate(cR(nlev,nlev))
      allocate(unoL(nlev))
      allocate(unoR(nlev))
      allocate(dueL(nlev))
      allocate(dueR(nlev))
      allocate(unoStarL(nlev))
      allocate(unoStarR(nlev))
      allocate(dueStarL(nlev))
      allocate(dueStarR(nlev))

      nstep2 = int((2*Xmax)/dX) +1
      do n=1,nstep2
   
         x = (n - 1 - int((nstep2-1)/2))*dX      

         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !KEY MANUAL CHANGE. DECIDE HERE WHICH MODES ARE INCLUDED IN THE CALCULATION
         effe = effeOLD - (inter(1,:,:) * x) - (inter(2,:,:) * x)
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
         !diagonalize effe
         call zgeev('V','V',nlev,effe,nlev,lambda,evecL,nlev,evecR,nlev, &
                     work,LWORK,rwork,info)
         !write(*,*) info
         lambdaRe = real(lambda,dp)
         lambdaIm = aimag(lambda)
         lambdaStarIm = -aimag(lambda)

         !fixed normalization
         do i=1,nlev   
            evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
            evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
         enddo

         !computation of the coefficient before starting with the integrals
         cL = 0.0_dp
         cR = 0.0_dp

         if(h2o == 0) then !normal case
            do i = 1,nlev
               do j = 1,nlev
                
                  !in these evec, the TB basis need always to be on the left, while the eigenvectors of F basis on the right
                  do L = 1,NL
                     cL(i,j) = cL(i,j) + ( conjg(evecR(L,j)) * evecR(L,i)  )
                  enddo
                  do R = NL+NC+1,nlev
                     cR(i,j) = cR(i,j) + ( conjg(evecR(R,j)) * evecR(R,i)  )
                  enddo

                  cL(i,j) = cL(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 
                  cR(i,j) = cR(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 

               enddo
            enddo
         else
            do i = 1,nlev
               do j = 1,nlev
                
                  !in these evec, the TB basis need always to be on the left, while the eigenvectors of F basis on the right
                  do L = 1,NL-meterlead
                     cL(i,j) = cL(i,j) + ( conjg(evecR(L,j)) * evecR(L,i)  )
                  enddo
                  do R = NL+NC+1+meterlead,nlev
                     cR(i,j) = cR(i,j) + ( conjg(evecR(R,j)) * evecR(R,i)  )
                  enddo

                  cL(i,j) = cL(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 
                  cR(i,j) = cR(i,j) * ( conjg(evecL(siteJ,i)) * evecL(siteJ+1,j)  ) 

               enddo
            enddo
         endif
  
         !scan over different voltages to plot the conduction curve
         nstep = int(Vmax/dV)
         do k=1,nstep

            vi = k*dV
            mL = chempot + vi/2.0_dp 
            mR = chempot - vi/2.0_dp     

            do i = 1,nlev
               unoL(i) = 0.5_dp*log( ((mL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
               dueL(i) = im*atan( ( mL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
               unoStarL(i) = 0.5_dp*log( ((mL - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
               dueStarL(i) = im*atan( ( mL - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
               
               unoR(i) = 0.5_dp*log( ((mR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
               dueR(i) = im*atan( ( mR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
               unoStarR(i) = 0.5_dp*log( ((mR - lambdaRe(i))**2 + lambdaStarIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaStarIm(i)**2) )
               dueStarR(i) = im*atan( ( mR - lambdaRe(i) )/lambdaStarIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaStarIm(i) )
            enddo

            enint = 0.0_dp
            do i = 1,nlev
               do j = 1,nlev
                  if((lambda(i)) .ne. (conjg(lambda(j))) ) then  !to avoid NaN       
                     enint = enint + cL(i,j) * (unoL(i) + dueL(i) - unoStarL(j) - dueStarL(j) ) *( 1.0_dp / (lambda(i)-conjg(lambda(j))) )      
                     enint = enint + cR(i,j) * (unoR(i) + dueR(i) - unoStarR(j) - dueStarR(j) ) *( 1.0_dp / (lambda(i)-conjg(lambda(j))) )
                  endif
               enddo
            enddo
            !write(*,*) lambda
            !pause
            enint = enint * (gamma/(2.0_dp*pi)) 

            !the current for that voltage
            current = -microampere*aimag(H(NL-1,NL)*enint)
            write(*,*) x,vi,current
            write(21,*) x,vi,current
        
         enddo

      enddo

      deallocate(inter)
      deallocate(effeOLD)

   endif
   
   !-----------------------------DOS
   if((drawDOS == 1) .and. (drawDOS == 2)) then
      call DOS(drawDOS)
   endif
   if(drawDOS == 3) then !plot the DOS from Im(G-)
      call DOSG(lambda,evecL,evecR,ldos)
   endif
   if(drawDOS == 4) then !plot the DOS from Im(G-), but the hamiltonian has xclass now
      open(62, file='coupHO',form='unformatted')
      allocate(inter(nho,nlev,nlev))
      do i=1,nho
         read(62) inter(i,:,:)
      enddo  
      close(62)
      allocate(effeOLD(nlev,nlev))
      call buildF(effe,H,gamma)
      effeOLD = effe
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !KEY MANUAL CHANGE. DECIDE HERE WHICH MODES ARE INCLUDED IN THE CALCULATION
      !effe = effeOLD - (inter(1,:,:) * Xclass)
      !effe = effeOLD  - (inter(2,:,:) * Xclass)
      effe = effeOLD - (inter(1,:,:) * Xclass) - (inter(2,:,:) * Xclass)
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      call zgeev('V','V',nlev,effe,nlev,lambda,evecL,nlev,evecR,nlev, &
                  work,LWORK,rwork,info)
         !fixed normalization
      do i=1,nlev   
         evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
         evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
      enddo
      deallocate(inter)
      deallocate(effeOLD)

      call DOSG(lambda,evecL,evecR,ldos)
   endif
   if(drawDOS == 5) then !plot the DOS for a range of X
      open(62, file='coupHO',form='unformatted')
      allocate(inter(nho,nlev,nlev))
      do i=1,nho
         read(62) inter(i,:,:)
      enddo  
      close(62)
      allocate(effeOLD(nlev,nlev))
      call buildF(effe,H,gamma)
      effeOLD = effe
      open(49,file='ldos_X.dat')

      nstep = int((enmax-enmin)/deltaen)
      allocate(ldos(nstep+1,2))
      nstep2 = int((2*Xmax)/dX) +1
      do n=1,nstep2  
         x = (n - 1 - int((nstep2-1)/2))*dX 

         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !KEY MANUAL CHANGE. DECIDE HERE WHICH MODES ARE INCLUDED IN THE CALCULATION
         effe = effeOLD - (inter(1,:,:) * x) - (inter(2,:,:) * x)
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         call zgeev('V','V',nlev,effe,nlev,lambda,evecL,nlev,evecR,nlev, &
                     work,LWORK,rwork,info)
         !fixed normalization
         do i=1,nlev   
            evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
            evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
         enddo
         call DOSG(lambda,evecL,evecR,ldos)
         do i=0,nstep
            write(49,*) ldos(i+1,1),x,ldos(i+1,2)   
         enddo
      enddo

      deallocate(inter)
      deallocate(effeOLD)
   endif 

   !-----------------------------Trasmission
   if(drawTrasmission == 1) then
      call drawT(lambda,evecL,evecR,gamma,H,traE)
   endif
   if(drawTrasmission == 4) then
      open(62, file='coupHO',form='unformatted')
      allocate(inter(nho,nlev,nlev))
      do i=1,nho
         read(62) inter(i,:,:)
      enddo  
      close(62)
      allocate(effeOLD(nlev,nlev))
      call buildF(effe,H,gamma)
      effeOLD = effe
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !KEY MANUAL CHANGE. DECIDE HERE WHICH MODES ARE INCLUDED IN THE CALCULATION
      !effe = effeOLD - (inter(1,:,:) * Xclass)
      !effe = effeOLD  - (inter(2,:,:) * Xclass)
      effe = effeOLD - (inter(1,:,:) * Xclass) - (inter(2,:,:) * Xclass)
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      call zgeev('V','V',nlev,effe,nlev,lambda,evecL,nlev,evecR,nlev, &
                  work,LWORK,rwork,info)
      !fixed normalization
      do i=1,nlev   
         evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
         evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
      enddo
      deallocate(inter)
      deallocate(effeOLD)

      call drawT(lambda,evecL,evecR,gamma,H,traE)
   endif
   if(drawTrasmission == 5) then
      open(62, file='coupHO',form='unformatted')
      allocate(inter(nho,nlev,nlev))
      do i=1,nho
         read(62) inter(i,:,:)
      enddo  
      close(62)
      allocate(effeOLD(nlev,nlev))
      call buildF(effe,H,gamma)
      effeOLD = effe
      open(59,file='transmission_X.dat')

      nstep = int((enmax-enmin)/deltaen)
      allocate(traE(nstep+1,2))
      nstep2 = int((2*Xmax)/dX) +1
      do n=1,nstep2  
         x = (n - 1 - int((nstep2-1)/2))*dX 
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         !KEY MANUAL CHANGE. DECIDE HERE WHICH MODES ARE INCLUDED IN THE CALCULATION
         effe = effeOLD
         do i=1,nho
            effe = effe - (inter(i,:,:) * x)
         enddo
         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         call zgeev('V','V',nlev,effe,nlev,lambda,evecL,nlev,evecR,nlev, &
                     work,LWORK,rwork,info)
         !fixed normalization
         do i=1,nlev   
            evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
            evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
         enddo
         call drawT(lambda,evecL,evecR,gamma,H,traE)
         do i=0,nstep
            write(59,*) traE(i+1,1),x,traE(i+1,2)   
         enddo
      enddo

      deallocate(inter)
      deallocate(effeOLD)
   endif
   if(drawTrasmission == 10) then
      open(62, file='coupHO',form='unformatted')
      allocate(inter(nho,nlev,nlev))
      do i=1,nho
         read(62) inter(i,:,:)
      enddo  
      close(62)

      allocate(effeOLD(nlev,nlev))
      call buildF(effe,H,gamma)
      effeOLD = effe
      open(59,file='transmission_X.dat')

      nstep = int((enmax-enmin)/deltaen)
      allocate(traE(nstep+1,2))
      allocate(gelle(nstep+1))
      allocate(gerre(nstep+1))
      allocate(dielle(nstep+1))
      allocate(dierre(nstep+1))
      gelle = 0.0_dp
      gerre = 0.0_dp

      !*******************************************************
      !one off diagonalization of the submatrices of the leads
      allocate(effeL(NL,NL))
      allocate(effeR(NL,NL))
      allocate(effeC(NC,NC))
      allocate(effeCOLD(NC,NC))
      effeL = effeOLD(1:NL,1:NL)
      effeR = effeOLD(NL+NC+1:nlev,NL+NC+1:nlev)

      deallocate(lambda)
      deallocate(evecL)
      deallocate(evecR)
      allocate(lambda(NL))
      allocate(evecL(NL,NL))
      allocate(evecR(NL,NL))

      !LEFT LEAD
      call zgeev('V','V',NL,effeL,NL,lambda,evecL,NL,evecR,NL, &
                  work,LWORK,rwork,info)
      !fixed normalization
      do i=1,NL   
         evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
         evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
      enddo
      !in these evec, the TB basis need always to be on the left, while the eigenvectors of F basis on the right
      do k=0,nstep
         en = enmin + (k*deltaen)
         gelle(k+1) = 0.0_dp
         do i = 1,NL
            gelle(k+1) = gelle(k+1) + ( (1.0_dp/(en - lambda(i))) * evecR(NL,i) * conjg(evecL(NL,i)) )
         enddo
      enddo
      dielle =  -(im/(2.0_dp*pi))*(gelle  - conjg(gelle))

      !RIGHT LEAD
      call zgeev('V','V',NL,effeR,NL,lambda,evecL,NL,evecR,NL, &
                  work,LWORK,rwork,info)
      !fixed normalization
      do i=1,NL   
         evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
         evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
      enddo
      !in these evec, the TB basis need always to be on the left, while the eigenvectors of F basis on the right
      do k=0,nstep
         en = enmin + (k*deltaen)
         gerre(k+1) = 0.0_dp
         do i = 1,NL
            gerre(k+1) = gerre(k+1) + ( (1.0_dp/(en - lambda(i))) * evecR(1,i) * conjg(evecL(1,i)) )  !careful that here the lead is inverted, I need the border facing the center, so site 1
         enddo
      enddo
      dierre =  -(im/(2.0_dp*pi))*(gerre  - conjg(gerre))
      !write(*,*) dielle(5),dierre(5)
      !write(*,*) dielle(40),dierre(40)
      !do k=1,nstep
      !   if(real(dierre(k),dp)<0.0_dp)   write(*,*) k,dierre(k)
      !enddo
      !STOP
      !*******************************************************

      nstep2 = int((2*Xmax)/dX) +1
      do n=1,nstep2  
         x = (n - 1 - int((nstep2-1)/2))*dX 

         effe = effeOLD
         do i=1,nho
            effe = effe - (inter(i,:,:) * x)
         enddo

         !!!!!!!!!!KEY CHANGE, I diagonalize ONLY the central part of the matrix
         effeC = effe(NL+1:NL+NC,NL+1:NL+NC) !!!!!!!!!!!it still must be embedded, using self energies
         effeCOLD = effeC
         deallocate(lambda)
         deallocate(evecL)
         deallocate(evecR)
         allocate(lambda(NC))
         allocate(evecL(NC,NC))
         allocate(evecR(NC,NC))

         do k=0,nstep
            en = enmin + (k*deltaen)

            effeC = effeCOLD
            effeC(1,1) = effeC(1,1) + (tchainW*tchainW)*gelle(k+1)  !questa è gelle meno, quindi mi uscirà G- da qua
            effeC(NC,NC) = effeC(NC,NC) + (tchainW*tchainW)*gerre(k+1)  
            call zgeev('V','V',NC,effeC,NC,lambda,evecL,NC,evecR,NC, &
                        work,LWORK,rwork,info)
            !fixed normalization
            do i=1,NC   
               evecL(:,i) = evecL(:,i) / dot_product(evecL(:,i),evecR(:,i))
               evecR(:,i) = evecR(:,i) / dot_product(evecL(:,i),evecR(:,i))
            enddo

            gcentro = 0.0_dp
            gcentrostar = 0.0_dp
            !do j = 1,NC !on TB central region
               do i = 1,NC !on central region eigenvalues
                  gcentro = gcentro + ( (1.0_dp/(en - lambda(i))) * evecR(1,i) * conjg(evecL(NC,i)) )
                  !gcentrostar  = gcentrostar + conjg( (1.0_dp/(en - lambda(i))) * evecR(NC,i) * conjg(evecL(1,i)) )
               enddo
            !enddo
            gcentrostar = conjg(gcentro)

            !tra = 4.0_dp*pi*pi*gcentro*conjg(gcentro)*dierre(k+1)*dielle(k+1)*(tchainW**4)
            tra = 4.0_dp*pi*pi*gcentro*(gcentrostar)*dierre(k+1)*dielle(k+1)*(tchainW**4)
            traE(k+1,1) = en
            traE(k+1,2) = real(tra,dp)
   
            write(59,'(F10.4,F10.4,E15.5)') traE(k+1,1),x,traE(k+1,2)
         enddo

      enddo

      deallocate(inter)
      deallocate(effeOLD)

   endif

   !draw energy levels
   if(drawEnergiesNC == 1) then

      open(62, file='coupHO',form='unformatted')
      allocate(inter(nho,nlev,nlev))
      do i=1,nho
         read(62) inter(i,:,:)
      enddo  
      close(62)

      allocate(effeNC(NC,NC))
      allocate(effeOLDNC(NC,NC))
      allocate(interNC(NC,NC))
      effeOLDNC = effe(NL+1:NC,NL+1:NC)
      interNC = inter(1,NL+1:NC,NL+1:NC)

      open(46, file='energiesNC.dat')
      open(47, file='hamCentralX.dat')
      allocate(work(lwork))
      allocate(rwork(lrwork))
      allocate(iwork(liwork))
      allocate(eigv(NC))

      nstep2 = int((2*Xmax)/dX) +1
      allocate(store(NC,nstep2))

      do n=1,nstep2
   
         x = (n - 1 - int((nstep2-1)/2))*dX      

         effeNC = effeOLDNC - (interNC * x) 
      
         !diagonalize effe
         call zheevd('V','U',NC,effeNC,NC,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
         !call zgeev('V','V',NC,effeNC,NC,lambda,evecL,NC,evecR,NC,work,LWORK,rwork,info)
                  
         !write(*,*) x,eigv(40)-chempot
         !if (n==51) then
         !   do j=1,NC
         !      write(*,*) x,j,eigv(j)-chempot
         !   enddo
         !endif

         do j=1,NC
            !write(46,*) x,j,eigv(j)-chempot
            store(j,n) = eigv(j)-chempot
         enddo

         !I print the projections only for certain X
         if((n==23).or.(n==30).or.(n==37).or.(n==44).or.(n==51).or.(n==58).or.(n==65)) then!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            do j = 1,NC
               do i = 1,NC
                  !first index atomic base, second index energy base
                  write(47,'(i8,i8,f15.7,f10.3,e17.9)') i,j,eigv(j)-chempot,x,real(effeNC(i,j)*conjg(effeNC(i,j)),dp)
               enddo
               write(47,*)
            enddo
            write(47,*)
         endif


      enddo

      !print
      do j=1,NC
         do n=1,nstep2
            x = (n - 1 - int((nstep2-1)/2))*dX      
            write(46,*) x,j,store(j,n)
         enddo
      enddo

   endif


!-------------------------------------

contains

subroutine buildF(effe,H,gamma)

!this is the operatorial part in G- = H_S0 + Sigma- 

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: effe
   real(dp), dimension(:,:), INTENT(IN) :: H
   real(dp), INTENT(IN) :: gamma
   integer :: i,j

   effe = H

   if(h2o == 0) then !normal case
      do i= 1,NL
         effe(i,i) = effe(i,i) + im*0.5_dp*gamma
      enddo

      do i= NL+NC+1,nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gamma
      enddo
   else !with h2o I need to create a part of the leads where to measure the current
      do i= 1,NL-meterlead
         effe(i,i) = effe(i,i) + im*0.5_dp*gamma
      enddo

      do i= NL+NC+1+meterlead,nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gamma
      enddo
   endif 

end subroutine buildF

!---------------------------------

subroutine DOS(input)

   use modvar
   implicit none

   real(dp), allocatable :: energies(:)
   real(dp) :: en,density
   integer :: i,j,numen,nstep,numrow,input
   
   open(17, file='energiesfordos.dat')
   if(input==1) then !qbox style input
      read(17,*)  numen,numrow
      allocate(energies(numen))
      j=1
      do i=1,numrow
         read(17,*) energies(j),energies(j+1),energies(j+2),energies(j+3),energies(j+4) !FIX this depending on the file
         j = j + 5
      enddo
      close(17)
   elseif(input==2) then !plain list input
      read(17,*)  numen
      allocate(energies(numen))
      do j=1,numen
         read(17,*) energies(j)
      enddo
      close(17)   
   endif
   
   nstep = int((enmax-enmin)/deltaen)
   open(18,file='dos.dat')
   
   do i=0,nstep
      en = enmin + (i*deltaen)
      density = 0.0_dp
      do j=1,numen
         density = density + exp(-(en-energies(j))**2/(2.0_dp*sigmagauss**2))
      enddo
      write(18,*) en,density  
   enddo
   close(18)
   write(*,*) 'Density of states computed successfully!'

end subroutine DOS

!---------------------------------

subroutine DOSG(lam,evecL,evecR,ldos)

   use modvar
   implicit none

   complex(dp), dimension(:), INTENT(IN) :: lam
   complex(dp), dimension(:,:), INTENT(IN) :: evecL,evecR
   real(dp), dimension(:,:), INTENT(OUT) :: ldos
   complex(dp), dimension(:), allocatable :: gimeno
   complex(dp) :: gim
   real(dp) :: en,density
   integer :: i,j,k,numen,nstep,numrow,input  

   open(18,file='dos.dat')
   open(19,file='ldos_central.dat')
   numen = size(lam)
   nstep = int((enmax-enmin)/deltaen)
   allocate(gimeno(numen))

   !DOS summed over all eigenstates of G-
   do i=0,nstep
      en = enmin + (i*deltaen)
      density = 0.0_dp
      do j=1,numen
         gimeno(j) = 1.0_dp/(en - lam(j))
         density = density + aimag(gimeno(j))/pi
      enddo
      write(18,*) en,density  
      !write(*,*) en,density 
   enddo

   !LDOS over the states in the central region
   do i=0,nstep
      en = enmin + (i*deltaen)
      density = 0.0_dp
      do j=1,numen
         gimeno(j) = 1.0_dp/(en - lam(j))
         do k=NL+1,NL+NC
            gim = gimeno(j)*evecR(k,j)*conjg(evecL(k,j))
            density = density + aimag(gim)/pi
         enddo
      enddo
      write(19,*) en,density  
      !write(*,*) en,density
      ldos(i+1,1) = en
      ldos(i+1,2) = density
   enddo

end subroutine DOSG

!---------------------------------

subroutine drawT(lam,evecL,evecR,gamma,H,traE)

   use modvar
   implicit none

   complex(dp), dimension(:), INTENT(IN) :: lam
   real(dp), INTENT(IN) :: gamma
   real(dp), dimension(:,:), INTENT(OUT) :: traE
   real(dp), dimension(:,:), INTENT(IN) :: H
   complex(dp), dimension(:,:), INTENT(IN) :: evecL,evecR
   complex(dp), allocatable :: cL(:,:),cR(:,:),c(:,:)
   complex(dp) :: factor,tra
   real(dp) :: en,tbar
   integer :: i,j,k,L,R,nstep

   open(27,file='trasmission.dat')
   nstep = int((enmax-enmin)/deltaen)
   allocate(cL(nlev,nlev))
   allocate(cR(nlev,nlev))
   allocate(c(nlev,nlev))
   cL = 0.0_dp
   cR = 0.0_dp

   do k=0,nstep
      en = enmin + (k*deltaen)
      tra = 0.0_dp
      cL = 0.0_dp
      cR = 0.0_dp

      do i = 1,nlev
         do j = 1,nlev
                
            do L = 1,NL-meterlead
               cL(i,j) = cL(i,j) + ( conjg(evecL(L,i)) * evecL(L,j)  )
            enddo
            do R = NL+NC+1+meterlead,nlev
               cR(i,j) = cR(i,j) + ( conjg(evecR(R,j)) * evecR(R,i)  )
            enddo          
            c(i,j) = cL(i,j) * cR(i,j)
            factor = 1.0_dp/((en - conjg(lam(j))) * (en - lam(i)))
            tra = tra + c(i,j)*factor*gamma*gamma
      
         enddo
      enddo

      !the trasmission for that energy
      write(27,*) en,real(tra,dp)
      !write(*,*) en,real(tra,dp)
      traE(k+1,1) = en
      traE(k+1,2) = real(tra,dp)

   enddo

end subroutine drawT

end program steady
