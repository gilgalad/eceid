code ELPH, Release 0.2, 02/05/2014

Changes:

(major)
-  very important Hartree-Fock term in mu and Ndot. Basically the previous program treated the electrons classically. 
   If they were not coupled with an oscillator, this did not afect the results at all 
   (because the terms affected by the change include F).
   The calculations are in the sheets of paper. They restrict the number of allowed transitions in the wire with the correct
   fermionic statistics. They force the rate of transition to be like   f_i (1 - f_f).

(very minor)
-  Slight change in the initial state for ATOM, the DM include half an electron on the next excited state. 
   The problem was that this system had an odd number of electrons and the initial DM didn't take this into account
-  Temporarily changed the coupling in ATOM to a free small parameter in input.dat

---------------------------------------------------------------------------------------

code ELPH, Release 0.11, 17/04/2014

Changes:

-  elph.f90 and diagH.f90 have been tidied up with more section dividers and a reordering of routines based on their role

-  a switch in modvar.f90, "geomwire", for changing the geometry of the wire. For now only 2 are available: the dimer and the single atom in the middle of the chain. 
   There is a warning in diagH.f90 for the parity of NC, different geometries have different simmetries and require different number of atoms.
   
-  Now the coupling of the eventual oscillators is done in diagH.f90, so that in there there is the building of all the initial matrices and the geometry of the wire.
   In elph.90 there is only the evolution of the system. Whenever a new system needs to be introduced, it will affect only diagH.f90.
   
Problems:

- the code requires a many electron -> electron treatment like Hartree-Fock to be working properly with phonons. This is completely absent for now. Working on it.

---------------------------------------------------------------------------------------

code ELPH, Release 0.1, 15/04/2014

Changes:

-  introduction of a lapack complex matmul: zgemm. It can be turned on with the switch lapackMatmul in modvar.f90. 
   The speedup with -mkl and the ifort compiler is very big, the code is at least 2 times faster. The flag -laback with ifort is worse in performance than the normal matmul though.
   
-  different approach for the matrix multimplication of Fs/Fc and rho. Now that is done in the routine integralf and the result sotred for each timestep.
   The expensive matmul operation is done two times less per timestep (60% speed improvement).
   
-  New geometry of the system that is simulated. Now it's a perfect wire with a single oscillator in the middle. The number of atoms in the central region
   has to be odd becasue of this. Therefore there's a new set of parameters . Atom defines the position of the oscillator in the chain.
   Achar the characteristic length of the system. The coupling of the oscillator is given by the hopping in the chain over achar. The coupling that appears in input.dat for each oscillator
   is here meaningless.
   
-  The oscillator here is turned linearly on over a period TOB (like the OB). Basically its coupling to the system increases over time until t=TOB is reached.

-  The energy of the oscillator is calculated and stored in energiaHO.dat (it is a function of the number of phonons in phon.dat, nothing really new).

- the mass of the oscillator is now in amu, not in proton mass anymore
   
---------------------------------------------------------------------------------------

code ELPH, Release 0.0, 10/04/2014

This code can compute the steady state current in a 1D Tight Binding wire with an arbitrary set of coupling between the sites.
By using the open boundary technique, it can estimate the asymptotic steady state current through a time dependent process.
It can compare it to the exact one for finite wires, obtained with an energy integral of the system's Green's Functions. 
For the specific case of a dimer embedded in the wire, it can also give the exact Landauer current for an infinite wire.

A new feature yet to be tested properly is the possible inclusion of phonons that interact in real time with the electronic current.
The code is parallelizable with OpenMP and , since it has an MD structure, it has an intrinsic limitation to scalability.
It scales very well for a number of processor equal to the number of oscillators (never more cores than oscillators!).

Guide to the components:

-  Makefile: to compile the program and its components. The top bit uses ifort and mkl, generally faster. The botton bit gfortran with different option. 
   The -O0 for debugging and profiling, then the normal and the one without openmp for long wires (NL>500) and without phonons where memory is an issue.

-  modvar.f90: it contains all the common parameters that the other programs need, from the geometry of the wire, to the switches for different options.
   It also defines structures (that are like objects, sets of variables) that are either global or bound to a single oscillator.
   
-  input.dat: it contains the parameters for the oscillators. Every line pertains to a different oscillator. The column are respectively:
   coupling, initial number of phonons, characteristic energy, mass. It was written like this so that the elph program runs smoothly without any modification for a different number of oscillators.
   Basically, changing nho in modvar lets the program read more lines in input.dat and makes the system work with a different number of oscillators.
   Putting the parameters here makes the program much more dynamical.
   
-  diagH.f90: the initial program that builds the system.In "ham" it writes the hamiltonian, its eigenvectors and eigenvalues.
   It writes to screen the average and maximum energy spacing in the leads. In "rhozero" it writes the initial DM. 
   In "inj" it writes the OB injection matrix. NOTE: It MUST be run after any modification in modvar.f90 because it provides the right set
   of initial conditions and variables to the other programs.

-  steady.f90: depending on the switches in modvar, it computes the exact steady state current for a finite wire or 
   a scan for different voltages so that a I-V curve can be built. It can do the same for the Landauer results, but it depends on the trasmission function that
   is embedded in the program (and was computed with Mathematica). So the Landauer bit works only for a dimer like Eunan's one. To test different geometries
   the program must be modified with a different trasmission to be itnegrated. 

-  elph.f90: the main program. It simulates the time evolution of the electronic DM and the phonon number. It computes also the time dependent current.
   If the phonons are of it just computes the OB current in the wire. It is the program that scales with OpenMP for a different number of phonons.
   Just remember the command export OMP_NUM_THREADS=? before a parallel execution.


