#!/usr/bin/env python

import sys,getopt




usage = '''
plot_col.py [-x|--xaxis col] [-y|--yaxes col1,col2,...] [-s|--skiplines n] [-w|--linewidth w] [-l|--linestyle] [-m|--marker m] file1 [file2 ...]
   options:
      -h, --help
      -x, --xaxis 
         choose column to use as X axis. The default is 0 from each file.
      -y, --yaxes
         coma separated numbers of columns to plot on the Y axis. The default is to plot all columns except X.
      -s, --skiplines
         skip n lines from the beginning of the file
      -m, --marker
         specify the marker to be used. Does not use markers by default.
      -w, --linewidth
         used for all lines. Default 1.
      -l, --linestyle
         default '-'
   note: there is plenty of room for improvements on the idividuality of the files.
'''

def options_error(msg):
   print msg
   print 'usage:'
   print usage
   print 'Have another go!'
   sys.exit(1)


if len(sys.argv) == 1:
   options_error('data file not provided!')

try:
   opts, args = getopt.gnu_getopt(sys.argv[1:],'hx:y:s:m:w:l:', \
      ['help','xaxis=','yaxes=','skiplines=','marker=','linewidth=','linestyle='])
except getopt.GetoptError:
   options_error('gnu_getopt couldn\'t parse the command!')



from pylab import *

xcol = 0
ycols = None
skip = 0
comment = '#'
marker=None
lw = 1
ls = '-'

for opt,val in opts:
   if opt in ('-h', '--help'):
      print usage
      sys.exit(0)
   if opt in ('-x','--xaxis'):
      xcol = int(val)
   if opt in ('-y','--yaxes'):
      ycols = [int(i) for i in val.split(',')]
   if opt in ('-s','--skiplines'):
      skip = int(val)
   if opt in ('-m', '--marker'):
      marker = val
   if opt in ('-w', '--linewidth'):
      lw = val
   if opt in ('-l', '--linestyle'):
      ls = val
   

if ycols != None:
   if xcol in ycols:
      ycols.remove(x)
   ycols.insert(0,xcol)       

flns = args

axhline(0,color='black',linestyle='--')
axvline(0,color='black',linestyle='--')

for f in flns:
    d = loadtxt(f,dtype=float64,comments=comment,skiprows=skip,usecols=ycols)
    if len(d.shape) == 2:
        for i in range(1,d.shape[1]):
            plot(d[:,0], d[:,i], label='%s: %i' % (f, i),marker=marker, lw=lw, ls=ls)
    elif len(d.shape) == 1:
        plot(d,label=f,marker=marker,lw=lw)
    
legend()    
show()