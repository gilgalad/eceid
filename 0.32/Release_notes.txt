---------------------------------------------------------------------------------------

code ELPH, Release 0.32, 16/06/2014, Codename: California Sun

- with Alfredo's suggestion, the trace of mu is computed. It is the displacement of the ion at every time. It's not integrated out like with the phonon number.

- also the variance of mu is written to varMu.dat and its normalized counterpart to varmunorm.dat. They could be related to the microscopic noise that we want to introduce.
  If for long times the normalized goes to 0, the microscopic noise seemingly plays a role only in the first part of the heat trnsfer but not at equilibrium.
  If it doesn't, it is important throughout the whole process. This may strongly depend on the parameters of the system, like the stiffnes of the oscillator.
  If it is very stiff (high omegas, like the hopping integral more or less) the transfer of energy is low but the oscillations are very visible in trMu, they go back and forth. While the number of phonons integrates out that information. If it is less stiff, it's like an overdamped system and no oscillations appear in trMu. The value that trMu assumes at equilibrium is shifted forwards and it is expectable because of the asymmetry in the system, an electronic current goes from L to R. The sift might strongly depend on a classical ratio: tha mass of the ion and that of the electron. The mass of our electrons is hidden in the energy scale and distance. (HOW TO INFER IT?).
  It is possible that for high energies (and a lot of electrons) like in Alfredo's case with Ehrenfest, the movement of energy electrons-> ions is captured, but it suppresses oscillations, it gets only the rigid shift in the oscillator position. 
Tchvadar answered. This part must be fixed.

- small fix for systems with an odd NC, now the DM is filled correctly for any of them. Before it was correct only for the 'atom' system.

- new geometry: Einstein (geomwire = 4). It creates an even number of Einstein oscillators in the middle of the wire, each separated by 2 TB sites.

To do:

- better printing for multiple oscillators and new observables for descibing their behaviour, possibly using Statistical Mechanics

- the trace and variance of mu need to be corrected with Tchavdar's suggestion of using all the terms in HF and trace of rho.


---------------------------------------------------------------------------------------

code ELPH, Release 0.31, 02/06/2014

Bugfixes and changes:

-  in leapfrog the new part for newFcFs=3 appeared only in the non parallel branch

-  in leapfrog the label of the oscillator was j not i

-  in Fcspunto there was a typo in the second matrix product, temp instead of tempp

-  Hplus can be made by SigmaPlus or an epsE on the diagonal.  I introduced a switch, HplusSwitch to decide which one.

-  in Fcspunto the routine was wrong, the second term was not the h.c. of the first. Maybe because H is no hermitian here.
   I forced it to be the h.c. and I get results comparable to newFcFs=3 when 

-  in 'steady', I made the Landauer calculation to be compatible with the system ATOM

-  CAREFUL to HF, it was turned on with the traces untouched, option 1, not ignored as in option 2

Improvement to implement:

-  more flexibility in changing compiler. For now I need to comment out 2 lines when switching from ifort to gfortran

---------------------------------------------------------------------------------------

code ELPH, Release 0.3, 01/06/2014, MAJOR RELEASE, it finally works!!!! 

NOTES

Finally fixed the problem of the different results compared to Tchavdar's program. Basically there were 2 major differences:
In T.'s program the evolution of Fc/Fs used H0 + Sigma, so it had an intrinsic damping.
The ramp TOB that turns on also the coupling implies that F is time dependent, while in the explicit integrals in integralF2
assume that it is time INDEPENDENT. In T's program this problem didn't appear because the integrals were not evaluated.
The steps that I got were artifacts due to to internal reflections in the finite wires, in fact their time length depended 
solely on the physical length of the leads. With the dissipation they are smoothed out until they disappear.

Changes:

-  new routines for computing Fc/Fs. If newFcFs=1, they behave as before. If newFcFs=2 there's the routine integralF2 that has got
   a new form for the explicit integrals that includes an eps factor that model dissipation in the force operators. 
   That eps is added to the eigenvalues of H0. The form of the integrals is slightly different. There's plenty of new variables
   initialized because of this.
   If newFcFs=3 there's an absolutely major change. Fc/Fs are evolved explicitly with Verlet. The hamiltonian that their evolution
   use is called Hplus = H0 + Sigma. There's other possible choices, like adding diagonally another complex epsilon.
   This looks like the most physical one. This option 3 looks to be much faster than the previous ones.

-  introduction of findnonzeroC that finds the nonzero elements in sparse complex matrices. I use it fo Hplus and for SigmaPlus
   that finally gets a uniform treatment with the other sparse matrices. Now all of them are treated the same dynamical way:
   they get read or filled, a findnonzero routine is called and a matrix stores the position of their nonzero elements.

-  small bug in a call of rhopunto that used time instead of time + dt

-  checkHermite is slightly different. It uses a zero set in modvar to check if two entries in matrices are the same.

Issues and future developments:

-  The code looks to be working fine! I can test with different systems. The dissipation looks to have a major importance.

-  Implement other dissipation epsilon in Hplus, maybe site dependent. For now, there's only the self energy in Hplus.

-  Eventually clean all the previous integralF and related variables. The code looks to be much faster with the 
   Verlet integration of Fc/Fs. After a bit of testing I think it is worth it for clarity reasons, at the expence of
   legacy compatibility.

---------------------------------------------------------------------------------------

code ELPH, Release 0.21, 06/05/2014

Changes:

-  different parallelisation. Now the parameter "ncore" in modvar.f90 means the total number of cores available.
   The parallel section on the oscillators is now forced to have num_threads=nho.
   Now "parallelHO" can be left set to 1, the program will detect if the oscillator is 1 only and 
   will not try to parallelize it. The command export OMP_NUM THREADS=? is not necessary anymore. 
   If there is one oscillator the parallelisation goes in the mkl matmul 
   (if available and if it is set as parallel in the makefile). If there is more than one oscillator, the mkl
   becomes serial and the parallelisation is the trivial OpenMP over the oscillators.

-  now the "hf" parameter can be 1 for the exact HF term or 2 for the term with the exclusion of the first part,
   the trace, as Tchavdar suggested.

-  Change of the order of the parameters in modvar.f90. Now it is clearer

-  printing on the screen now includes the oscillator too

Issues and future developments:

-  BAD ISSUE: this release has got weird steps in the phonon number and current every 30fs more or less.
   The previous version of the program didn't have them for the dimer case, but I cannot reproduce it anymore.
   It looks to be not related to the OB or HF. It appears whenever there is an oscillator.

-  It would be fantastic to have nested parallelisation. I tried and tried but it doesn't work. I would love to 
   have a parallelisation over the oscillators AND within them with mkl. I think that a solution is to write the
   program using MPI so the oscillators run on different processes that can use different threads with mkl.
   It's not easy and immediate, but in the future it will be necessary.

-  A save and restart function would be nice. It would require a bit of work though and now it is not necessary.

---------------------------------------------------------------------------------------

code ELPH, Release 0.2, 02/05/2014

Changes:

(major)
-  very important Hartree-Fock term in mu and Ndot. Basically the previous program treated the electrons classically. 
   If they were not coupled with an oscillator, this did not afect the results at all 
   (because the terms affected by the change include F).
   The calculations are in the sheets of paper. They restrict the number of allowed transitions in the wire with the correct
   fermionic statistics. They force the rate of transition to be like   f_i (1 - f_f).

(minor)
-  Slight change in the initial state for ATOM, the DM include half an electron on the next excited state. 
   The problem was that this system had an odd number of electrons and the initial DM didn't take this into account
-  Temporarily changed the coupling in ATOM to a free small parameter in input.dat

---------------------------------------------------------------------------------------

code ELPH, Release 0.11, 17/04/2014

Changes:

-  elph.f90 and diagH.f90 have been tidied up with more section dividers and a reordering of routines based on their role

-  a switch in modvar.f90, "geomwire", for changing the geometry of the wire. For now only 2 are available: the dimer and the single atom in the middle of the chain. 
   There is a warning in diagH.f90 for the parity of NC, different geometries have different simmetries and require different number of atoms.
   
-  Now the coupling of the eventual oscillators is done in diagH.f90, so that in there there is the building of all the initial matrices and the geometry of the wire.
   In elph.90 there is only the evolution of the system. Whenever a new system needs to be introduced, it will affect only diagH.f90.
   
Problems:

- the code requires a many electron -> electron treatment like Hartree-Fock to be working properly with phonons. This is completely absent for now. Working on it.

---------------------------------------------------------------------------------------

code ELPH, Release 0.1, 15/04/2014

Changes:

-  introduction of a lapack complex matmul: zgemm. It can be turned on with the switch lapackMatmul in modvar.f90. 
   The speedup with -mkl and the ifort compiler is very big, the code is at least 2 times faster. The flag -laback with ifort is worse in performance than the normal matmul though.
   
-  different approach for the matrix multimplication of Fs/Fc and rho. Now that is done in the routine integralf and the result sotred for each timestep.
   The expensive matmul operation is done two times less per timestep (60% speed improvement).
   
-  New geometry of the system that is simulated. Now it's a perfect wire with a single oscillator in the middle. The number of atoms in the central region
   has to be odd becasue of this. Therefore there's a new set of parameters . Atom defines the position of the oscillator in the chain.
   Achar the characteristic length of the system. The coupling of the oscillator is given by the hopping in the chain over achar. The coupling that appears in input.dat for each oscillator
   is here meaningless.
   
-  The oscillator here is turned linearly on over a period TOB (like the OB). Basically its coupling to the system increases over time until t=TOB is reached.

-  The energy of the oscillator is calculated and stored in energiaHO.dat (it is a function of the number of phonons in phon.dat, nothing really new).

- the mass of the oscillator is now in amu, not in proton mass anymore
   
---------------------------------------------------------------------------------------

code ELPH, Release 0.0, 10/04/2014

This code can compute the steady state current in a 1D Tight Binding wire with an arbitrary set of coupling between the sites.
By using the open boundary technique, it can estimate the asymptotic steady state current through a time dependent process.
It can compare it to the exact one for finite wires, obtained with an energy integral of the system's Green's Functions. 
For the specific case of a dimer embedded in the wire, it can also give the exact Landauer current for an infinite wire.

A new feature yet to be tested properly is the possible inclusion of phonons that interact in real time with the electronic current.
The code is parallelizable with OpenMP and , since it has an MD structure, it has an intrinsic limitation to scalability.
It scales very well for a number of processor equal to the number of oscillators (never more cores than oscillators!).

Guide to the components:

-  Makefile: to compile the program and its components. The top bit uses ifort and mkl, generally faster. The botton bit gfortran with different option. 
   The -O0 for debugging and profiling, then the normal and the one without openmp for long wires (NL>500) and without phonons where memory is an issue.

-  modvar.f90: it contains all the common parameters that the other programs need, from the geometry of the wire, to the switches for different options.
   It also defines structures (that are like objects, sets of variables) that are either global or bound to a single oscillator.
   
-  input.dat: it contains the parameters for the oscillators. Every line pertains to a different oscillator. The column are respectively:
   coupling, initial number of phonons, characteristic energy, mass. It was written like this so that the elph program runs smoothly without any modification for a different number of oscillators.
   Basically, changing nho in modvar lets the program read more lines in input.dat and makes the system work with a different number of oscillators.
   Putting the parameters here makes the program much more dynamical.
   
-  diagH.f90: the initial program that builds the system.In "ham" it writes the hamiltonian, its eigenvectors and eigenvalues.
   It writes to screen the average and maximum energy spacing in the leads. In "rhozero" it writes the initial DM. 
   In "inj" it writes the OB injection matrix. NOTE: It MUST be run after any modification in modvar.f90 because it provides the right set
   of initial conditions and variables to the other programs.

-  steady.f90: depending on the switches in modvar, it computes the exact steady state current for a finite wire or 
   a scan for different voltages so that a I-V curve can be built. It can do the same for the Landauer results, but it depends on the trasmission function that
   is embedded in the program (and was computed with Mathematica). So the Landauer bit works only for a dimer like Eunan's one. To test different geometries
   the program must be modified with a different trasmission to be itnegrated. 

-  elph.f90: the main program. It simulates the time evolution of the electronic DM and the phonon number. It computes also the time dependent current.
   If the phonons are of it just computes the OB current in the wire. It is the program that scales with OpenMP for a different number of phonons.
   Just remember the command export OMP_NUM_THREADS=? before a parallel execution.


