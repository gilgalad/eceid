program elph

   !this program computes the current in a 1D wire with the geometry in the hamiltonian built in diagH.
   !it can simulate phonons that interact in real time with the OB driven current
   !REQUIREMENTS: the execution of diagH

   use modvar
   implicit none
   
   integer :: i
   type(global) :: g
   type(oscillator), allocatable :: o(:)
  
   allocate(g%r0(nlev,nlev))  !the density matrix at the old step
   allocate(g%r1(nlev,nlev))  !the density matrix at the present step
   allocate(g%r2(nlev,nlev))  !the density matrix at the future step
   allocate(g%rdot(nlev,nlev))

   allocate(g%Hel(nlev,nlev))
   allocate(g%Hprojections(nlev,nlev))
   allocate(g%HprojectionsTC(nlev,nlev))
   allocate(g%E(nlev))
   allocate(g%SigmaPlus(nlev,nlev)) !OB term
   allocate(g%inj(nlev,nlev))       !OB injection term for a 0 temperature FD distribution
   allocate(g%epsnum(nlev,nlev))   !static variable that will be used in the NEW integralF2 routine
   allocate(g%Hplus(nlev,nlev))

   if(nho .ne. 0) then
      allocate(o(nho))
      open(33, file='input.dat')
      do i=1,nho
         allocate(o(i)%F(nlev,nlev))
         allocate(o(i)%Fij(nlev,nlev))
         allocate(o(i)%Fc(nlev,nlev))
         allocate(o(i)%Fs(nlev,nlev))
         allocate(o(i)%Fc0(nlev,nlev))  !new Fc/Fs because of their explicit evolution, to use them newFcFs=3 is necessary
         allocate(o(i)%Fs0(nlev,nlev))
         allocate(o(i)%Fc1(nlev,nlev))  
         allocate(o(i)%Fs1(nlev,nlev))
         allocate(o(i)%Fc2(nlev,nlev))  
         allocate(o(i)%Fs2(nlev,nlev))
         allocate(o(i)%Fcdot(nlev,nlev))  
         allocate(o(i)%Fsdot(nlev,nlev))
         allocate(o(i)%FsRho(nlev,nlev)) !NEW, these 4 variables avoid to do matmul twice per timestep
         allocate(o(i)%FcRho(nlev,nlev))
         allocate(o(i)%RhoFs(nlev,nlev))
         allocate(o(i)%RhoFc(nlev,nlev))
         allocate(o(i)%mu(nlev,nlev))
         allocate(o(i)%omegameno(nlev,nlev)) !omega_ij - omega
         allocate(o(i)%omegapiu(nlev,nlev)) !omega_ij + omega
         allocate(o(i)%epsden(nlev,nlev)) 
         read(33,*) o(i)%coup, o(i)%N1, o(i)%om, o(i)%mass
         o(i)%om = o(i)%om/hb !so that the om is in eV/hb units = fs^-1
         o(i)%mass = o(i)%mass*amu !so that the oscillator mass is in amu units
      end do
      close(33)
   else
      !just allocate a fake oscillator so that the subroutines don't crash
      allocate(o(1))
   endif

   open(34, file='phon.dat')
   open(36, file='bondcurrent.dat')
   open(37, file='energiaHO.dat')
   open(41, file='tracerho.dat')
   open(42, file='debug.dat')
   open(43, file='trMu.dat')
   open(66, file='varMu.dat')
   open(67, file='varMunorm.dat')
   
   call initialize(g,o)
   call leapfrog(g,o)


!-------------------END MAIN PROGRAM-------------------------


contains

!----------------------
!setting up the initial parameters and the step 0 of the time evolution
!----------------------
subroutine initialize(g,o)

   use modvar
   implicit none
   
   integer :: i,j,l,k,numel
   integer, dimension(nlev*nlev,2) :: temp
   complex(dp), dimension(nlev,nlev) :: buffer
   type(global) :: g
   type(oscillator), dimension(:) :: o   

   !files where the initial hamiltonian and its GS DM are stored
   open(44, file='ham',form='unformatted')
   open(45, file='rhozero',form='unformatted')
   open(46, file='inj',form='unformatted')
    
   !initialize the electronic hamiltonian
   read(44) g%Hel,g%Hprojections,g%E
   !initialize the electronic density matrix
   read(45) g%r1
   !initialize the injection term in the OB
   read(46) g%inj
   close(44)
   close(45)
   close(46)

   !find nonzero elements of the Hamiltonian and store their position, great optimization for matmul with sparse matrices
   call findnonZero(g%Hel,temp,numel)
   allocate(g%nonZeroH(numel,2))
   g%nonZeroH = temp(1:numel,:)
   !write(*,*) 'H',numel
    
   !it's real BUT I use it as complex because of the lapack function zgemm
   g%HprojectionsTC = conjg(transpose(g%Hprojections)) 
   !g%HprojectionsTC = (transpose(g%Hprojections))        
  
   !OB terms initialization
   g%SigmaPlus = 0.0_dp
   do i= 1,NL
      g%SigmaPlus(i,i) = -im*gam*0.5_dp
   enddo
   do i= NL+NC+1,nlev
      g%SigmaPlus(i,i) = -im*gam*0.5_dp
   enddo
   call findnonZeroC(g%SigmaPlus,temp,numel)
   allocate(g%nonZeroSigma(numel,2))
   g%nonZeroSigma = temp(1:numel,:)

   !NEW, creation of Hplus for the explicit evolution of Fc/Fs
   if(HplusSwitch == 1) then   
      g%Hplus = g%Hel + g%SigmaPlus
   elseif(HplusSwitch == 2) then  
      g%Hplus = g%Hel
      do i=1,nlev
         g%Hplus(i,i) = g%Hplus(i,i) - im*epsE*0.5_dp
      enddo
   endif
   call findnonZeroC(g%Hplus,temp,numel)  !careful that Hplus is complex, findnonzeroC is needed
   allocate(g%nonZeroHplus(numel,2))
   g%nonZeroHplus = temp(1:numel,:)

   !initialize the phonon variables
   if(nho .ne. 0) then
      open(62, file='coupHO',form='unformatted')

      do l=1,nlev
         do k=1,nlev
            g%epsnum(l,k) = eps - im*(g%E(l) - g%E(k))/hb
         enddo
      enddo  

      do i=1,nho
         
         !read the interaction built in diagH
         read(62) o(i)%F
         
         call findnonZero(o(i)%F,temp,numel)
         allocate(o(i)%nonZeroF(numel,2)) !labels of the nonzero elements of F
         o(i)%nonZeroF = temp(1:numel,:)
         !write(*,*) 'F',numel
         
         !DEBUG
         !do j=1,numel
         !   write(*,*) o(1)%nonZeroF(j,1),o(1)%nonZeroF(j,2)
         !enddo
         
         !computation of F_ij without the time dependent phase
         !call FmatmulRrrr(o(i)%nonZeroF,real(g%HprojectionsTC,dp),o(i)%F, buffer)
         buffer = matmul(g%HprojectionsTC,o(i)%F)
         o(i)%Fij = matmul(buffer,g%Hprojections )          
      
         do l=1,nlev
            do k=1,nlev
               !for the old integralF
               o(i)%omegameno(l,k) = (g%E(l) - g%E(k))/hb - o(i)%om
               o(i)%omegapiu(l,k) = (g%E(l) - g%E(k))/hb + o(i)%om

               !for the new integralF2
               o(i)%epsden(l,k) = o(i)%om * o(i)%om + (g%epsnum(l,k)*g%epsnum(l,k))
            enddo
         enddo        

         if(newFcFs == 1) then
            call integralF(g, o(i),0.0_dp)
         elseif(newFcFs == 2) then
            call integralF2(g, o(i),0.0_dp)            
          elseif(newFcFs == 3) then !KEY NEW STEP, explicit evolution of Fc/Fs
            o(i)%Fc1 = 0.0_dp
            o(i)%Fs1 = 0.0_dp
            call Fcspunto(g, o(i) )  
            !backward Euler
            o(i)%Fc0 = o(i)%Fc1 - (dt*o(i)%Fcdot)
            o(i)%Fs0 = o(i)%Fs1 - (dt*o(i)%Fsdot)
            call Fcsmul(g, o(i))
         endif
         call Npunto(g, o(i)) !I use assumed shape arrays to optimise memory transfer
         call mu(g,o(i))
      end do
      deallocate(g%E)
      !backward Euler only for the first step of the oscillators
      do i=1,nho
         o(i)%N0 = o(i)%N1 - (dt*o(i)%Ndot)
      end do
   endif
   close(62)
   
   call rhopunto(g,o,0.0_dp)
   !backward Euler only for the first step
   g%r0 = g%r1 - (dt*g%rdot)

end subroutine

!----------------------
!time evolution function, it evaluates also the observables
!----------------------
subroutine leapfrog(g,o)

   use OMP_LIB
   use MKL_SERVICE
   use modvar
   implicit none

   type(global) :: g
   type(oscillator), dimension(:) :: o
   integer :: i,nstep,j,k,l,flag,dummy,num
   real(dp) :: time,J1,J2
   complex(dp) :: en,Etot,debug,trace,EHO,EHO2,buffer,buffer2
   complex(dp), dimension(nlev,nlev) :: musq

   !LAPACK
   complex(dp) :: alpha,beta
   
   call OMP_SET_NUM_THREADS(ncore)

   nstep = int(T/dt)
   do i=0,nstep
      time = dt*real(i,dp)

      !EVOLUTION
      !EULER TRICK
      if(mod(i+1,trick)==0) then         
         g%r2 = g%r1 + (dt*g%rdot)
         if(nho .ne. 0) then
            do j=1,nho
               o(j)%N2 = o(j)%N1 + (dt*o(j)%Ndot)
            end do 
         endif
      !NORMAL LEAPFROG
      else
         g%r2 = g%r0 + (g%rdot*dt*2.0_dp)
         if(nho .ne. 0) then
            do j=1,nho
               o(j)%N2 = o(j)%N0 + (o(j)%Ndot*dt*2.0_dp)
            enddo   
         endif
      endif   
   
      !OBSERVABLES & DEBUG
      if(mod(i,obsfreq)==0) then
         !DEBUG on hermiticity
         !call checkHermite(o(1)%FcRho - o(1)%RhoFc,flag)
         !call checkHermite(o(1)%mu,flag)                  
         !call checksame(o(1)%FcRho, conjg(transpose(o(1)%RhoFc)),time,flag)
         !call checksame(o(1)%FsRho, conjg(transpose(o(1)%RhoFs)),time,flag)

         !bond current
         J1 = -aimag(g%Hel(oscL+2,oscL+3)*g%r1(oscL+3,oscL+2))
         J2 = -aimag(g%Hel(siteJ,siteJ+1)*g%r1(siteJ+1,siteJ))

         !check of the trace of rho
         trace = 0.0_dp
         do k=1,nlev
            trace = trace + g%r1(k,k)
         enddo
         write(41,*) time,trace

         !CHECK of the total energy (valid for a single oscillator, trivially extendable to multiple oscillators)
         !wrong for OB, needs other terms
         !energia = matmul(g%Hel,g%r1) - matmul(g%F,o(1)%mu) 
         !Etot = (o(1)%N1 + 0.5_dp)*hb*o(1)%om
         !do j=1,nlev
         !   Etot = Etot + energia(j,j)
         !enddo
         !write(37,*) time, real(Etot)       

         !energy of the oscillator
         EHO = (o(1)%N1 + 0.5_dp)*hb*o(1)%om
         EHO2 = (o(12)%N1 + 0.5_dp)*hb*o(12)%om
         write(37,*) time, real(EHO,dp), real(EHO2,dp)

         write(36,*) time,microampere*J2
         !write(36,*) time,microampere*J1,microampere*J2
         write(*,'(f7.3,f11.6,f11.6,f11.6,f11.6)') time,microampere*J1,microampere*J2, real(EHO,dp), real(EHO2,dp)
         !write(*,'(f7.3,f11.6,f11.6,f11.6)') time,microampere*J1,microampere*J2,real(o(1)%N1,dp)
         !write(*,'(f7.3,f11.6,f11.6,f11.6,i10)') time,microampere*J1,microampere*J2,real(o(1)%N1,dp),flag
         
         if(nho .ne. 0) then

            !WARNING: ALL THEE FOLLOWING PART WIRITES OBSERVABLES ONLY FOR OSCILLATOR 1
            !!!!!!!!!!!!!!!FIX IT FOR MULTIPLE OSCILLATORS, LIKE IN THE EINSTEIN CASE

            !write(34,*) time,real(o(1)%N1,dp),aimag(o(1)%N1)
            !write(34,*) time,real(o(1)%N1,dp),real(o(2)%N1)
            write(34,'(f7.3,f11.6,f11.6,f11.6,f11.6)') time,real(o(1)%N1,dp),real(o(2)%N1),real(o(10)%N1),real(o(12)%N1)
            !write(34,'(f7.3,f11.6,f11.6)') time,real(o(1)%N1,dp),aimag(o(1)%N1)
            !write(41,*) time,real(o(1)%Ndot,dp),aimag(o(1)%Ndot)

            !check of the trace of mu
            trace = 0.0_dp
            do k=1,nlev
               trace = trace + o(1)%mu(k,k)
            enddo
            write(43,*) time,real(trace,dp)
            
            !variance of mu
            buffer = trace*trace
            if(lapackMatmul == 1) then
               alpha = 1.0_dp
               beta = 0.0_dp
               call zgemm('N','N',nlev,nlev,nlev,alpha,o(1)%mu,nlev,o(1)%mu,nlev,beta,musq,nlev) 
            else
               musq = matmul(o(1)%mu,o(1)%mu)
            endif
            musq = musq - buffer 
            buffer = 0.0_dp
            do k=1,nlev
               buffer = buffer + musq(k,k)
            enddo
            buffer = sqrt(real(buffer,dp))
            write(66,*) time,real(buffer,dp)  !the variance of mu
            buffer = sqrt(real(buffer,dp))/real(trace,dp)
            write(67,*) time,real(buffer,dp)  !the variance of mu over its average value

         endif       
      endif
     
      !RENAMING
      !trasforming the new step into step 1 and proceding to the next time step
      !Effectively the following is for the following timestep
      g%r0 = g%r1
      g%r1 = g%r2
      !horrible workaround, NUM_THREADS didn't accept an argument 0, even if the cycle was not to get entered
      num = 1
      if(nho .ne. 0) then
         num = nho  
         if(parallelHO == 1) then
            !$OMP PARALLEL DEFAULT(shared),PRIVATE(j) NUM_THREADS(num)
            !$OMP DO SCHEDULE(STATIC,1)
            do j=1,nho
               !call MKL_SET_DYNAMIC(0)
               !dummy = omp_get_thread_num()
               o(j)%N0 = o(j)%N1
               o(j)%N1 = o(j)%N2
               if(newFcFs == 1) then
                  call integralF(g, o(j), time+dt)
               elseif(newFcFs == 2) then
                  call integralF2(g, o(j), time+dt)
               elseif(newFcFs == 3) then !KEY NEW STEP, explicit evolution of Fc/Fs
                  o(j)%Fc0 = o(j)%Fc1
                  o(j)%Fs0 = o(j)%Fs1
                  o(j)%Fc1 = o(j)%Fc2
                  o(j)%Fs1 = o(j)%Fs2
                  call Fcspunto(g, o(j) )  
                  !it is necessary that I evolve the Fc/Fs here because Ndot and mu depend on them
                  !it's not really elegant, I should evolve everything in the same place at the beginning of leapfrog
                  !EULER TRICK
                  if(mod(i+1,trick)==0) then  
                     o(j)%Fc2 = o(j)%Fc1 + (dt*o(j)%Fcdot)
                     o(j)%Fs2 = o(j)%Fs1 + (dt*o(j)%Fsdot)       
                  !NORMAL LEAPFROG
                  else
                     o(j)%Fc2 = o(j)%Fc0 + (dt*o(j)%Fcdot*2.0_dp)
                     o(j)%Fs2 = o(j)%Fs0 + (dt*o(j)%Fsdot*2.0_dp) 
                  endif  
                  call Fcsmul(g, o(j)) 
               endif
               call Npunto(g, o(j))
               call mu(g, o(j))
            enddo
            !$OMP END DO
            !$OMP END PARALLEL
         else
            do j=1,nho
               o(j)%N0 = o(j)%N1
               o(j)%N1 = o(j)%N2
               !I have 3 alternatives for the time dependence of Fc/Fs: the first 2 involve an explicit integration
               !the second has a dissipation epsilon implemented, it's more general
               !the third is completely different and eventually will take over the other 2  
               if(newFcFs == 1) then
                  call integralF(g, o(j), time+dt)
               elseif(newFcFs == 2) then
                  call integralF2(g, o(j), time+dt)
               elseif(newFcFs == 3) then !KEY NEW STEP, explicit evolution of Fc/Fs
                  o(j)%Fc0 = o(j)%Fc1
                  o(j)%Fs0 = o(j)%Fs1
                  o(j)%Fc1 = o(j)%Fc2
                  o(j)%Fs1 = o(j)%Fs2
                  call Fcspunto(g, o(j) )  
                  !it is necessary that I evolve the Fc/Fs here because Ndot and mu depend on them
                  !it's not really elegant, I should evolve everything in the same place at the beginning of leapfrog
                  !EULER TRICK
                  if(mod(i+1,trick)==0) then  
                     o(j)%Fc2 = o(j)%Fc1 + (dt*o(j)%Fcdot)
                     o(j)%Fs2 = o(j)%Fs1 + (dt*o(j)%Fsdot)       
                  !NORMAL LEAPFROG
                  else
                     o(j)%Fc2 = o(j)%Fc0 + (dt*o(j)%Fcdot*2.0_dp)
                     o(j)%Fs2 = o(j)%Fs0 + (dt*o(j)%Fsdot*2.0_dp) 
                  endif  
                  call Fcsmul(g, o(j))
               endif
               call Npunto(g, o(j))
               call mu(g, o(j))
            enddo         
         endif
      endif
      call rhopunto(g,o,time+dt)
           
   enddo
   
end subroutine

!----------------------
!EVOLUTION subroutines
!----------------------

subroutine Fcspunto(g,ol)

   use modvar
   implicit none

   type(global) :: g
   type(oscillator) :: ol
   integer :: i,j
   complex(dp), dimension(nlev,nlev) :: temp,tempp

   !OPTIMIZED
   call FmatmulL(g%nonZeroHplus,g%Hplus,ol%Fc1,temp)
   !call FmatmulR(g%nonZeroHplus,ol%Fc1,g%Hplus,tempp)
   tempp = conjg(transpose(temp))
   ol%Fcdot = - (im/hb) * ( temp - tempp ) + ( ol%om * ol%Fs1 ) + ol%F

   !NON OPTIMIZED
   !ol%Fcdot = - (im/hb)*( matmul(g%Hplus,ol%Fc1) - matmul(ol%Fc1,g%Hplus) ) + ( ol%om * ol%Fs1 ) + ol%F

   !OPTIMIZED
   call FmatmulL(g%nonZeroHplus,g%Hplus,ol%Fs1,temp)
   !call FmatmulR(g%nonZeroHplus,ol%Fs1,g%Hplus,tempp)
   tempp = conjg(transpose(temp))
   ol%Fsdot = - (im/hb) * ( temp - tempp ) - ( ol%om * ol%Fc1 ) 

   !NON OPTIMIZED
   !ol%Fsdot = - (im/hb)*( matmul(g%Hplus,ol%Fs1) - matmul(ol%Fs1,g%Hplus) ) - ( ol%om * ol%Fc1 )

end subroutine

!-------------

subroutine rhopunto(g,o,time)

   use modvar
   implicit none

   integer :: i,j
   type(global) :: g
   type(oscillator), dimension(:) :: o
   complex(dp), dimension(nlev,nlev) :: OBbuffer,temp,tempp
   real(dp) :: time
   
   !it's a collective variable, depends on ALL the oscillators better to parallelize inside rather than outside
   !OPTIMIZED
   call FmatmulLrcc(g%nonZeroH,g%Hel,g%r1,temp)
   call FmatmulRcrc(g%nonZeroH,g%r1,g%Hel,tempp)
   g%rdot = - (im/hb) * ( temp - tempp )

   !NON OPTIMIZED
   !g%rdot = - (im/hb) * ( matmul(g%Hel,g%r1) - matmul(g%r1,g%Hel) )

   !eventually parallelizable for a lot of oscilators, use critical though, it is
   !an atomic udate I think
   if(nho .ne. 0) then
      do i=1,nho
         !OPTIMIZED
         call FmatmulLrcc(o(i)%nonZeroF,o(i)%F,o(i)%mu,temp)
         call FmatmulRcrc(o(i)%nonZeroF,o(i)%mu,o(i)%F,tempp)
         !NON OPTIMIZED
         !g%rdot =  g%rdot + (im/hb) * ( matmul(o(i)%F,o(i)%mu) - matmul(o(i)%mu,o(i)%F) ) 
         
         !normal oscillator after TOB
         if(time >= TOB) then
            g%rdot =  g%rdot + (im/hb) * ( temp - tempp )
         !turning on the oscillator linearly
         else
            g%rdot =  g%rdot + (im/hb) * ( temp - tempp )*(time)*(1.0_dp/TOB) 
         endif         
      enddo
   endif

   !OB!!!!!!!!!!!
   if(openboundaries > 0) then
      !OPTIMIZED
      call FmatmulL(g%nonZeroSigma,g%SigmaPlus,g%r1,temp)
      call FmatmulR(g%nonZeroSigma,g%r1,-(g%SigmaPlus),tempp)
      OBbuffer = temp - tempp
      !NON OPTIMIZED
      !OBbuffer = matmul(g%SigmaPlus,g%r1) - matmul(g%r1,(-g%SigmaPlus)) 

      !normal OB after TOB
      if(time >= TOB) then
         g%rdot =  g%rdot - (im/hb)*(OBbuffer + g%inj) 
      !turning on he OB linearly
      else
         g%rdot =  g%rdot - (im/hb)*(OBbuffer + g%inj)*(time)*(1.0_dp/TOB) 
      endif

   endif

end subroutine

!-------------

subroutine Npunto(g,ol)

   use modvar
   implicit none

   type(global), INTENT(IN) :: g
   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL, it will be a vector of dimension 1
   complex(dp) :: buf1(nlev,nlev),buf2(nlev,nlev),temp(nlev,nlev)
   complex(dp) :: s1,s2
   integer :: i
   complex(dp) :: tr1,tr2,tr3,alpha,beta
   
   !Fs part  
   !Partially OPTIMIZED
   temp = - ol%FsRho + ol%RhoFs
   call FmatmulLrcc(ol%nonZeroF,ol%F,temp,buf1)
   !NON OPTIMIZED
   !buf1 = -(matmul(ol%Fs,g%r1) - matmul(g%r1,ol%Fs))
   !buf1 = matmul(ol%F,buf1)
   
   s1 = 0.0_dp
   do i=1,nlev
      s1 = s1 + buf1(i,i)
   enddo
   s1 = (1.0_dp/(im* ol%mass * ol%om *hb))*(ol%N1 + 0.5_dp) * s1 

   
   !Fc part 
   !Partially OPTIMIZED
   temp = ol%FcRho + ol%RhoFc 
   call FmatmulLrcc(ol%nonZeroF,ol%F,temp,buf2) 
   !NON OPTIMIZED
   !buf2 = (matmul(ol%Fc,g%r1) + matmul(g%r1,ol%Fc))
   !buf2 = matmul(ol%F,buf2)
   
   s2 = 0.0_dp
   do i=1,nlev
      s2 = s2 + buf2(i,i)
   enddo
   s2 = (1.0_dp/(2.0_dp* ol%mass * ol%om *hb)) * s2
   
   ol%Ndot = s1 + s2

   !the HF approximation terms are added, they impose fermionic statistics for the electrons
   if(hf .ne. 0) then
      !ignore this term for now        
      if(hf == 1) then
         call FmatmulLrcc(ol%nonZeroF,ol%F,g%r1,buf1)      
      endif

      if(lapackMatmul == 1) then
         alpha = 1.0_dp
         beta = 0.0_dp
         call zgemm('N','N',nlev,nlev,nlev,alpha,g%r1,nlev,ol%FcRho,nlev,beta,temp,nlev) 
      else
         temp = matmul(g%r1,ol%FcRho)
      endif
      call FmatmulLrcc(ol%nonZeroF,ol%F,temp,buf2)

      tr1 = 0.0_dp
      tr2 = 0.0_dp
      tr3 = 0.0_dp
      do i=1,nlev
         !ignore this term for now  
         if(hf == 1) then
            tr1 = tr1 + ol%FcRho(i,i)
            tr2 = tr2 + buf1(i,i)
         endif
         tr3 = tr3 + buf2(i,i)
      enddo
      
      s1 = (1.0_dp/(ol%mass * ol%om *hb)) * tr1 * tr2
      s2 = -(1.0_dp/(ol%mass * ol%om *hb)) * tr3
      ol%Ndot = ol%Ndot + s1 + s2

   endif
   
   !SPIN DEGENERACY for all the terms since they are traced over all the electrons
   ol%Ndot = ol%Ndot * 2.0_dp

end subroutine

!-------------

subroutine mu(g,ol)

   use modvar
   implicit none

   type(global), INTENT(IN) :: g
   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL
   complex(dp) :: buf1(nlev,nlev),buf2(nlev,nlev)
   complex(dp) :: traccia,alpha,beta
   integer :: i
   
   buf1 = ol%FcRho - ol%RhoFc
   buf1 = (im/(ol%mass * ol%om ))*(ol%N1 + 0.5_dp)*buf1
   
   buf2 = ol%FsRho + ol%RhoFs
   buf2 = -(1.0_dp/(2.0_dp* ol%mass * ol%om ))*buf2

   ol%mu = buf1 + buf2  

   !the HF approximation terms are added, they impose fermionic statistics for the electrons
   if(hf .ne. 0) then
      buf1 = 0.0_dp
      !ignore this term for now
      if(hf == 1) then     
         traccia = 0.0_dp      
         do i=1,nlev
            traccia = traccia + ol%FsRho(i,i)
         enddo
         buf1 = -(1.0_dp/(ol%mass * ol%om )) * traccia * g%r1
      endif

      if(lapackMatmul == 1) then
         alpha = 1.0_dp
         beta = 0.0_dp
         call zgemm('N','N',nlev,nlev,nlev,alpha,g%r1,nlev,ol%FsRho,nlev,beta,buf2,nlev) 
      else
         buf2 = matmul(g%r1,ol%FsRho)
      endif
      buf2 = (1.0_dp/(ol%mass * ol%om )) * buf2

      ol%mu = ol%mu + buf1 + buf2      
   endif
   
end subroutine

!-------------

subroutine integralF(g,ol,time)
   
   use modvar
   implicit none

   type(global), INTENT(IN) :: g
   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL
   integer i,j
   complex(dp) :: term1,term2
   complex(dp), dimension(nlev,nlev) :: Fijc,Fijs
   real(dp) :: time
   
   !LAPACK
   complex(dp) :: alpha,beta
   complex(dp), dimension(nlev,nlev) :: uno,due !temporary matrices     

   !CAREFUL at the order of i,j and j,i
   do i=1,nlev
      do j = 1,nlev        
         term1 = (1.0_dp - exp(im*( ol%omegameno(j,i) )*time ) )/ &
                 ( ol%omegameno(j,i) )
         term2 = (1.0_dp - exp(im*( ol%omegapiu(j,i) )*time ) )/ &
                 ( ol%omegapiu(j,i) )   
         Fijs(i,j) =  0.5_dp * ol%Fij(i,j) * ( term1 - term2 ) 
         Fijc(i,j) =  im * 0.5_dp * ol%Fij(i,j) * ( term1 + term2 ) 
      enddo
   enddo
    
   !LAPACK
   if(lapackMatmul == 1) then
      alpha = 1.0_dp
      beta = 0.0_dp
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%Hprojections,nlev,Fijs,nlev,beta,uno,nlev)          
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%Hprojections,nlev,Fijc,nlev,beta,due,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,uno,nlev,g%HprojectionsTC,nlev,beta,ol%Fs,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,due,nlev,g%HprojectionsTC,nlev,beta,ol%Fc,nlev)  
      !key improvement
      !I move the computation of the products of Fc/Fs wirth rho here, so that I don't compute twice in mu and Npunto
      !the only downside is a slightly higher impact on memory
      call zgemm('N','N',nlev,nlev,nlev,alpha,ol%Fc,nlev,g%r1,nlev,beta,ol%FcRho,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,ol%Fs,nlev,g%r1,nlev,beta,ol%FsRho,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%r1,nlev,ol%Fc,nlev,beta,ol%RhoFc,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%r1,nlev,ol%Fs,nlev,beta,ol%RhoFs,nlev)    
      
      !DEBUG
      !Fijs = matmul(g%Hprojections,Fijs)
      !call checkSame(Fijs,uno,time)       
      
   else   !normal matrix multiplication  
      Fijs = matmul(g%Hprojections,Fijs)
      Fijc = matmul(g%Hprojections,Fijc)
      ol%Fs = matmul(Fijs,g%HprojectionsTC)
      ol%Fc = matmul(Fijc,g%HprojectionsTC)
      !key improvement
      ol%FcRho = matmul(ol%Fc,g%r1)
      ol%FsRho = matmul(ol%Fs,g%r1)
      ol%RhoFc = matmul(g%r1,ol%Fc)
      ol%RhoFs = matmul(g%r1,ol%Fs)
   endif
   
end subroutine

!-------------

subroutine integralF2(g,ol,time)
   
   use modvar
   implicit none

   type(global), INTENT(IN) :: g
   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL
   integer i,j
   complex(dp) :: terms,termc,temps,tempc
   complex(dp) :: compesp
   complex(dp), dimension(nlev,nlev) :: Fijc,Fijs
   real(dp) :: time
   
   !LAPACK
   complex(dp) :: alpha,beta
   complex(dp), dimension(nlev,nlev) :: uno,due !temporary matrices     

   !computation of Fs/Fc
   !!!!!!!!!!!!!!!CAREFUL at the order of i,j and j,i
   do i=1,nlev
      do j = 1,nlev
         terms = ol%om * cos(time * ol%om) + (g%epsnum(j,i) * sin(time * ol%om))
         termc = ol%om * sin(time * ol%om) - (g%epsnum(j,i) * cos(time * ol%om))
         !terms = ol%om * cos(time * ol%om) + (g%epsnum(i,j) * sin(time * ol%om))
         !termc = ol%om * sin(time * ol%om) - (g%epsnum(i,j) * cos(time * ol%om))

         temps = -ol%om + (exp(-time * g%epsnum(j,i)) * terms)
         tempc = g%epsnum(j,i) + (exp(-time * g%epsnum(j,i)) * termc)
         !temps = -ol%om + (exp(-time * g%epsnum(i,j)) * terms)
         !tempc = g%epsnum(i,j) + (exp(-time * g%epsnum(i,j)) * termc)

         
         Fijs(i,j) =  (ol%Fij(i,j) * temps) / ol%epsden(j,i)
         Fijc(i,j) =  (ol%Fij(i,j) * tempc) / ol%epsden(j,i)
      enddo
   enddo

   !matrices that will be used more than once per timestep    
   !LAPACK
   if(lapackMatmul == 1) then
      alpha = 1.0_dp
      beta = 0.0_dp
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%Hprojections,nlev,Fijs,nlev,beta,uno,nlev)          
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%Hprojections,nlev,Fijc,nlev,beta,due,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,uno,nlev,g%HprojectionsTC,nlev,beta,ol%Fs,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,due,nlev,g%HprojectionsTC,nlev,beta,ol%Fc,nlev)  
      !key improvement
      !I move the computation of the products of Fc/Fs wirth rho here, so that I don't compute twice in mu and Npunto
      !the only downside is a slightly higher impact on memory
      call zgemm('N','N',nlev,nlev,nlev,alpha,ol%Fc,nlev,g%r1,nlev,beta,ol%FcRho,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,ol%Fs,nlev,g%r1,nlev,beta,ol%FsRho,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%r1,nlev,ol%Fc,nlev,beta,ol%RhoFc,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%r1,nlev,ol%Fs,nlev,beta,ol%RhoFs,nlev)       
      
   else   !normal matrix multiplication  
      Fijs = matmul(g%Hprojections,Fijs)
      Fijc = matmul(g%Hprojections,Fijc)
      ol%Fs = matmul(Fijs,g%HprojectionsTC)
      ol%Fc = matmul(Fijc,g%HprojectionsTC)
      !key improvement
      ol%FcRho = matmul(ol%Fc,g%r1)
      ol%FsRho = matmul(ol%Fs,g%r1)
      ol%RhoFc = matmul(g%r1,ol%Fc)
      ol%RhoFs = matmul(g%r1,ol%Fs)
   endif
   
end subroutine

!-------------

subroutine Fcsmul(g,ol)
   
   use modvar
   implicit none

   type(global), INTENT(IN) :: g
   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL

   complex(dp) :: alpha,beta
   complex(dp), dimension(nlev,nlev) :: uno,due !temporary matrices    

   !matrices that will be used more than once per timestep    
   if(lapackMatmul == 1) then  !LAPACK
      alpha = 1.0_dp
      beta = 0.0_dp
      !key improvement
      !I move the computation of the products of Fc/Fs with rho here, so that I don't compute twice in mu and Npunto
      !the only downside is a slightly higher impact on memory
      call zgemm('N','N',nlev,nlev,nlev,alpha,ol%Fc1,nlev,g%r1,nlev,beta,ol%FcRho,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,ol%Fs1,nlev,g%r1,nlev,beta,ol%FsRho,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%r1,nlev,ol%Fc1,nlev,beta,ol%RhoFc,nlev)
      call zgemm('N','N',nlev,nlev,nlev,alpha,g%r1,nlev,ol%Fs1,nlev,beta,ol%RhoFs,nlev)            
   else   !normal matrix multiplication  
      ol%FcRho = matmul(ol%Fc1,g%r1)
      ol%FsRho = matmul(ol%Fs1,g%r1)
      ol%RhoFc = matmul(g%r1,ol%Fc1)
      ol%RhoFs = matmul(g%r1,ol%Fs1)
   endif

end subroutine

!----------------------
!efficient matrix multiplications when one matrix is sparse
!----------------------

!-------------!efficient matmul Left CC->C

subroutine FmatmulL(noz,sparse,gen,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the left element is F or similar

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(i,j)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Left RC->C

subroutine FmatmulLrcc(noz,sparse,gen,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the left element is F or similar

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:,:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(i,j)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CC->C

subroutine FmatmulR(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right RR->R

subroutine FmatmulRrrr(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   real(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CR->C

subroutine FmatmulRcrc(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   real(dp), dimension(:,:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!-------------!sparse matrix storage of elements and their position

subroutine findnonZero(matrix,label,numel)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(OUT) :: label
   real(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: numel
   integer i,j,n,q,elementi
   
   n=1
   do i=1,nlev
      do j=1,nlev
         if(matrix(i,j) .ne. 0.0_dp) then
            label(n,1) = i
            label(n,2) = j
            n = n+1
            !write(*,*) i,j
         endif
      enddo
   enddo

   numel = n-1
   
end subroutine

!-------------!sparse matrix storage of elements and their position, COMPLEX input

subroutine findnonZeroC(matrix,label,numel)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(OUT) :: label
   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: numel
   integer i,j,n,q,elementi
   
   n=1
   do i=1,nlev
      do j=1,nlev
         if(abs(matrix(i,j)) .ne. 0.0_dp) then
            label(n,1) = i
            label(n,2) = j
            n = n+1
            !write(*,*) i,j
         endif
      enddo
   enddo

   numel = n-1
   
end subroutine

!----------------------
!Check routines
!----------------------

!-------------!checks the hermiticity of a matrix

subroutine checkHermite(matrix,flag)
   use modvar
   implicit none

   !routine for checking the hermiticity of matrices

   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: flag
   integer i,j,n,q,elementi
   
   flag = 0
   do i=1,nlev
      do j=1,nlev
         if(abs(matrix(i,j) - conjg(matrix(j,i))) .ge. zero ) then
            flag = flag + 1
            if(abs(matrix(i,j) - conjg(matrix(j,i))) .ge. 1.0E-3_dp ) then
               write(42,*) i,j,matrix(i,j),conjg(matrix(j,i))
            endif
         endif
      enddo
   enddo
  
end subroutine

!-------------!checks if 2 matrices are identical
!maybe it's better I put some flexibility like a +- epsilon, for including the machin error

subroutine checkSame(matrix,matrix2,time,flag)
   use modvar
   implicit none

   !routine for checking if 2 complex matrices are the same
   !it's useful if I use 2 different methods to get them

   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   complex(dp), dimension(:,:), INTENT(IN) :: matrix2
   integer, INTENT(OUT) :: flag
   real(dp), INTENT(IN)  :: time
   integer i,j,n,q,elementi
   
   flag = 0
   do i=1,nlev
      do j=1,nlev
         if( abs(matrix(i,j) - matrix2(i,j)) .ge. zero ) then
            flag = flag + 1
            !write(42,*) i,j,matrix(i,j),matrix2(i,j)
         endif
      enddo
   enddo

   !if(flag .ne. 0) then
   !   write(*,*) 'At time',time,'the matrices do not coincide, check debug.dat'
   !   stop
   !endif
  
end subroutine
    
end program elph
