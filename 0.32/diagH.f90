program diagH

   !this program builds a hamiltonian for a TB system, diagonalizes it
   !and computes eigenvalues and eigenstates. It will mainly be used for getting the GS of a system, 
   !which, in the initial basis, is the first column of the outcoming H matrix

   !The file ham contains the hamiltonian
   !the file rhozero the DM with the first nlev/2 energy eigenstates of H as generating state

   !IMPORTANT this file has the injection terms calculated by summing each four of the terms,
   !it has the same result as Eunan

   use modvar
   implicit none

   !LAPACK VARIABLES   
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   integer :: info

   !SYSTEM VARIABLES (use allocatable for avoiding problems with openMP)
   complex(dp), allocatable :: H(:,:),Hold(:,:),effe(:,:),effeOLD(:,:) !hamiltonian of the system
   real(dp), allocatable :: eigv(:)    !ordered eigevalue vector
   complex(dp), allocatable :: eigvaluesEffe(:)    !ordered eigevalue vector
   complex(dp), allocatable :: eigvectorsEffeL(:,:),eigvectorsEffeR(:,:) !Left and right eigenvectors of effe
   complex(dp), allocatable :: rhozero(:,:)
   complex(dp), allocatable :: vediamo(:,:),G(:,:),Ginv(:,:)   
   integer :: i,j,s,sp,n
   real(dp) :: buffer,maxspace,avspace,E 
   real(dp) :: coup
   real(dp), allocatable :: inter(:,:)
  
   !-----------------------------
   !BUILDING THE WIRE
   
   !Hamiltonian
   allocate(H(nlev,nlev))
   allocate(Hold(nlev,nlev))
   call buildH(H)
   Hold = H
    
   !for the OB, operatorial part of G-. It has to be diagonalised
   allocate(effe(nlev,nlev))
   allocate(effeOLD(nlev,nlev))   
   call buildF(effe,Hold)
   effeOLD = effe 
   
   !building the coupling matrix with the oscillators (if present)
   if(nho .ne. 0) then
      open(62, file='coupHO',form='unformatted')   
      open(33, file='input.dat')
      allocate(inter(nlev,nlev))
      do i=1,nho
         read(33,*) coup      !it reads only the elements in the first column of input, that's all we need here
         call buildInteraction(inter,coup,i)
         write(62) inter
      enddo
   endif
   close(62)

   !-----------------------------
   !LAPACK diagonalisations and storing the resulting eigenvectors/eigenvalues
   
   !diagonalize H and effe
   allocate(work(lwork))
   allocate(rwork(lrwork))
   allocate(iwork(liwork))
   allocate(eigv(nlev))   
   allocate(eigvaluesEffe(nlev))
   allocate(eigvectorsEffeL(nlev,nlev))
   allocate(eigvectorsEffeR(nlev,nlev))
   call zheevd('V','U',nlev,H,nlev,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
   !write(*,*) info
   call zgeev('V','V',nlev,effe,nlev,eigvaluesEffe,eigvectorsEffeL,nlev,eigvectorsEffeR,nlev, &
               work,LWORK,rwork,info)
   !write(*,*) info
   deallocate(work)
   deallocate(rwork)
   deallocate(iwork)
   
   
   open(44, file='ham',form='unformatted')
   !H is real, I could store it as real, but it is used in Lapack complex multiplications, better to use it complex
   write(44) real(Hold,dp),H,eigv
   close(44)
   deallocate(Hold)
   deallocate(effe)

   !----------------------------- 
   !VERY IMPORTANT renormalization
   !the default normalization is NOT what I need to be, I want the identity to appear when i multply 
   !a left and a right vector with the same index. Here I enforce this
   !i is the label of the eigenvectors, it corresponds to the i eigenvalue
   do i=1,nlev   
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      eigvectorsEffeL(:,i) = eigvectorsEffeL(:,i) / dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      eigvectorsEffeR(:,i) = eigvectorsEffeR(:,i) / dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*)
   enddo

   !computation of the average and minimum energy spacing in the system (the central region is 
   !smaller than he leads, so effectively this means in the leads)
   avspace = 0.0_dp
   maxspace = 0.0_dp
   do i=1,nlev-1
      buffer = eigv(i+1)-eigv(i)
      if(abs(buffer) > maxspace) then
         maxspace = buffer
      endif
      avspace = avspace + buffer
   enddo
   avspace = avspace/real(nlev-1,dp)
   write(*,*) 'The maximum energy spacing in S is', maxspace,'eV'
   write(*,*) 'The average energy spacing in S is', avspace,'eV'
   deallocate(eigv)   

   !building the corresponding DM, rhozero
   !VERY IMPORTANT, it's the sum of nlev/2 DM of the filled band. It's got nlev/2 as a trace!!!
   open(45, file='rhozero',form='unformatted')
   allocate(rhozero(nlev,nlev))   
   rhozero = 0.0_dp   
   do n=1,nlev/2
      do i = 1,nlev
         do j = 1,nlev
            rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )
         enddo
      enddo
   enddo
   !if the system has an odd number of electrons, like ATOM, I add "half" an electron more on the next excited state
   if(mod(NC,2) == 1) then   
      write(*,*) 'ok funge'
      n = nlev/2 + 1
      do i = 1,nlev
         do j = 1,nlev
            rhozero(i,j) = rhozero(i,j) + 0.5_dp * ( conjg(H(j,n)) * H(i,n) )
         enddo
      enddo
   endif
   write(45) rhozero
   close(45)
   deallocate(rhozero)
   deallocate(H)
   
   !check if the wire is appropriate for the geometry chosen
   !modify it with other checks on gamma and other appropriate parameters
   !e.g. if gamma is too small, print warning
   if(geomwire == 1) then
      !DIMER, NC must be even -O-0~0-O-
      if(mod(NC,2).ne.0) then
         write(*,*) 'Unappropriate NC for the geometry chosen'
         STOP
      endif
   elseif(geomwire == 2) then
      !ATOM, NC must be odd -O-O~0~O-O-
      if(mod(NC,2).ne.1) then
         write(*,*) 'Unappropriate NC for the geometry chosen'
         STOP
      endif   
   endif

   !----------------------------- 
   !OB injection term
   if(openboundaries .ne. 0) then
      call injection(eigvectorsEffeL,eigvectorsEffeR,eigvaluesEffe)
   endif

   
!-------------------END MAIN PROGRAM-------------------------


contains

!----------------------
!GEOMETRY of the WIRE ROUTINES
!----------------------
!change these if you want to add another configuration of the 1D wire
!add another case for the parameter geomwire

subroutine buildH(H)
   
   !chain of TB orbitals with the same hopping, without any imaginary term
   !it needs explicit modifications if we want the central region to be any different
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: H
   integer :: i,j

   H = 0.0_dp

   do i=1,nlev
      H(i,i) = onsite
   enddo

   !couplings chain (the wrong terms will be corrected below)
   do i = 1,nlev-1
      H(i,i+1) = tchain
   enddo
   do i = 2,nlev
      H(i,i-1) = tchain
   enddo

   if(geomwire == 1) then
      !DIMER
      !couplings dimer
      H(oscL,oscR) = tdimer
      H(oscR,oscL) = tdimer   
      
      !couplings chain-dimer  
      H(oscL-1,oscL) = tcd
      H(oscL,oscL-1) = tcd
      H(oscR,oscR+1) = tcd
      H(oscR+1,oscR) = tcd
   elseif(geomwire == 2) then
      !ATOM: SINGLE ATOM OSCILLATING COUPLED THE SAME  
      !do nothing: H is a perfect chain
      H(atom,atom) = onsiteAtom
   elseif(geomwire == 3) then
      !STM, perfect wire with a weaker bond in the middle
      H(leftend,leftend + 1) = middle
      H(leftend + 1,leftend) = middle
   elseif(geomwire == 4) then
      !EINSTEIN, several Einstein oscillators every 2 TB sites
      !for now, perfect chain
   endif   
   
   !-----------------------
   !ATOM: SINGLE ATOM OSCILLATING COUPLED DIFFERENTLY
   !H(atom,atom+1) = abs(tchain)/acar !positive
   !H(atom+1,atom) = abs(tchain)/acar
   !H(atom,atom-1) = -abs(tchain)/acar !negative
   !H(atom-1,atom) = -abs(tchain)/acar

end subroutine buildH

subroutine buildInteraction(inter,coup,j)
   
   use modvar
   implicit none

   real(dp), dimension(:,:), INTENT(OUT) :: inter
   real(dp), INTENT(IN) :: coup
   integer, INTENT(IN) :: j

   inter = 0.0_dp
   
   if(geomwire == 1) then
      !DIMER
      inter(oscL,oscR) = coup
      inter(oscR,oscL) = coup
   elseif(geomwire == 2) then      
      !ATOM
      !Tchavdar's suggestion, same coupling but opposite signs for the two sides of the moving atom -O-O~0~O-O-
      !inter(atom,atom + 1) = abs(tchain)/acar !positive 
      !inter(atom + 1,atom) = abs(tchain)/acar 
      !inter(atom,atom - 1) = - abs(tchain)/acar !negative
      !inter(atom - 1,atom) = - abs(tchain)/acar

      inter(atom,atom + 1) = coup !positive 
      inter(atom + 1,atom) = coup 
      inter(atom,atom - 1) = - coup !negative
      inter(atom - 1,atom) = - coup  
   elseif(geomwire == 3) then 
      !STM
      inter(leftend,leftend + 1) = coup
      inter(leftend + 1,leftend) = coup
   elseif(geomwire == 4) then 
      !EINSTEIN
      !several oscillators, the i dependence of this routine here is essential
      inter(Lfirst + (i-1)*2, Lfirst + (i-1)*2 +1) = coup
      inter(Lfirst + (i-1)*2 +1, Lfirst + (i-1)*2) = coup
      !write(*,*) i,Lfirst + (i-1)*2
   endif

end subroutine buildInteraction

!----------------------
!OB ROUTINES
!----------------------

subroutine buildF(effe,H)

!this is the operatorial part in G- = H_S0 + Sigma- + i I_s Delta

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: effe
   complex(dp), dimension(:,:), INTENT(IN) :: H
   integer :: i,j

   effe = H
   if(openboundaries == 1) then 
      do i = 1, nlev
         effe(i,i) = effe(i,i) + im*del
      enddo

      do i= 1,NL
         effe(i,i) = effe(i,i) + im*0.5_dp*gam
      enddo

      do i= NL+NC+1,nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gam
      enddo
   elseif(openboundaries == 2) then
      do i = 1, nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gam
      enddo
   endif

end subroutine buildF

subroutine injection(evecL,evecR,lambda)

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: evecL,evecR !R and L eigenvectors of effe
   complex(dp), dimension(:), INTENT(IN) :: lambda        !the eigenvalues of effe
   
   complex(dp), allocatable :: inj(:,:),ccinj(:,:) 
   complex(dp), allocatable :: unoL(:),dueL(:) !buffer terms to speed up the calculation
   complex(dp), allocatable :: unoR(:),dueR(:)
   real(dp), allocatable :: lambdaRe(:),lambdaIm(:)
   integer :: i,L,R,s,sp,flag
   
   !evaluation of the buffers
   allocate(lambdaRe(nlev))
   allocate(lambdaIm(nlev))
   allocate(unoL(nlev))
   allocate(unoR(nlev))
   allocate(dueL(nlev))
   allocate(dueR(nlev))
   lambdaRe = real(lambda,dp)
   lambdaIm = aimag(lambda)
   do i = 1,nlev
      unoL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
      unoR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
   enddo
   deallocate(lambdaRe)
   deallocate(lambdaIm)

   !evaluating the matrix elements s,s' of the injection term
   open(46, file='inj',form='unformatted')
   allocate(inj(nlev,nlev))
   inj = 0.0_dp

   do sp = 1,nlev
      do L=1,NL
         do i=1,nlev                      
            inj(L,sp) = inj(L,sp) + ( evecR(L,i) * conjg(evecL(sp,i)) ) * ( unoL(i) + dueL(i) )
         enddo              
      enddo
   enddo

   do sp = 1,nlev
      do R= NL+NC+1,nlev
         do i=1,nlev                      
            inj(R,sp) = inj(R,sp) + ( evecR(R,i) * conjg(evecL(sp,i)) ) * ( unoR(i) + dueR(i) )
         enddo              
      enddo
   enddo
   deallocate(unoL)
   deallocate(unoR)
   deallocate(dueL)
   deallocate(dueR)

   allocate(ccinj(nlev,nlev))
   ccinj = conjg(transpose(inj))
   inj = inj - ccinj
   inj = inj * (im*gam) * (1.0_dp/(2.0_dp*pi*im))

   write(46) inj
   close(46)

end subroutine injection

end program diagH
