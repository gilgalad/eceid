code ELPH, Release 0.0, 10/04/2014

This code can compute the steady state current in a 1D Tight Binding wire with an arbitrary set of coupling between the sites.
By using the open boundary technique, it can estimate the asymptotic steady state current through a time dependent process.
It can compare it to the exact one for finite wires, obtained with an energy integral of the system's Green's Functions. 
For the specific case of a dimer embedded in the wire, it can also give the exact Landauer current for an infinite wire.

A new feature yet to be tested properly is the possible inclusion of phonons that interact in real time with the electronic current.
The code is parallelizable with OpenMP and , since it has an MD structure, it has an intrinsic limitation to scalability.
It scales very well for a number of processor equal to the number of oscillators (never more cores than oscillators!).

Guide to the components:

-  Release_notes.txt: this file.

-  Makefile: to compile the program and its components. The top bit uses ifort and mkl, generally faster. The botton bit gfortran with different option. 
   The -O0 for debugging and profiling, then the normal and the one without openmp for long wires (NL>500) and without phonons where memory is an issue.

-  modvar.f90: it contains all the common parameters that the other programs need, from the geometry of the wire, to the switches for different options.
   It also defines structures (that are like objects, sets of variables) that are either global or bound to a single oscillator.
   
-  input.dat: it contains the parameters for the oscillators. Every line pertains to a different oscillator. The column are respectively:
   coupling, initial number of phonons, characteristic energy, mass. It was written like this so that the elph program runs smoothly without any modification for a different number of oscillators.
   Basically, changing nho in modvar lets the program read more lines in input.dat and makes the system work with a different number of oscillators.
   Putting the parameters here makes the program much more dynamical.
   
-  diagH.f90: the initial program that builds the system.In "ham" it writes the hamiltonian, its eigenvectors and eigenvalues.
   It writes to screen the average and maximum energy spacing in the leads. In "rhozero" it writes the initial DM. 
   In "inj" it writes the OB injection matrix. NOTE: It MUST be run after any modification in modvar.f90 because it provides the right set
   of initial conditions and variables to the other programs.

-  steady.f90: depending on the switches in modvar, it computes the exact steady state current for a finite wire or 
   a scan for different voltages so that a I-V curve can be built. It can do the same for the Landauer results, but it depends on the trasmission function that
   is embedded in the program (and was computed with Mathematica). So the Landauer bit works only for a dimer like Eunan's one. To test different geometries
   the program must be modified with a different trasmission to be itnegrated. 

-  elph.f90: the main program. It simulates the time evolution of the electronic DM and the phonon number. It computes also the time dependent current.
   If the phonons are of it just computes the OB current in the wire. It is the program that scales with OpenMP for a different number of phonons.
   Just remember the command export OMP_NUM_THREADS=? before a parallel execution.


