module modvar
   
   implicit none

   !numerical parameters and switches
   integer, parameter :: dp = selected_real_kind(12) !precision for reals and complex numbers   
   integer, parameter :: trick = 100  !Euler trick, every trick step an Euler integration is performed instead of a leapfrog 
   integer, parameter :: openboundaries = 1 !if 0 the OB are off, if 1 they are on, 
                                            !if 2 they are on in the crippled Eunan's version (del will be forced to be gam/2) 
   integer, parameter :: nho = 0  !if 0, no oscillators (large speedup), if not it's the number of independent oscillators
   integer, parameter :: parallel = 0 !If 1 it activates openMP (remember export OMP_NUM_THREADS=?) 
                                      !It is effective only for nho=num_threads
 
   !Wire parameters
   integer, parameter :: NL = 90    !number of sites in the Left lead
   integer, parameter :: NR = NL    !number of sites in the Right lead
   integer, parameter :: NC = 22    !number of sites in the center of the junction, 
                                    !careful that Eunan puts the dimer in the middle so it is equivalent to 20 +2
   integer, parameter :: oscL = NL + NC/2     !position of the LEFT atom in the dimer
   integer, parameter :: oscR = NL + NC/2 + 1    !position of the LEFT atom in the dimer
   integer, parameter :: siteJ = NL+NC-2 !site where the current evaluated (and the relatice +1 site on the right of it)
   integer, parameter :: nlev = NL + NR + NC  !number of sites in the quatum wire

   !LAPACK PARAMETERS
   integer, parameter :: LWORK = 10000000 !MAX integer is 2147483647, but I get errors. It must be at least (2 nlev + nlev**2)
   integer, parameter :: LRWORK=20000000
   integer, parameter :: LIWORK=15000
   
   !UNIVERSAL CONSTANTS
   real(dp), parameter :: hb = 0.658211942_dp  !hbar in eV*fs
   real(dp), parameter :: mp = 104.3968511_dp  !proton mass in eV*fs^2/A^2
   real(dp), parameter :: pi =  3.141592653589793_dp
   complex(dp), parameter :: im = (0.0_dp,1.0_dp) !i
   real(dp), parameter :: quantumConductance = 77.480917 ! muA/eV
   real(dp), parameter :: microampere = 973.6538812 ! muA/eV

   !SIMULATION CONDITIONS
   real(dp), parameter :: dt = 0.001_dp !time step in fs
   real(dp), parameter :: T = 200.000_dp !total simulation time in fs
   real(dp), parameter :: onsite = 0.0_dp !onsite energy for the TB orbitals
   real(dp), parameter :: perfectchain = -1.0_dp !coupling of the sites in the whole wire
   !real(dp), parameter :: tchain = perfectchain !hopping parameter for the atoms in the leads L and R
   !real(dp), parameter :: tcd = perfectchain !hopping parameter for the atoms in the junction between LC and CR
   !real(dp), parameter :: tdimer = perfectchain !hopping parameter for the atoms in the central region C
   real(dp), parameter :: tchain = -3.88_dp !hopping parameter for the atoms in the leads L and R
   real(dp), parameter :: tcd = -0.5_dp !hopping parameter for the atoms in the junction between LC and CR
   real(dp), parameter :: tdimer = -3.88_dp !hopping parameter for the atoms in the central region C   
   
   !The INITIAL CONDITIONS for the oscillators are in input.dat
   !every line corresponds to a different oscillator
   !the listed parameters are coupling, initial N, omega (in eV), mass (adimensional)
   
   !OB parameters
   real(dp), parameter :: Volt = 15.0_dp !difference of potential betwen the left and the right lead
   real(dp), parameter :: gam = 0.35_dp !wide band approximation parameter
   real(dp), parameter :: muL = Volt/2.0_dp !chemical potential in the LEFT lead
   real(dp), parameter :: muR = -Volt/2.0_dp !chemical potential in the RIGHT lead
   real(dp), parameter :: del = 0.0000001_dp !probes decoherence approximation parameter
   real(dp), parameter :: TOB = 5.000_dp !time where the OB are turned on linearly
   real(dp), parameter :: BI = -100000.0_dp !lower boundary of energy integral in the injection term

   !exact conduction for finite wires and Landauer (used ONLY in steady.f90)
   integer, parameter :: scanon = 0  !if 1 it turns on the scan of the current for voltages from dV to Vmax
                                     !if 0 it evaluates one single value of the current for the OB parameters above
   real(dp), parameter :: dV = 0.1_dp  !increment voltage in the scan
   real(dp), parameter :: Vmax = 15.0_dp  !increment voltage in the scan
   integer, parameter :: integralLand = 0  !if 1 it turns on the Landauer integration of the Trasmission for the dimer system   
   real(dp), parameter :: dE = 0.0001_dp
  
   type global
      complex(dp), allocatable :: r0(:,:), r1(:,:), r2(:,:)  !for leapfrog
      complex(dp), allocatable :: rdot(:,:),SigmaPlus(:,:),inj(:,:)       
      real(dp), allocatable :: Hel(:,:)
      real(dp), allocatable :: Hprojections(:,:),HprojectionsTC(:,:)
      real(dp), allocatable :: E(:)
      integer, allocatable :: nonZeroSigma(:,:), nonZeroH(:,:) !storage of labels created to optimize 
                              !the matrix multiplications since F is very sparse
                              !the 1 index runs 1:numNonZero, the second from 1:2 with 1 for rows and 2 for columns
                              !e.g. nonZeroF(5,1) contains the label of the row of the fifth non zero element of F
   end type

!if the oscillators and these variables are independent
   type oscillator
      complex(dp) :: N0, N1, N2, Ndot
      complex(dp), allocatable :: Fc(:,:), Fs(:,:)
      complex(dp), allocatable :: omegameno(:,:), omegapiu(:,:)
      complex(dp), allocatable :: mu(:,:)
      real(dp) :: om,mass,coup
      real(dp), allocatable :: F(:,:)
      complex(dp), allocatable :: Fij(:,:)  
      integer, allocatable :: nonZeroF(:,:)
   end type


end module modvar
