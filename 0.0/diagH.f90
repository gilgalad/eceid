program diagH

   !this program builds a hamiltonian for a TB system, diagonalizes it
   !and computes eigenvalues and eigenstates. It will mainly be used for getting the GS of a system, 
   !which, in the initial basis, is the first column of the outcoming H matrix

   !The file ham contains the hamiltonian
   !the file rhozero the DM with the first nlev/2 energy eigenstates of H as generating state

   !IMPORTANT this file has the injection terms calculated by summing each four of the terms,
   !it has the same result as Eunan

   use modvar
   implicit none

   !LAPACK VARIABLES   
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   integer :: info

   !SYSTEM VARIABLES (use allocatable for avoiding problems with openMP)
   complex(dp), allocatable :: H(:,:),Hold(:,:),effe(:,:),effeOLD(:,:) !hamiltonian of the system
   real(dp), allocatable :: eigv(:)    !ordered eigevalue vector
   complex(dp), allocatable :: eigvaluesEffe(:)    !ordered eigevalue vector
   complex(dp), allocatable :: eigvectorsEffeL(:,:),eigvectorsEffeR(:,:) !Left and right eigenvectors of effe
   complex(dp), allocatable :: rhozero(:,:)
   complex(dp), allocatable :: vediamo(:,:),G(:,:),Ginv(:,:)   
   integer :: i,j,s,sp,n
   real(dp) :: buffer,maxspace,avspace,E  

   open(44, file='ham',form='unformatted')
   open(45, file='rhozero',form='unformatted')

   allocate(H(nlev,nlev))
   allocate(Hold(nlev,nlev))
   call buildH(H)
   Hold = H
   
   allocate(effe(nlev,nlev))
   allocate(effeOLD(nlev,nlev))   
   call buildF(effe,Hold)
   effeOLD = effe 

   !diagonalize H
   allocate(work(lwork))
   allocate(rwork(lrwork))
   allocate(iwork(liwork))
   allocate(eigv(nlev))   
   allocate(eigvaluesEffe(nlev))
   allocate(eigvectorsEffeL(nlev,nlev))
   allocate(eigvectorsEffeR(nlev,nlev))
   call zheevd('V','U',nlev,H,nlev,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
   !write(*,*) info
   call zgeev('V','V',nlev,effe,nlev,eigvaluesEffe,eigvectorsEffeL,nlev,eigvectorsEffeR,nlev, &
               work,LWORK,rwork,info)
   !write(*,*) info
   deallocate(work)
   deallocate(rwork)
   deallocate(iwork)

   write(44) real(Hold,dp),real(H,dp),eigv
   close(44)
   deallocate(Hold)
   deallocate(effe)

   open(56, file='lambda')
   do i=1,nlev
      write(56,*) eigvaluesEffe(i)
   enddo

   !VERY IMPORTANT
   !possibly the default normalization is NOT what I need to be, I want the identity to appear when i multply 
   !a left and a right vector with the same index. Here I enforce this
   !I CHECKED WELL, G^-1 G = I with this normalization, so it is ok
   !i is the label of the eigenvectors, it corresponds to the i eigenvalue
   do i=1,nlev   
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      eigvectorsEffeL(:,i) = eigvectorsEffeL(:,i) / dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      eigvectorsEffeR(:,i) = eigvectorsEffeR(:,i) / dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*)
   enddo

   !computation of the average and minimum energy spacing in the system (the central region is 
   !smaller than he leads, so effectively this means in the leads)
   avspace = 0.0_dp
   maxspace = 0.0_dp
   do i=1,nlev-1
      buffer = eigv(i+1)-eigv(i)
      if(abs(buffer) > maxspace) then
         maxspace = buffer
      endif
      avspace = avspace + buffer
   enddo
   avspace = avspace/real(nlev-1,dp)
   write(*,*) 'The maximum energy spacing in S is', maxspace,'eV'
   write(*,*) 'The average energy spacing in S is', avspace,'eV'
   deallocate(eigv)   

   !building the corresponding DM, rhozero
   !VERY IMPORTANT, it's the sum of nlev/2 DM of the filled band. It's got nlev/2 as a trace!!!
   allocate(rhozero(nlev,nlev))   
   rhozero = 0.0_dp   
   do n=1,nlev/2
      do i = 1,nlev
         do j = 1,nlev
            rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )
         enddo
      enddo
   enddo
   write(45) rhozero
   close(45)
   deallocate(rhozero)
   deallocate(H)

   !VERY IMPORTANT AND COMPUTATIONALLY INTENSIVE ROUTINE
   !comment it out for testing the previous routines
   call injectionbis(eigvectorsEffeL,eigvectorsEffeR,eigvaluesEffe)

   !ROUTINE THAT COMPUTES THE EXACT STEADY STATE SOLUTION FOR THE WIRE


!    !there was a lot of debugging tests in the middle here. 
!    !Look for an older version of this program to dig them
!    !BUILDING G and Ginv and checking that their product is the identity
!    allocate(G(nlev,nlev)) 
!    allocate(Ginv(nlev,nlev)) 
!    allocate(vediamo(nlev,nlev))    
!    E = 0.0_dp  
!    do i = 1,nlev
!       do j = 1,nlev
!          Ginv(i,j) = - effeOLD(i,j)
!       enddo
!    enddo
!    do i = 1,nlev
!       Ginv(i,i) = E + Ginv(i,i)
!    enddo
!    G = 0.0_dp
!    do s = 1,nlev
!       do sp = 1,nlev
!          do i = 1,nlev
!             G(s,sp) = G(s,sp) + ( eigvectorsEffeR(s,i) * conjg(eigvectorsEffeL(sp,i)) )* 1.0_dp/(E - eigvaluesEffe(i))
!             !G(s,sp) = G(s,sp) + ( eigvectorsEffeR(s,i) * eigvectorsEffeL(sp,i)         )* 1.0_dp/(E - eigvaluesEffe(i))
!          enddo         
!       enddo
!    enddo
!    deallocate(eigvectorsEffeR)
!    deallocate(eigvectorsEffeL)
!    deallocate(eigvaluesEffe)   
!    vediamo = matmul(Ginv,G)
!    do i = 1,nlev
!       do j = 1,nlev
!          
!          if( (i==j) .and. (( abs(vediamo(i,j)*conjg(vediamo(i,j))) > 1.0_dp + 1.0E-12 ) &
!                .or. ( abs(vediamo(i,j)*conjg(vediamo(i,j))) < 1.0_dp - 1.0E-12 )) ) then
!             write(*,*) 'DIAGONAL WARNING'
!             write(*,*) i, j, vediamo(i,j)
!          endif
! 
!          if( (i.ne.j) .and. (abs(vediamo(i,j)*conjg(vediamo(i,j))) > 1.0E-25) ) then
!             write(*,*) 'OFF DIAGONAL WARNING'
!             write(*,*) i, j, vediamo(i,j)
!          endif
! 
!       enddo
!    enddo
!    deallocate(G)
!    deallocate(Ginv)
!    deallocate(vediamo)
!    deallocate(effeOLD)


!-------------------END MAIN PROGRAM-------------------------

contains

subroutine buildH(H)
   
   !chain of TB orbitals with the same hopping, without any imaginary term
   !it needs modifications if we want the central region to be any different
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: H
   integer :: i,j

   H = 0.0_dp

   do i=1,nlev
      H(i,i) = onsite
   enddo

   !couplings chain (the wrong terms will be corrected below)
   do i = 1,nlev-1
      H(i,i+1) = tchain
   enddo
   do i = 2,nlev
      H(i,i-1) = tchain
   enddo

   !couplings dimer
   H(oscL,oscR) = tdimer
   H(oscR,oscL) = tdimer   
   
   !couplings chain-dimer  !TEMPORARY, USE OTHER VARIABLES FOR THE DIMER
   H(oscL-1,oscL) = tcd
   H(oscL,oscL-1) = tcd
   H(oscR,oscR+1) = tcd
   H(oscR+1,oscR) = tcd

end subroutine buildH

!----------------------

subroutine buildF(effe,H)

!this is the operatorial part in G- = H_S0 + Sigma- + i I_s Delta

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: effe
   complex(dp), dimension(:,:), INTENT(IN) :: H
   integer :: i,j

   effe = H
   if(openboundaries == 1) then 
      do i = 1, nlev
         effe(i,i) = effe(i,i) + im*del
      enddo

      do i= 1,NL
         effe(i,i) = effe(i,i) + im*0.5_dp*gam
      enddo

      do i= NL+NC+1,nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gam
      enddo
   elseif(openboundaries == 2) then
      do i = 1, nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gam
      enddo
   endif

end subroutine buildF

!----------------------

subroutine injection(evecL,evecR,lambda,inj)

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: evecL,evecR !R and L eigenvectors of effe
   complex(dp), dimension(:), INTENT(IN) :: lambda        !the eigenvalues of effe
   complex(dp), dimension(:,:), INTENT(OUT) :: inj        !matrix to be computed, it will be printed
   complex(dp), dimension(nlev) :: cL, cpL,cR,cpR             !coefficients projection dependent
   integer :: i,L,s,sp
   real(dp), dimension(nlev) :: lambdaRe,lambdaIm
   complex(dp), dimension(nlev) :: unoL,dueL !buffer terms to speed up the calculation
   complex(dp), dimension(nlev) :: unoR,dueR

   !evaluation of the buffers
   lambdaRe = real(lambda,dp)
   lambdaIm = aimag(lambda)
   do i = 1,nlev
      unoL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
      unoR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
   enddo

   !evaluating the matrix elements s,s' of the injection term
   open(46, file='inj',form='unformatted')
   inj = 0.0_dp
   do s = 1,nlev
      do sp=1,nlev
         
         !calculation of c and c', they depend on i, the index of the eigenvalue
         cL = 0.0_dp
         cpL = 0.0_dp
         cR = 0.0_dp
         cpR = 0.0_dp
         do i=1,nlev
            !they are summed over the left and right lead respectively
            !left lead
            do j = 1,NL
               if(s==j) then !term <S|L>
                  cL(i) = cL(i) + ( evecR(j,i) * conjg(evecL(sp,i)) )
               endif
               if(sp==j) then !term <L|S'>
                  cpL(i) = cpL(i) + ( evecL(s,i) * conjg(evecR(j,i)) )
               endif
            enddo
            !right lead
            do j=NL+NC+1,nlev
               if(s==j) then !term <S|R>
                  cR(i) = cR(i) + ( evecR(j,i) * conjg(evecL(sp,i)) )
               endif
               if(sp==j) then !term <R|S'>
                  cpR(i) = cpR(i) + ( evecL(s,i) * conjg(evecR(j,i)) )
               endif
            enddo

            !inj(s,sp) = inj(s,sp) + ( cpL(i)-cL(i) ) * ( unoL(i) ) 
            !inj(s,sp) = inj(s,sp) + ( cpL(i)+cL(i) ) * ( dueL(i) )
            !inj(s,sp) = inj(s,sp) + ( cpR(i)-cR(i) ) * ( unoR(i) )
            !inj(s,sp) = inj(s,sp) + ( cpR(i)+cR(i) ) * ( dueR(i) )

            inj(s,sp) = inj(s,sp) + ( cL(i)-cpL(i) ) * ( unoL(i) ) 
            inj(s,sp) = inj(s,sp) + ( cL(i)+cpL(i) ) * ( dueL(i) )
            inj(s,sp) = inj(s,sp) + ( cR(i)-cpR(i) ) * ( unoR(i) )
            inj(s,sp) = inj(s,sp) + ( cR(i)+cpR(i) ) * ( dueR(i) )

         enddo
         
         inj(s,sp) = inj(s,sp) * (im*gam) !(gam/(2.0_dp*pi)) 
         write(47,*) s,sp,inj(s,sp)

      enddo
   enddo

   !write(*,*) 2,2, inj(2,2)
   !write(*,*) 2,3, inj(2,3)
   !write(*,*) 3,2, inj(3,2)
   !write(*,*) NL,NL-1, inj(NL-1,NL)

   !write(46) inj
   !close(46)

end subroutine injection

!----------------------

subroutine injectionbis(evecL,evecR,lambda)

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: evecL,evecR !R and L eigenvectors of effe
   complex(dp), dimension(:), INTENT(IN) :: lambda        !the eigenvalues of effe
   
   complex(dp), allocatable :: inj(:,:),ccinj(:,:) 
   complex(dp), allocatable :: unoL(:),dueL(:) !buffer terms to speed up the calculation
   complex(dp), allocatable :: unoR(:),dueR(:)
   real(dp), allocatable :: lambdaRe(:),lambdaIm(:)
   integer :: i,L,R,s,sp,flag
   
   !evaluation of the buffers
   allocate(lambdaRe(nlev))
   allocate(lambdaIm(nlev))
   allocate(unoL(nlev))
   allocate(unoR(nlev))
   allocate(dueL(nlev))
   allocate(dueR(nlev))
   lambdaRe = real(lambda,dp)
   lambdaIm = aimag(lambda)
   do i = 1,nlev
      unoL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
      unoR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
   enddo
   deallocate(lambdaRe)
   deallocate(lambdaIm)

   !evaluating the matrix elements s,s' of the injection term
   open(46, file='inj',form='unformatted')
   allocate(inj(nlev,nlev))
   inj = 0.0_dp

   do sp = 1,nlev
      do L=1,NL
         do i=1,nlev                      
            inj(L,sp) = inj(L,sp) + ( evecR(L,i) * conjg(evecL(sp,i)) ) * ( unoL(i) + dueL(i) )
         enddo              
      enddo
   enddo

   do sp = 1,nlev
      do R= NL+NC+1,nlev
         do i=1,nlev                      
            inj(R,sp) = inj(R,sp) + ( evecR(R,i) * conjg(evecL(sp,i)) ) * ( unoR(i) + dueR(i) )
         enddo              
      enddo
   enddo
   deallocate(unoL)
   deallocate(unoR)
   deallocate(dueL)
   deallocate(dueR)

   allocate(ccinj(nlev,nlev))
   ccinj = conjg(transpose(inj))
   inj = inj - ccinj
   inj = inj * (im*gam) * (1.0_dp/(2.0_dp*pi*im))

   write(46) inj
   close(46)

end subroutine injectionbis

end program diagH
