module constx3l
   
   implicit none
   
   integer, parameter :: dp = selected_real_kind(12) 
   integer, parameter :: nlev = 20    !number of electronic levels available, better if it is even now
   integer, parameter :: nex = 10    !number of excitations to consider for oscillator1 and 2, change freely
   integer, parameter :: lmat = nlev*nex    !dimension of a side of the hamiltonian
!the number of oscillators in this program is striclty 1, it needs heavy modifications to increase it
!the number of electronic levels is 3 but it can be increased easily just changing the hamiltonian
   integer, parameter :: start = 0 !if 0, the initial state is a TB state with a certain electronic level fully occupied and N
                                   !if 1, the initial state is an excited state of the unperturbed electronic hamiltonian

   !LAPACK PARAMETERS
   integer, parameter :: LWORK =10000000 !MAX integer is 2147483647, but I get errors
   integer, parameter :: LRWORK=20000000
   integer, parameter :: LIWORK=15000
   
   !UNIVERSAL CONSTANTS
   real(dp), parameter :: hb = 0.658211942_dp  !hbar in eV*fs
   real(dp), parameter :: mp = 104.3968511_dp  !proton mass in eV*fs^2/A^2
   real(dp), parameter :: amu = 103.642753  !atomic mass unit in eV*fs^2/A^2  (1/12 of the mass of carbon)
   complex(dp), parameter :: im = (0.0_dp,1.0_dp) 

   !GEOMETRIES
   integer, parameter :: geo = 1  !if 1, STM a wire with a weak bond tchainw and an oscillator there
                                   !if 2, RING a ring with a weak bond tchainw and an oscillator there
                                   !if 16, MOL2L

   !SIMULATION CONDITIONS   
   real(dp), parameter :: dt = 0.1_dp !time step in fs
   real(dp), parameter :: T = 100.0_dp !total simulation time in fs
   integer, parameter :: nstep = int(T/dt)
   integer, parameter :: middle = int(nlev/2) !site in the middle of the electronic chain
   real(dp), parameter :: coupling = 0.50_dp   !coupling strength between the oscillator1 and the electron
   real(dp), parameter :: eps = 0.000_dp !small imaginary part for F, dissipative term
   real(dp), parameter :: onsiteL = 0.0_dp !onsite energy for the TB orbitals on the LEFT
   real(dp), parameter :: onsiteR = 0.0_dp !onsite energy for the TB orbitals on the RIGHT
   real(dp), parameter :: tchain = -1.0_dp !hopping parameter for the atoms 
   real(dp), parameter :: tchainw = -0.01_dp !weak hopping parameter in the middle of the chain


   !INITIAL CONDITIONS
   integer, parameter :: elle  = 3  !initial electronic level occupied (1:nlev). Depending on start it is either a TB state or a Hel eigenstate
   integer, parameter :: enne1 = 0  !initial value for the number of phonons in oscillator 1 (0:nex-1)
   real(dp), parameter :: omega1 = 0.200_dp/hb !characteristic frequency of the oscillator1 in PHz
   real(dp), parameter :: Mass1 = 0.5_dp*amu !mass of the oscillator1 in mp units  
   real(dp), parameter :: fact1 = sqrt(hb/(2.0_dp*Mass1*omega1)) !factor in front of the ann/creation operator in X for oscillator1

end module constx3l
