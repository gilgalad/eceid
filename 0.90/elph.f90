program elph

   !this program computes the current in a 1D wire with the geometry in the hamiltonian built in diagH.
   !it can simulate phonons that interact in real time with the OB driven current
   !REQUIREMENTS: the execution of diagH

   use modvar
   implicit none
   
   integer :: i,j
   real(dp) :: fake
   type(global) :: g
   type(rhoEvo), allocatable :: rEvo(:) 
   type(oscillator), allocatable :: o(:)
   type(oscillatorEvo), allocatable :: oEvo(:,:) 
 
   allocate(g%Hel(nlev,nlev))
   allocate(g%Hprojections(nlev,nlev))
   allocate(g%HprojectionsTC(nlev,nlev))
   allocate(g%E(nlev))
   allocate(g%inj(nlev,nlev))       !OB injection term for a 0 temperature FD distribution
   allocate(g%rdot(nlev,nlev))

   allocate(rEvo(3))  !NEW! !allocate entire type, DM at 3 times

   if(nho .ne. 0) then
      allocate(o(nho)) 
      open(33, file='input.dat')
      do i=1,nho
         allocate(o(i)%F(nlev,nlev))
         allocate(o(i)%Fstore(nlev,nlev))
         allocate(o(i)%CCdot(nlev,nlev))
         allocate(o(i)%CSdot(nlev,nlev))
         allocate(o(i)%ACdot(nlev,nlev))
         allocate(o(i)%ASdot(nlev,nlev))
         allocate(o(i)%mu(nlev,nlev))
         if(nmodes == 0) then !case of einstein oscillators
            read(33,*) o(i)%coup, o(i)%N, o(i)%om, o(i)%mass
            if( anderson == 1 ) then  !<N> is determined by the temperature of the bath and omega (which is in eV here)
               o(i)%N = kb*anderTbath/(o(i)%om) -0.5_dp  !definition of T in the e mfp, it's a FAKE T
               !o(i)%N = 1.0_dp/( exp( o(i)%om/(kb*anderTbath)  ) - 1.0_dp)  !Bose Einstein N WRONG for e mfp
            endif 
            if(geomwire == 19) then            
               o(i)%N = 1.0_dp/( exp( o(i)%om/(kb*alfredoT)  ) - 1.0_dp)  !Bose Einstein N WRONG for e mfp
            endif
         else !case of normal modes !NEW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if(i==1) then
               read(33,*) o(1)%coup, o(1)%N, o(1)%om, o(1)%mass
            else
               o(i)%N = o(1)%N
               o(i)%coup = o(1)%coup
               o(i)%mass = o(1)%mass
            endif
            o(i)%om = 2.0_dp*o(1)%om*abs(sin(pi*real(i,dp)/real(nmnumber,dp)))   !CAREFUL HERE!!!!!!!!!!!!!! It's been defined also in diagH, check that it's the same
         endif 
         
         o(i)%om = o(i)%om/hb !so that the om is in eV/hb units = fs^-1
         o(i)%mass = o(i)%mass*amu !so that the oscillator mass is in amu units
      end do
      close(33)

      allocate(oEvo(nho,3)) !NEW!!!!!!!
      do i=1,nho
         oEvo(i,2)%N = o(i)%N
      enddo

   else
      !just allocate a fake oscillator so that the subroutines don't crash
      allocate(o(1))
   endif

   open(34, file='phon.dat')
   open(35, file='phon2.dat')   
   open(22, file='phon3.dat') 
   open(21, file='phon4.dat') 
   open(36, file='bondcurrent.dat')
   open(37, file='energiaHO.dat')
   open(41, file='tracerho.dat')
   open(42, file='debug.dat')
   open(43, file='trMu.dat')
   open(85, file='correlation.dat')
   open(66, file='varMu.dat')
   open(67, file='varMunorm.dat') 
   open(73, file='enel.dat')
   open(72, file='enel2.dat')
   open(74, file='entrel.dat')
   open(47, file='entr2el.dat') !TOGLIMI  
   open(75, file='Tel.dat')
   open(57, file='T2el.dat')  !TOGLIMI
   open(76, file='enosc.dat')
   open(77, file='entrosc.dat')
   open(78, file='Tosc.dat')  
   open(79, file='totenergy.dat')
   open(39, file='truetotenergy.dat')
   open(38, file='truenel.dat')
   open(82, file='work.dat')
   open(84, file='heat.dat')
   open(86, file='heatmap.dat') 
   open(87, file='encorr.dat') 
   open(83, file='H0elocCEID.dat')  !H0 electronic occupation
   open(89, file='TBelocCEID.dat')  !TB electronic occupation
   open(90, file='wellOcc.dat')     !occupation of the well, for geometries 12 and 13
   open(91, file='avinvkTel.dat')
   open(92, file='avinvkT2.dat')  
   open(93, file='avinvkTo.dat')
   open(94, file='Toscbos.dat')
   open(95, file='Toscboltz.dat')
   open(30, file='purity.dat')
   open(31, file='HFFrho.dat')
   open(32, file='violmaxcomm.dat')

   call initialize(g,rEvo,o,oEvo)
   call leapfrog(g,rEvo,o,oEvo)


!-------------------END MAIN PROGRAM-------------------------


contains

!----------------------
!setting up the initial parameters and the step 0 of the time evolution
!----------------------
subroutine initialize(g,rEvo,o,oEvo)

   use modvar
   implicit none
   
   integer :: i,j,l,k,numel
   integer, dimension(nlev*nlev,2) :: temp
   complex(dp), dimension(nlev,nlev) :: buf,buf2
   type(global) :: g
   type(rhoEvo), dimension(:) :: rEvo
   type(oscillator), dimension(:) :: o
   type(oscillatorEvo), dimension(:,:) :: oEvo    

   !files where the initial hamiltonian and its GS DM are stored
   open(44, file='ham',form='unformatted')
   open(45, file='rhozero',form='unformatted')
    
   !initialize the electronic hamiltonian
   read(44) g%Hel,g%Hprojections,g%E
   !initialize the electronic density matrix
   read(45) rEvo(2)%rho
   close(44)
   close(45)
   if(openboundaries .ne. 0) then
      !initialize the injection term in the OB
      open(46, file='inj',form='unformatted')
      read(46) g%inj
      close(46)
   endif

   !find nonzero elements of the Hamiltonian and store their position, great optimization for matmul with sparse matrices
   call findnonZero(g%Hel,temp,numel)
   allocate(g%nonZeroH(numel,2))
   g%nonZeroH = temp(1:numel,:)
   !write(*,*) 'H',numel
    
   !it's real BUT I use it as complex because of the lapack function zgemm
   g%HprojectionsTC = conjg(transpose(g%Hprojections)) 
   !g%HprojectionsTC = (transpose(g%Hprojections))
   !print the initial population in the H0 basis
   buf = matmul(g%HprojectionsTC,rEvo(2)%rho)
   buf = matmul(buf,g%Hprojections) 
   do j = 1,nlev
      write(83,*) 0.0_dp,g%E(j), real(buf(j,j),dp)
   enddo 
   !write(83,*) time,g%E(dimen)+0.01_dp, real(small(dimen,dimen),dp)  !fake last row
   write(83,*) 

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!DEBUG
   !buf = matmul(rEvo(2)%rho,g%Hprojections)
   !buf = matmul(g%HprojectionsTC,buf)
   !buf2 = 0.0_dp
   !do i=1,nlev
   !   buf2(i,i) = g%E(i)   
   !enddo
   !buf = matmul(buf2,buf)
   !buf = matmul(buf,g%HprojectionsTC)
   !buf = matmul(g%Hprojections,buf)
   !!
   !buf2 = matmul(g%Hel,rEvo(2)%rho)    
   !do i=1,nlev   
   !   do j=1,nlev 
   !      write(42,*) i,j,buf(i,j)-buf2(i,j)         
   !   enddo
   !enddo
   !STOP
  
   !OB terms initialization. g%SigmaPlus   
   buf = 0.0_dp  !this effectively is SigmaPlus
   do i= 1,NL
      buf(i,i) = -im*gamOB*0.5_dp
   enddo
   do i= NL+NC+1,nlev
      buf(i,i) = -im*gamOB*0.5_dp
   enddo
   call findnonZeroC(buf,temp,numel)
   allocate(g%nonZeroSigma(numel,2))  
   g%nonZeroSigma = temp(1:numel,:)
   allocate(g%SigmaPlus(numel))
   call buildSparseC(g%nonZeroSigma,buf,g%SigmaPlus) !!!!!!!!!!!!!!KEY NEW FUNCTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !NEW, creation of Hplus for the explicit evolution of CC/CS/AC/AS
   if(HplusSwitch == 1) then
      buf = 0.0_dp 
      do i= 1,NL
         buf(i,i) = -im*gamFIN*0.5_dp
      enddo
      do i= NL+NC+1,nlev
         buf(i,i) = -im*gamFIN*0.5_dp
      enddo  
      buf2 = g%Hel + buf  !NOTE, different gamma
   elseif(HplusSwitch == 2) then  
      buf2 = g%Hel
      do i=1,nlev
         buf2(i,i) = buf2(i,i) - im*epsE*0.5_dp
      enddo
   endif
   call findnonZeroC(buf2,temp,numel)  !careful that Hplus is complex, findnonzeroC is needed
   allocate(g%nonZeroHplus(numel,2))
   g%nonZeroHplus = temp(1:numel,:)
   allocate(g%Hplus(numel))
   call buildSparseC(g%nonZeroHplus,buf2,g%Hplus) !!!!!!!!!!!!!!KEY NEW FUNCTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   buf = conjg(transpose(buf2))
   call findnonZeroC(buf,temp,numel)  
   allocate(g%nonZeroHplusTC(numel,2))
   g%nonZeroHplusTC = temp(1:numel,:)
   allocate(g%HplusTC(numel))
   call buildSparseC(g%nonZeroHplusTC,buf,g%HplusTC) !!!!!!!!!!!!!!KEY NEW FUNCTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   if(Telrho == 1) then  !the sendity matrix starts from a certain temperature inTel
      g%avTel = inTel 
      g%avTel2 = inTel 
      g%avkTel = 1.0_dp/(inTel*kb) 
      g%avkT2 = 1.0_dp/(inTel*kb) 
   else  !I start the running averages from T=0K for lack of a better starting point
      g%avTel = 0.0_dp
      g%avTel2 = 0.0_dp 
      g%avkTel = 1.0_dp/(0.01_dp*kb) 
      g%avkT2 = 1.0_dp/(0.01_dp*kb) 
   endif 

   !initialize the phonon variables
   if(nho .ne. 0) then
      open(62, file='coupHO',form='unformatted')

      !educated guess for the average phonon temperature
      g%avkTo = 1.0_dp/((o(1)%N + 0.5_dp)*hb*o(1)%om)

      do i=1,nho       
         !read the interaction built in diagH
         read(62) o(i)%F
         call findnonZeroC(o(i)%F,temp,numel)
         allocate(o(i)%nonZeroF(numel,2)) !labels of the nonzero elements of F
         o(i)%nonZeroF = temp(1:numel,:)
         !write(*,*) 'F',numel
         !call FmatmulL(o(i)%nonZeroF,o(i)%F,rEvo(2)%rho,buf) !CAREFUL about this, for specific rho it may be wrong and break the program
         !~~~~~~~~~~~~~~~~~Brutal BUGFIX. Here buf2 is a generic mat, as generic as possible
         do k=1,nlev
            do l=1,nlev
               buf2(k,l) = 52.0_dp + k*l + k**2 
            enddo
         enddo
         !~~~~~~~~~~~~~~~~~
         call FmatmulL(o(i)%nonZeroF,o(i)%F,buf2,buf) !CAREFUL about this, for specific rho it may be wrong and break the program
         call findnonZeroC(buf,temp,numel)
         if(numel .ne. (3*nlev) ) then
            write(*,*) 'Something is wrong with the initial DM, check!'
            write(*,*) 'Numel should be', 3*nlev,'but for oscillator',i,'is',numel
            allocate(o(i)%nonZeroFrho(numel,2))
            o(i)%nonZeroFrho = temp(1:numel,:)
            do k=1,numel
               write(*,*) k,o(i)%nonZeroFrho(k,1),o(i)%nonZeroFrho(k,2),numel
            enddo
            STOP
         endif
         allocate(o(i)%nonZeroFrho(numel,2))
         o(i)%nonZeroFrho = temp(1:numel,:)
         o(i)%Fstore = o(i)%F   !trick for the linear turning on of F, the real F is stored in Fstore
         if(Tcoup > zero) then
            o(i)%F = 0.0_dp    !if there is a linear turning on of F, at the first time step it is zero
         endif      
         !NEW!!!!!!!!!!!!!!
         oEvo(i,2)%CC = 0.0_dp
         oEvo(i,2)%CS = 0.0_dp
         oEvo(i,2)%AC = 0.0_dp
         oEvo(i,2)%AS = 0.0_dp

         if( fixedN == 0 ) then  !normal evolution for N, isolated system
            call Npunto(o(i),oEvo(i,2)) 
         elseif( fixedN == 1 ) then  !N is fixed, we consider Ndot zero
            o(i)%Ndot = 0.0_dp
         elseif( fixedN == 2 ) then
            if(j==1) then
               o(i)%Ndot = 0.0_dp
            else
               call Npunto(o(i),oEvo(i,2)) 
            endif
         endif
         call ABCDpunto(g, rEvo(2)%rho, o(i), oEvo(i,2) )
         !MIXED, careful
         oEvo(i,1)%N = oEvo(i,2)%N - (dt*o(i)%Ndot)
         oEvo(i,1)%CC = oEvo(i,2)%CC - (dt*o(i)%CCdot)
         oEvo(i,1)%CS = oEvo(i,2)%CS - (dt*o(i)%CSdot)
         oEvo(i,1)%AC = oEvo(i,2)%AC - (dt*o(i)%ACdot)
         oEvo(i,1)%AS = oEvo(i,2)%AS - (dt*o(i)%ASdot)
         oEvo(i,3)%N = oEvo(i,1)%N + (2.0_dp*dt*o(i)%Ndot)
         oEvo(i,3)%CC = oEvo(i,1)%CC + (2.0_dp*dt*o(i)%CCdot)
         oEvo(i,3)%CS = oEvo(i,1)%CS + (2.0_dp*dt*o(i)%CSdot)
         oEvo(i,3)%AC = oEvo(i,1)%AC + (2.0_dp*dt*o(i)%ACdot)
         oEvo(i,3)%AS = oEvo(i,1)%AS + (2.0_dp*dt*o(i)%ASdot)  
         
         !constants for mu, I store them so it's faster
         o(i)%c1mu = (im/(o(i)%mass * o(i)%om ))
         o(i)%c2mu = (1.0_dp/(o(i)%mass * o(i)%om))
         call mu(o(i), oEvo(i,2) )
      end do
   endif
   close(62)

   if(fixedRho == 0) then  !normal evolution for rho
      call rhopunto(g,rEvo(2)%rho,o,0.0_dp)
   elseif(fixedRho == 1) then  !rho is fixed to its initial condition
      g%rdot = 0.0_dp    
   endif  
   
   !backward Euler only for the first step
   rEvo(1)%rho = rEvo(2)%rho - (dt*g%rdot)
   rEvo(3)%rho = rEvo(1)%rho + (g%rdot*dt*2.0_dp)

end subroutine

!----------------------
!time evolution function, it evaluates also the observables
!----------------------
subroutine leapfrog(g,rEvo,o,oEvo)

   use OMP_LIB
   use MKL_SERVICE
   use modvar
   implicit none

   type(global) :: g
   type(rhoEvo), dimension(:), target :: rEvo
   type(oscillator), dimension(:) :: o  !global time local or static oscillator variables
   type(oscillatorEvo), dimension(:,:), target :: oEvo  !time evolving oscillator variables, they mean the physical address, I use pointers to cycle throug them
   type(pointer), allocatable :: p(:,:)    !pointers to the oscillator evolving varibales
   type(pointerDM), allocatable :: pDM(:)  !pointer to the DM
   integer :: i,nstep,j,dummy,num,k
   real(dp) :: time
   complex(dp) :: buffer
   complex(2*dp), dimension(7) :: OLD !I store here the old values for entropy and energy used for the time local temperature 

   !horrible workaround, NUM_THREADS didn't accept an argument 0, even if the cycle was not to get entered
   num = 1
   if(nho .ne. 0) then
      num = nho
      call OMP_SET_NUM_THREADS(nho)
   endif 
   nstep = int(T/dt)

   !NEW!!!!!!! initialize the pointers
   allocate(pDM(3))
   allocate(p(nho,3))
   pDM(2)%DM => rEvo(2)
   pDM(3)%DM => rEvo(3)
   pDM(1)%DM => rEvo(1)
   do j=1,nho
      p(j,2)%ho => oEvo(j,2)
      p(j,3)%ho => oEvo(j,3)
      p(j,1)%ho => oEvo(j,1)
   enddo 
 
   do i=1,nstep
      time = dt*real(i,dp)

      !OBSERVABLES & DEBUG only once every obsfreq steps
      if(mod(i,obsfreq)==0) then
         do j=1,nho
            o(j)%N = p(j,2)%ho%N  !legacy notation, TOGLIMI
         enddo
         call observables(g,pDM(2)%DM%rho,o,time,OLD)
      endif

      !CYCLING PAST, PRESENT AND FUTURE
      !NEW, KEY POINT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if( associated(pDM(2)%DM, rEvo(2)) ) then
         pDM(2)%DM => rEvo(3)
         pDM(3)%DM => rEvo(1)
         pDM(1)%DM => rEvo(2) 
         do j=1,nho
            p(j,2)%ho => oEvo(j,3)
            p(j,3)%ho => oEvo(j,1)
            p(j,1)%ho => oEvo(j,2)              
         enddo             
      elseif( associated(pDM(2)%DM, rEvo(3)) ) then
         pDM(2)%DM => rEvo(1)
         pDM(3)%DM => rEvo(2)
         pDM(1)%DM => rEvo(3)
         do j=1,nho
            p(j,2)%ho => oEvo(j,1)
            p(j,3)%ho => oEvo(j,2)
            p(j,1)%ho => oEvo(j,3)              
         enddo  
      elseif( associated(pDM(2)%DM, rEvo(1)) ) then
         pDM(2)%DM => rEvo(2)
         pDM(3)%DM => rEvo(3)
         pDM(1)%DM => rEvo(1) 
         do j=1,nho
            p(j,2)%ho => oEvo(j,2)
            p(j,3)%ho => oEvo(j,3)
            p(j,1)%ho => oEvo(j,1)              
         enddo 
      endif

      if(nho .ne. 0) then
         !$OMP PARALLEL DEFAULT(shared),PRIVATE(j) NUM_THREADS(num)         
         !$OMP DO SCHEDULE(STATIC)
         do j=1,nho
            !Linear ramp for F
            if(Tcoup > zero) then
               if(time <= Tcoup) then
                  o(j)%F = o(j)%Fstore*(time)*(1.0_dp/Tcoup)  !after Tcoup, it should be just F, CHECK that it is
               endif 
            endif
               
            call ABCDpunto(g, pDM(2)%DM%rho, o(j), p(j,2)%ho ) 

            !IMPORTANT, mu and Npunto use A,B,C,D at the next step, CHECK WELL!!!!!!!!!!!!!!!!!
            !the following quantities are computed for the neext time step. Is mu not necessary for this time step?
            !or maybe not, it should be at the next because it appears in rhopunto for the next
            !comment better leapfrog, this whole section is uniform now            
            if( fixedN == 0 ) then  
               call Npunto(o(j), p(j,2)%ho) 
            elseif( fixedN == 1 ) then
               o(j)%Ndot = 0.0_dp
            elseif( fixedN == 2 ) then
               if(j==1) then
                  o(j)%Ndot = 0.0_dp
               else
                  call Npunto(o(j), p(j,2)%ho)
               endif
            endif
            call mu(o(j), p(j,2)%ho )

            !EVOLUTION of the oscillator variables
            !EULER TRICK
            if(mod(i+1,trick)==0) then         
               p(j,3)%ho%N  = p(j,2)%ho%N + (dt*o(j)%Ndot)
               p(j,3)%ho%CC = p(j,2)%ho%CC + (dt*o(j)%CCdot)
               p(j,3)%ho%CS = p(j,2)%ho%CS + (dt*o(j)%CSdot)
               p(j,3)%ho%AC = p(j,2)%ho%AC + (dt*o(j)%ACdot)
               p(j,3)%ho%AS = p(j,2)%ho%AS + (dt*o(j)%ASdot)
            !NORMAL LEAPFROG
            else      
               p(j,3)%ho%N  = p(j,1)%ho%N + (2.0_dp*dt*o(j)%Ndot)
               p(j,3)%ho%CC = p(j,1)%ho%CC + (2.0_dp*dt*o(j)%CCdot)
               p(j,3)%ho%CS = p(j,1)%ho%CS + (2.0_dp*dt*o(j)%CSdot)
               p(j,3)%ho%AC = p(j,1)%ho%AC + (2.0_dp*dt*o(j)%ACdot)
               p(j,3)%ho%AS = p(j,1)%ho%AS + (2.0_dp*dt*o(j)%ASdot)
            endif   
         enddo
         !$OMP END DO
         !$OMP END PARALLEL
      endif

      if(fixedRho == 0) then  !normal evolution for rho
         call rhopunto(g,pDM(2)%DM%rho,o,time+dt)
      elseif(fixedRho == 1) then  !rho is fixed to its initial condition
         g%rdot = 0.0_dp    
      endif          
      
      !EVOLUTION RHO
      !EULER TRICK
      if(mod(i+1,trick)==0) then         
         pDM(3)%DM%rho = pDM(2)%DM%rho + (dt*g%rdot)
      !NORMAL LEAPFROG
      else
         pDM(3)%DM%rho = pDM(1)%DM%rho + (g%rdot*dt*2.0_dp)
      endif
           
   enddo
   
end subroutine

!----------------------
!EVOLUTION subroutines
!----------------------

subroutine ABCDpunto(g,r,ol,olEvo)

   use modvar
   implicit none

   type(global) :: g
   type(oscillator) :: ol
   type(oscillatorEvo) :: olEvo
   complex(dp), dimension(:,:) :: r

   integer :: i,j
   complex(dp), dimension(nlev,nlev) :: temp,tempp,buf1,buf2
   complex(dp) :: traccia,alpha,beta

   !----
   call SmatmulL(g%nonZeroHplus,g%Hplus,olEvo%CC,temp)
   call SmatmulR(g%nonZeroHplusTC,olEvo%CC,g%HplusTC,tempp)
   !call FmatmulLrcc(g%nonZeroH,g%Hel,ol%CC1,temp)
   !call FmatmulRcrc(g%nonZeroH,ol%CC1,g%Hel,tempp)
   !simil Born-Oppenheimer, the commutator is off
   if (Ehrenfest == 2) then
      ol%CCdot = - (im/hb) * ( temp - tempp ) + ( ol%om * olEvo%CS ) 
   else
      call FmatmulL(ol%nonZeroF,ol%F,r,buf1)
      call FmatmulR(ol%nonZeroF,r,ol%F,buf2)
      ol%CCdot = - (im/hb) * ( temp - tempp ) + ( ol%om * olEvo%CS ) + (olEvo%N + 0.5_dp)*(buf1 - buf2) !check the SIGN of omega, me and tchavdar is different because I use tau-t
                                                                              !maybe split the above into two, I don't know how slow it is to do everything in one line
                                                                              !possibly the EOM are different too, it's better to derive everything from scratch myself!!!!!!!!
                                                                              !For now I simulate EXACTLY what Tchavdar told me, then I rederive
   endif
   
   !----
   call SmatmulL(g%nonZeroHplus,g%Hplus,olEvo%CS,temp)
   call SmatmulR(g%nonZeroHplusTC,olEvo%CS,g%HplusTC,tempp)  
   !call FmatmulLrcc(g%nonZeroH,g%Hel,ol%CS1,temp)
   !call FmatmulRcrc(g%nonZeroH,ol%CS1,g%Hel,tempp)
   ol%CSdot = - (im/hb) * ( temp - tempp ) - ( ol%om * olEvo%CC )

   !----
   call SmatmulL(g%nonZeroHplus,g%Hplus,olEvo%AC,temp)
   call SmatmulR(g%nonZeroHplusTC,olEvo%AC,g%HplusTC,tempp)
   !call FmatmulLrcc(g%nonZeroH,g%Hel,ol%AC1,temp)
   !call FmatmulRcrc(g%nonZeroH,ol%AC1,g%Hel,tempp)
   !simil-Ehrenfest, the anticommutator is off
   if (Ehrenfest >= 1) then
      ol%ACdot = - (im/hb) * ( temp - tempp ) + ( ol%om * olEvo%AS ) 
   else
      ol%ACdot = - (im/hb) * ( temp - tempp ) + ( ol%om * olEvo%AS ) + 0.5_dp*( buf1 + buf2 )
      !!!HARTREE-FOCK!!!
      !the HF approximation terms are added, they impose fermionic statistics for the electrons
      if(hf .ne. 0) then
         temp = 0.0_dp
         !ignore this term for now
         if(hf == 1) then     
            traccia = 0.0_dp      
            do i=1,nlev
               traccia = traccia + buf1(i,i)  !buf1 is computed above and it is F rho 
            enddo
            write(31,*) traccia
            !temp = traccia * r
         endif

         call FmatmulR(ol%nonZeroFrho,r,buf1,tempp) !faster than below
         !alpha = 1.0_dp
         !beta = 0.0_dp
         !call zgemm('N','N',nlev,nlev,nlev,alpha,r,nlev,buf1,nlev,beta,tempp,nlev)  
        
         ol%ACdot = ol%ACdot + temp - tempp 

         if(hf == 3) then
            !!!!!!!!!!!!!!!!!!!!NEW EXTENDED HF!!!!!!!!!!!!!!!!!!
            call FmatmulL(ol%nonZeroF,ol%F,ol%mu,temp)           
            alpha = 1.0_dp/(-(real(olEvo%N,dp) + 0.5_dp)*ol%om*hb/(ol%mass*ol%om*ol%om))

            call FmatmulR(ol%nonZeroFrho,ol%mu,temp,tempp)           
            ol%ACdot = ol%ACdot + (alpha*tempp)
            !slower, it does not use the sparsity of temp
            !beta = 0.0_dp
            !call zgemm('N','N',nlev,nlev,nlev,alpha,ol%mu,nlev,temp,nlev,beta,tempp,nlev) 
         endif     
      endif
   endif

   !---
   call SmatmulL(g%nonZeroHplus,g%Hplus,olEvo%AS,temp)
   call SmatmulR(g%nonZeroHplusTC,olEvo%AS,g%HplusTC,tempp)  
   !call FmatmulLrcc(g%nonZeroH,g%Hel,ol%AS1,temp)
   !call FmatmulRcrc(g%nonZeroH,ol%AS1,g%Hel,tempp)
   ol%ASdot = - (im/hb) * ( temp - tempp ) - ( ol%om * olEvo%AC )

end subroutine

!-------------

subroutine rhopunto(g,r,o,time)

   use modvar
   implicit none

   integer :: i,j
   type(global) :: g
   type(oscillator), dimension(:) :: o
   complex(dp), dimension (:,:) :: r
   complex(dp), dimension(nlev,nlev) :: OBbuffer,temp,tempp
   real(dp) :: time
   
   !it's a collective variable, depends on ALL the oscillators better to parallelize inside rather than outside
   !OPTIMIZED
   call FmatmulLrcc(g%nonZeroH,g%Hel,r,temp)
   call FmatmulRcrc(g%nonZeroH,r,g%Hel,tempp)
   g%rdot = - (im/hb) * ( temp - tempp )

   !NON OPTIMIZED
   !g%rdot = - (im/hb) * ( matmul(g%Hel,r) - matmul(r,g%Hel) )

   !eventually parallelizable for a lot of oscilators, use critical though, it is
   !an atomic udate I think
   if(nho .ne. 0) then
      do i=1,nho
         !OPTIMIZED
         call FmatmulL(o(i)%nonZeroF,o(i)%F,o(i)%mu,temp)
         call FmatmulR(o(i)%nonZeroF,o(i)%mu,o(i)%F,tempp)
         !NON OPTIMIZED
         !g%rdot =  g%rdot + (im/hb) * ( matmul(o(i)%F,o(i)%mu) - matmul(o(i)%mu,o(i)%F) ) 
         
         g%rdot =  g%rdot + (im/hb) * ( temp - tempp )      
      enddo
   endif

   !OB!!!!!!!!!!!
   if(openboundaries > 0) then
      if( (shapeV == 0) .or. ((shapeV==1).and.(time.le.timeV)) ) then  !NEW!!!
         !OPTIMIZED
         call SmatmulL(g%nonZeroSigma,g%SigmaPlus,r,temp)
         call SmatmulR(g%nonZeroSigma,r,-(g%SigmaPlus),tempp)
         OBbuffer = temp - tempp
         !NON OPTIMIZED
         !OBbuffer = matmul(g%SigmaPlus,r) - matmul(r,(-g%SigmaPlus)) 

         !normal OB after TOB
         if(time >= TOB) then
            g%rdot =  g%rdot - (im/hb)*(OBbuffer + g%inj) 
         !turning on he OB linearly
         else
            g%rdot =  g%rdot - (im/hb)*(OBbuffer + g%inj)*(time)*(1.0_dp/TOB) 
         endif
      endif
   endif

end subroutine

!-------------

subroutine Npunto(ol,olEvo)

   use modvar
   implicit none

   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL, it will be a vector of dimension 1
   type(oscillatorEvo) :: olEvo

   complex(dp) :: buf(nlev,nlev)
   complex(dp) :: s1,s2
   integer :: i
   complex(dp) :: tr1,tr2,tr3,alpha,beta
   
   call FmatmulL(ol%nonZeroF,ol%F,olEvo%CS,buf)
   s1 = 0.0_dp
   do i=1,nlev
      s1 = s1 + buf(i,i)
   enddo
   s1 = (im/(ol%mass * ol%om * hb)) * s1 
  
   call FmatmulL(ol%nonZeroF,ol%F,olEvo%AC,buf)
   s2 = 0.0_dp
   do i=1,nlev
      s2 = s2 + buf(i,i)
   enddo
   s2 = (1.0_dp/(ol%mass * ol%om *hb)) * s2

   ol%Ndot = s1 + s2

   !SPIN DEGENERACY for all the terms since they are traced over all the electrons
   !the spin degeneracy is absent if in x3l mode
   if(x3l == 0) then
      ol%Ndot = ol%Ndot*2.0_dp
   endif

end subroutine

!-------------

subroutine mu(ol,olEvo)

   use modvar
   implicit none

   type(oscillator) :: ol !ASSUMED SHAPE, LOCAL
   type(oscillatorEvo) :: olEvo
   
   ol%mu = ol%c1mu * olEvo%CC   -   ol%c2mu * olEvo%AS

end subroutine

!----------------------
!OBSERVABLES subroutines
!----------------------

subroutine observables(g,r,o,time,OLD)

   use modvar
   implicit none

   type(global) :: g
   type(oscillator), dimension(:) :: o
   complex(dp), dimension(:,:) :: r
   complex(dp) :: en,debug,trace,trace2,EHO,EHO2,EHO3,EHO4,buffer,purity
   complex(dp), dimension(nlev,nlev) :: musq,rhosq,temp
   complex(dp) :: Etot,Tel,Tel2,Tosc,heat,corr,Toscbos,eperosc,Toscboltz
   complex(2*dp) :: eel,entr,eosc,entrosc,ecorr,trueEel
   complex(2*dp) :: eel2,entr2,entr2old,trueEelold  !TOGLIMI
   complex(2*dp) :: eelold,eel2old,entrold,eoscold,entroscold
   complex(2*dp), dimension(:) :: OLD
   real(dp) :: time,J1,J2,buca
   integer :: nstep,i,j,k,l,flag,dummy,num,iter

   !LAPACK
   complex(dp) :: alpha,beta

   i = int(time/dt)

   !DEBUG on hermiticity
   !call checkHermite(o(1)%FcRho - o(1)%RhoFc,flag)
   !call checkHermite(o(1)%mu,flag)                  
   !call checksame(o(1)%FcRho, conjg(transpose(o(1)%RhoFc)),time,flag)
   !call checksame(o(1)%FsRho, conjg(transpose(o(1)%RhoFs)),time,flag)

   !bond current
   J1 = -aimag(g%Hel(oscL+2,oscL+3)*r(oscL+3,oscL+2))
   J2 = -aimag(g%Hel(siteJ,siteJ+1)*r(siteJ+1,siteJ))

   !energy of a couple of oscillators
   EHO = (o(1)%N + 0.5_dp)*hb*o(1)%om
   EHO2 = (o(2)%N + 0.5_dp)*hb*o(2)%om
   EHO3 = (o(3)%N + 0.5_dp)*hb*o(3)%om
   EHO4 = (o(4)%N + 0.5_dp)*hb*o(4)%om
   write(37,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time, real(EHO,dp), real(EHO2,dp), real(EHO3,dp), real(EHO4,dp)

   !Bondcurrent output
   !write(36,*) time,microampere*J2
   write(36,*) time,microampere*J1,microampere*J2

   !screen output
   !write(*,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,microampere*J1,microampere*J2, real(EHO,dp), real(EHO2,dp)
   !write(*,'(f8.3,f11.6,f11.6,f11.6)') time,microampere*J1,microampere*J2,real(o(1)%N,dp)
   !write(*,'(f8.3,f11.6,f11.6,f11.6,i10)') time,microampere*J1,microampere*J2,real(o(1)%N,dp),flag

   !check of the trace of rho
   trace = 0.0_dp
   do k=1,nlev
      trace = trace + r(k,k)
   enddo
   write(41,*) time,trace

   !PURITY CHECK for rho. Is the system in a pure state and does it stay in it??
   !alpha = 1.0_dp
   !beta = 0.0_dp
   !call zgemm('N','N',nlev,nlev,nlev,alpha,r,nlev,r,nlev,beta,rhosq,nlev) 
   !trace = 0.0_dp
   !do k=1,nlev
   !   trace = trace + rhosq(k,k)
   !enddo
   !purity = 1.0_dp - trace/(nlev/2.0_dp)
   !write(68,*) time,real(purity,dp)
                    
   entr = entropy(g,r,o%N,1,time) !electronic entropy     
   write(74,*) time,real(entr,2*dp) 
   entr2 = entropy(g,r,o%N,3,time) !electronic entropy 2    
   write(47,*) time,real(entr2,2*dp) 
   Eel = energy(g,r,o,1)   !electronic energy        
   write(73,*) time, real(Eel,2*dp)
   Eel2 = energy(g,r,o,3)   !electronic energy 2 
   write(72,*) time, real(Eel2,2*dp)
   trueEel = energy(g,r,o,0)   !TRUE electronic energy        
   write(38,*) time, real(trueEel,2*dp)  

   entrosc = entropy(g,r,o%N,2,time) !oscillator entropy
   write(77,*) time,real(entrosc,2*dp)
   eosc = energy(g,r,o,2)   !oscillator energy
   write(76,*) time,real(eosc,2*dp) 

   ecorr = energy(g,r,o,4)   !NEW!!! CORRELATION energy        
   write(87,*) time, real(ecorr,2*dp)

   !TOTAL energy
   write(79,*) time, real(Eel2,dp)+real(eosc,dp)+real(ecorr,dp)
   !TRUE TOTAL energy
   write(39,*) time, real(trueEel,dp)+real(eosc,dp)+real(ecorr,dp)

   if(time==Tcoup) g%Ein = real(trueEel,dp)+real(eosc,dp)+real(ecorr,dp)   !sets the initial energy after the ramp

   if(i == obsfreq) then  !temporary workaround
      OLD(1) = eel     !eelold
      OLD(2) = entr    !entrold
      OLD(3) = eosc    !eoscold
      OLD(4) = entrosc !entroscold
      OLD(5) = entr2   !entr2old  
      OLD(6) = eel2    !eel2old
      OLD(7) = trueEel !trueEelold
   endif
   !local names, the old values for these are stored in OLD which is preserved for any timestep
   eelold = OLD(1) 
   entrold = OLD(2)
   eoscold = OLD(3)
   entroscold = OLD(4)
   entr2old = OLD(5) !TOGLIMI
   eel2old = OLD(6)
   trueEelold = OLD(7)

   Tel = temperature(eel,eelold,entr,entrold,1,OLD,flag)
   if((flag==1).and.(time > Tcoup)) write(75,*) time,real(Tel,dp)    !print only if a temperature is calculated, flag controls it
   Tel2 = temperature(eel2,eel2old,entr2,entr2old,3,OLD,flag) 
   if((flag==1).and.(time > Tcoup)) write(57,*) time,real(Tel2,dp)   
   Tosc = temperature(eosc,eoscold,entrosc,entroscold,2,OLD,flag)
   if((flag==1).and.(time > Tcoup)) write(78,*) time,real(Tosc,dp)
   OLD(7) = trueEel
   !bosonic temperature, it's not entropic
   eperosc = eosc/real(nho,dp)  !average energy per oscillator
   buffer = log(( (2.0_dp*eperosc) + hb*o(1)%om ) / ( (2.0_dp*eperosc) - hb*o(1)%om )) 
   Toscbos = hb*o(1)%om / ( kb*buffer )
   if(time > Tcoup) write(94,*) time,real(Toscbos,dp)
   !boltzmann temperature
   if(time > Tcoup) write(95,*) time,real(eperosc/kb,dp)

   !TOGLIMI        
   g%avTel = (1.0_dp - mixTel)*g%avTel + (mixTel*Tel)
   g%avTel2 = (1.0_dp - mixTel)*g%avTel2 + (mixTel*Tel2)   
   g%avkTel = (1.0_dp - mixTel)*g%avkTel +  (mixTel*(1.0_dp/(kb*Tel)))
   g%avkT2 = (1.0_dp - mixTel)*g%avkT2 + (mixTel*(1.0_dp/(kb*Tel2)))
   g%avkTo = (1.0_dp - mixTel)*g%avkTo + (mixTel*(1.0_dp/(kb*Toscbos)))
   write(91,*) time,real(1.0_dp/(kb*g%avkTel),dp)
   write(92,*) time,real(1.0_dp/(kb*g%avkT2),dp)
   write(93,*) time,real(1.0_dp/(kb*g%avkTo),dp)

   !HEAT & WORK
   if(i>obsfreq) then
      heat = heat_transfer(entr,entrold,1.0_dp/(kb*g%avkTel),entrosc,entroscold,Toscbos) !if positive Q_e > Q_i
      write(84,*) time,real(heat,dp)
      write(86,*) real(1.0_dp/(kb*g%avkTel),dp),real(Toscbos,dp),real(heat,dp)
      write(82,*) time,real(heat,dp) - (real(trueEel,dp)-real(trueEelold,dp))  !print work
   endif

   !Screen Output
   if(openboundaries.ne.0) then
      write(*,'(f9.3,f11.6,f11.6,f11.2,f11.2)') time,microampere*J1,microampere*J2,real(Tel2,dp),real(Tosc,dp)
   else
      write(*,'(f9.3,f11.2,f11.2,f11.2)') time,real(1.0_dp/(kb*g%avkTel),dp),real(1.0_dp/(kb*g%avkT2),dp),real(Toscbos,dp)   
   endif

   if(x3l .ne. 0) then
      !occupation TB
      do j = 1,nlev
         write(89,'(F11.3,i10,F12.7)') time,j, real(r(j,j),dp)
      enddo 
      write(89,'(F11.3,i10,F12.7)') time,nlev+1, 0.0_dp !fake last row,bug in the plotting with gnuplot and pm3d
      write(89,*)

      !occupation of the well and the 2 sites next to it
      if(geomwire==12) then
         buca = 0.0_dp
         do j = atom-1,atom+1
            buca = buca + real(r(j,j),dp)    
         enddo
         write(90,*) time, buca
      elseif(geomwire==13) then
         buca = 0.0_dp
         iter = (wwidth + 1)/2
         do j = atom-iter,atom+iter
            buca = buca + real(r(j,j),dp)    
         enddo
         write(90,*) time, buca
      elseif(geomwire==16) then
         buca = real(r(atom,atom),dp)
         write(90,*) time, buca
      endif
   else
      !TOGLIMI
      !occupation
      if(mod(int(time/dt),obsfreq*10)==0) then
         do j = 1,nlev
            write(89,'(F11.3,i10,F12.7)') time,j, real(r(j,j),dp)
         enddo 
         write(89,'(F11.3,i10,F12.7)') time,nlev+1, 0.0_dp !fake last row,bug in the plotting with gnuplot and pm3d
         write(89,*)
      endif
   endif
         
   if(nho .ne. 0) then

      !write(34,*) time,real(o(1)%N,dp),aimag(o(1)%N)
      !write(34,*) time,real(o(1)%N,dp),real(o(2)%N)
      if(x3l == 1) then   !in x3l mode = 1, there's only 1 oscillator
         write(34,'(f9.3,f11.6)') time,real(o(1)%N,dp)
      else
         write(34,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,real(o(1)%N,dp),real(o(2)%N),real(o(3)%N),real(o(4)%N)
         write(35,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,real(o(5)%N,dp),real(o(6)%N),real(o(7)%N),real(o(8)%N)
         write(22,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,real(o(9)%N,dp),real(o(10)%N),real(o(11)%N),real(o(12)%N)
         write(21,'(f9.3,f11.6,f11.6,f11.6,f11.6)') time,real(o(13)%N,dp),real(o(14)%N),real(o(15)%N),real(o(16)%N)
         !write(34,'(f9.3,f11.6,f11.6)') time,real(o(1)%N,dp),aimag(o(1)%N)
         !write(41,*) time,real(o(1)%Ndot,dp),aimag(o(1)%Ndot)
      endif

      !WARNING: ALL THEE FOLLOWING PART WRITES OBSERVABLES ONLY FOR OSCILLATOR 1
      !!!!!!!!!!!!!!!FIX IT FOR MULTIPLE OSCILLATORS, LIKE IN THE EINSTEIN CASE

      !check of the trace of mu
      trace = 0.0_dp
      do k=1,nlev
         trace = trace + o(1)%mu(k,k)
      enddo
      write(43,*) time,real(trace,dp)
            
      !variance of mu
      buffer = trace*trace
      alpha = 1.0_dp
      beta = 0.0_dp
      call zgemm('N','N',nlev,nlev,nlev,alpha,o(1)%mu,nlev,o(1)%mu,nlev,beta,musq,nlev) 
      musq = musq - buffer 
      buffer = 0.0_dp
      do k=1,nlev
         buffer = buffer + musq(k,k)
      enddo
      buffer = sqrt(real(buffer,dp))
      write(66,*) time,real(buffer,dp)  !the variance of mu
      buffer = sqrt(real(buffer,dp))/real(trace,dp)
      write(67,*) time,real(buffer,dp)  !the variance of mu over its average value
      
      !correlation
      buffer = 0.0_dp
      trace = 0.0_dp
      trace2 = 0.0_dp
      corr = 0.0_dp
      do j=1,nho
         call FmatmulL(o(j)%nonZeroF,o(j)%F,o(j)%mu,musq)  !bad choice of names for now, change it
         call FmatmulL(o(j)%nonZeroF,o(j)%F,r,temp)  !bad choice of names for now, change it
         do k=1,nlev
            buffer = buffer + musq(k,k)
            trace = trace + o(j)%mu(k,k)
            trace2 = trace2 + temp(k,k)
         enddo
         corr = corr + (trace*trace2) - buffer
      enddo
      write(85,*) time,real(corr,dp) 

   endif  

   if( (geomwire == 19) .and. (time == T) ) then
      !local file where at the end of the simulation one writes the set of results
      !it is to be used with a script that launches several simulations in one folder
      open(96, file='rescv.dat', access = 'append')
      write(*,'(f10.1,f10.1,f14.2,f14.2,f16.4)') inTel,alfredoT,real(g%avkTel,dp),real(Toscbos,dp),real(trueEel,dp)+real(eosc,dp)+real(ecorr,dp)
      write(96,'(f9.1,f9.1,f12.2,f12.2,f15.4)') inTel,alfredoT,real(g%avkTel,dp),real(Toscbos,dp),real(trueEel,dp)+real(eosc,dp)+real(ecorr,dp)
      close(96)
   endif

end subroutine

!----------------------

function energy(g,r,o,system)

   use modvar
   implicit none

   type(global) :: g
   type(oscillator), dimension(:) :: o
   complex(dp), dimension(:,:) :: r
   complex(dp), dimension(nlev,nlev) :: temp,energia
   integer, INTENT(IN) :: system  !if 1 electrons, if 2 ions
   complex(2*dp) :: energy,Eel,eosc
   integer :: i,j

   if(system == 0) then
      !true electronic energy that uses the true DM
      call FmatmulLrcc(g%nonZeroH,g%Hel,r,energia) 

      Eel = 0.0_dp
      do j=1,nlev
         Eel = Eel + energia(j,j)
      enddo        

      if(x3l == 0) then
         energy = 2.0_dp*Eel   !It's double because of the spin!!!!!!!!!!
      else
         energy = Eel   
      endif

      return

   elseif(system == 1) then
      !NEW
      !electronic energy, missing the OB
      !electronic energy using the purified DM (eigenvalues)
      call FmatmulLrcc(g%nonZeroH,g%Hel,g%tarocrhoeig,energia) 
      !------     

      !!!!!!!!!!CAREFUL
      !does it make sense to trace only over the central region?? I am ignoring the OB and the HF too
      Eel = 0.0_dp
      if(tracecentral == 1) then
         do j=NL+1,NL+NC           
            Eel = Eel + energia(j,j)
         enddo
      else
         do j=1,nlev
            Eel = Eel + energia(j,j)
         enddo       
      endif

      if(x3l == 0) then
         energy = 2.0_dp*Eel   !It's double because of the spin!!!!!!!!!!
      else
         energy = Eel   
      endif

      return
   
   elseif(system==2) then

      if(nho .ne. 0) then
         eosc = 0.0_dp               
         do j=1,nho
            eosc = eosc + (o(j)%N + 0.5_dp)*hb*o(j)%om
         enddo
      endif
      energy = eosc
      return

   elseif(system == 3) then
      !electronic energy, missing the OB
      !electronic energy using the purified DM (in the H0 basis), it's not strictly conserved, especially for low T_el
      temp = 0.0_dp  
      do i=1,nlev
         temp(i,i) = g%E(i)
      enddo
      temp = matmul(temp,g%tarocrho)
      temp = matmul(temp,g%HprojectionsTC)
      energia = matmul(g%Hprojections,temp) 
      !------     

      !!!!!!!!!!CAREFUL
      !does it make sense to trace only over the central region?? I am ignoring the OB and the HF too
      Eel = 0.0_dp
      if(tracecentral == 1) then
         do j=NL+1,NL+NC           
            Eel = Eel + energia(j,j)
         enddo
      else
         do j=1,nlev
            Eel = Eel + energia(j,j)
         enddo       
      endif

      if(x3l == 0) then
         energy = 2.0_dp*Eel   !It's double because of the spin!!!!!!!!!!
      else
         energy = Eel   
      endif

      return

   elseif(system==4) then !correlation energy

      if(nho .ne. 0) then
         energia = 0.0_dp
         do j=1,nho
            call FmatmulL(o(j)%nonZeroF,o(j)%F,o(j)%mu,temp)
            energia = energia - temp
         enddo
      endif 
      Eel = 0.0_dp
      if(tracecentral == 1) then 
         do j=NL+1,NL+NC           
            Eel = Eel + energia(j,j)
         enddo
      else
         do j=1,nlev
            Eel = Eel + energia(j,j)
         enddo       
      endif

      if(x3l == 0) then
         energy = 2.0_dp*Eel   !It's double because of the spin???????
      else
         energy = Eel   
      endif

      return

   endif
   
end function

!----------------------

function entropy(g,r,enne,system,time)

   use modvar
   implicit none

   type(global) :: g
   complex(dp), dimension(:,:) :: r
   complex(dp), dimension(:) :: enne
   complex(dp), dimension(nlev,nlev) :: temp
   integer, INTENT(IN) :: system  !if 1 electrons, if 2 ions, if 3 electrons in another way
   complex(dp), dimension(:,:), allocatable :: small,smallTC,hpsmall,hpTCsmall
   complex(dp*2) :: entropy,entrosc,trace,uno,due
   integer :: i,j,dimen,imax,jmax
   real(dp) :: time,maxviol,buf

   !LAPACK
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   real(dp), allocatable :: eigv(:)
   integer :: info

   if(system == 1) then  !electronic entropy, from the eigenvalues of rho in the TB basis and the Von Neumann entropy
      allocate(work(lwork))
      allocate(rwork(lrwork))
      allocate(iwork(liwork))
      if(tracecentral == 1) then
         dimen = NC
         allocate(eigv(NC))
         allocate(small(NC,NC))
         allocate(smallTC(nlev,nlev))
         small = r((NL+1):(NL+NC),(NL+1):(NL+NC))
      else   
         dimen = nlev
         allocate(eigv(nlev))
         allocate(small(nlev,nlev))
         allocate(smallTC(nlev,nlev))
         small = r     
      endif

      !call checkHermite(small,flag)
      !call zheevd('N','U',dimen,small,dimen,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
      call zheevd('V','U',dimen,small,dimen,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)  !computes also the eigenvectors
      smallTC = conjg(transpose(small))

      trace = 0.0_dp
      do j = 1,dimen
         
         if(eigv(j) < 0.0_dp) then

            !check violations of fermionicity
            if(abs(eigv(j)) > violfermi) then
              write(30,*) time,'eig',j,eigv(j)
            endif

            if(Flipswitch == 1) then
               eigv(j) = - eigv(j)  !usually it's very small, I just invert its sign
            else
               eigv(j) = purenonflip
            endif 
         endif
         if(eigv(j) > 1.0_dp) then
            if(Flipswitch == 1) then
               eigv(j) = eigv(j) - 2.0_dp*(eigv(j) - 1.0_dp)
            else
               eigv(j) = 1 - purenonflip
            endif 
         endif
              
         uno = eigv(j)*log(eigv(j))
         due = (1.0_dp - eigv(j))*log(1.0_dp - eigv(j))  

         !TOGLIMI         
         !uno = abs(eigv(j))*log(abs(eigv(j))) 
         !due = abs(1.0_dp - eigv(j))*log(abs(1.0_dp - eigv(j)))
         !TOGLIMI  
         !if((eigv(j) < 0.0_dp) .or. ((1.0_dp-eigv(j)) < 0.0_dp)) then
         !   write(42,'(i5,f11.6,f11.6,f11.6)') j,eigv(j),real(uno,dp),real(due,dp)
         !endif         
         trace = trace + uno + due

      enddo

      !rewrites purified rho in the TB basis
      temp = 0.0_dp  
      do j=1,dimen
         temp(j,j) = eigv(j)
      enddo
      temp = matmul(temp,smallTC)
      g%tarocrhoeig = matmul(small,temp)     

      if(x3l == 0) then
         entropy = -kb*trace*2.0_dp   !SPIN!!!!!!!
      else
         entropy = -kb*trace
      endif    

      return
    
   elseif(system == 2) then  !oscillator entropy         
      if(nho .ne. 0) then
         entrosc = 0.0_dp           
         do j=1,nho
            entrosc = entrosc - enne(j)*log(real(enne(j),dp)) + (enne(j)+1)*log(real(enne(j),dp)+1)
         enddo
      endif
      
      entropy = kb*entrosc
      return

   elseif(system == 3) then  !TOGLIMI !electronic entropy from the diagonal elements of rho in the H basis  
      if(tracecentral == 1) then
         dimen = NC
         allocate(small(NC,NC))
         allocate(hpsmall(NC,NC))       !CAREFUL!!!!!!!!!!!!!!!!!!!!!!! Sarà giusto fare a pezzi così una matrice??????? NOOO!
         allocate(hpTCsmall(NC,NC))
         small = r((NL+1):(NL+NC),(NL+1):(NL+NC))
         hpsmall = g%Hprojections((NL+1):(NL+NC),(NL+1):(NL+NC))
         hpTCsmall = g%HprojectionsTC((NL+1):(NL+NC),(NL+1):(NL+NC))
         small = matmul(hpTCsmall,small) 
         small = matmul(small,hpsmall) 
      else   
         dimen = nlev
         allocate(small(nlev,nlev))
         small = r
         small = matmul(g%HprojectionsTC,small) !cambio di base, scrivo rho nella base dell'hamiltoniana
         small = matmul(small,g%Hprojections)   
      endif    
   
      trace = 0.0_dp      
      do j = 1,dimen

         if(real(small(j,j),dp) < 0.0_dp) then

            !check violations of fermionicity
            if(abs(real(small(j,j),dp)) > violfermi) then
              write(30,*) time,'rH0',j,real(small(j,j),dp)
            endif

            if(Flipswitch == 1) then
               small(j,j) = - small(j,j)  !usually it's very small, I just invert its sign
            else
               small(j,j) = purenonflip
            endif   
         endif
         if(real(small(j,j),dp) > 1.0_dp) then
            if(Flipswitch == 1) then
               small(j,j) = small(j,j) - 2.0_dp*(small(j,j) - 1.0_dp)
            else
               small(j,j) = 1.0_dp - purenonflip
            endif   
         endif

         trace = trace + (small(j,j))*log(small(j,j)) + (1.0_dp - small(j,j))*log(1.0_dp - small(j,j))
      enddo 

      if(x3l == 0) then
         entropy = -kb*trace*2.0_dp   !SPIN!!!!!!!
      else
         entropy = -kb*trace
      endif    
      
      g%tarocrho = small

      !occupation
      i = int(time/dt)
      if(mod(i,obsfreq*obsfreq)==0) then
         do j = 1,dimen
            write(83,*) time,g%E(j), real(small(j,j),dp)
         enddo 
         !write(83,*) time,g%E(dimen)+0.01_dp, real(small(dimen,dimen),dp)  !fake last row
         write(83,*)
      endif

      !off-diagonal values of rho in the H0 basis, it measures how far from 0 is the commutator of rho with H0
      maxviol = 0.0_dp
      do i=1,dimen
         do j=i+1,dimen
            if(abs(g%tarocrho(i,j)) > maxviol ) then
               maxviol = abs(g%tarocrho(i,j))
               imax = i
               jmax = j
            endif
         enddo
      enddo
      write(32,*) time, imax, jmax, maxviol
   
      return
  
   endif

end function

!----------------------

function temperature(enuno,endue,entropuno,entropdue,system,OLD,flag)

   use modvar
   implicit none
   
   complex(2*dp) :: enuno,endue,entropuno,entropdue
   complex(dp) :: temperature
   integer, INTENT(IN) :: system 
   complex(2*dp), dimension(:) :: OLD
   integer, INTENT(OUT) :: flag

   flag = 0
   if(abs(entropuno - entropdue) > zero) then            
      temperature = (enuno - endue) / (entropuno - entropdue)
      flag = 1
      if(system == 1) then !electrons update
         OLD(1) = enuno
         OLD(2) = entropuno
      elseif(system == 2) then !ions update
         OLD(3) = enuno
         OLD(4) = entropuno
      elseif(system == 3) then !electrons 2 update
         OLD(6) = enuno
         OLD(5) = entropuno
      endif
   else !!!!!!!!!!!TOGLIMI, dirty trick to make g%avinkTel work
      temperature = g%avTel
   endif

   return

end function

!----------------------

function heat_transfer(entr,entrold,Tel,entrosc,entroscold,Tosc)

   use modvar
   implicit none

   complex(2*dp), INTENT(IN) :: entr,entrold,entrosc,entroscold
   complex(dp), INTENT(IN) :: Tel, Tosc
   complex(dp) :: heat_transfer

   !heat_transfer = (entr-entrold)*Tel - (entrosc-entroscold)*Tosc
   heat_transfer = ((entr+entrosc)-(entrold+entroscold))*(Tel - Tosc)

   return

end function

!----------------------
!efficient matrix multiplications when one matrix is sparse AND the matrix is stored
!----------------------

!-------------!efficient matmul Left CC->C

subroutine FmatmulL(noz,sparse,gen,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the left element is F or similar

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(i,j)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Left RC->C

subroutine FmatmulLrcc(noz,sparse,gen,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the left element is F or similar

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:,:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(i,j)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CC->C

subroutine FmatmulR(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right RR->R

subroutine FmatmulRrrr(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:,:), INTENT(IN) :: sparse,gen
   real(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CR->C

subroutine FmatmulRcrc(noz,gen,sparse,res)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   real(dp), dimension(:,:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(i,j)
      enddo

   enddo
   
end subroutine

!----------------------
!efficient matrix multiplications when one matrix is sparse but the matrix IS NOT stored
!----------------------

!-------------!efficient matmul Left CC->C

subroutine SmatmulL(noz,sparse,gen,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + gen(j,q)*sparse(n)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Left RC->C

subroutine SmatmulLrcc(noz,sparse,gen,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi

   elementi = size(noz,1)   
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole row i will be not empty in the result, cycle over the results' columns
      do q=1,nlev
         res(i,q) = res(i,q) + sparse(n)*gen(j,q)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CC->C

subroutine SmatmulR(noz,gen,sparse,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   complex(dp), dimension(:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(n)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right RR->R

subroutine SmatmulRrrr(noz,gen,sparse,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   real(dp), dimension(:), INTENT(IN) :: sparse
   real(dp), dimension(:,:), INTENT(IN) :: gen
   real(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(n)
      enddo

   enddo
   
end subroutine

!-------------!efficient matmul Right CR->C

subroutine SmatmulRcrc(noz,gen,sparse,res)
   use modvar
   implicit none

   integer, dimension(:,:), INTENT(IN) :: noz
   complex(dp), dimension(:,:), INTENT(IN) :: gen
   real(dp), dimension(:), INTENT(IN) :: sparse
   complex(dp), dimension(:,:), INTENT(OUT) :: res
   integer i,j,n,q,elementi
   
   elementi = size(noz,1)
   res = 0.0_dp
   do n=1,elementi
      i = noz(n,1)
      j = noz(n,2)

      !the whole column j will be not empty in the result, cycle over the results' rows
      do q=1,nlev
         res(q,j) = res(q,j) +  gen(q,i)*sparse(n)
      enddo

   enddo
   
end subroutine

!-------------!sparse matrix storage of elements and their position

subroutine findnonZero(matrix,label,numel)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(OUT) :: label
   real(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: numel
   integer i,j,n,q,elementi
   
   n=1
   do i=1,nlev
      do j=1,nlev
         if(matrix(i,j) .ne. 0.0_dp) then
            label(n,1) = i
            label(n,2) = j
            n = n+1
            !write(*,*) i,j
         endif
      enddo
   enddo

   numel = n-1
   
end subroutine

!-------------!sparse matrix storage of elements and their position, COMPLEX input

subroutine findnonZeroC(matrix,label,numel)
   use modvar
   implicit none

   !routine for improving the speed of matmul where the right element is F or similar
   !Careful to the order of the elements

   integer, dimension(:,:), INTENT(OUT) :: label
   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: numel
   integer i,j,n,q,elementi
   
   n=1
   do i=1,nlev
      do j=1,nlev
         if(abs(matrix(i,j)) .ne. 0.0_dp) then
            label(n,1) = i
            label(n,2) = j
            n = n+1
            !write(*,*) i,j
         endif
      enddo
   enddo

   numel = n-1
   
end subroutine

!-------------!sparse matrix builder, only the nonzero elements are stored; COMPLEX input and output

subroutine buildSparseC(nonzero,full,reduced)

   integer, dimension(:,:), INTENT(IN) :: nonzero
   complex(dp), dimension(:,:), INTENT(IN) :: full
   complex(dp), dimension(:), INTENT(OUT) :: reduced
   integer i,j,n,elementi
   
   elementi = size(nonzero,1)

   do n=1,elementi
      i = nonzero(n,1)
      j = nonzero(n,2)

      reduced(n) = full(i,j)
   enddo

end subroutine

!----------------------
!Check routines
!----------------------

!-------------!checks the hermiticity of a matrix

subroutine checkHermite(matrix,flag)
   use modvar
   implicit none

   !routine for checking the hermiticity of matrices

   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: flag
   integer i,j,n,q,elementi,enne
   
   flag = 0
   enne = size(matrix,1)
   do i=1,enne
      do j=1,enne
         if(abs(matrix(i,j) - conjg(matrix(j,i))) .ge. zero ) then
            flag = flag + 1
            write(42,*) i,j,matrix(i,j),conjg(matrix(j,i))
         endif
      enddo
   enddo
  
end subroutine

!-------------!checks if a matrix is symmetrical

subroutine checkSymm(matrix,flag)
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   integer, INTENT(OUT) :: flag
   integer i,j,n,q,elementi,enne
   
   flag = 0
   enne = size(matrix,1)
   do i=1,enne
      do j=1,enne
         if( abs(matrix(i,j) - matrix(j,i)) .ge. zero ) then
            flag = flag + 1
            write(42,*) i,j,matrix(i,j),conjg(matrix(j,i))
         endif
      enddo
   enddo
  
end subroutine

!-------------!checks if 2 matrices are identical
!maybe it's better I put some flexibility like a +- epsilon, for including the machine error

subroutine checkSame(matrix,matrix2,time,flag)
   use modvar
   implicit none

   !routine for checking if 2 complex matrices are the same
   !it's useful if I use 2 different methods to get them

   complex(dp), dimension(:,:), INTENT(IN) :: matrix
   complex(dp), dimension(:,:), INTENT(IN) :: matrix2
   integer, INTENT(OUT) :: flag
   real(dp), INTENT(IN)  :: time
   integer i,j,n,q,elementi,enne
   
   flag = 0
   enne = size(matrix,1)
   do i=1,enne
      do j=1,enne
         if( abs(matrix(i,j) - matrix2(i,j)) .ge. zero ) then
            flag = flag + 1
            !write(42,*) i,j,matrix(i,j),matrix2(i,j)
         endif
      enddo
   enddo

   !if(flag .ne. 0) then
   !   write(*,*) 'At time',time,'the matrices do not coincide, check debug.dat'
   !   stop
   !endif
  
end subroutine

end program elph
