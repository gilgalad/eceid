program diagH

   !this program builds a hamiltonian for a TB system, diagonalizes it
   !and computes eigenvalues and eigenstates. It will mainly be used for getting the GS of a system, 
   !which, in the initial basis, is the first column of the outcoming H matrix

   !The file ham contains the hamiltonian
   !the file rhozero the DM with the first nlev/2 energy eigenstates of H as generating state

   !IMPORTANT this file has the injection terms calculated by summing each four of the terms,
   !it has the same result as Eunan 

   use modvar
   implicit none

   !LAPACK VARIABLES   
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   integer :: info

   !SYSTEM VARIABLES (use allocatable for avoiding problems with openMP)
   complex(dp), allocatable :: H(:,:),Hold(:,:),effe(:,:),effeOLD(:,:) !hamiltonian of the system
   real(dp), allocatable :: eigv(:)    !ordered eigevalue vector
   complex(dp), allocatable :: eigvaluesEffe(:)    !ordered eigevalue vector
   complex(dp), allocatable :: eigvectorsEffeL(:,:),eigvectorsEffeR(:,:) !Left and right eigenvectors of effe
   complex(dp), allocatable :: rhozero(:,:)
   complex(dp), allocatable :: G(:,:),Ginv(:,:)   
   integer :: i,j,s,sp,n
   real(dp) :: buffer,maxspace,avspace,E 
   real(dp) :: coup,FD
   complex(dp), allocatable :: inter(:,:)

   !CV Calculations
   integer :: nstep,k
   real(dp) :: dtemp,Tmax,temperature,omega(nho)
   complex(dp), allocatable :: buf(:,:),energia(:,:)
   complex(dp) :: trace,trace2,alpha,beta,Eos,Eos2,fake

   !SCREEN OUTPUT REGARDING THE INFO OF THE SYSTEM
   write(*,*)
   write(*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~'
   write(*,*) 'This is the setup part for elPh release',release
   write(*,*)
   write(*,*) 'The system chosen is in', ndim,'dimension'
   if(ndim == 1) then
      write(*,*) 'It is a nanowire with',NL ,'sites in the leads and',NC,'sites in the central region'
   endif
   write(*,*) 'Gamma for the Cs/c As/c operators is', gamFin,'eV'
   if(openboundaries == 1) then
      write(*,*) 'The OB are on and their gamma is',gamOB,'eV'
   endif
   if(nmodes == 0) then
      write(*,*) 'The phonons are considered Einstein oscillators'
   elseif(nmodes == 1) then
      write(*,*) 'The phonons are considered Normal Modes'
   endif
   write(*,*)
  
   !-----------------------------
   !BUILDING THE WIRE
    
   !Hamiltonian
   allocate(H(nlev,nlev))
   allocate(Hold(nlev,nlev))
   call buildH(H)
   Hold = H
    
   !for the OB, operatorial part of G-. It has to be diagonalised
   allocate(effe(nlev,nlev))
   allocate(effeOLD(nlev,nlev))   
   call buildF(effe,Hold,gamOB)
   effeOLD = effe 
   
   !building the coupling matrix with the oscillators (if present)
   !call random_seed(seed)  !some geometries require random numbers
   if(nho .ne. 0) then
      open(62, file='coupHO',form='unformatted')   
      open(33, file='input.dat')
      allocate(inter(nlev,nlev))
      do i=1,nho
         if(nmodes == 0) then !case of einstein oscillators
            read(33,*) coup, fake, omega(i)
            omega(i) = omega(i)/hb
            call buildInteraction(inter,coup,i)
         elseif(nmodes == 1) then !!!!!!!!case of normal modes, the coupling and the omega are only the ones of the first line in input
            if(i == 1) then        
               read(33,*) coup, fake, omega(1)
            endif
            omega(i) = 2.0_dp*omega(1)*abs(sin(pi*real(i,dp)/real(nho,dp)))                      
            write(*,*) 'The frequency of normal mode',i,'is',omega(i),'eV'
            omega(i) = omega(i)/hb !fixing the dimension of omega to fs^-1
            call buildInteractionNM(inter,coup,i)
         endif         
         write(62) inter
      enddo
   endif
   close(62)

   !-----------------------------
   !LAPACK diagonalisations and storing the resulting eigenvectors/eigenvalues
   
   !diagonalize H and effe
   allocate(work(lwork))
   allocate(rwork(lrwork))
   allocate(iwork(liwork))
   allocate(eigv(nlev))   
   allocate(eigvaluesEffe(nlev))
   allocate(eigvectorsEffeL(nlev,nlev))
   allocate(eigvectorsEffeR(nlev,nlev))
   call zheevd('V','U',nlev,H,nlev,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
   !write(*,*) info
   call zgeev('V','V',nlev,effe,nlev,eigvaluesEffe,eigvectorsEffeL,nlev,eigvectorsEffeR,nlev, &
               work,LWORK,rwork,info)
   !write(*,*) info
   deallocate(work)
   deallocate(rwork)
   deallocate(iwork)
   
   
   open(44, file='ham',form='unformatted')
   !H is real, I could store it as real, but it is used in Lapack complex multiplications, better to use it complex
   write(44) real(Hold,dp),H,eigv
   close(44)
   !deallocate(Hold)
   deallocate(effe)

   !----------------------------- 
   !VERY IMPORTANT renormalization
   !the default normalization is NOT what I need to be, I want the identity to appear when i multply 
   !a left and a right vector with the same index. Here I enforce this
   !i is the label of the eigenvectors, it corresponds to the i eigenvalue
   do i=1,nlev   
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      eigvectorsEffeL(:,i) = eigvectorsEffeL(:,i) / dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      eigvectorsEffeR(:,i) = eigvectorsEffeR(:,i) / dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*)
   enddo

   !computation of the average and minimum energy spacing in the system (the central region is 
   !smaller than he leads, so effectively this means in the leads)
   avspace = 0.0_dp
   maxspace = 0.0_dp
   do i=1,nlev-1
      buffer = eigv(i+1)-eigv(i)
      if(abs(buffer) > maxspace) then
         maxspace = buffer
      endif
      avspace = avspace + buffer
   enddo
   avspace = avspace/real(nlev-1,dp)
   write(*,*) 'The maximum energy spacing in S is', maxspace,'eV'
   write(*,*) 'The average energy spacing in S is', avspace,'eV' 

   !building the corresponding DM, rhozero
   !VERY IMPORTANT, it's the sum of nlev/2 DM of the filled band. It's got nlev/2 as a trace!!!
   open(45, file='rhozero',form='unformatted')
   allocate(rhozero(nlev,nlev))   
   rhozero = 0.0_dp 
   if(Telrho == 0) then  
      do n=1,nlev/2
         do i = 1,nlev
            do j = 1,nlev
               rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )
            enddo
         enddo
      enddo
      !if the system has an odd number of electrons, like ATOM, I add "half" an electron more on the next excited state
      if(mod(NC,2) == 1) then   
         n = nlev/2 + 1
         do i = 1,nlev
            do j = 1,nlev
               rhozero(i,j) = rhozero(i,j) + 0.5_dp * ( conjg(H(j,n)) * H(i,n) )
            enddo
         enddo
      endif
   elseif(Telrho == 1) then  !the initial DM starts from an electronic temperature inTel
      write(*,*) 'The initial electronic Density matrix is at a temperature of', inTel, 'K'
      do n=1,nlev
         FD = 1.0_dp/( exp( eigv(n)/(kb*inTel) )  + 1.0_dp )
         !write(*,*) n,FD !TOGLIMI
         do i = 1,nlev
            do j = 1,nlev
               rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )*FD
            enddo
         enddo
      enddo
      !rhozero = 0.0_dp  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TOGLIMI
      !do n=1,nlev/2
      !   rhozero(n,n) = 1.0_dp
      !enddo
   endif
   write(45) rhozero
   close(45)

   !NEW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! METTILA in una funzione con uno switch
   !Energy vs. Temperature plot (and then C_V) for the electrons
   if(specheat ==1) then
      write(*,*) '... Computing the Heat Capacity ...'
      open(13, file='entempel.dat')
      open(14, file='cvel.dat')
      open(15, file='entemposc.dat')
      open(16, file='cvos.dat')
      open(17, file='cv.dat')
      allocate(buf(nlev,nlev))
      allocate(energia(nlev,nlev))
      Tmax = 50000.0_dp !maximum temperature plotted
      dtemp = 10.0_dp !step in temperature for the plot
      nstep = int(Tmax/dtemp)
      do k=1,nstep  !I start from temperature dtemp, not 0K
         temperature = k*dtemp
         buf = 0.0_dp
         !buiding the DM for the temperature chosen 
         do n=1,nlev
            FD = 1.0_dp/( exp( eigv(n)/(kb*temperature) )  + 1.0_dp )
            do i = 1,nlev
               do j = 1,nlev
                  buf(i,j) = buf(i,j) + ( conjg(H(j,n)) * H(i,n) )*FD
               enddo
            enddo
         enddo
         !computing the electronic energy
         alpha = 1.0_dp
         beta = 0.0_dp
         call zgemm('N','N',nlev,nlev,nlev,alpha,Hold,nlev,buf,nlev,beta,energia,nlev)
         trace = 0.0_dp
         do i=1,nlev
            trace = trace + energia(i,i)
         enddo
         if(k==1) trace2=trace
         write(13,*) temperature, real(trace,dp)
         write(14,*) temperature, real(trace - trace2,dp)/dtemp
         !write(*,*) temperature, real(trace,dp), real(trace - trace2,dp)/dtemp


         !Energy vs. Temperature plot (and then C_V) for the oscillators.
         eos = 0.0_dp      
         do j = 1,nho
            eos = eos + (0.5_dp*hb*omega(j) + hb*omega(j)*exp(-(hb*omega(j))/(kb*temperature))/(1.0_dp - exp(-(hb*omega(j))/(kb*temperature))) )
         enddo
         if(k==1) eos2=eos
         write(15,*) temperature, real(eos,dp)
         write(16,*) temperature, real(eos - eos2,dp)/dtemp
         write(17,*) temperature, real(trace - trace2,dp)/dtemp, real(eos - eos2,dp)/dtemp

         trace2 = trace
         eos2=eos
      enddo
   endif
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




   deallocate(rhozero)
   deallocate(H)
   deallocate(eigv) 
   
   !check if the wire is appropriate for the geometry chosen
   !modify it with other checks on gamma and other appropriate parameters
   !e.g. if gamma is too small, print warning
   if(geomwire == 1) then
      !DIMER, NC must be even -O-0~0-O-
      if(mod(NC,2).ne.0) then
         write(*,*) 'Unappropriate NC for the geometry chosen'
         STOP
      endif
   elseif(geomwire == 2) then
      !ATOM, NC must be odd -O-O~0~O-O-
      if(mod(NC,2).ne.1) then
         write(*,*) 'Unappropriate NC for the geometry chosen'
         STOP
      endif   
   endif
   if((geomwire < 100) .and. (nmodes == 1)) then
      write(*,*) 'Unappropriate geometry, normal modes require a geometry label > 99'
      STOP
   elseif((geomwire > 100) .and. (nmodes == 0)) then
      write(*,*) 'Unappropriate geometry, Einstein oscillators require a geometry label < 100'
      STOP
   endif

   !----------------------------- 
   !OB injection term
   if(openboundaries .ne. 0) then
      call injection(eigvectorsEffeL,eigvectorsEffeR,eigvaluesEffe,gamOB)
   endif

   
!-------------------END MAIN PROGRAM-------------------------


contains

!----------------------
!GEOMETRY of the WIRE ROUTINES
!----------------------
!change these if you want to add another configuration of the 1D wire
!add another case for the parameter geomwire

subroutine buildH(H)
   
   !chain of TB orbitals with the same hopping, without any imaginary term
   !it needs explicit modifications if we want the central region to be any different
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: H
   integer :: i,j,k,element

   H = 0.0_dp

   do i=1,nlev
      H(i,i) = onsite
   enddo

   !couplings chain (the wrong terms will be corrected below)
   do i = 1,nlev-1
      H(i,i+1) = tchain
   enddo
   do i = 2,nlev
      H(i,i-1) = tchain
   enddo

   if(ndim == 1) then !WIRE
      if(geomwire == 1) then
         !DIMER
         write(*,*) 'The geometry chosen is DIMER'
         !couplings dimer
         H(oscL,oscR) = tdimer
         H(oscR,oscL) = tdimer   
         
         !couplings chain-dimer  
         H(oscL-1,oscL) = tcd
         H(oscL,oscL-1) = tcd
         H(oscR,oscR+1) = tcd
         H(oscR+1,oscR) = tcd
      elseif(geomwire == 2) then
         !ATOM: SINGLE ATOM OSCILLATING COUPLED THE SAME  
         write(*,*) 'The geometry chosen is ATOM'
         !do nothing: H is a perfect chain
         H(atom,atom) = onsiteAtom
      elseif(geomwire == 3) then
         !STM, perfect wire with a weaker bond in the middle
         write(*,*) 'The geometry chosen is STM'
         H(leftend,leftend + 1) = middle
         H(leftend + 1,leftend) = middle
      elseif(geomwire == 4) then
         !EINSTEIN, several Einstein oscillators every 2 TB sites
         write(*,*) 'The geometry chosen is EINSTEIN'
         !for now, perfect chain
      elseif(geomwire == 5) then
         !OHM, several Einstein oscillators at a uniform distance from each other
         write(*,*) 'The geometry chosen is OHM'
         !for now, perfect chain
      elseif(geomwire == 6) then
         !LEGION, several Einstein oscillators at random position in the central region excluding the boundaries
         write(*,*) 'The geometry chosen is LEGION'
         !for now, perfect chain
      elseif(geomwire == 7) then
         !FULL_CIRCLE, like OHM but for a wire with PBC, the last site is connected to the first in the hamiltonian
         write(*,*) 'The geometry chosen is FULL_CIRCLE'
         H(1,nlev) = tchain
         H(nlev,1) = tchain
      elseif(geomwire == 8) then
         !DISSIPION, the first 4 oscillators are placed 2 by 2 in the leads to replace gamma
         write(*,*) 'The geometry chosen is DISSIPION'
      elseif(geomwire == 9) then
         !RANDSEA, the oscillators are coupled to all the sites within the central region with F = (0,coup_max(in input.dat)
         write(*,*) 'The geometry chosen is RANDSEA'
         !for now, perfect chain
      !-------------------------------------------------------------
      elseif(geomwire == 100) then
         !RING, the normal modes are in a ring within the central region
         write(*,*) 'The geometry chosen is RING'
         write(*,*) 'There are', nho,'Normal Modes in an electronic ring between position', NL + 2,'and', NL + 2 + nmring
         !the last site of the ring is NOT connected to the next in the chain, but to the first in the ring
         H(NL+2+nmring,NL+2+nmring + 1) = 0.0_dp
         H(NL+2+nmring + 1,NL+2+nmring) = 0.0_dp
         H(NL+2+nmring,NL+2) = tchain
         H(NL+2,NL+2+nmring) = tchain
         !the site halfway through the ring is connected to the one on the right
         write(*,*) 'Site', NL+2+(nmring/2),'is connected to site', NL + 2 + nmring + 1
         H(NL+2+(nmring/2),NL+2+nmring+1) = tchain
         H(NL+2+nmring+1,NL+2+(nmring/2)) = tchain
      endif
   elseif(ndim == 2)  then !SURFACE, every site is coupled to 4 others. If it is on the border, PBC are applied
      do i=1,righe
         do j=1,colonne
            !the element in the matrix, its label
            element = (i-1)*colonne + j
            !write(*,*) element
            
            !RIGHT
            if(j .ne. colonne) then !if we are not on the right border of the box
               H(element,element+1) = tgrid
               H(element+1,element) = tgrid
            else
               H(element,element-colonne+1) = tgrid
               H(element-colonne+1,element) = tgrid            
            endif
            
            !LEFT
            if(j .ne. 1) then !if we are not on the left border of the box
               H(element,element-1) = tgrid
               H(element-1,element) = tgrid
            else
               H(element,element+colonne-1) = tgrid
               H(element+colonne-1,element) = tgrid            
            endif            
            
            !UP
            if(i .ne. 1) then !if we are not on the top border of the box
               H(element,element-colonne) = tgrid
               H(element-colonne,element) = tgrid
            else
               H(element,element+(righe-1)*colonne) = tgrid
               H(element+(righe-1)*colonne,element) = tgrid            
            endif            
            
            !DOWN
            if(i .ne. righe) then !if we are not on the bottom border of the box
               H(element,element+colonne) = tgrid
               H(element+colonne,element) = tgrid
            else
               H(element,element-(righe-1)*colonne) = tgrid
               H(element-(righe-1)*colonne,element) = tgrid            
            endif                         
         enddo
      enddo
   elseif(ndim == 3)  then !SOLID, every site is coupled to 6 others. If it is on the border, PBC are applied
      do i=1,LZ
         do j=1,LY
            do k=1,LX
               !the element in the matrix, its label
               element = (i-1)*LX*LY + (j-1)*LY + k
               !write(*,*) element
               
               !RIGHT
               if(k .ne. LX) then !if we are not on the right border of the plane
                  H(element,element+1) = tcube
                  H(element+1,element) = tcube
               else
                  H(element,element-LX+1) = tcube
                  H(element-LX+1,element) = tcube            
               endif
               
               !LEFT
               if(k .ne. 1) then !if we are not on the left border of the plane
                  H(element,element-1) = tcube
                  H(element-1,element) = tcube
               else
                  H(element,element+LX-1) = tcube
                  H(element+LX-1,element) = tcube            
               endif            
               
               !UP
               if(j .ne. 1) then !if we are not on the top border of the plane
                  H(element,element-LX) = tcube
                  H(element-LX,element) = tcube
               else
                  H(element,element+(LY-1)*LX) = tcube
                  H(element+(LY-1)*LX,element) = tcube            
               endif            
               
               !DOWN
               if(j .ne. LY) then !if we are not on the bottom border of the plane
                  H(element,element+LX) = tcube
                  H(element+LX,element) = tcube
               else
                  H(element,element-(LY-1)*LX) = tcube
                  H(element-(LY-1)*LX,element) = tcube            
               endif
               
               !OUT OF THE PAPER
               if(i .ne. LZ) then !if we are not on the bottom border of the box
                  H(element,element+(LX*LY)) = tcube
                  H(element,element+(LX*LY)) = tcube
               else
                  H(element,element-(LZ-1)*(LX*LY)) = tcube
                  H(element-(LZ-1)*(LX*LY),element) = tcube            
               endif 
               
               !IN THE PAPER
               if(i .ne. 1) then !if we are not on the bottom border of the box
                  H(element,element-(LX*LY)) = tcube
                  H(element-(LX*LY),element) = tcube
               else
                  H(element,element+(LZ-1)*(LX*LY)) = tcube
                  H(element+(LZ-1)*(LX*LY),element) = tcube            
               endif                             
            enddo
         enddo
      enddo      
   endif
   
end subroutine buildH

subroutine buildInteraction(inter,coup,j)
   
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: inter
   real(dp), INTENT(IN) :: coup
   real(dp) :: dado
   integer, INTENT(IN) :: j
   integer :: pos,densityOhm,intdado,i

   inter = 0.0_dp

   if(ndim == 1) then !WIRE
      if(geomwire == 1) then
         !DIMER
         inter(oscL,oscR) = coup
         inter(oscR,oscL) = coup
         write(*,*) 'Oscillator',j,'between position',oscL,'and',oscR
         if(((oscR)>=(NL+NC+1)).or.((oscL)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif
      elseif(geomwire == 2) then      
         !ATOM
         !Tchavdar's suggestion, same coupling but opposite signs for the two sides of the moving atom -O-O~0~O-O-
         !inter(atom,atom + 1) = abs(tchain)/acar !positive 
         !inter(atom + 1,atom) = abs(tchain)/acar 
         !inter(atom,atom - 1) = - abs(tchain)/acar !negative
         !inter(atom - 1,atom) = - abs(tchain)/acar

         inter(atom,atom + 1) = coup !positive 
         inter(atom + 1,atom) = coup 
         inter(atom,atom - 1) = - coup !negative
         inter(atom - 1,atom) = - coup
         write(*,*) 'Oscillator',j,'in position',atom,', between',atom+1,'and',atom-1
         if(((atom+1)>=(NL+NC+1)).or.((atom-1)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif  
      elseif(geomwire == 3) then 
         !STM
         inter(leftend,leftend + 1) = coup
         inter(leftend + 1,leftend) = coup
         write(*,*) 'Oscillator',j,'between position',leftend,'and',leftend+1
         if(((leftend+1)>=(NL+NC+1)).or.((leftend)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif
      elseif(geomwire == 4) then 
         !EINSTEIN
         !several oscillators, the j dependence of this routine here is essential
         pos = Lfirst + (j-1)*2
         inter(pos,pos+1) = coup
         inter(pos+1,pos) = coup
         write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
         if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif
      elseif(geomwire == 5) then
         !OHM, several Einstein oscillators at a uniform distance from each other
         if(nho .ne. 0) then    
            densityOhm = (NC-1)/nho !number of sites per oscillator, excluded the borders of NC
            pos = NL +2 + (j-1)*densityOhm
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
            if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 6) then
         !LEGION
         if(nho .ne. 0) then    
            call random_number(dado)
            !write(*,*) dado
            intdado = int(dado*(NC-2))
            !write(*,*) intdado
            pos=NL+1+intdado
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
            if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 7) then
         !FULL_CIRCLE, like OHM with PBC
         if(nho .ne. 0) then    
            densityOhm = (NC-1)/nho !number of sites per oscillator, excluded the borders of NC
            pos = NL +2 + (j-1)*densityOhm
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
            if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 8) then
         !DISSIPION, 2 fake oscillators Left, 2 Right, the following like OHM
         if(j == 1) then
            pos = 2
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'FAKE Oscillator',j,'between position',pos,'and',pos+1
         elseif(j == 2) then
            pos = 4
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'FAKE Oscillator',j,'between position',pos,'and',pos+1         
         elseif(j == 3) then
            pos = nlev - 4
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'FAKE Oscillator',j,'between position',pos,'and',pos+1         
         elseif(j == 4) then
            pos = nlev - 2
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'FAKE Oscillator',j,'between position',pos,'and',pos+1
         else
            if(nho > 4) then    
               densityOhm = (NC-1)/(nho-4) !number of sites per oscillator, excluded the borders of NC
               pos = NL +2 + (j-1-4)*densityOhm
               inter(pos,pos+1) = coup
               inter(pos+1,pos) = coup
               write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
               if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
                  write(*,*) 'ERROR, out of bounds, check the geometry'
                  stop
               endif
            endif         
         endif
      elseif(geomwire == 9) then
         !RANDSEA, the oscillators are coupled to all the sites within the central region with F = (0,coup_max(in input.dat)
         if(nho .ne. 0) then    
            do i=1,NC-2
               pos = NL + 1 + i
               !generate a random coup here, it will be dado
               call random_number(dado)       
               inter(pos,pos) = coup*dado
            enddo
            write(*,*) 'Oscillator',j,'spread between positions',NL + 2,'and',NL + NC - 1
         endif        
      endif
   elseif(ndim == 2) then !SURFACE
   elseif(ndim == 3) then !SOLID
      inter(j*4 +2,j*4+ 1) = coup
      inter(j*4 + 1,j*4 +2) = coup
      write(*,*) 'Oscillator',j,'between position',j*4+1,'and',j*4+1
   endif   

end subroutine buildInteraction

subroutine buildInteractionNM(inter,coup,j)
   
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: inter
   real(dp), INTENT(IN) :: coup
   complex(dp) :: factor
   integer, INTENT(IN) :: j
   integer :: i,pos

   inter = 0.0_dp
   
   factor = coup * 2.0_dp * im * sin(pi*real(j,dp)/real(nho,dp)) / sqrt(real(nho,dp))

   do i=1,nmring
      pos = NL + 1 + i
      inter(pos,pos+1) = factor * exp(-2.0_dp*pi*im*(real(i,dp)+0.5_dp)*real(j,dp)/real(nho,dp))
      inter(pos+1,pos) = inter(pos,pos+1)
      !Last piece of the ring , IS IT CORRECT?????????????????????? What 'i' do I need??????
      if (i == nmring) then 
         inter(NL+2+nmring,NL+2) = factor * exp(-2.0_dp*pi*im*(real(i+1,dp)+0.5_dp)*real(j,dp)/real(nho,dp))
         inter(NL+2,NL+2+nmring) = inter(NL+2+nmring,NL+2)
      endif
   enddo

end subroutine buildInteractionNM

!----------------------
!OB ROUTINES
!----------------------

subroutine buildF(effe,H,gamma)

!this is the operatorial part in G- = H_S0 + Sigma- + i I_s Delta

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: effe
   complex(dp), dimension(:,:), INTENT(IN) :: H
   real(dp), INTENT(IN) :: gamma
   integer :: i,j

   effe = H
   if(openboundaries == 1) then 
      do i = 1, nlev
         effe(i,i) = effe(i,i) + im*del
      enddo

      do i= 1,NL
         effe(i,i) = effe(i,i) + im*0.5_dp*gamma
      enddo

      do i= NL+NC+1,nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gamma
      enddo
   elseif(openboundaries == 2) then
      do i = 1, nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gamma
      enddo
   endif

end subroutine buildF

subroutine injection(evecL,evecR,lambda,gamma)

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: evecL,evecR !R and L eigenvectors of effe
   complex(dp), dimension(:), INTENT(IN) :: lambda        !the eigenvalues of effe
   
   complex(dp), allocatable :: inj(:,:),ccinj(:,:) 
   complex(dp), allocatable :: unoL(:),dueL(:) !buffer terms to speed up the calculation
   complex(dp), allocatable :: unoR(:),dueR(:)
   real(dp), allocatable :: lambdaRe(:),lambdaIm(:)
   real(dp), INTENT(IN) :: gamma   
   integer :: i,L,R,s,sp,flag
   
   !evaluation of the buffers
   allocate(lambdaRe(nlev))
   allocate(lambdaIm(nlev))
   allocate(unoL(nlev))
   allocate(unoR(nlev))
   allocate(dueL(nlev))
   allocate(dueR(nlev))
   lambdaRe = real(lambda,dp)
   lambdaIm = aimag(lambda)
   do i = 1,nlev
      unoL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
      unoR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
   enddo
   deallocate(lambdaRe)
   deallocate(lambdaIm)

   !evaluating the matrix elements s,s' of the injection term
   open(46, file='inj',form='unformatted')
   allocate(inj(nlev,nlev))
   inj = 0.0_dp

   do sp = 1,nlev
      do L=1,NL
         do i=1,nlev                      
            inj(L,sp) = inj(L,sp) + ( evecR(L,i) * conjg(evecL(sp,i)) ) * ( unoL(i) + dueL(i) )
         enddo              
      enddo
   enddo

   do sp = 1,nlev
      do R= NL+NC+1,nlev
         do i=1,nlev                      
            inj(R,sp) = inj(R,sp) + ( evecR(R,i) * conjg(evecL(sp,i)) ) * ( unoR(i) + dueR(i) )
         enddo              
      enddo
   enddo
   deallocate(unoL)
   deallocate(unoR)
   deallocate(dueL)
   deallocate(dueR)

   allocate(ccinj(nlev,nlev))
   ccinj = conjg(transpose(inj))
   inj = inj - ccinj
   inj = inj * (im*gamma) * (1.0_dp/(2.0_dp*pi*im))

   write(46) inj
   close(46)

end subroutine injection

end program diagH
