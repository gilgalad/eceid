program diagH

   !this program builds a hamiltonian for a TB system, diagonalizes it
   !and computes eigenvalues and eigenstates. It will mainly be used for getting the GS of a system, 
   !which, in the initial basis, is the first column of the outcoming H matrix

   !The file ham contains the hamiltonian
   !the file rhozero the DM with the first nlev/2 energy eigenstates of H as generating state

   !IMPORTANT this file has the injection terms calculated by summing each four of the terms,
   !it has the same result as Eunan 

   use modvar
   implicit none

   !LAPACK VARIABLES   
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   integer :: info

   !SYSTEM VARIABLES (use allocatable for avoiding problems with openMP)
   complex(dp), allocatable :: H(:,:),Hold(:,:),effe(:,:),effeOLD(:,:) !hamiltonian of the system
   complex(dp), allocatable :: HNC(:,:)
   real(dp), allocatable :: eigv(:),eigvNC(:)    !ordered eigevalue vector
   complex(dp), allocatable :: eigvaluesEffe(:)    !ordered eigevalue vector
   complex(dp), allocatable :: eigvectorsEffeL(:,:),eigvectorsEffeR(:,:) !Left and right eigenvectors of effe
   complex(dp), allocatable :: rhozero(:,:),rhozeroNC(:,:)
   complex(dp), allocatable :: G(:,:),Ginv(:,:)   
   integer :: i,j,s,sp,n
   real(dp) :: buffer,maxspace,avspace,E,elinelmfp,elelmfp
   real(dp) :: coup,FD,mass
   complex(dp), allocatable :: inter(:,:)
   integer, allocatable :: list(:)
   complex(dp), allocatable :: gaussbuf(:)
   complex(dp), allocatable :: gaussrhoH(:,:),temprho(:,:)
   complex(dp) :: gaussnor,gaussen,gaussen2

   !CV Calculations
   integer :: nstep,k
   real(dp) :: dtemp,Tmax,temperature,omega(nho)
   complex(dp), allocatable :: buf(:,:),energia(:,:)
   complex(dp) :: trace,trace2,alpha,beta,Eos,Eos2,fake

   !SCREEN OUTPUT REGARDING THE INFO OF THE SYSTEM
   write(*,*)
   write(*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~'
   write(*,*) 'This is the setup part for elPh release',release
   write(*,*)
   write(*,*) 'The system chosen is in', ndim,'dimension'
   if((ndim == 1).and.(molecular==0)) then
      write(*,*) 'It is a nanowire with',NL ,'sites in the leads and',NC,'sites in the central region'
   endif
   if((ndim == 1).and.(molecular==1)) then
      write(*,*) 'Molecular mode.'
      write(*,*) 'It is a nanowire with 2 orbitals per site. It has',NL/2 ,'sites in the leads and',NC/2,'sites in the central region'
      if(geomwire.ne.15) then
         write(*,*) 'ERROR, the only geometry supported now is 15: MOL2L'
         STOP
      endif
   endif
   write(*,*) 'Gamma for the Cs/c As/c operators is', gamFin,'eV'
   if(x3l.ne.0) then
      if(hf.ne.0) then
         write(*,*) 'ERROR, hf must be off'
         STOP
      endif
      if(Tcoup.ne.0.0_dp) then
         write(*,*) 'ERROR, Tcoup must be 0.0'
         STOP
      endif
      if(abs(gamFin)>0.0_dp) then
         write(*,*) 'ERROR, Gamma must be 0 in x3l mode'
         STOP
      endif
   endif
   if(openboundaries .ne. 0) then
      write(*,*) 'The OB are on and their gamma is',gamOB,'eV'
      if(openboundaries .eq. 11) then
         write(*,*) 'ELECTRON GUN MODE with injection from',egE-egW,'eV to',egE+egW,'eV.'
      endif
      if(shapeV .eq. 2) then
         write(*,*) 'GAUSSIAN PULSE, the injection is time limited by a gaussian centered on',egunmu,'fs with variance',egunsigma,'fs.'
      endif
   endif
   if(nmodes == 0) then
      write(*,*) 'The phonons are considered Einstein oscillators'
   elseif(nmodes == 1) then
      write(*,*) 'The phonons are considered Normal Modes'
   endif
   if(x3l == 1) then
      write(*,*) 'We are in X3L mode 1 (STM), comparison mode with the exact program, 1 electron and 1 oscillator'
      if(nho.ne.1) then
         write(*,*) 'ERROR, nho must be 1'
         STOP
      endif
   endif
   if(x3l == 2) then
      write(*,*) 'We are in X3L mode 2 (WELL), 1 electron in a wire with a well with oscillators'
      if((geomwire == 12).or.(geomwire == 13)) then
         if(mod(NC,2).ne.1) then
            write(*,*) 'ERROR, NC must be odd'
            STOP
         endif
      endif
      if(geomwire == 13) then
         if(nho > wwidth) then
            write(*,*) 'ERROR, nho must be less or equal than wwidth'
            STOP
         endif
         if(mod(wwidth,2).ne.1) then
            write(*,*) 'ERROR, wwidth must be odd'
            STOP
         endif
      endif
   endif
   if(x3l == 3) then
      write(*,*) 'We are in X3L2o mode (3L2P), comparison mode with the exact program, 1 electron, 3 levels and 2 oscillators'
      if(nho.ne.2) then
         write(*,*) 'ERROR, nho must be 2'
         STOP
      endif
      !if(nlev.ne.3) then
      !   write(*,*) 'ERROR, nlev must be 3'
      !   STOP
      !endif
   endif
   if(anderson == 1) then
      write(*,*) 'We are in ANDERSON localization mode. Ndot=0'
      write(*,*) 'The oscillators are a bath (T fixed), so <N> is determined by omega'
      write(*,*) 'The temperature of the ionic bath is', anderTbath,'K'
      if(openboundaries == 0) then
         write(*,*) 'ERROR, openboundaries must be on'
         STOP
      endif
      if(fixedN .ne. 1) then
         write(*,*) 'ERROR, Ndot must be fixed to 0 (for now)'
         STOP
      endif
   endif
   if(h2o .ne. 0) then
      write(*,*) 'We are in H2O mode.'
      write(*,*) 'There are',h2o,'molecules of water in the system'
      write(*,*) 'with',orbh2o,'orbitals each.'
      if(geomwire == 50) then
         if(orbh2o .ne. 5) then
            write(*,*) 'ERROR, orbh2o must be 5'
            STOP
         endif
      endif
      if(geomwire == 51) then
         if(orbh2o .ne. 6) then
            write(*,*) 'ERROR, orbh2o must be 6'
            STOP
         endif
      endif
      if((geomwire == 52).or.(geomwire == 53).or.(geomwire == 54)) then
         if(orbh2o .ne. 4) then
            write(*,*) 'ERROR, orbh2o must be 4'   
            STOP
         endif
      endif
      !if((nho .ne. 2*h2o).and.(nho .ne. 0)) then
      !   write(*,*) 'ERROR, nho is not twice the number of h2o molecules'   
         !STOP
      !endif
   endif
   write(*,*)

   !RANDOM NUMBER GENERATOR INITIALIZATION
   call init_random_seed()
   do i=1,100  !discard the first few numbers of the sequence
      call random_number(buffer)
      !write(*,*) buffer
   enddo
  
   !-----------------------------
   !BUILDING THE WIRE
    
   !Hamiltonian
   allocate(H(nlev,nlev))
   allocate(Hold(nlev,nlev))
   call buildH(H)
   Hold = H
    
   !for the OB, operatorial part of G-. It has to be diagonalised
   allocate(effe(nlev,nlev))
   allocate(effeOLD(nlev,nlev))   
   call buildF(effe,Hold,gamOB)
   effeOLD = effe 
   
   !building the coupling matrix with the oscillators (if present)
   if(nho .ne. 0) then
      open(62, file='coupHO',form='unformatted')   
      open(33, file='input.dat')
      allocate(inter(nlev,nlev))
      allocate(list(nho))
      list = 0
      do i=1,nho
         if(nmodes == 0) then !case of einstein oscillators
            read(33,*) coup, fake, omega(i),mass
            call buildInteraction(inter,coup,i,list)
            if(anderson == 1) then
               if(i == 1) then        
                  write(*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
                  write(*,*) 'Oscillator | site | frequency (eV) |   <N>   | coupling (eV/A) | mass (amu)'
               endif
               write(*,'(i7,i10,f14.3,f13.3,f14.3,f15.3)')  i,list(i),omega(i),(kb*anderTbath/omega(i))-0.5_dp,coup,mass  !Fake T for emfp
               !write(*,'(i7,i10,f14.3,f13.3,f14.3,f15.3)')  i,list(i),omega(i),1.0_dp/(exp(omega(i)/(kb*anderTbath))-1.0_dp),coup,mass  !WROONG
            endif
            omega(i) = omega(i)/hb
         elseif(nmodes == 1) then !!!!!!!!case of normal modes, the coupling and the omega are only the ones of the first line in input.dat
            if(i == 1) then        
               read(33,*) coup, fake, omega(1)
               write(*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
               write(*,*) 'Normal mode | frequency (eV) | maxcoup (eV/A) | site | mincoup (eV/A) | site'
            endif
            omega(i) = 2.0_dp*omega(1)*abs(sin(pi*real(i,dp)/real(nmnumber,dp)))                      
            call buildInteractionNM(inter,coup,i,omega(i))
            omega(i) = omega(i)/hb !fixing the dimension of omega to fs^-1
            write(*,*) '--------------'
         endif         
         write(62) inter
      enddo
      write(*,*) '--------------'
   endif
   close(62)

   !inelastic mean free path evaluation 
   if(geomwire == 14) then               
      buffer = ((omega(1))**2)*mass*amu/(kb*anderTbath)
      elinelmfp = (buffer*tchain*tchain) / (4.0_dp*coup*coup*real(1.0_dp/eqperiod,dp))
      write(*,*) 'The inelastic electron MFP is', elinelmfp
      if(anderdis > 0.0_dp) then
         elelmfp = 12.0_dp*tchain*tchain/(anderdis*anderdis)
         write(*,*) 'The elastic (disordered) electron MFP is', elelmfp
         write(*,*)
         write(*,*) 'The total electron MFP is', 1.0_dp/((1.0_dp/elelmfp)+(1.0_dp/elinelmfp))
      endif
   endif   

   !ALFREDOCV geometry where the N is set by alfredoT instead of input.dat
   if(geomwire == 19) then               
      write(*,*) 'The initial oscillator temperature is', alfredoT
   endif   

   !-----------------------------
   !LAPACK diagonalisations and storing the resulting eigenvectors/eigenvalues
   
   !diagonalize H and effe
   allocate(work(lwork))
   allocate(rwork(lrwork))
   allocate(iwork(liwork))
   allocate(eigv(nlev))   
   allocate(eigvaluesEffe(nlev))
   allocate(eigvectorsEffeL(nlev,nlev))
   allocate(eigvectorsEffeR(nlev,nlev))
   call zheevd('V','U',nlev,H,nlev,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
   !write(*,*) info
   call zgeev('V','V',nlev,effe,nlev,eigvaluesEffe,eigvectorsEffeL,nlev,eigvectorsEffeR,nlev, &
               work,LWORK,rwork,info)

   allocate(HNC(NC,NC))  !NEW, central part of the hamiltonian, to diagonalize
   allocate(eigvNC(NC))
   HNC = Hold(NL+1:NL+NC,NL+1:NL+NC)
   call zheevd('V','U',NC,HNC,NC,eigvNC,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
   open(49, file='hamNC',form='unformatted')
   write(49) HNC,eigvNC
   close(49)

   open(47, file='hamCentral.dat')
   do j = 1,NC
      do i = 1,NC
         !first index atomic base, second index energy base
         write(47,*) i,j,eigvNC(j)-chempot,real(HNC(i,j)*conjg(HNC(i,j)),dp)
      enddo
      write(47,*)
   enddo 
   close(47)

   deallocate(work)
   deallocate(rwork)
   deallocate(iwork)
   
   write(*,*)
   write(*,*) 'The electronic energy levels are:'
   write(*,*) eigv
   write(*,*)
   
   open(44, file='ham',form='unformatted')
   !H is real, I could store it as real, but it is used in Lapack complex multiplications, better to use it complex
   write(44) real(Hold,dp),H,eigv
   close(44)
   !deallocate(Hold)
   deallocate(effe)
   open(25, file='mapstates.dat')
   do i = 1,nlev
      do j = 1,nlev
         write(25,*) i,j,real(H(i,j)*conjg(H(i,j)),dp)
      enddo
      write(25,*)
   enddo  
   close(25)

   !----------------------------- 
   !VERY IMPORTANT renormalization
   !the default normalization is NOT what I need to be, I want the identity to appear when i multply 
   !a left and a right vector with the same index. Here I enforce this
   !i is the label of the eigenvectors, it corresponds to the i eigenvalue
   do i=1,nlev   
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      eigvectorsEffeL(:,i) = eigvectorsEffeL(:,i) / dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      eigvectorsEffeR(:,i) = eigvectorsEffeR(:,i) / dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*) dot_product(eigvectorsEffeL(:,i),eigvectorsEffeR(:,i))
      !write(*,*)
   enddo

   !computation of the average and minimum energy spacing in the system (the central region is 
   !smaller than he leads, so effectively this means in the leads)
   avspace = 0.0_dp
   maxspace = 0.0_dp
   do i=1,nlev-1
      buffer = eigv(i+1)-eigv(i)
      if(abs(buffer) > maxspace) then
         maxspace = buffer
      endif
      avspace = avspace + buffer
   enddo
   avspace = avspace/real(nlev-1,dp)
   write(*,*) 'The maximum energy spacing in S is', maxspace,'eV'
   write(*,*) 'The average energy spacing in S is', avspace,'eV' 

   !building the corresponding DM, rhozero
   !VERY IMPORTANT, it's the sum of nlev/2 DM of the filled band. It's got nlev/2 as a trace!!!
   open(45, file='rhozero',form='unformatted')
   allocate(rhozero(nlev,nlev))   
   rhozero = 0.0_dp 
   write(*,*) '~~~'
   if(Telrho == 0) then  
      do n=1,nlev/2
         do i = 1,nlev
            do j = 1,nlev
               rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )
            enddo
         enddo
      enddo
      !if the system has an odd number of electrons, like ATOM, I add "half" an electron more on the next excited state
      if(mod(NC,2) == 1) then   
         n = nlev/2 + 1
         do i = 1,nlev
            do j = 1,nlev
               rhozero(i,j) = rhozero(i,j) + 0.5_dp * ( conjg(H(j,n)) * H(i,n) )
            enddo
         enddo
      endif
   elseif(Telrho == 1) then  !the initial DM starts from an electronic temperature inTel
      write(*,*) 'The initial electronic Density matrix is at a temperature of', inTel, 'K'
      do n=1,nlev
         FD = 1.0_dp/( exp( eigv(n)/(kb*inTel) )  + 1.0_dp )
         !write(*,*) n,FD !TOGLIMI
         do i = 1,nlev
            do j = 1,nlev
               rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )*FD
            enddo
         enddo
      enddo
      !rhozero = 0.0_dp  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TOGLIMI
      !do n=1,nlev/2
      !   rhozero(n,n) = 1.0_dp
      !enddo
   !the following are x3l mode, one electron only!
   elseif(Telrho == 2) then  !the initial DM starts from an electronic TB state
      if(x3l==0) then
         write(*,*) 'ERROR, switch x3l ON'
         STOP
      endif
      write(*,*) 'COMPARISON MODE with x3l'
      write(*,*) 'TB Basis'
      write(*,*) 'The initial electronic Density matrix has 1 electron in TB site',initTB
      rhozero(initTB,initTB) = 1.0_dp
   elseif(Telrho == 3) then  !the initial DM starts from an electronic H0 state
      if(x3l==0) then
         write(*,*) 'ERROR, the switch x3l is off'
         STOP
      endif
      write(*,*) 'COMPARISON MODE with x3l'
      write(*,*) 'H0 Basis'
      write(*,*) 'The initial electronic Density matrix has 1 electron in H0 state', initx3l
      write(*,*) 'with energy', eigv(initx3l),'eV'
      write(*,*)
      write(*,*) 'The H0 energy levels are:'
      write(*,*) eigv
      write(*,*)
      do i = 1,nlev
         do j = 1,nlev
            !converting rhozero to the TB basis
            rhozero(i,j) = ( conjg(H(j,initx3l)) * H(i,initx3l) )
         enddo
      enddo
      write(*,*) 'Occupation of the electronic state left of the oscillator:',real(rhozero(leftend,leftend))
      write(*,*) 'Occupation of the electronic state right of the oscillator:',real(rhozero(leftend+1,leftend+1))
   elseif(Telrho == 15) then  !the initial DM starts from a custom configuration for MOLECULAR mode. All the 2s sites on the left populated
      write(*,*) 'The initial electronic Density matrix has 1 electron on every 2s state on the left part of the wire'
      do i = 2,leftend,2
         rhozero(i,i) = 1.0_dp
      enddo
   elseif(Telrho == 16) then  !the initial DM starts from a custom configuration for WELL2L, we start from the lower level of the well
      write(*,*) 'The initial electronic Density matrix has 1 electron on the upper level of the well'
      rhozero(atom,atom) = 1.0_dp
   elseif(Telrho == 20) then  !WATER, the initial DM starts populated TB states
      write(*,*) 'The initial electronic H2O Density matrix is at a temperature of', inTel, 'K'
      if(orbh2o == 5) then  !I have 6 electrons in total, for 5*2 states. +1 electron, the excited one
         !state 1= H1 1s
         !state 2-4= O 3p x,y,z
         !state 5= H2 1s
         rhozero(NL+1,NL+1) = 0.5_dp
         rhozero(NL+orbh2o,NL+orbh2o) = 0.5_dp
         rhozero(NL+2,NL+2) = 0.5_dp
         rhozero(NL+3,NL+3) = 1.0_dp
         rhozero(NL+4,NL+4) = 0.5_dp
      endif
   elseif(Telrho == 21) then  !WATER, the initial DM starts from an electronic temperature inTel
      write(*,*) 'The initial electronic H2O Density matrix is at a temperature of', inTel, 'K'
      !RIVEDI PERCHÉ PROBABILMENTE È SBAGLIATO
      if(orbh2o == 5) then  !I have 6 electrons in total, for 5*2 states. +1 electron, the excited one
         !do n=1,nlev
         !   FD = 1.0_dp/( exp( ( eigv(n) - (eigv(3)) ) /(kb*inTel) )  + 1.0_dp )
         !   !write(*,*) n,FD !TOGLIMI
         !   do i = 1,nlev
         !      do j = 1,nlev
         !         rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )*FD
         !      enddo
         !   enddo
         !enddo
         do n=1,3*h2o !3 states are fully populated, with spin they correspond to 6 electrons
                  !this correspond to a 0 temperature FD population
            do i = 1,nlev
               do j = 1,nlev
                  rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )
               enddo
            enddo
         enddo
      endif
   elseif(Telrho == 22) then  !WATER, the initial DM is filled until chempot
      write(*,*) 'The initial electronic H2O Density matrix has a chemical potential of', chempot, 'eV'
      do n=1,nlev !the states are fully populated until chempot
                  !this correspond to a 0 temperature FD population
         if(eigv(n)<=chempot) then
            do i = 1,nlev
               do j = 1,nlev
                  rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )
               enddo
            enddo
         endif
      enddo
   elseif(Telrho == 23) then  !WATER, the initial DM is filled with inTel around chempot
      write(*,*) 'The initial electronic H2O Density matrix has a chemical potential of', chempot, 'eV'
      write(*,*) 'The initial electronic Density matrix is at a temperature of', inTel, 'K'
      do n=1,nlev 
         FD = 1.0_dp/( exp( (eigv(n) - chempot)/(kb*inTel) )  + 1.0_dp )   
         !write(*,*) n,eigv(n),FD !TOGLIMI               
         do i = 1,nlev
            do j = 1,nlev
               rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )*FD
            enddo
         enddo
      enddo
   elseif(Telrho == 24) then  !WATER GAUSSIAN WP, only the lower levels of the water are occupied, the metal is empty except for a plane wave, completely different!
      write(*,*) 'The initial electronic H2O Density matrix has a chemical potential of', chempot, 'eV'
      write(*,*) 'Only the lower levels of the water are occupied'
      !filling the lower water levels
      do n=1,(h2o*orbh2o /2)          
         do i = 1,nlev
            do j = 1,nlev
               rhozero(i,j) = rhozero(i,j) + ( conjg(H(j,n)) * H(i,n) )*1.00_dp
            enddo
         enddo
      enddo

      allocate(gaussbuf(nlev))
      gaussnor = 0.0_dp
      do i = 1,nlev
         gaussbuf(i) = exp(-((i-gaussianwp_atom)**2)/(2.0_dp*gaussianwp_sigma**2))
         gaussnor = gaussnor + gaussbuf(i)*conjg(gaussbuf(i))
      enddo
      gaussnor = sqrt(real(gaussnor,dp))
      !write(*,*) gaussnor
      gaussbuf = gaussbuf / gaussnor

      allocate(gaussrhoH(nlev,nlev))
      allocate(temprho(nlev,nlev))
      !energia dell'acqua
      gaussrhoH = matmul(rhozero,Hold)
      gaussen = 0.0_dp
      do i = 1,nlev
         gaussen = gaussen + gaussrhoH(i,i)
      enddo

      do i = 1,nlev
         do j = 1,nlev
            !gaussbuf = (1.0_dp/(gaussianwp_sigma**2 * 2.0_dp * pi)) * exp(-((i-gaussianwp_atom)**2)/(2.0_dp*gaussianwp_sigma**2))
            !gaussbuf = exp(-((i-gaussianwp_atom)**2)/(2.0_dp*gaussianwp_sigma**2))
            !gaussbuf = gaussbuf * exp(-((j-gaussianwp_atom)**2)/(2.0_dp*gaussianwp_sigma**2))
            !gaussbuf = gaussbuf * exp(im * gaussianwp_kappa * real(i-j,dp))
            rhozero(i,j) = rhozero(i,j) + (gaussbuf(i) * conjg(gaussbuf(j))) * exp(im * gaussianwp_kappa * real(i-j,dp))
         enddo
      enddo

      !energia di questo pacchetto
      gaussrhoH = matmul(rhozero,Hold)
      gaussen2 = 0.0_dp
      do i = 1,nlev
         gaussen2 = gaussen2 + gaussrhoH(i,i)
      enddo
      gaussen2 = gaussen2 - gaussen - chempot !energia pachetto shiftata con la Fermi energy dell'acqua
      gaussen = 2.0_dp*tchain*(cos(gaussianwp_kappa) + 1) - chempot
      write(*,*)
      write(*,*) 'The k of the gaussian wavepacket makes its kinetic energy', real(gaussen,dp), 'eV'
      write(*,*)
      write(*,*) 'The energy the gaussian wavepacket is', real(gaussen2,dp), 'eV'

      !distribuzione energia pacchetto
      temprho = rhozero
      temprho = matmul(temprho,H)
      temprho = matmul(conjg(transpose(H)),temprho)
      open(24, file='gausswp_occ.dat') 
      do i = 1,nlev
         write(24,*) eigv(i)- chempot,abs(temprho(i,i))
      enddo        
    
   elseif(Telrho == 25) then  !WATER 1LOCC
      write(*,*) 'The initial electronic H2O Density matrix has a chemical potential of', chempot, 'eV'
      write(*,*) 'The lower levels of the water are occupied plus level',locc

      allocate(rhozeroNC(NC,NC))
      rhozeroNC = 0.0_dp
      !central region basis
      rhozeroNC = matmul(conjg(transpose(HNC)) ,rhozeroNC)
      rhozeroNC = matmul(rhozeroNC,HNC)

      !filling the lower water levels
      do n=1,(h2o*orbh2o /2)          
         rhozeroNC(n,n) = 1.0_dp
      enddo
      !extra level
      n = locc
      rhozeroNC(n,n) = 1.0_dp

      !change basis back to atomic
      rhozeroNC = matmul(HNC,rhozeroNC)
      rhozeroNC = matmul(rhozeroNC,conjg(transpose(HNC)))

      !immersion
      rhozero(NL+1:NL+NC,NL+1:NL+NC) = rhozeroNC
      
   endif
   write(45) rhozero
   close(45)
   !!!!!!!!!!!!!!!!!!!!!!!
            !do i = 1,nlev
            !   do j = 1,nlev
            !      write(*,*) i,j,real(rhozero(i,j),dp)
            !   enddo
            !enddo
            !do i = 1,nlev
            !      write(*,*) i,i,real(rhozero(i,i),dp)
            !enddo

   !NEW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! METTILA in una funzione con uno switch
   !Energy vs. Temperature plot (and then C_V) for the electrons
   if(specheat ==1) then
      write(*,*) '... Computing the Heat Capacity ...'
      open(13, file='entempel.dat')
      open(14, file='cvel.dat')
      open(15, file='entemposc.dat')
      open(16, file='cvos.dat')
      open(17, file='cv.dat')
      allocate(buf(nlev,nlev))
      allocate(energia(nlev,nlev))
      Tmax = 30000.0_dp !maximum temperature plotted
      dtemp = 10.0_dp !step in temperature for the plot
      nstep = int(Tmax/dtemp)
      do k=1,nstep  !I start from temperature dtemp, not 0K
         temperature = k*dtemp
         buf = 0.0_dp
         !buiding the DM for the temperature chosen 
         do n=1,nlev
            FD = 1.0_dp/( exp( eigv(n)/(kb*temperature) )  + 1.0_dp )
            do i = 1,nlev
               do j = 1,nlev
                  buf(i,j) = buf(i,j) + ( conjg(H(j,n)) * H(i,n) )*FD
               enddo
            enddo
         enddo
         !computing the electronic energy
         alpha = 1.0_dp
         beta = 0.0_dp
         call zgemm('N','N',nlev,nlev,nlev,alpha,Hold,nlev,buf,nlev,beta,energia,nlev)
         trace = 0.0_dp
         do i=1,nlev
            trace = trace + energia(i,i)
         enddo
         !SPIN!!
         if(x3l == 0) then
            trace = trace*2.0_dp
         endif
         if(k==1) trace2=trace
         write(13,*) temperature, real(trace,dp)
         write(14,*) temperature, real(trace - trace2,dp)/dtemp
         !write(*,*) temperature, real(trace,dp), real(trace - trace2,dp)/dtemp


         !Energy vs. Temperature plot (and then C_V) for the oscillators.
         eos = 0.0_dp      
         do j = 1,nho
            eos = eos + (0.5_dp*hb*omega(j) + hb*omega(j)*exp(-(hb*omega(j))/(kb*temperature))/(1.0_dp - exp(-(hb*omega(j))/(kb*temperature))) )
         enddo
         if(k==1) eos2=eos
         write(15,*) temperature, real(eos,dp)
         write(16,*) temperature, real(eos - eos2,dp)/dtemp
         write(17,*) temperature, real(trace - trace2,dp)/dtemp, real(eos - eos2,dp)/dtemp

         trace2 = trace
         eos2=eos
      enddo
   endif
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




   deallocate(rhozero)
   deallocate(H)
   deallocate(eigv) 
   
   !check if the wire is appropriate for the geometry chosen
   !modify it with other checks on gamma and other appropriate parameters
   !e.g. if gamma is too small, print warning
   if(geomwire == 1) then
      !DIMER, NC must be even -O-0~0-O-
      if(mod(NC,2).ne.0) then
         write(*,*)
         write(*,*) 'ERROR!!!!'
         write(*,*) 'Unappropriate NC for the geometry chosen'
         STOP
      endif
   elseif(geomwire == 2) then
      !ATOM, NC must be odd -O-O~0~O-O-
      if(mod(NC,2).ne.1) then
         write(*,*)
         write(*,*) 'ERROR!!!!'
         write(*,*) 'Unappropriate NC for the geometry chosen'
         STOP
      endif   
   endif
   if((geomwire < 100) .and. (nmodes == 1)) then
      write(*,*)
      write(*,*) 'ERROR!!!!'
      write(*,*) 'Unappropriate geometry, normal modes require a geometry label > 99'
      STOP
   elseif((geomwire > 100) .and. (nmodes == 0)) then
      write(*,*)
      write(*,*) 'ERROR!!!!'
      write(*,*) 'Unappropriate geometry, Einstein oscillators require a geometry label < 100'
      STOP
   endif

   !----------------------------- 
   !OB injection term
   if(openboundaries .ne. 0) then
      call injection(eigvectorsEffeL,eigvectorsEffeR,eigvaluesEffe,gamOB)
   endif

   
!-------------------END MAIN PROGRAM-------------------------


contains

!----------------------
!GEOMETRY of the WIRE ROUTINES
!----------------------
!change these if you want to add another configuration of the 1D wire
!add another case for the parameter geomwire

subroutine buildH(H)
   
   !chain of TB orbitals with the same hopping, without any imaginary term
   !it needs explicit modifications if we want the central region to be any different
   use modvar
   implicit none

   real(dp) :: andrand,Vr0,Vcut
   complex(dp), dimension(:,:), INTENT(OUT) :: H
   integer :: i,j,k,element,iter,orbital

   H = 0.0_dp

   do i=1,nlev
      H(i,i) = onsite
   enddo

   !couplings chain (the wrong terms will be corrected below)
   do i = 1,nlev-1
      H(i,i+1) = tchain
   enddo
   do i = 2,nlev
      H(i,i-1) = tchain
   enddo

   if(ndim == 1) then !WIRE
      if(geomwire == 1) then
         !DIMER
         write(*,*) 'The geometry chosen is DIMER'
         !couplings dimer
         H(oscL,oscR) = tdimer
         H(oscR,oscL) = tdimer   
         
         !couplings chain-dimer  
         H(oscL-1,oscL) = tcd
         H(oscL,oscL-1) = tcd
         H(oscR,oscR+1) = tcd
         H(oscR+1,oscR) = tcd
      elseif(geomwire == 2) then
         !ATOM: SINGLE ATOM OSCILLATING COUPLED THE SAME  
         write(*,*) 'The geometry chosen is ATOM'
         !do nothing: H is a perfect chain
         H(atom,atom) = onsiteAtom
      elseif(geomwire == 3) then
         !STM, perfect wire with a weaker bond in the middle
         write(*,*) 'The geometry chosen is STM'
         H(leftend,leftend + 1) = middle
         H(leftend + 1,leftend) = middle
         write(*,*) 'The coupling between sites',leftend,'and',leftend+1,'is',middle,'eV'
      elseif(geomwire == 4) then
         !EINSTEIN, several Einstein oscillators every 2 TB sites
         write(*,*) 'The geometry chosen is EINSTEIN'
         !for now, perfect chain
      elseif(geomwire == 5) then
         !OHM, several Einstein oscillators at a uniform distance from each other
         write(*,*) 'The geometry chosen is OHM'
         !for now, perfect chain
      elseif(geomwire == 6) then
         !LEGION, several Einstein oscillators at random position in the central region excluding the boundaries
         write(*,*) 'The geometry chosen is LEGION'
         !for now, perfect chain
      elseif(geomwire == 7) then
         !FULL_CIRCLE, like OHM but for a wire with PBC, the last site is connected to the first in the hamiltonian
         write(*,*) 'The geometry chosen is FULL_CIRCLE'
         H(1,nlev) = tchain
         H(nlev,1) = tchain
      elseif(geomwire == 8) then
         !DISSIPION, the first 4 oscillators are placed 2 by 2 in the leads to replace gamma
         write(*,*) 'The geometry chosen is DISSIPION'
      elseif(geomwire == 9) then
         !RANDSEA, the oscillators are coupled to all the sites within the central region with F = (0,coup_max(in input.dat)
         write(*,*) 'The geometry chosen is RANDSEA'
         !for now, perfect chain
      elseif(geomwire == 10) then
         !if 10, TURIN, wire to test the scent theory
         write(*,*) 'The geometry chosen is TURIN'
         H(leftend,leftend + 1) = middle
         H(leftend + 1,leftend) = middle         
      elseif(geomwire == 11) then
         !ANDERRAND
         if(anderson .ne. 1) then 
            write(*,*) 'ERROR, turn on anderson'
            STOP
         endif
         do i=NL+1,NL+NC  !random onsite energies in the central region
            call random_number(andrand)
            H(i,i) = (1.0_dp - 2.0_dp*andrand)*anderdis
            write(12,*) andrand,H(i,i)
         enddo
      elseif(geomwire == 12) then
         !WELL, Jorge's idea of a single level with less energy in the middle of the wire
         write(*,*) 'The geometry chosen is WELL'
         H(atom,atom) = onsiteAtom
      elseif(geomwire == 13) then
         !WELLS, Jorge's idea of several single level (large well) with less energy in the middle of the wire
         write(*,*) 'The geometry chosen is WELLS'
         H(atom,atom) = onsiteAtom
         iter = (wwidth - 1)/2
         do i=1,iter
            H(atom - i, atom - i) = onsiteAtom
            H(atom + i, atom + i) = onsiteAtom
         enddo
      elseif(geomwire == 14) then
         !ANDEREQUAL, Anderson dsordered system with equally spaced oscillators
         write(*,*) 'The geometry chosen is ANDEREQUAL, Anderson disordered system with equally spaced oscillators'
         write(*,*) 'Anderson disorder is',Anderdis
         write(*,*) 'The oscillator spacing is',eqperiod
         write(*,*) 'In this geometry the oscillators are coupled atomically, to the right with + and to the left with -.' 
         if(anderson .ne. 1) then 
            write(*,*) 'ERROR, turn on anderson'
            STOP
         endif
         do i=NL+1,NL+NC  !random onsite energies in the central region
            call random_number(andrand)
            H(i,i) = (1.0_dp - 2.0_dp*andrand)*anderdis
            write(12,*) andrand,H(i,i)
         enddo
      elseif(geomwire == 15) then
         !MOL2L
         if(molecular == 0) then 
            write(*,*) 'ERROR, turn on molecular'
            STOP
         endif
         H = 0.0_dp  !start again with the hamiltonian, it's very different compared to the single orbital wire one

         !onsite energies          
         do i=1,nlev
            if(mod(i,2)==1) then  !orbital 1s
               H(i,i) = onsite1s
            elseif(mod(i,2)==0) then  !orbital 2s
               H(i,i) = onsite2s
            endif
         enddo

         !hoppings 1s->1s and 2s->2s are the only ones allowed
         do i = 1,nlev-2
            if(mod(i,2)==1) then  !orbital 1s
               H(i,i+2) = tchain1s
            elseif(mod(i,2)==0) then  !orbital 2s
               H(i,i+2) = tchain2s
            endif
         enddo
         do i = 3,nlev
            if(mod(i,2)==1) then  !orbital 1s
               H(i,i-2) = tchain1s
            elseif(mod(i,2)==0) then  !orbital 2s
               H(i,i-2) = tchain2s
            endif
         enddo

         H(29,31) = 0.1_dp         !!!!!!!!!!!!
         H(31,29) = 0.1_dp
         H(30,32) = 0.1_dp         
         H(32,30) = 0.1_dp
      elseif(geomwire == 16) then
         !WELL2L
         write(*,*) 'The geometry chosen is WELL2L'
         H(atom-1,atom-1) = wellshift
         H(atom,atom) = wellshift + onsiteAtom
      
         !coupling of the well to the chain
         H(atom-2,atom-1) = middle  
         H(atom-1,atom-2) = middle
         H(atom-1,atom+1) = middle  
         H(atom+1,atom-1) = middle  

         !coupling inside the well
         H(atom,atom-1) = 0.00_dp  
         H(atom-1,atom) = 0.00_dp           

         !the bottom level of the well is isolated from the chain
         H(atom,atom+1) = 0.00_dp  
         H(atom+1,atom) = 0.00_dp
      elseif(geomwire == 17) then
         !ATOMS
         write(*,*) 'The geometry chosen is ATOMS'
         !for now, perfect chain
      elseif(geomwire == 18) then
         !BONDS
         write(*,*) 'The geometry chosen is BONDS'
         !for now, perfect chain
      elseif(geomwire == 19) then
         !ALFREDOCV, as many atomic oscillators as electrons in the central region
         write(*,*) 'The geometry chosen is ALFREDOCV'
         !for now, perfect chain   
      elseif(geomwire == 20) then
         !2L1P
         write(*,*) 'The geometry chosen is 2L1P'
         !onsite energies
         H(oscL,oscL) = 0.0_dp
         H(oscR,oscR) = onsite
         write(*,*) 'Site',oscL,'has onsite energy',0
         write(*,*) 'Site',oscR,'has onsite energy',onsite,'eV'
      elseif(geomwire == 21) then
         !RINGWELL, like WELL2L but with the end of the wire connected in a ring
         write(*,*) 'The geometry chosen is RINGWELL2L' 
         H(atom-1,atom-1) = wellshift
         H(atom,atom) = wellshift + onsiteAtom
      
         !coupling of the well to the chain
         H(atom-2,atom-1) = middle  
         H(atom-1,atom-2) = middle
         H(atom-1,atom+1) = middle  
         H(atom+1,atom-1) = middle  

         !coupling inside the well
         H(atom,atom-1) = 0.00_dp  
         H(atom-1,atom) = 0.00_dp           

         !the bottom level of the well is isolated from the chain
         H(atom,atom+1) = 0.00_dp  
         H(atom+1,atom) = 0.00_dp

         H(1,nlev) = tchain
         H(nlev,1) = tchain
      elseif(geomwire == 22) then
         !3L2P
         write(*,*) 'The geometry chosen is 3L2'
         !onsite energies
         H(1,1) = onsiteE1
         H(2,2) = onsiteE2
         H(3,3) = onsiteE3
         write(*,*) 'Site 1 has onsite energy',onsiteE1,'eV'
         write(*,*) 'Site 2 has onsite energy',onsiteE2,'eV'
         write(*,*) 'Site 3 has onsite energy',onsiteE3,'eV'

         !zero hoppings
         H(1,2) = 0.0_dp         
         H(2,1) = 0.0_dp         
         H(2,3) = 0.0_dp         
         H(3,2) = 0.0_dp         
         H(1,3) = 0.0_dp         
         H(3,1) = 0.0_dp 
      elseif(geomwire == 23) then
         !3LBond
         write(*,*) 'The geometry chosen is 3L1PBond'
         !onsite energies
         H(1,1) = onsiteE1
         H(2,2) = onsiteE2
         H(3,3) = onsiteE3
         write(*,*) 'Site 1 has onsite energy',onsiteE1,'eV'
         write(*,*) 'Site 2 has onsite energy',onsiteE2,'eV'
         write(*,*) 'Site 3 has onsite energy',onsiteE3,'eV'

         !zero hoppings
         H(1,2) = 0.0_dp         
         H(2,1) = 0.0_dp         
         H(2,3) = 0.0_dp         
         H(3,2) = 0.0_dp         
         H(1,3) = 0.0_dp         
         H(3,1) = 0.0_dp 
      elseif(geomwire == 24) then
         !3LRINGWELL
         write(*,*) 'The geometry chosen is 3LRing' 
         !onsite energies
         H(atom-1,atom-1) = onsiteE1
         H(atom,atom) =     onsiteE2
         H(atom+1,atom+1) = onsiteE3
         write(*,*) 'Site 1 has onsite energy',onsiteE1,'eV'
         write(*,*) 'Site 2 has onsite energy',onsiteE2,'eV'
         write(*,*) 'Site 3 has onsite energy',onsiteE3,'eV'

         !zero hoppings in the 3 levels
         H(atom-1,atom) = 0.0_dp         
         H(atom,atom-1) = 0.0_dp         
         H(atom,atom+1) = 0.0_dp         
         H(atom+1,atom) = 0.0_dp         
         H(atom-1,atom+1) = 0.0_dp         
         H(atom+1,atom-1) = 0.0_dp 
      
         !coupling of levels to the chain
         H(atom-2,atom-1) = tchainw  !left
         H(atom-2,atom) = tchainw
         H(atom-2,atom+1) = tchainw  
         H(atom-1,atom-2) = tchainw
         H(atom,atom-2) = tchainw
         H(atom+1,atom-2) = tchainw  
         H(atom+2,atom-1) = tchainw  !right
         H(atom+2,atom) = tchainw
         H(atom+2,atom+1) = tchainw  
         H(atom-1,atom+2) = tchainw
         H(atom,atom+2) = tchainw
         H(atom+1,atom+2) = tchainw  

         !closing the ring
         H(1,nlev) = tchain
         H(nlev,1) = tchain
      elseif(geomwire == 44) then
         !4L
         H = 0.0_dp
         write(*,*) 'The geometry chosen is 4L'
         !onsite energies
         H(1,1) = onsiteE1
         H(2,2) = onsiteE2
         H(3,3) = onsiteE2
         H(4,4) = onsiteE3
         write(*,*) 'Site 1 has onsite energy',onsiteE1,'eV'
         write(*,*) 'Site 2 has onsite energy',onsiteE2,'eV'
         write(*,*) 'Site 3 has onsite energy',onsiteE2,'eV'
         write(*,*) 'Site 4 has onsite energy',onsiteE3,'eV'

         !one weak hopping between the 2-level systems
         H(2,3) = tchainw
         H(3,2) = tchainw
      elseif(geomwire == 45) then
         !4Lelimbo
         write(*,*) 'The geometry chosen is 4Lelimbo'
         !onsite energies
         H(1,1) = onsiteE1
         H(nlev,nlev) = onsiteE3

         write(*,*) 'Site 1 has onsite energy',onsiteE1,'eV'
         write(*,*) 'Site nlev has onsite energy',onsiteE3,'eV'

         H(1,2) = 0.0_dp         
         H(2,1) = 0.0_dp   
         H(nlev-1,nlev) = 0.0_dp         
         H(nlev,nlev-1) = 0.0_dp   

         H(2,3) = tchain        
         H(3,2) = tchain
         H(nlev-1,nlev-2) = tchain
         H(nlev-2,nlev-1) = tchain
      elseif(geomwire == 46) then
         !SemicImp
         write(*,*) 'The geometry chosen is SemicImp'
         !onsite energies
         H(1,1) = onsiteE1
         H(nlev,nlev) = onsiteE1

         write(*,*) 'Site 1 has onsite energy',onsiteE1,'eV'
         write(*,*) 'Site nlev has onsite energy',onsiteE1,'eV'

         H(1,2) = 0.0_dp                  
         H(2,1) = 0.0_dp            
         H(nlev-1,nlev) = 0.0_dp         
         H(nlev,nlev-1) = 0.0_dp   

         do i=2,nlev-1
            if(mod(i,2)==0) then  
               !H(i,i+1) = tchain
               !H(i+1,i) = tchain
               H(i,i) = onsiteE2
            elseif(mod(i,2)==1) then  
               !H(i,i+1) = tchain2
               !H(i+1,i) = tchain2
               H(i,i) = onsiteE3
            endif
         enddo
      !----------------------- WATER
      elseif(geomwire == 50) then
         !Water
         write(*,*) 'The geometry chosen is Water'

         H = 0.0_dp

         !LEADS (disconnected from the system anyway)
         do i = 1,NL-1
            H(i,i+1) = tchain
            H(i,i) = chempot
         enddo
         do i = NL+NC+1,nlev-1
            H(i,i+1) = tchain
            H(i,i) = chempot
         enddo
         do i = 2,NL
            H(i,i-1) = tchain
            H(i,i) = chempot
         enddo
         do i = NL+NC+2,nlev
            H(i,i-1) = tchain
            H(i,i) = chempot
         enddo

         !LEADS, connection to the system
         H(NL,NL+1) = tchainw
         H(NL+1,NL) = tchainw
         H(NL+NC,NL+NC+1) = tchainw
         H(NL+NC+1,NL+NC) = tchainw

         !state 1= H1 1s
         !state 2-4= O 3p x,y,z
         !state 5= H2 1s

         !EACH water
         do i=1,h2o
            !Onsite
            j = (i-1)*orbh2o
            H(NL+j+1,NL+j+1) = EHs
            H(NL+j+2,NL+j+2) = EO2p !x
            H(NL+j+3,NL+j+3) = EO2p !y
            H(NL+j+4,NL+j+4) = EO2p !z
            H(NL+j+5,NL+j+5) = EHs
            
            if(mod(i,2)==1) then
               H(NL+j+1,NL+j+2) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)
!write(*,*) tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) )
!write(*,*) H(NL+j+1,NL+j+2)

               H(NL+j+1,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)   !hopping H1 1s - O 2pz
               H(NL+j+4,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)
!write(*,*) H(NL+j+1,NL+j+4)
            elseif(mod(i,2)==0) then
               H(NL+j+1,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)

               H(NL+j+1,NL+j+4) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)   !hopping H1 1s - O 2pz
               H(NL+j+4,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)
            endif

            H(NL+j+5,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)  !hopping H2 1s - O 2px
            H(NL+j+2,NL+j+5) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)
!write(*,*) H(NL+j+5,NL+j+2)
            H(NL+j+5,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)   !hopping H2 1s - O 2pz
            H(NL+j+4,NL+j+5) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)
!write(*,*) H(NL+j+5,NL+j+4)
         enddo

         !hydrogen bonding PROBABLY WRONG
         do i=2,h2o
            !Onsite
            j = (i-1)*orbh2o !H2 1s of the water on the left

            H(NL+j,NL+j+2) = -tsp* ( (hb*hb)/((amu/masselectron)*Rhydbond*Rhydbond) ) * cos(theta)  !hopping H1 1s (LEFT) - O 2px (RIGHT)
            H(NL+j+2,NL+j) = -tsp* ( (hb*hb)/((amu/masselectron)*Rhydbond*Rhydbond) ) * cos(theta)

            H(NL+j,NL+j+4) = -tsp* ( (hb*hb)/((amu/masselectron)*Rhydbond*Rhydbond) ) * sin(theta)  !hopping H1 1s (LEFT) - O 2pz (RIGHT)
            H(NL+j+4,NL+j) = -tsp* ( (hb*hb)/((amu/masselectron)*Rhydbond*Rhydbond) ) * sin(theta)
         enddo
      !WATER with also O 2s
      elseif(geomwire == 51) then
         !Water
         write(*,*) 'The geometry chosen is Water'

         H = 0.0_dp

         !LEADS (disconnected from the system anyway)
         do i = 1,NL-1
            H(i,i+1) = tchain
            H(i,i) = chempot
         enddo
         do i = NL+NC+1,nlev-1
            H(i,i+1) = tchain
            H(i,i) = chempot
         enddo
         do i = 2,NL
            H(i,i-1) = tchain
            H(i,i) = chempot
         enddo
         do i = NL+NC+2,nlev
            H(i,i-1) = tchain
            H(i,i) = chempot
         enddo

         !LEADS, connection to the system
         H(NL,NL+1) = tchainw
         H(NL+1,NL) = tchainw
         H(NL+NC,NL+NC+1) = tchainw
         H(NL+NC+1,NL+NC) = tchainw

         !state 1= H1 1s
         !state 2-4= O 2p x,y,z
         !state 5= H2 1s
         !state 6= O 2s

         !EACH water
         do i=1,h2o
            !Onsite
            j = (i-1)*orbh2o
            H(NL+j+1,NL+j+1) = EHs
            H(NL+j+2,NL+j+2) = EO2p !x
            H(NL+j+3,NL+j+3) = EO2p !y
            H(NL+j+4,NL+j+4) = EO2p !z
            H(NL+j+5,NL+j+5) = EHs
            H(NL+j+6,NL+j+6) = EO2s
            
            if(mod(i,2)==1) then
               H(NL+j+1,NL+j+2) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)

               H(NL+j+1,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)   !hopping H1 1s - O 2pz
               H(NL+j+4,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)
            elseif(mod(i,2)==0) then
               H(NL+j+1,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)

               H(NL+j+1,NL+j+4) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)   !hopping H1 1s - O 2pz
               H(NL+j+4,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)
            endif

            H(NL+j+5,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)  !hopping H2 1s - O 2px
            H(NL+j+2,NL+j+5) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)

            H(NL+j+5,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)   !hopping H2 1s - O 2pz
            H(NL+j+4,NL+j+5) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)

            H(NL+j+6,NL+j+1) = tss* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) !hopping H1 1s - O 2s
            H(NL+j+1,NL+j+6) = tss* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) )

            H(NL+j+6,NL+j+5) = tss* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) !hopping H2 1s - O 2s
            H(NL+j+5,NL+j+6) = tss* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) )  

         enddo

         !hydrogen bonding PROBABLY WRONG
         do i=2,h2o
            !Onsite
            j = (i-1)*orbh2o !H2 1s of the water on the left

            H(NL+j,NL+j+2) = -tsp* ( (hb*hb)/((amu/masselectron)*Rhydbond*Rhydbond) ) * cos(theta)  !hopping H1 1s (LEFT) - O 2px (RIGHT)
            H(NL+j+2,NL+j) = -tsp* ( (hb*hb)/((amu/masselectron)*Rhydbond*Rhydbond) ) * cos(theta)

            H(NL+j,NL+j+4) = -tsp* ( (hb*hb)/((amu/masselectron)*Rhydbond*Rhydbond) ) * sin(theta)  !hopping H1 1s (LEFT) - O 2pz (RIGHT)
            H(NL+j+4,NL+j) = -tsp* ( (hb*hb)/((amu/masselectron)*Rhydbond*Rhydbond) ) * sin(theta)
         enddo

      !WATER without O py
      elseif(geomwire == 52) then
         !Water
         write(*,*) 'The geometry chosen is Water'

         H = 0.0_dp

         !LEADS (disconnected from the system anyway)
         do i = 1,NL-1
            H(i,i+1) = tchain
            H(i,i) = chempot
         enddo
         do i = NL+NC+1,nlev-1
            H(i,i+1) = tchain
            H(i,i) = chempot
         enddo
         do i = 2,NL
            H(i,i-1) = tchain
            H(i,i) = chempot
         enddo
         do i = NL+NC+2,nlev
            H(i,i-1) = tchain
            H(i,i) = chempot
         enddo

         !LEADS, connection to the system
         H(NL,NL+1) = tchainw
         H(NL+1,NL) = tchainw
         H(NL+NC,NL+NC+1) = tchainw
         H(NL+NC+1,NL+NC) = tchainw

         !state 1= H1 1s
         !state 2-3= O 2p x,z
         !state 4= H2 1s

         !EACH water
         do i=1,h2o
            !Onsite
            j = (i-1)*orbh2o
            H(NL+j+1,NL+j+1) = EHs
            H(NL+j+2,NL+j+2) = EO2p !x
            H(NL+j+3,NL+j+3) = EO2p !z
            H(NL+j+4,NL+j+4) = EHs
            
            if(mod(i,2)==1) then
               H(NL+j+1,NL+j+2) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)

               H(NL+j+1,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)   !hopping H1 1s - O 2pz
               H(NL+j+3,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)
            elseif(mod(i,2)==0) then
               H(NL+j+1,NL+j+2) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(3.0_dp*theta)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(3.0_dp*theta)

               H(NL+j+1,NL+j+3) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(3.0_dp*theta)   !hopping H1 1s - O 2pz
               H(NL+j+3,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(3.0_dp*theta)
            endif

            H(NL+j+4,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)  !hopping H2 1s - O 2px
            H(NL+j+2,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * cos(theta)

            H(NL+j+4,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)   !hopping H2 1s - O 2pz
            H(NL+j+3,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) ) * sin(theta)

         enddo

         !hydrogen bonding from Jorge's article
         do i=2,h2o
            !Onsite
            j = (i-1)*orbh2o !H2 1s of the water on the left

            Vr0 = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*ROH*ROH) )
            Vcut = ((ROH/Rhydbond)**2) * exp( 2.0_dp*(-(Rhydbond/(ROH*Rcutoff))**4 + (1.0_dp/Rcutoff)**4 ) )

            H(NL+j,NL+j+2) = - Vr0 * Vcut * cos(theta)  !hopping H2 1s (LEFT) - O 2px (RIGHT)
            H(NL+j+2,NL+j) = - Vr0 * Vcut * cos(theta)

            H(NL+j,NL+j+3) = - Vr0 * Vcut * sin(theta)  !hopping H2 1s (LEFT) - O 2pz (RIGHT)
            H(NL+j+3,NL+j) = - Vr0 * Vcut * sin(theta)

            write(*,*) H(NL+j,NL+j+2)
         enddo

      !CP2K_WATER
      elseif(geomwire == 53) then
         !Water
         write(*,*) 'The geometry chosen is CP2K_Water'

         H = 0.0_dp

         !LEADS (disconnected from the system anyway)
         do i = 1,NL-1
            H(i,i+1) = tchain
            H(i,i) = chempot
         enddo
         do i = NL+NC+1,nlev-1
            H(i,i+1) = tchain
            H(i,i) = chempot
         enddo
         do i = 2,NL
            H(i,i-1) = tchain
            H(i,i) = chempot
         enddo
         do i = NL+NC+2,nlev
            H(i,i-1) = tchain
            H(i,i) = chempot
         enddo

         !LEADS, connection to the system
         !on the left I connect H1, on the right H2
         H(NL,NL+1) = tchainw
         H(NL+1,NL) = tchainw
         H(NL+NC,NL+NC+1) = tchainw
         H(NL+NC+1,NL+NC) = tchainw


         !FOR ALL THE MOLECULES
         !state 1= H1 1s (points out of the chain)
         !state 2-3= O 2p x,z
         !state 4= H2 1s (point in the chain, hydrogen bond)

         !EACH water
         do i=1,h2o
            !Onsite
            j = (i-1)*orbh2o
            H(NL+j+1,NL+j+1) = EHs
            H(NL+j+2,NL+j+2) = EO2p !x
            H(NL+j+3,NL+j+3) = EO2p !z
            H(NL+j+4,NL+j+4) = EHs
            
            if(mod(i,2)==1) then
               H(NL+j+1,NL+j+2) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * cos(theta_cp2k)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * cos(theta_cp2k)

               H(NL+j+1,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * sin(theta_cp2k)   !hopping H1 1s - O 2pz
               H(NL+j+3,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * sin(theta_cp2k)

               H(NL+j+4,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * cos(theta_cp2k)  !hopping H2 1s - O 2px
               H(NL+j+2,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * cos(theta_cp2k)

               H(NL+j+4,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * sin(theta_cp2k)   !hopping H2 1s - O 2pz
               H(NL+j+3,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * sin(theta_cp2k)
            elseif(mod(i,2)==0) then
               !I invert H1 with H2, for molecules with an even label in the chain
               H(NL+j+4,NL+j+2) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * cos(theta_cp2k)  !hopping H2 1s - O 2px
               H(NL+j+2,NL+j+4) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * cos(theta_cp2k)

               H(NL+j+4,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * sin(theta_cp2k)   !hopping H2 1s - O 2pz
               H(NL+j+3,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * sin(theta_cp2k)

               H(NL+j+1,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * cos(theta_cp2k)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * cos(theta_cp2k)

               H(NL+j+1,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * sin(theta_cp2k)   !hopping H1 1s - O 2pz
               H(NL+j+3,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * sin(theta_cp2k)
            endif

         enddo

         !hydrogen bonding from Jorge's article
         do i=2,h2o
            !Onsite
            j = (i-1)*orbh2o !H2 1s of the water on the left, the only hdrogen involved in the hydrogen bond

            H(NL+j,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*(R_OO-R_OH2)*(R_OO-R_OH2)) )  !hopping H2 1s (LEFT) - O 2pz (RIGHT)
            H(NL+j+3,NL+j) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*(R_OO-R_OH2)*(R_OO-R_OH2)) )
         enddo

      !WATER GAUSSIAN WP
      elseif(geomwire == 54) then
         !Water
         write(*,*) 'The geometry chosen is CP2K_Water GAUSSIAN WP'

         H = 0.0_dp

         !LEADS (disconnected from the system anyway)
         do i = 1,NL-1
            H(i,i+1) = tchain
            H(i,i) = chempot + gaussianwp_shift !!!!!!!!HERE it is different
         enddo
         do i = NL+NC+1,nlev-1
            H(i,i+1) = tchain
            H(i,i) = chempot + gaussianwp_shift !!!!!!!!HERE it is different
         enddo
         do i = 2,NL
            H(i,i-1) = tchain
            H(i,i) = chempot + gaussianwp_shift !!!!!!!!HERE it is different
         enddo
         do i = NL+NC+2,nlev
            H(i,i-1) = tchain
            H(i,i) = chempot + gaussianwp_shift !!!!!!!!HERE it is different
         enddo

         !LEADS, connection to the system
         !on the left I connect H1, on the right H2
         H(NL,NL+1) = tchainw
         H(NL+1,NL) = tchainw
         H(NL+NC,NL+NC+1) = tchainw
         H(NL+NC+1,NL+NC) = tchainw


         !FOR ALL THE MOLECULES
         !state 1= H1 1s (points out of the chain)
         !state 2-3= O 2p x,z
         !state 4= H2 1s (point in the chain, hydrogen bond)

         !EACH water
         do i=1,h2o
            !Onsite
            j = (i-1)*orbh2o
            H(NL+j+1,NL+j+1) = EHs
            H(NL+j+2,NL+j+2) = EO2p !x
            H(NL+j+3,NL+j+3) = EO2p !z
            H(NL+j+4,NL+j+4) = EHs
            
            if(mod(i,2)==1) then
               H(NL+j+1,NL+j+2) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * cos(theta_cp2k)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * cos(theta_cp2k)

               H(NL+j+1,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * sin(theta_cp2k)   !hopping H1 1s - O 2pz
               H(NL+j+3,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * sin(theta_cp2k)

               H(NL+j+4,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * cos(theta_cp2k)  !hopping H2 1s - O 2px
               H(NL+j+2,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * cos(theta_cp2k)

               H(NL+j+4,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * sin(theta_cp2k)   !hopping H2 1s - O 2pz
               H(NL+j+3,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * sin(theta_cp2k)
            elseif(mod(i,2)==0) then
               !I invert H1 with H2, for molecules with an even label in the chain
               H(NL+j+4,NL+j+2) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * cos(theta_cp2k)  !hopping H2 1s - O 2px
               H(NL+j+2,NL+j+4) = -tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * cos(theta_cp2k)

               H(NL+j+4,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * sin(theta_cp2k)   !hopping H2 1s - O 2pz
               H(NL+j+3,NL+j+4) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH2*R_OH2) ) * sin(theta_cp2k)

               H(NL+j+1,NL+j+2) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * cos(theta_cp2k)  !hopping H1 1s - O 2px
               H(NL+j+2,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * cos(theta_cp2k)

               H(NL+j+1,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * sin(theta_cp2k)   !hopping H1 1s - O 2pz
               H(NL+j+3,NL+j+1) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*R_OH1*R_OH1 ) ) * sin(theta_cp2k)
            endif

         enddo

         !hydrogen bonding from Jorge's article
         do i=2,h2o
            !Onsite
            j = (i-1)*orbh2o !H2 1s of the water on the left, the only hdrogen involved in the hydrogen bond

            H(NL+j,NL+j+3) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*(R_OO-R_OH2)*(R_OO-R_OH2)) )  !hopping H2 1s (LEFT) - O 2pz (RIGHT)
            H(NL+j+3,NL+j) = tsp* ( (hb*hb)/(4.0_dp*(amu/masselectron)*(R_OO-R_OH2)*(R_OO-R_OH2)) )
         enddo

      !-------------------------------------------------------------
      elseif(geomwire == 100) then
         !RING, the normal modes are in a ring within the central region
         write(*,*) 'The geometry chosen is RING'
         write(*,*) 'There are', nmnumber,'Normal Modes in an electronic ring between position', nminit,'and', nminit + nmnumber
         !the last site of the ring is NOT connected to the next in the chain, but to the first in the ring
         H(nminit+nmnumber,nminit+nmnumber + 1) = 0.0_dp
         H(nminit+nmnumber + 1,nminit+nmnumber) = 0.0_dp
         H(nminit+nmnumber,nminit) = tchain
         H(nminit,nminit+nmnumber) = tchain
         !the site halfway through the ring is connected to the one on the right
         write(*,*) 'Site', nminit+(nmnumber/2),'is connected to site', nminit + nmnumber + 1
         H(nminit+(nmnumber/2),nminit+nmnumber+1) = tchain
         H(nminit+nmnumber+1,nminit+(nmnumber/2)) = tchain
      !-------------------------------------------------------------
      elseif(geomwire == 101) then
         !if 101, LINE, the normal modes are in on a line inside the central region
         write(*,*) 'The geometry chosen is LINE'
         write(*,*) 'There are', nmnumber,'Normal Modes in a region between position', nminit ,'and', nminit + nmnumber
      endif
      write(*,*)
   elseif(ndim == 2)  then !SURFACE, every site is coupled to 4 others. If it is on the border, PBC are applied
      do i=1,righe
         do j=1,colonne
            !the element in the matrix, its label
            element = (i-1)*colonne + j
            !write(*,*) element
            
            !RIGHT
            if(j .ne. colonne) then !if we are not on the right border of the box
               H(element,element+1) = tgrid
               H(element+1,element) = tgrid
            else
               H(element,element-colonne+1) = tgrid
               H(element-colonne+1,element) = tgrid            
            endif
            
            !LEFT
            if(j .ne. 1) then !if we are not on the left border of the box
               H(element,element-1) = tgrid
               H(element-1,element) = tgrid
            else
               H(element,element+colonne-1) = tgrid
               H(element+colonne-1,element) = tgrid            
            endif            
            
            !UP
            if(i .ne. 1) then !if we are not on the top border of the box
               H(element,element-colonne) = tgrid
               H(element-colonne,element) = tgrid
            else
               H(element,element+(righe-1)*colonne) = tgrid
               H(element+(righe-1)*colonne,element) = tgrid            
            endif            
            
            !DOWN
            if(i .ne. righe) then !if we are not on the bottom border of the box
               H(element,element+colonne) = tgrid
               H(element+colonne,element) = tgrid
            else
               H(element,element-(righe-1)*colonne) = tgrid
               H(element-(righe-1)*colonne,element) = tgrid            
            endif                         
         enddo
      enddo
   elseif(ndim == 3)  then !SOLID, every site is coupled to 6 others. If it is on the border, PBC are applied
      do i=1,LZ
         do j=1,LY
            do k=1,LX
               !the element in the matrix, its label
               element = (i-1)*LX*LY + (j-1)*LY + k
               !write(*,*) element
               
               !RIGHT
               if(k .ne. LX) then !if we are not on the right border of the plane
                  H(element,element+1) = tcube
                  H(element+1,element) = tcube
               else
                  H(element,element-LX+1) = tcube
                  H(element-LX+1,element) = tcube            
               endif
               
               !LEFT
               if(k .ne. 1) then !if we are not on the left border of the plane
                  H(element,element-1) = tcube
                  H(element-1,element) = tcube
               else
                  H(element,element+LX-1) = tcube
                  H(element+LX-1,element) = tcube            
               endif            
               
               !UP
               if(j .ne. 1) then !if we are not on the top border of the plane
                  H(element,element-LX) = tcube
                  H(element-LX,element) = tcube
               else
                  H(element,element+(LY-1)*LX) = tcube
                  H(element+(LY-1)*LX,element) = tcube            
               endif            
               
               !DOWN
               if(j .ne. LY) then !if we are not on the bottom border of the plane
                  H(element,element+LX) = tcube
                  H(element+LX,element) = tcube
               else
                  H(element,element-(LY-1)*LX) = tcube
                  H(element-(LY-1)*LX,element) = tcube            
               endif
               
               !OUT OF THE PAPER
               if(i .ne. LZ) then !if we are not on the bottom border of the box
                  H(element,element+(LX*LY)) = tcube
                  H(element,element+(LX*LY)) = tcube
               else
                  H(element,element-(LZ-1)*(LX*LY)) = tcube
                  H(element-(LZ-1)*(LX*LY),element) = tcube            
               endif 
               
               !IN THE PAPER
               if(i .ne. 1) then !if we are not on the bottom border of the box
                  H(element,element-(LX*LY)) = tcube
                  H(element-(LX*LY),element) = tcube
               else
                  H(element,element+(LZ-1)*(LX*LY)) = tcube
                  H(element+(LZ-1)*(LX*LY),element) = tcube            
               endif                             
            enddo
         enddo
      enddo      
   endif
   
end subroutine buildH

subroutine buildInteraction(inter,coup,j,list)
   
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: inter
   real(dp), INTENT(IN) :: coup
   real(dp) :: dado
   integer, INTENT(IN) :: j
   integer :: pos,densityOhm,intdado,i,retry,iter
   integer, dimension(:) :: list

   inter = 0.0_dp

   if(ndim == 1) then !WIRE
      if(geomwire == 1) then
         !DIMER
         inter(oscL,oscR) = coup
         inter(oscR,oscL) = coup
         write(*,*) 'Oscillator',j,'between position',oscL,'and',oscR
         if(((oscR)>=(NL+NC+1)).or.((oscL)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif
      elseif(geomwire == 2) then      
         !ATOM
         !Tchavdar's suggestion, same coupling but opposite signs for the two sides of the moving atom -O-O~0~O-O-
         !inter(atom,atom + 1) = abs(tchain)/acar !positive 
         !inter(atom + 1,atom) = abs(tchain)/acar 
         !inter(atom,atom - 1) = - abs(tchain)/acar !negative
         !inter(atom - 1,atom) = - abs(tchain)/acar

         inter(atom,atom + 1) = coup !positive 
         inter(atom + 1,atom) = coup 
         inter(atom,atom - 1) = - coup !negative
         inter(atom - 1,atom) = - coup
         write(*,*) 'Oscillator',j,'in position',atom,', between',atom+1,'and',atom-1
         if(((atom+1)>=(NL+NC+1)).or.((atom-1)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif  
      elseif(geomwire == 3) then 
         !STM
         inter(leftend,leftend + 1) = coup
         inter(leftend + 1,leftend) = coup
         write(*,*) 'Oscillator',j,'between position',leftend,'and',leftend+1
         if(((leftend+1)>=(NL+NC+1)).or.((leftend)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif
      elseif(geomwire == 4) then 
         !EINSTEIN
         !several oscillators, the j dependence of this routine here is essential
         pos = Lfirst + (j-1)*eqperiod
         inter(pos,pos+1) = coup
         inter(pos+1,pos) = coup
         write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
         if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif
      elseif(geomwire == 5) then
         !OHM, several Einstein oscillators at a uniform distance from each other
         if(nho .ne. 0) then    
            densityOhm = (NC-2)/nho !number of sites per oscillator, excluded the borders of NC
            pos = NL +3 + (j-1)*densityOhm
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            inter(pos,pos-1) = -coup
            inter(pos-1,pos) = -coup
            write(*,*) 'Oscillator',j,'in the middle between position',pos-1,'and',pos+1
            if(((pos+1)>=(NL+NC+1)).or.((pos-1)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 6) then
         !LEGION
         if(nho .ne. 0) then    
            call random_number(dado)
            !write(*,*) dado
            intdado = int(dado*(NC-2))
            !write(*,*) intdado
            pos=NL+1+intdado
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
            if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 7) then
         !FULL_CIRCLE, like OHM with PBC
         if(nho .ne. 0) then    
            densityOhm = (NC-1)/nho !number of sites per oscillator, excluded the borders of NC
            pos = NL +2 + (j-1)*densityOhm
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
            if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 8) then
         !DISSIPION, 2 fake oscillators Left, 2 Right, the following like OHM
         if(j == 1) then
            pos = 2
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'FAKE Oscillator',j,'between position',pos,'and',pos+1
         elseif(j == 2) then
            pos = 4
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'FAKE Oscillator',j,'between position',pos,'and',pos+1         
         elseif(j == 3) then
            pos = nlev - 4
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'FAKE Oscillator',j,'between position',pos,'and',pos+1         
         elseif(j == 4) then
            pos = nlev - 2
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'FAKE Oscillator',j,'between position',pos,'and',pos+1
         else
            if(nho > 4) then    
               densityOhm = (NC-1)/(nho-4) !number of sites per oscillator, excluded the borders of NC
               pos = NL +2 + (j-1-4)*densityOhm
               inter(pos,pos+1) = coup
               inter(pos+1,pos) = coup
               write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
               if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
                  write(*,*) 'ERROR, out of bounds, check the geometry'
                  stop
               endif
            endif         
         endif
      elseif(geomwire == 9) then
         !RANDSEA, the oscillators are coupled to all the sites within the central region with F = (0,coup_max(in input.dat)
         if(nho .ne. 0) then    
            do i=1,NC-2
               pos = NL + 1 + i
               !generate a random coup here, it will be dado
               call random_number(dado)    
               inter(pos,pos) = coup*dado
            enddo
            write(*,*) 'Oscillator',j,'spread between positions',NL + 2,'and',NL + NC - 1
         endif  
      elseif(geomwire == 10) then
         !if 10, TURIN, wire to test the scent theory
         if(nho .ne. 0) then
            if(j==1) then !!!!odorant oscillator
               pos = leftend
               inter(pos,pos+1) = coup
               inter(pos+1,pos) = coup
               write(*,*) 'The ODORANT is between position',pos,'and',pos+1
            else    
               !select randomly left/right of the odorant
               call random_number(dado)
               if(dado < 0.5_dp) then !LEFT
                  !generate a random position here
                  call random_number(dado)     
                  pos = NL + 2 + int(dado*(NC/2 - 2))
               else !RIGHT
                  !generate a random position here
                  call random_number(dado)
                  pos = NL + int(NC/2) + 2 + int(dado*(NC/2 - 4))
               endif
               inter(pos,pos+1) = coup
               inter(pos+1,pos) = coup
               write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
            endif
         endif        
      elseif(geomwire == 11) then
         !ANDERRAND
         if(nho .ne. 0) then  
            retry = 1 !I generate oscillators in random positions as long as they don't overlap
            do while (retry > 0)
               retry = 0
               call random_number(dado)
               intdado = int(dado*(NC-2))
               pos=NL+1+intdado
               !check if pos is allowed
               if((pos == siteJ) .or. (pos == siteJ+1) ) then !sites where the current is evaluated, not allowed
                  retry = 1
               else
                  do i=1,j !check on the previous oscillators
                     if(list(i) == pos) then
                        retry = retry + 1
                     endif
                  enddo
               endif
               !if there is no superposition
               if(retry == 0)  then
                  list(j) = pos
               endif
            enddo
            inter(pos,pos-1) = coup
            inter(pos-1,pos) = coup
            inter(pos,pos+1) = -coup
            inter(pos+1,pos) = -coup
            !write(*,*) 'Oscillator',j,'on atom',pos
            if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 12) then
         !WELL
         inter(atom,atom + 1) = coup !positive 
         inter(atom + 1,atom) = coup 
         inter(atom,atom - 1) = - coup !negative
         inter(atom - 1,atom) = - coup
         write(*,*) 'Oscillator',j,'in position',atom,', between atom',atom-1,'and',atom+1
      elseif(geomwire == 13) then
         !WELLS
         if(j==1) then 
            inter(atom,atom + 1) = coup !positive 
            inter(atom + 1,atom) = coup 
            inter(atom,atom - 1) = - coup !negative
            inter(atom - 1,atom) = - coup
            write(*,*) 'Oscillator',j,'in position',atom,', between atom',atom-1,'and',atom+1
         elseif(mod(j,2)==0) then !if j is even I choose the right side of atom
            iter = j/2
            inter(atom+iter,atom+1+iter) = coup !positive 
            inter(atom+1+iter,atom+iter) = coup 
            inter(atom+iter,atom-1+iter) = - coup !negative
            inter(atom-1+iter,atom+iter) = - coup
            write(*,*) 'Oscillator',j,'in position',atom+iter,', between atom',atom-1+iter,'and',atom+1+iter
         elseif(mod(j,2)==1) then !if j is odd I choose the left side of atom
            iter = -(j-1)/2
            inter(atom+iter,atom+1+iter) = coup !positive 
            inter(atom+1+iter,atom+iter) = coup 
            inter(atom+iter,atom-1+iter) = - coup !negative
            inter(atom-1+iter,atom+iter) = - coup
            write(*,*) 'Oscillator',j,'in position',atom+iter,', between atom',atom-1+iter,'and',atom+1+iter
         endif
      elseif(geomwire == 14) then 
         !ANDEREQUAL, Anderson localized system, with equally spaced oscillators, like EINSTEIN
         pos = Lfirst + (j-1)*eqperiod
         inter(pos,pos+1) = coup
         inter(pos+1,pos) = coup
         inter(pos,pos-1) = -coup
         inter(pos-1,pos) = -coup
         list(j) = pos
         if(((pos+1)>=(NL+NC+1)).or.((pos)<=(NL))) then
            write(*,*) 'ERROR, out of bounds, check the geometry'
            stop
         endif         
      elseif(geomwire == 15) then 
         !MOL2L !CHANGE ME!!!!!!!!!!!!!!!!
         if(j==1) then        
            pos = 29
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup   
         elseif(j==2) then
            pos = 31
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup   
         elseif(j==3) then
            pos = 29
            inter(pos,pos+2) = coup
            inter(pos+2,pos) = coup   
         elseif(j==4) then
            pos = 30
            inter(pos,pos+2) = coup
            inter(pos+2,pos) = coup 
         elseif(j==5) then
            pos = 10
            inter(pos,pos) = coup
            inter(pos,pos) = coup 
         elseif(j==6) then
            pos = 51
            inter(pos,pos) = coup
            inter(pos,pos) = coup   
         endif
      elseif(geomwire == 16) then
         !WELL2L
         if(j==1) then        
            inter(atom-1,atom) = coup
            inter(atom,atom-1) = coup

            !inter(atom,atom+1) = coup
            !inter(atom+1,atom) = coup
            !inter(atom,atom-2) = -coup
            !inter(atom-2,atom) = -coup

            write(*,*) 'Oscillator',j,'in position',atom
         elseif(j==2) then
            inter(atom-1,atom) = coup
            inter(atom,atom-1) = coup
            write(*,*) 'Oscillator',j,'in position',atom-1
         endif
      elseif(geomwire == 17) then
         !ATOMS
         if(nho .ne. 0) then    
            densityOhm = (NC-2)/nho !number of sites per oscillator, excluded the borders of NC
            pos = NL +3 + (j-1)*densityOhm
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            inter(pos,pos-1) = -coup
            inter(pos-1,pos) = -coup
            write(*,*) 'Oscillator',j,'in the middle between position',pos-1,'and',pos+1
            if(((pos+1)>=(NL+NC+1)).or.((pos-1)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 18) then
         !BONDS
         if(nho .ne. 0) then    
            densityOhm = (NC-2)/nho !number of sites per oscillator, excluded the borders of NC
            pos = NL +3 + (j-1)*densityOhm
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            write(*,*) 'Oscillator',j,'between position',pos,'and',pos+1
            if(((pos+1)>=(NL+NC+1)).or.((pos-1)<=(NL))) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 19) then
         !ALFREDOCV
         if(nho .ne. 0) then    
            pos = NL + 1 + j 
            inter(pos,pos+1) = coup
            inter(pos+1,pos) = coup
            inter(pos,pos-1) = -coup
            inter(pos-1,pos) = -coup
            write(*,*) 'Oscillator',j,'in the middle between position',pos-1,'and',pos+1
            if(pos>=NL+NC) then
               write(*,*) 'ERROR, out of bounds, check the geometry'
               stop
            endif
         endif
      elseif(geomwire == 20) then
         !2L1P
         inter(oscL,oscR) = coup
         inter(oscR,oscL) = coup
         write(*,*) 'Oscillator',j,'between position',oscL,'and',oscR
      elseif(geomwire == 21) then
         !RINGWELL2L
         if(j==1) then        
            inter(atom-1,atom) = coup
            inter(atom,atom-1) = coup
            write(*,*) 'Oscillator',j,'in position',atom
         endif
      elseif(geomwire == 22) then
         !3L2P
         if(j==1) then        
            inter(1,2) = coup
            inter(2,1) = coup
            write(*,*) 'Oscillator',j,'between position 1 and 2'
         elseif(j==2) then        
            inter(2,3) = coup
            inter(3,2) = coup
            write(*,*) 'Oscillator',j,'between position 2 and 3'
         endif
      elseif(geomwire == 23) then
         !3LBond
         if(j==1) then        
            inter(1,2) = coup/2.0_dp
            inter(2,1) = coup/2.0_dp
            inter(2,3) = -coup/2.0_dp
            inter(3,2) = -coup/2.0_dp
            inter(1,1) = coup/sqrt(2.0_dp)
            inter(3,3) = -coup/sqrt(2.0_dp)
            write(*,*) 'Oscillator',j,'between position 1 and 2, and 2 and 3'
         elseif(j==2) then        
            inter(1,2) = -coup/2.0_dp
            inter(2,1) = -coup/2.0_dp
            inter(2,3) = coup/2.0_dp
            inter(3,2) = coup/2.0_dp
            inter(1,1) = coup/sqrt(2.0_dp)
            inter(3,3) = -coup/sqrt(2.0_dp)
            write(*,*) 'Oscillator',j,'between position 1 and 2, and 2 and 3'
         endif
      elseif(geomwire == 24) then
         !3LRing
         if(j==1) then        
            inter(atom-1,atom) = coup
            inter(atom,atom-1) = coup
            write(*,*) 'Oscillator',j,'between position 1 and 2'
            !inter(atom-1,atom) = coup/2.0_dp
            !inter(atom,atom-1) = coup/2.0_dp
            !inter(atom,atom+1) = -coup/2.0_dp
            !inter(atom+1,atom) = -coup/2.0_dp
            !inter(atom-1,atom-1) = coup/sqrt(2.0_dp)
            !inter(atom+1,atom+1) = -coup/sqrt(2.0_dp)
            !write(*,*) 'Oscillator',j,'between position 1 and 2, and 2 and 3'
         elseif(j==2) then        
            inter(atom+1,atom) = coup
            inter(atom,atom+1) = coup
            write(*,*) 'Oscillator',j,'between position 2 and 3'
            !inter(atom-1,atom) = -coup/2.0_dp
            !inter(atom,atom-1) = -coup/2.0_dp
            !inter(atom,atom+1) = coup/2.0_dp
            !inter(atom+1,atom) = coup/2.0_dp
            !inter(atom-1,atom-1) = coup/sqrt(2.0_dp)
            !inter(atom+1,atom+1) = -coup/sqrt(2.0_dp)
            !write(*,*) 'Oscillator',j,'between position 1 and 2, and 2 and 3'
         endif
      elseif(geomwire == 44) then
         !4L
         if(j==1) then        
            inter(1,2) = coup
            inter(2,1) = coup
            write(*,*) 'Oscillator',j,'between position 1 and 2'
         elseif(j==2) then        
            inter(3,4) = coup
            inter(4,3) = coup
            write(*,*) 'Oscillator',j,'between position 3 and 4'
         endif
      elseif(geomwire == 45) then
         !4Lelimbo
         if(j==1) then        
            inter(1,2) = coup
            inter(2,1) = coup
            write(*,*) 'Oscillator',j,'between position 1 and 2'
         elseif(j==2) then        
            inter(nlev-1,nlev) = coup
            inter(nlev,nlev-1) = coup
            write(*,*) 'Oscillator',j,'between position nlev-1 and nlev'
         endif
      elseif(geomwire == 46) then
         !SemicImp
         if(j==1) then        
            inter(1,2) = coup
            inter(2,1) = coup
            inter(1,3) = coup
            inter(3,1) = coup
            write(*,*) 'Oscillator',j,'between position 1 and both 2/3'
         elseif(j==2) then        
            inter(nlev-1,nlev) = coup
            inter(nlev,nlev-1) = coup
            inter(nlev-2,nlev) = coup
            inter(nlev,nlev-2) = coup
            write(*,*) 'Oscillator',j,'between position nlev-1/nlev-2 and nlev'
         endif
      !--------
      elseif((geomwire == 50).or.(geomwire == 51)) then
         !Water
         !state 1= H1 1s
         !state 2-4= O 3p x,y,z
         !state 5= H2 1s

         pos = NL + (int((j+1)/2)-1)*orbh2o !I place myself at the beginning of the interested Molecule of water
         
         if(mod(j,4)==1) then !SYMM mode, config V
            inter(pos+1,pos+2) = -coup * cos(theta) / sqrt(2.0_dp) !H1-px           
            inter(pos+2,pos+1) = -coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+1,pos+4) = coup * sin(theta) / sqrt(2.0_dp)!H1-pz
            inter(pos+4,pos+1) = coup * sin(theta) / sqrt(2.0_dp)
            !write(*,*)  inter(pos+1,pos+2) 
            !write(*,*)  inter(pos+4,pos+1) 
            inter(pos+5,pos+2) = coup * cos(theta) / sqrt(2.0_dp)!H2-px           
            inter(pos+2,pos+5) = coup * cos(theta) / sqrt(2.0_dp) 
            inter(pos+5,pos+4) = coup * sin(theta) / sqrt(2.0_dp)!H2-pz
            inter(pos+4,pos+5) = coup * sin(theta) / sqrt(2.0_dp)

         elseif(mod(j,4)==2) then !ANTISYMM mode, config V
            inter(pos+1,pos+2) = coup * cos(theta) / sqrt(2.0_dp)!H1-px           
            inter(pos+2,pos+1) = coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+1,pos+4) = -coup * sin(theta) / sqrt(2.0_dp)!H1-pz
            inter(pos+4,pos+1) = -coup * sin(theta) / sqrt(2.0_dp)

            inter(pos+5,pos+2) = coup * cos(theta) / sqrt(2.0_dp)!H2-px           
            inter(pos+2,pos+5) = coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+5,pos+4) = coup * sin(theta) / sqrt(2.0_dp)!H2-pz
            inter(pos+4,pos+5) = coup * sin(theta) / sqrt(2.0_dp)
         endif

         if(mod(j,4)==3) then !SYMM mode, config C
            inter(pos+1,pos+2) = coup * cos(theta) / sqrt(2.0_dp)!H1-px           
            inter(pos+2,pos+1) = coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+1,pos+4) = -coup * sin(theta)/ sqrt(2.0_dp)!H1-pz
            inter(pos+4,pos+1) = -coup * sin(theta)/ sqrt(2.0_dp)

            inter(pos+5,pos+2) = coup * cos(theta)/ sqrt(2.0_dp)!H2-px           
            inter(pos+2,pos+5) = coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+5,pos+4) = coup * sin(theta)/ sqrt(2.0_dp)!H2-pz
            inter(pos+4,pos+5) = coup * sin(theta)/ sqrt(2.0_dp)

         elseif(mod(j,4)==0) then !ANTISYMM mode, config C
            inter(pos+1,pos+2) = -coup * cos(theta)/ sqrt(2.0_dp)!H1-px           
            inter(pos+2,pos+1) = -coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+1,pos+4) = coup * sin(theta)/ sqrt(2.0_dp)!H1-pz
            inter(pos+4,pos+1) = coup * sin(theta) / sqrt(2.0_dp)

            inter(pos+5,pos+2) = coup * cos(theta)/ sqrt(2.0_dp)!H2-px           
            inter(pos+2,pos+5) = coup * cos(theta)/ sqrt(2.0_dp)
            inter(pos+5,pos+4) = coup * sin(theta)/ sqrt(2.0_dp)!H2-pz
            inter(pos+4,pos+5) = coup * sin(theta)/ sqrt(2.0_dp)
         endif
      !--------
      elseif((geomwire == 52)) then
         !Water
         !state 1= H1 1s
         !state 2-3= O 3p x,z
         !state 4= H2 1s

         pos = NL + (int((j+1)/2)-1)*orbh2o !I place myself at the beginning of the interested Molecule of water
         !these are the single modes of every molecule, but it's wrong
         
         if(mod(j,4)==1) then !SYMM mode, config V
            inter(pos+1,pos+2) = -coup * cos(theta) / sqrt(2.0_dp) !H1-px           
            inter(pos+2,pos+1) = -coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+1,pos+3) = coup * sin(theta) / sqrt(2.0_dp)!H1-pz
            inter(pos+3,pos+1) = coup * sin(theta) / sqrt(2.0_dp)
            !write(*,*)  inter(pos+1,pos+2) 
            !write(*,*)  inter(pos+4,pos+1) 
            inter(pos+4,pos+2) = coup * cos(theta) / sqrt(2.0_dp)!H2-px           
            inter(pos+2,pos+4) = coup * cos(theta) / sqrt(2.0_dp) 
            inter(pos+4,pos+3) = coup * sin(theta) / sqrt(2.0_dp)!H2-pz
            inter(pos+3,pos+4) = coup * sin(theta) / sqrt(2.0_dp)

         elseif(mod(j,4)==2) then !ANTISYMM mode, config V
            inter(pos+1,pos+2) = coup * cos(theta) / sqrt(2.0_dp)!H1-px           
            inter(pos+2,pos+1) = coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+1,pos+3) = -coup * sin(theta) / sqrt(2.0_dp)!H1-pz
            inter(pos+3,pos+1) = -coup * sin(theta) / sqrt(2.0_dp)

            inter(pos+4,pos+2) = coup * cos(theta) / sqrt(2.0_dp)!H2-px           
            inter(pos+2,pos+4) = coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+4,pos+3) = coup * sin(theta) / sqrt(2.0_dp)!H2-pz
            inter(pos+3,pos+4) = coup * sin(theta) / sqrt(2.0_dp)
         endif

         if(mod(j,4)==3) then !SYMM mode, config C
            inter(pos+1,pos+2) = -coup * cos(3.0_dp*theta) / sqrt(2.0_dp)!H1-px           
            inter(pos+2,pos+1) = -coup * cos(3.0_dp*theta) / sqrt(2.0_dp)
            inter(pos+1,pos+3) = -coup * sin(3.0_dp*theta)/ sqrt(2.0_dp)!H1-pz
            inter(pos+3,pos+1) = -coup * sin(3.0_dp*theta)/ sqrt(2.0_dp)

            inter(pos+4,pos+2) = coup * cos(theta)/ sqrt(2.0_dp)!H2-px           
            inter(pos+2,pos+4) = coup * cos(theta) / sqrt(2.0_dp)
            inter(pos+4,pos+3) = coup * sin(theta)/ sqrt(2.0_dp)!H2-pz
            inter(pos+3,pos+4) = coup * sin(theta)/ sqrt(2.0_dp)

         elseif(mod(j,4)==0) then !ANTISYMM mode, config C
            inter(pos+1,pos+2) = coup * cos(3.0_dp*theta)/ sqrt(2.0_dp)!H1-px           
            inter(pos+2,pos+1) = coup * cos(3.0_dp*theta) / sqrt(2.0_dp)
            inter(pos+1,pos+3) = coup * sin(3.0_dp*theta)/ sqrt(2.0_dp)!H1-pz
            inter(pos+3,pos+1) = coup * sin(3.0_dp*theta) / sqrt(2.0_dp)

            inter(pos+4,pos+2) = coup * cos(theta)/ sqrt(2.0_dp)!H2-px           
            inter(pos+2,pos+4) = coup * cos(theta)/ sqrt(2.0_dp)
            inter(pos+4,pos+3) = coup * sin(theta)/ sqrt(2.0_dp)!H2-pz
            inter(pos+3,pos+4) = coup * sin(theta)/ sqrt(2.0_dp)
         endif
      !--------
      elseif((geomwire == 53).or.(geomwire == 54)) then
         !CP2K_Water
         !state 1= H1 1s
         !state 2-3= O 3p x,z
         !state 4= H2 1s

         !collective modes here       
         if(j==1) then !MODE OUT OF the CHAIN, only mode included for now          
            do i = 1,h2o !all the h2o molecules are involved in the mode
               pos = NL + (i-1)*orbh2o !I place myself at the beginning of the interested Molecule of water
               if(mod(i,2)==1) then 
                  inter(pos+1,pos+2) = -coup * cos(theta_cp2k) / sqrt(real(h2o,dp)) !H1-px           
                  inter(pos+2,pos+1) = -coup * cos(theta_cp2k) / sqrt(real(h2o,dp))
                  inter(pos+1,pos+3) = coup * sin(theta_cp2k) / sqrt(real(h2o,dp)) !H1-pz
                  inter(pos+3,pos+1) = coup * sin(theta_cp2k) / sqrt(real(h2o,dp))
               elseif(mod(i,2)==0) then 
                  inter(pos+1,pos+2) = coup * cos(theta_cp2k) / sqrt(real(h2o,dp)) !H1-px           
                  inter(pos+2,pos+1) = coup * cos(theta_cp2k) / sqrt(real(h2o,dp))
                  inter(pos+1,pos+3) = coup * sin(theta_cp2k) / sqrt(real(h2o,dp))!H1-pz
                  inter(pos+3,pos+1) = coup * sin(theta_cp2k) / sqrt(real(h2o,dp))
               endif
            enddo
         endif

      endif
   elseif(ndim == 2) then !SURFACE
   elseif(ndim == 3) then !SOLID
      inter(j*4 +2,j*4+ 1) = coup
      inter(j*4 + 1,j*4 +2) = coup
      write(*,*) 'Oscillator',j,'between position',j*4+1,'and',j*4+1
   endif   

end subroutine buildInteraction

subroutine buildInteractionNM(inter,coup,j,omega)
   
   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: inter
   real(dp), INTENT(IN) :: coup,omega
   complex(dp) :: factor
   real(dp) :: massimo,minimo
   integer, INTENT(IN) :: j !label of normal mode computed here
   integer :: i,pos,k,posmax,posmin

   inter = 0.0_dp  
   factor = coup * 2.0_dp * sin(pi*real(j,dp)/real(nmnumber,dp)) / sqrt(real(nmnumber,dp))

   do i=1,nmnumber
      if(geomwire==100) then
         !if 100, RING, the normal modes are in a ring within the central region
         pos = nminit - 1 + i
         !inter(pos,pos+1) = factor * im * exp(-2.0_dp*pi*im*(real(i,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
         inter(pos,pos+1) = factor * sin(2.0_dp*pi*(real(i,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
         inter(pos+1,pos) = inter(pos,pos+1)
         !Last piece of the ring 
         if (i == nmnumber) then 
            !inter(nminit+nmnumber,nminit) = factor * im * exp(-2.0_dp*pi*im*(real(i+1,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
            inter(nminit+nmnumber,nminit) = factor * sin(2.0_dp*pi*(real(i+1,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
            inter(nminit,nminit+nmnumber) = inter(NL+2+nmnumber,NL+2)
         endif
      elseif(geomwire==101) then
         !if 101, LINE, the normal modes are in on a line inside the central region
         pos = nminit - 1 + i
         !inter(pos,pos+1) = factor * im * exp(-2.0_dp*pi*im*(real(i,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
         inter(pos,pos+1) = factor * sin(2.0_dp*pi*(real(i,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
         inter(pos+1,pos) = inter(pos,pos+1)
         if(i==1) then
            !inter(pos,pos-1) = factor * im * exp(-2.0_dp*pi*im*(real(i-1,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
            inter(pos,pos-1) = factor * sin(2.0_dp*pi*(real(i-1,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
            inter(pos-1,pos) = inter(pos,pos-1)
         endif
         if (i == nmnumber) then 
            !inter(pos+2,pos+1) = factor * im * exp(-2.0_dp*pi*im*(real(i+1,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
            inter(pos+2,pos+1) = factor * sin(2.0_dp*pi*(real(i+1,dp)+0.5_dp)*real(j,dp)/real(nmnumber,dp))
            inter(pos+1,pos+2) = inter(pos+1,pos+2)
         endif
      endif

      !computation of maximum and minimum coupling (absolute value of real part)
      if(i==1) then
         massimo = (abs(real(inter(pos,pos+1))))
         minimo = (abs(real(inter(pos,pos+1))))
         posmax = pos
         posmin = pos
      else
         if(abs(real(inter(pos,pos+1))) > massimo) then
            massimo = abs(real(inter(pos,pos+1)))
            posmax = pos
         endif
         if(abs(real(inter(pos,pos+1))) < minimo) then
            minimo = abs(real(inter(pos,pos+1)))
            posmin = pos
         endif
      endif
   enddo

   write(*,'(i8,f15.4,f18.4,i11,f13.4,i10)')  j,omega,massimo,posmax,minimo,posmin

end subroutine buildInteractionNM

!----------------------
!OB ROUTINES
!----------------------

subroutine buildF(effe,H,gamma)

!this is the operatorial part in G- = H_S0 + Sigma- + i I_s Delta

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: effe
   complex(dp), dimension(:,:), INTENT(IN) :: H
   real(dp), INTENT(IN) :: gamma
   integer :: i,j

   effe = H
   if(openboundaries .ne. 0) then 
      do i = 1, nlev
         effe(i,i) = effe(i,i) + im*del
      enddo

      if(switchmeter == 0) then !normal case
         do i= 1,NL
            effe(i,i) = effe(i,i) + im*0.5_dp*gamma
         enddo

         do i= NL+NC+1,nlev
            effe(i,i) = effe(i,i) + im*0.5_dp*gamma
         enddo
      else !with h2o I need to create a part of the leads where to measure the current
         do i= 1,NL-meterlead
            effe(i,i) = effe(i,i) + im*0.5_dp*gamma
         enddo

         do i= NL+NC+1+meterlead,nlev
            effe(i,i) = effe(i,i) + im*0.5_dp*gamma
         enddo
      endif
   endif 
   if(openboundaries == 2) then
      effe = 0.0_dp
      do i = 1, nlev
         effe(i,i) = effe(i,i) + im*0.5_dp*gamma
      enddo
   endif

end subroutine buildF

subroutine injection(evecL,evecR,lambda,gamma)

   use modvar
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: evecL,evecR !R and L eigenvectors of effe
   complex(dp), dimension(:), INTENT(IN) :: lambda        !the eigenvalues of effe
   
   complex(dp), allocatable :: inj(:,:),ccinj(:,:),eginj(:,:),egccinj(:,:)
   complex(dp), allocatable :: unoL(:),dueL(:) !buffer terms to speed up the calculation
   complex(dp), allocatable :: unoR(:),dueR(:)
   complex(dp), allocatable :: egunoL(:),egdueL(:) !egun mode
   real(dp), allocatable :: lambdaRe(:),lambdaIm(:)
   real(dp), INTENT(IN) :: gamma   
   real(dp) :: egU,egD !upper and lower limit of egun injection
   integer :: i,L,R,s,sp,flag
   
   !evaluation of the buffers
   allocate(lambdaRe(nlev))
   allocate(lambdaIm(nlev))
   allocate(unoL(nlev))
   allocate(unoR(nlev))
   allocate(dueL(nlev))
   allocate(dueR(nlev))
   lambdaRe = real(lambda,dp)
   lambdaIm = aimag(lambda)
   do i = 1,nlev
      unoL(i) = 0.5_dp*log( ((muL - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueL(i) = im*atan( ( muL - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
      unoR(i) = 0.5_dp*log( ((muR - lambdaRe(i))**2 + lambdaIm(i)**2)/((BI - lambdaRe(i))**2 + lambdaIm(i)**2) )
      !dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) + 0.5_dp*im*pi
      dueR(i) = im*atan( ( muR - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( Bi - lambdaRe(i) )/lambdaIm(i) )
   enddo
   if(openboundaries .eq. 11) then !electron gun mode
      allocate(egunoL(nlev))
      allocate(egdueL(nlev))
      !limits of injection of the electron
      egU = egE + eGW
      egD = egE - eGW
      do i = 1,nlev
         egunoL(i) = 0.5_dp*log( ((egU - lambdaRe(i))**2 + lambdaIm(i)**2)/((egD - lambdaRe(i))**2 + lambdaIm(i)**2) )       
         egdueL(i) = im*atan( ( egU - lambdaRe(i) )/lambdaIm(i) ) - im*atan( ( egD - lambdaRe(i) )/lambdaIm(i) )
         !unoL(i) = unoL(i) + egunoL(i)
         !dueL(i) = dueL(i) + egdueL(i)
      enddo

      open(48, file='eginj',form='unformatted')
      allocate(eginj(nlev,nlev))
      eginj = 0.0_dp

      if(switchmeter == 0) then
         do sp = 1,nlev
            do L=1,NL
               do i=1,nlev                      
                  eginj(L,sp) = eginj(L,sp) + ( evecR(L,i) * conjg(evecL(sp,i)) ) * ( egunoL(i) + egdueL(i) )
               enddo              
            enddo
         enddo
      else
         do sp = 1,nlev
            do L=1,NL-meterlead
               do i=1,nlev                      
                  eginj(L,sp) = eginj(L,sp) + ( evecR(L,i) * conjg(evecL(sp,i)) ) * ( egunoL(i) + egdueL(i) )
               enddo              
            enddo
         enddo
      endif

      allocate(egccinj(nlev,nlev))
      egccinj = conjg(transpose(eginj))
      eginj = eginj - egccinj
      eginj = eginj * (im*gamma) * (1.0_dp/(2.0_dp*pi*im))

      write(48) eginj
      close(48)

   endif   
   deallocate(lambdaRe)
   deallocate(lambdaIm)

   !evaluating the matrix elements s,s' of the injection term
   open(46, file='inj',form='unformatted')
   allocate(inj(nlev,nlev))
   inj = 0.0_dp

   if(switchmeter == 0) then
      do sp = 1,nlev
         do L=1,NL
            do i=1,nlev                      
               inj(L,sp) = inj(L,sp) + ( evecR(L,i) * conjg(evecL(sp,i)) ) * ( unoL(i) + dueL(i) )
            enddo              
         enddo
      enddo

      do sp = 1,nlev
         do R= NL+NC+1,nlev
            do i=1,nlev                      
               inj(R,sp) = inj(R,sp) + ( evecR(R,i) * conjg(evecL(sp,i)) ) * ( unoR(i) + dueR(i) )
            enddo              
         enddo
      enddo
   else
      do sp = 1,nlev
         do L=1,NL-meterlead
            do i=1,nlev                      
               inj(L,sp) = inj(L,sp) + ( evecR(L,i) * conjg(evecL(sp,i)) ) * ( unoL(i) + dueL(i) )
            enddo              
         enddo
      enddo

      do sp = 1,nlev
         do R= NL+NC+1+meterlead,nlev
            do i=1,nlev                      
               inj(R,sp) = inj(R,sp) + ( evecR(R,i) * conjg(evecL(sp,i)) ) * ( unoR(i) + dueR(i) )
            enddo              
         enddo
      enddo
   endif

   deallocate(unoL)
   deallocate(unoR)
   deallocate(dueL)
   deallocate(dueR)

   allocate(ccinj(nlev,nlev))
   ccinj = conjg(transpose(inj))
   inj = inj - ccinj
   inj = inj * (im*gamma) * (1.0_dp/(2.0_dp*pi*im))

   write(46) inj
   close(46)

end subroutine injection

!----------------------
!RANDOM NUMBER GENERATOR
!----------------------

subroutine init_random_seed()

   INTEGER :: i, n, clock
   INTEGER, DIMENSION(:), ALLOCATABLE :: seed

   CALL RANDOM_SEED(size = n)
   ALLOCATE(seed(n))

   CALL SYSTEM_CLOCK(COUNT=clock)

   if(iseed == 0) then
      seed = clock + 37 * (/ (i - 1, i = 1, n) /)
   else
      seed = iseed + 37 * (/ (i - 1, i = 1, n) /)  
   endif

   CALL RANDOM_SEED(PUT = seed)

   DEALLOCATE(seed)

end subroutine init_random_seed

end program diagH
