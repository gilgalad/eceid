program diag

   use OMP_LIB
   use constx3l  
   implicit none

   !LAPACK VARIABLES   
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   integer :: info
  
   !SYSTEM VARIABLES
   complex(dp), dimension(lmat,lmat) :: H !hamiltonian of the system
   complex(dp), dimension(nlev,nlev) :: Hel !electronic hamiltonian 
   complex(dp), dimension(lmat) :: psi0 !initial state
   real(dp) :: eigv(lmat),eigvel(nlev)    !ordered eigevalue vector
   complex(dp), dimension(lmat,lmat) :: rho !time dependent density matrix, and initial DM
   complex(dp), dimension(nlev,nlev) :: rhoel !time dependent ELECTRONIC density matrix
   complex(dp), dimension(lmat) :: expEt,evo   !temporary matrix to speed up the calculation
   real(dp), dimension(0:nstep,nlev+1) :: storageRho !matrix where I store the results, easier to fill it up and print it later with OMP
   complex(dp), dimension(2) :: storageBuffer !storage for the local e.v. of the double creation/annihilation operators
   real(dp), dimension(0:nstep,2) :: storageDoubleEx !storage for the local e.v. of the double creation/annihilation operators
   !label is the matrix where I store the names of the kets corresponding to a hamiltonian element
   !e.g. <ijk|H|i'j'k'> where i is the electronic label(1:3), 
   !and j and k are the oscillator 1 and 2 labels (1:nex)
   integer, dimension(lmat,lmat,4) :: label
   real(dp) :: time , tottr, evn, en
   complex(dp) :: buffer
   integer :: i,k,l,initial,nthreads
  
   !$OMP PARALLEL DEFAULT(shared), PRIVATE(i,time,expEt,evo,rho,k,l,rhoel,evn,tottr,storageBuffer)
 
   !---------------------------------------
   !single thread initialization and diagonalization of the hamiltonian
   !$OMP SINGLE
   call buildlabel(label)
   call buildH(H,label)
   call buildHel(Hel)

   !diagonalize H
   allocate(work(lwork))
   allocate(rwork(lrwork))
   allocate(iwork(liwork))
   call zheevd('V','U',lmat,H,lmat,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
   !electronic hamiltonian only
   call zheevd('V','U',nlev,Hel,nlev,eigvel,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
   deallocate(work)
   deallocate(rwork)
   deallocate(iwork)
  
   !CHECK VARIOUS QUANTITIES
   !write(*,*) info
   !write(*,*) WORK(1), RWORK(1), IWORK(1)
   !do i=1,lmat
   !   write(*,*) eigv(i)
   !enddo   
   !do i=1,lmat   
   !   write(*,*) real(H(i,lmat)),label(i,lmat,:)
   !enddo
   
   if( start == 0) then
      !find the name of the vector in the |ij> basis from which we start
      initial = findrhozero(H,label,eigv)
   elseif( start == 1) then
      call initialstate(Hel,H,label,psi0)
   elseif( start == 2) then
      call initialstateBE(H,label,psi0)
   endif
   open(33, file='dataX3l.dat')
   open(34, file='TBelocX3l.dat') !TB electronic occupation
   open(35, file='double_ex_oscillator.dat')
   open(90, file='wellX3locc.dat')
   open(93, file='test.dat')   
   call OMP_SET_NUM_THREADS(numthr)
   write(*,*) 'The number of threads is', omp_get_num_threads()
   nthreads = omp_get_num_threads()
   !$OMP END SINGLE
!--------------------------------------   
   
   !$OMP DO SCHEDULE(STATIC,nthreads)
   do i=0,nstep
      time = dt*i

      !computing once the exponential of the time evolution 
      !times the projection over the initial state
      if( start == 0) then
         call buildexpEt(H,expEt,eigv,time,initial)
      elseif( start == 1) then
         call buildexpEtNEW(expEt,eigv,time,psi0)
      elseif( start == 2) then
         call buildexpEtNEW(expEt,eigv,time,psi0)
      endif
      !building the old vectors time evoluted
      call buildevo(H,eigv,evo,expEt)

      !the density matrix in the old basis
      do k=1,lmat
         do l=1,lmat
            rho(k,l) = evo(k)*conjg(evo(l))
         enddo
      enddo

      !trace over the ionic DOF
      call traces(rho,rhoel,label,evn)
      !off diagonal traces to estimate the magnitude of the terms we ignore in the approximate method
      !it prints the file inside the subroutine
      !comment out for performance
      call tracesapp(rho,label,evn,time,storageBuffer)
      !total trace of rho
      tottr = 0.0_dp      
      do k=1,nlev
         tottr = tottr + real(rhoel(k,k),dp)
      enddo

      !store the data to print in vectors with a thread depending label
      !$OMP CRITICAL
      do k=1,nlev
         storageRho(i,k) = real(rhoel(k,k),dp)
         storageRho(i,k) = real(rhoel(k,k),dp)
         storageRho(i,k) = real(rhoel(k,k),dp)
      enddo
      storageRho(i,nlev+1) = evn
      storageDoubleEx(i,1) = real(storageBuffer(1),dp) !an1
      storageDoubleEx(i,2) = real(storageBuffer(2),dp) !cr1
      !$OMP END CRITICAL

      !print on screen (only the thread 0 by convention)
      if(omp_get_thread_num()==0) then
         !write(*,'(F7.2,F9.5,F9.5,F9.5,F9.5)') dt*i,storageRho(i,middle-1),storageRho(i,middle), &
         !                    storageRho(i,middle+1),storageRho(i,nlev+1)
         if(geo==23) then
            write(*,'(F9.2,F10.5,F10.5,F10.5)') dt*i,storageRho(i,1), &
                                storageRho(i,2),storageRho(i,3)
         else
            write(*,'(F9.2,F10.5,F10.5,F10.5)') dt*i,storageRho(i,middle+1), &
                             storageRho(i,middle),storageRho(i,nlev+1)  
         endif
      endif

   enddo
   !$OMP END DO
   
   !$OMP SINGLE
   !print the result to file
   do i=0,nstep
      if(geo == 23) then
         write(33,'(F9.3,F10.4,F10.4,F10.4,F10.4,F10.4,F10.4)') dt*i,storageRho(i,1),storageRho(i,2),&
                             storageRho(i,3),storageRho(i,4),storageRho(i,5)
      else
         !write(33,'(F7.2,F8.5,F8.5,F8.5,F8.5,F8.5,F8.5)') dt*i,storageRho(i,middle-1),storageRho(i,middle),&
         !                    storageRho(i,middle+1),storageRho(i,middle+2),storageRho(i,nlev+1)
         write(33,'(F10.2,F10.5,F10.5,F10.5,F10.5)') dt*i,storageRho(i,middle+1),&
                             storageRho(i,middle),storageRho(i,nlev+1)
      endif

      do k=1,nlev  !printing the TB occupation
         write(34,'(F11.3,i10,F12.7)') dt*i,k,storageRho(i,k)
      enddo
      !write(34,'(F11.3,i10,F12.7)') dt*i,nlev+1,0.0_dp  !fake last row,bug in the plotting with gnuplot and pm3d
      write(34,*)
      write(35,'(F10.2,F9.5,F9.5,F9.5)') dt*i,storageRho(i,nlev+1), &
                          storageDoubleEx(i,1),storageDoubleEx(i,2)
      write(90,*) dt*i,storageRho(i,middle+1)
      write(93,'(F10.2,F10.5,F10.5,F10.5,F10.5)') dt*i,0.5_dp*cos(omega1*dt*i) + 0.5_dp,0.5_dp*cos(omega1*dt*i) + 0.5_dp,0.5_dp*cos(omega1*dt*i) + 0.5_dp
   enddo
   !$OMP END SINGLE   
   
   !$OMP END PARALLEL


!-------------------END MAIN PROGRAM-------------------------


contains

!establishes a criterium with which to fill H later on with states |ijk>
!basically it's a map from |p> (with p from 1 to lmat+nex*nex*nlev) to |ijk>
subroutine buildlabel(mat)

   use constx3l
   implicit none
   
   integer, dimension(:,:,:), INTENT(OUT) :: mat
   integer :: i,j,l,m,in1,in2

   do in1 = 1,lmat
      do in2 = 1,lmat
         !each index moves modulus a different frequency. The third index of both bra and kets
         ! i.e. n and k, moves at the highest frequency, the others move slower

         !l,m,n depend only on the column i.e. in2
         m = mod( (in2-1) , nex )        !the phonons go from 0 to nex-1
         l = mod( (in2-1)/(nex) , nlev ) + 1 !the electronic levels go from 1 to nlev
         !i,j,k depend only on the row i.e. in1
         j = mod( (in1-1), nex )
         i = mod( (in1-1)/(nex) , nlev ) + 1

         !careful to overwriting of indexes!!!!!!
         mat(in1,in2,1) = i   !kind of electronic level in the bra
         mat(in1,in2,2) = j   !number of phonons in osc 1 in the bra

         mat(in1,in2,3) = l   !kind of electronic level in the ket
         mat(in1,in2,4) = m   !number of phonons in osc 1 in the ket
      enddo
   enddo

   !check
   !do i=1,lmat
   !   do j=1,lmat
   !      write(*,*) mat(i,j,:)
   !   enddo
   !   write(*,*)
   !enddo
   
end subroutine buildlabel

!----------------------------------------------------------------

subroutine buildH(H,mat)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: H
   integer, dimension(:,:,:), INTENT(IN) :: mat
   complex(dp) :: buff1,buffe
   real(dp) :: en1
   integer :: i,j,k

   H = 0.0_dp

   do i = 1,lmat
      do j = 1,lmat

         !--------------
         !DIAGONAL TERMS
         !warning: these terms are NOT degenerate in the system they don't act, 
         !i.e. if the electronic coordinate is diagonal, also the oscillators need to be 
         !otherwise <N'M'|NM> may be zero

         if( (mat(i,j,1)==mat(i,j,3)) .and. (mat(i,j,2)==mat(i,j,4)) ) then

            !REDESIGN, this part is not really flexible because of the initial conditions
            !ELECTRONIC PART
            !if we are on the left part of the wire
            if(mat(i,j,1) <= middle ) then
               H(i,j) = H(i,j) + onsiteL
            elseif(mat(i,j,1) > middle ) then
               H(i,j) = H(i,j) + onsiteR
            endif

            if((geo == 18) .or. (geo==19)) then !WELL2L or RINGWELL2L
               if(mat(i,j,1) == (middle+1) ) then
                  H(i,j) = onsiteM
               endif
            endif

            if(geo == 22) then !3LBond
               if(nlev.ne.3) then
                  write(*,*) 'ERROR, nlev must be 3'
                  STOP
               endif
               if(onsiteL .ne. 0.0_dp) then
                  write(*,*) 'ERROR, onsiteL must be 0'
                  STOP
               endif
               if(onsiteR .ne. 0.0_dp) then
                  write(*,*) 'ERROR, onsiteR must be 0'
                  STOP
               endif
               if(mat(i,j,1) == 1) then
                  H(i,j) = H(i,j) + E1
               endif
               !if we are on electronic level 2
               if(mat(i,j,1) == 2) then
                  H(i,j) = H(i,j) + E2
               endif
               !if we are on electronic level 3
               if(mat(i,j,1) == 3) then
                  H(i,j) = H(i,j) + E3
               endif
            endif

            !DISSIPATION
            if((mat(i,j,1) == 1) .or. (mat(i,j,1) == nlev)) then
               !H(i,j) = H(i,j) - im*0.1_dp 
            endif

            !OSCILLATOR1
            H(i,j) = H(i,j) + (real(mat(i,j,2),dp) + 0.5_dp)*hb*omega1

         endif


         !------------------
         !OFF-DIAGONAL TERMS

         !ELECTRONIC HOPPING
         !I must be on the oscillator diagonal for instance
         if( mat(i,j,2)==mat(i,j,4) ) then
            !simplest geometry possible
            if( (mat(i,j,1)==(mat(i,j,3)+1)) .or. (mat(i,j,1)==(mat(i,j,3)-1)) ) then
               H(i,j) = tchain
            endif
            !bond in the middle of the chain weaker STM STANDARD
            if(geo == 1) then !STM  
               if( ( mat(i,j,3) .eq. middle ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. middle ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif 
            endif
                        
            if(geo == 2) then  !RING
               if( ( mat(i,j,3) .eq. middle ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. middle ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif
               if( (mat(i,j,1)==(mat(i,j,3)+nlev-1)) .or. (mat(i,j,1)==(mat(i,j,3)-nlev+1)) ) then
                  H(i,j) = tchain
               endif         
            endif

            if(geo == 16) then !WELL2L
               if( ( mat(i,j,3) .eq. middle ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. middle ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif 
            endif

            if(geo == 17) then !2L1P
               if( ( mat(i,j,3) .eq. middle ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. middle ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif 
            endif

            if(geo == 18) then !WELL2L
               !electronic coupling inside the well
               if( ( mat(i,j,3) .eq. middle ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. middle ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif

               !coupling of the well to the chain 
               if( ( mat(i,j,3) .eq. (middle) ) .and. ( mat(i,j,1) .eq. (middle-1) ) ) then  
                  H(i,j) = tchain2
               endif
               if( ( mat(i,j,1) .eq. (middle) ) .and. ( mat(i,j,3) .eq. (middle-1) ) ) then
                  H(i,j) = tchain2
               endif
               if( ( mat(i,j,3) .eq. (middle+2) ) .and. ( mat(i,j,1) .eq. (middle) ) ) then  
                  H(i,j) = tchain2
               endif
               if( ( mat(i,j,1) .eq. (middle+2) ) .and. ( mat(i,j,3) .eq. (middle) ) ) then
                  H(i,j) = tchain2
               endif

               !the bottom level of the well is isolated from the chain
               if( ( mat(i,j,3) .eq. (middle+2) ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then  
                  H(i,j) = 0.0_dp
               endif
               if( ( mat(i,j,1) .eq. (middle+2) ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
                  H(i,j) = 0.0_dp
               endif
            endif

            if(geo == 19) then !RINGWELL2L
               !electronic coupling inside the well
               if( ( mat(i,j,3) .eq. middle ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. middle ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
                  H(i,j) = tchainw
               endif

               !coupling of the well to the chain 
               if( ( mat(i,j,3) .eq. (middle) ) .and. ( mat(i,j,1) .eq. (middle-1) ) ) then  
                  H(i,j) = tchain2
               endif
               if( ( mat(i,j,1) .eq. (middle) ) .and. ( mat(i,j,3) .eq. (middle-1) ) ) then
                  H(i,j) = tchain2
               endif
               if( ( mat(i,j,3) .eq. (middle+2) ) .and. ( mat(i,j,1) .eq. (middle) ) ) then  
                  H(i,j) = tchain2
               endif
               if( ( mat(i,j,1) .eq. (middle+2) ) .and. ( mat(i,j,3) .eq. (middle) ) ) then
                  H(i,j) = tchain2
               endif

               !the bottom level of the well is isolated from the chain
               if( ( mat(i,j,3) .eq. (middle+2) ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then  
                  H(i,j) = 0.0_dp
               endif
               if( ( mat(i,j,1) .eq. (middle+2) ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
                  H(i,j) = 0.0_dp
               endif

               !the last level is conncted to the first
               if( ( mat(i,j,3) .eq. 1 ) .and. ( mat(i,j,1) .eq. nlev ) ) then  
                  H(i,j) = tchain
               endif
               if( ( mat(i,j,1) .eq. nlev ) .and. ( mat(i,j,3) .eq. 1 ) ) then
                  H(i,j) = tchain
               endif
            endif

            if(geo == 23) then !3LBond
               !all the hoppings are 0
               if( (mat(i,j,1)==(mat(i,j,3)+1)) .or. (mat(i,j,1)==(mat(i,j,3)-1)) ) then
                  H(i,j) = 0.0_dp
               endif      
            endif

         endif
         
         !E-PHCOUPLING
         !OSCILLATOR1 (acts on the right)
         !When the oscillator1 is neither in the ground state or the most excited state (sort of cutoff)
         !both the terms of the annihilation and destruction operator COULD survive, otherwise only one
         buff1 = 0.0_dp
         buffe = 0.0_dp
         en1 = real(mat(i,j,4),dp)
         if( ( mat(i,j,4) .ne. 0 ) .and. ( mat(i,j,4) .ne. (nex-1) )  ) then
            !If the Creation operator acts on the ket AND the bra is correct
            if( mat(i,j,2) .eq. (mat(i,j,4) + 1) ) then
               buff1 = fact1 * (sqrt(en1+1.0_dp))
            endif
            !If the Annihilation operator acts on the ket AND the bra is correct
            if( mat(i,j,2) .eq. (mat(i,j,4) - 1) ) then
               buff1 = fact1 * (sqrt(en1))
            endif
         elseif( mat(i,j,4) .eq. 0 ) then
            !ONLY the Creation operator can act IF the bra is correct
            if( mat(i,j,2) .eq. (mat(i,j,4) + 1) ) then
               buff1 = fact1 * (sqrt(en1+1.0_dp))
            endif
         elseif( mat(i,j,4) .eq. (nex-1) ) then
            !ONLY the Annihilation operator can acts IF the bra is correct
            if( mat(i,j,2) .eq. (mat(i,j,4) - 1) ) then
               buff1 = fact1 * (sqrt(en1))
            endif
         endif
         !ELECTRONIC (acts on the left)
         !change it, it couples the oscillator to the jump between el level middle and middle+1
         !the coupling is only between contiguous levels, so if the energy level is not 
         !on the extremes of the scale it's coupled to both the next and previous level
         if(geo == 16) then  !the oscillator is coupled to the bottom level of the well
            if( ( mat(i,j,3) .eq. middle+1 ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,1) .eq. middle+1 ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
               buffe = - coupling
            endif
            H(i,j) = H(i,j) + (buff1*buffe)         

         elseif(geo == 23) then !3LBond
            if( ( mat(i,j,3) .eq. 1 ) .and. ( mat(i,j,1) .eq. 2 ) ) then   !!!!!!!!!!NEW
               buffe = - coupling/2.0_dp
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,3) .eq. 2 ) ) then
               buffe = - coupling/2.0_dp
            endif
            if( ( mat(i,j,3) .eq. 2 ) .and. ( mat(i,j,1) .eq. 3 ) ) then
               buffe = + coupling/2.0_dp
            endif
            if( ( mat(i,j,1) .eq. 2 ) .and. ( mat(i,j,3) .eq. 3 ) ) then
               buffe = + coupling/2.0_dp
            endif
            if( ( mat(i,j,3) .eq. 1 ) .and. ( mat(i,j,1) .eq. 1 ) ) then
               buffe = - coupling/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,1) .eq. 3 ) .and. ( mat(i,j,3) .eq. 3 ) ) then
               buffe = + coupling/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,3) .eq. 1 ) ) then
               buffe = - coupling/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,3) .eq. 3 ) .and. ( mat(i,j,1) .eq. 3 ) ) then
               buffe = + coupling/sqrt(2.0_dp)
            endif
            H(i,j) = H(i,j) + (buff1*buffe)

         else
            if( ( mat(i,j,3) .eq. middle ) .and. ( mat(i,j,1) .eq. (middle+1) ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,1) .eq. middle ) .and. ( mat(i,j,3) .eq. (middle+1) ) ) then
               buffe = - coupling
            endif
            H(i,j) = H(i,j) + (buff1*buffe)
         endif

      enddo
   enddo

   !check
   !do i=1,lmat
   !   do j=1,lmat
   !      write(*,*) real(H(i,j)),mat(i,j,:)
   !   enddo
   !   write(*,*)
   !enddo

end subroutine buildH

!----------------------------------------------------------------

subroutine buildHel(Hel)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: Hel
   integer :: i,j,k

   Hel = 0.0_dp
   
   do i=1,nlev
      if(i<=middle) then
         Hel(i,i) = onsiteL
      else
         Hel(i,i) = onsiteR
      endif
   enddo

   !couplings chain (the wrong terms will be corrected below)
   do i = 1,nlev-1
      Hel(i,i+1) = tchain
   enddo
   do i = 2,nlev
      Hel(i,i-1) = tchain
   enddo

   if(geo==1) then !STM
      Hel(middle,middle+1) = tchainw
      Hel(middle+1,middle) = tchainw   
   endif

   if(geo==2) then !RING
      Hel(middle,middle+1) = tchainw
      Hel(middle+1,middle) = tchainw   
      Hel(1,nlev) = tchain      
      Hel(nlev,1) = tchain 
   endif

   if(geo==16) then !WELL2L
      Hel(middle,middle+1) = tchainw
      Hel(middle+1,middle) = tchainw   
   endif     

   if(geo==16) then !2L1P
      Hel(middle,middle+1) = tchainw
      Hel(middle+1,middle) = tchainw   
   endif   

   if(geo==18) then !WELL2L
      !electronic coupling inside the well
      Hel(middle,middle+1) = tchainw
      Hel(middle+1,middle) = tchainw   

      !coupling of the well to the chain 
      Hel(middle,middle-1) = tchain2
      Hel(middle-1,middle) = tchain2
      Hel(middle+2,middle) = tchain2
      Hel(middle,middle+2) = tchain2

      !the bottom level of the well is isolated from the chain
      Hel(middle+2,middle+1) = 0.0_dp
      Hel(middle+1,middle+2) = 0.0_dp   
   endif  

   if(geo==19) then !RINGWELL2L
      !electronic coupling inside the well
      Hel(middle,middle+1) = tchainw
      Hel(middle+1,middle) = tchainw   

      !coupling of the well to the chain 
      Hel(middle,middle-1) = tchain2
      Hel(middle-1,middle) = tchain2
      Hel(middle+2,middle) = tchain2
      Hel(middle,middle+2) = tchain2

      !the bottom level of the well is isolated from the chain
      Hel(middle+2,middle+1) = 0.0_dp
      Hel(middle+1,middle+2) = 0.0_dp

      !ring
      Hel(1,nlev) = tchain
      Hel(nlev,1) = tchain
   endif  

   if(geo==23) then !3LBond
      Hel = 0.0_dp
      Hel(1,1) = E1
      Hel(2,2) = E2
      Hel(3,3) = E3
   endif  

end subroutine buildHel

!----------------------------------------------------------------

function findrhozero(H,mat,eigv)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: H
   integer, dimension(:,:,:), INTENT(IN) :: mat
   real(dp), dimension(:), INTENT(IN) :: eigv

   integer :: i,j,findrhozero

   !I start from a pure state |elle,enne1>. Search for the label that characterize it.
search: do i=1,lmat
           if( ( mat(i,1,1) .eq. elle ) .and. ( mat(i,1,2) .eq. enne1 ) ) then
             findrhozero = i
             exit search
           endif
        end do search  

end function findrhozero

!----------------------------------------------------------------

subroutine initialstate(Hel,H,mat,psi0)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: Hel
   complex(dp), dimension(:,:), INTENT(IN) :: H
   integer, dimension(:,:,:), INTENT(IN) :: mat
   complex(dp), dimension(:), INTENT(OUT) :: psi0
   complex(dp), dimension(lmat) :: psiTB
   integer :: i,A
   real(dp) :: norm

   !initial state in the TB basis
   psiTB = 0.0_dp
   norm = 0.0_dp
   do i = 1,lmat
      if(mat(i,1,2)==enne1) then !I'm on the diagonal of the ion
         psiTB(i) = Hel(mat(i,1,1),elle) !mat(i,1,1) is the TB electronic level associated with i
      endif
      norm = norm + real(psiTB(i),dp)*real(psiTB(i),dp)
   enddo
   write(*,*) sqrt(norm)

   psi0 = 0.0_dp   
   norm = 0.0_dp
   do A=1,lmat
      do i=1,lmat
         psi0(A) = psi0(A) + ( psiTB(i) * conjg(H(i,A)) )
      enddo
      norm = norm + real(psi0(A),dp)*conjg(psi0(A))
   enddo
   write(*,*) sqrt(norm)
   !pause

end subroutine initialstate

!----------------------------------------------------------------

subroutine initialstateBE(H,mat,psi0)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: H
   integer, dimension(:,:,:), INTENT(IN) :: mat
   complex(dp), dimension(:), INTENT(OUT) :: psi0
   complex(dp), dimension(lmat) :: psiosc
   integer :: i,A,initial,enne
   real(dp) :: Tinit,buf,Z,norm

   initial = findrhozero(H,label,eigv)
 
   !oscillators
   !initial temperature (from enne1)
   if(enne1.ne.0) then
      en = (0.5_dp + real(enne1,dp))*hb*omega1
      buf = log(( (2.0_dp*en) + hb*omega1 ) / ( (2.0_dp*en) - hb*omega1 )) 
      Tinit = hb*omega1 / ( kb*buf )
   else
      Tinit = 10.0_dp
   endif
   !partition function
   buf = 1.0_dp - exp(-(hb*omega1)/(kb*Tinit))
   Z = exp(-(hb*omega1)/(2.0_dp*kb*Tinit)) / buf
   write(*,*) initial,enne1,Tinit,Z     
   !stop

   !initial state in the TB basis
   psiosc = 0.0_dp
   norm = 0.0_dp
   do i = 1,lmat
      if(mat(i,1,1)==elle) then !I'm on the diagonal of the electron
         enne = mat(i,1,2)
         en = (0.5_dp + real(enne,dp))*hb*omega1
         psiosc(i) = sqrt(exp(-en/(kb*Tinit)) / Z)
         write(*,*) enne,en,real(psiosc(i),dp)
         norm = norm + real(psiosc(i),dp)*real(psiosc(i),dp)
      endif
   enddo 
   write(*,*) sqrt(norm)

   !conversion to H basis
   psi0 = 0.0_dp
   norm = 0.0_dp
   do A=1,lmat
      do i=1,lmat
         psi0(A) = psi0(A) + ( psiosc(i) * conjg(H(i,A)) )
      enddo
      norm = norm + real(psi0(A),dp)*conjg(psi0(A))
      !write(*,*) psi0(A)
   enddo
   write(*,*) sqrt(norm)
   pause

end subroutine initialstateBE

!----------------------------------------------------------------

subroutine buildexpEt(H,expEt,eigv,time,initial)
   
   use constx3l
   implicit none

   complex(dp), dimension(:), INTENT(OUT) :: expEt
   real(dp), dimension(:), INTENT(IN) :: eigv
   complex(dp), dimension(:,:), INTENT(IN) :: H
   real(dp) :: time
   integer :: A,initial

   do A=1,lmat
      expEt(A) = exp( -(im*time/hb)*eigv(A) ) * conjg(H(initial,A))
      !Convention: I consider <i|A> straight, the opposite I conjugate
   enddo

end subroutine buildexpEt

!----------------------------------------------------------------

subroutine buildexpEtNEW(expEt,eigv,time,psi0)
   
   use constx3l
   implicit none

   complex(dp), dimension(:), INTENT(OUT) :: expEt
   real(dp), dimension(:), INTENT(IN) :: eigv
   complex(dp), dimension(:), INTENT(IN) :: psi0
   real(dp) :: time
   integer :: A

   do A=1,lmat
      expEt(A) = exp( -(im*time/hb)*eigv(A) ) * psi0(A)
   enddo

end subroutine buildexpEtNEW

!----------------------------------------------------------------

subroutine buildevo(H,eigv,evo,expEt)
   
   use constx3l
   implicit none

   complex(dp), dimension(:), INTENT(OUT) :: evo
   complex(dp), dimension(:), INTENT(IN) :: expEt
   complex(dp), dimension(:,:), INTENT(IN) :: H
   real(dp), dimension(:), INTENT(IN) :: eigv
   integer :: A,i

   evo = 0.0_dp

   do i=1,lmat
      do A=1,lmat
         !the old vectors written in the eigenvector basis is this H(i,A)
         evo(i) = evo(i) + ( expEt(A) * H(i,A) )
      enddo
   enddo

end subroutine buildevo


!----------------------------------------------------------------
   
subroutine traces(rho,rhoel,mat,evn)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: rhoel
   complex(dp), dimension(:,:), INTENT(IN) :: rho
   integer, dimension(:,:,:), INTENT(IN) :: mat
   real(dp), INTENT(OUT) :: evn

   integer :: i,j,k,el1,el2, counter
   
   rhoel = 0.0_dp
   evn = 0.0_dp
   counter = 0

   !note to self: to speed it up I could store in a vector which elements are part of which trace
   !I'd build this vector at the very beginning with label, before building H

   do i=1,lmat
      do j=1,lmat
         
         !Trace over the phonon DOF, but I need a method to know 
         !to which electrons the labels i and j refer
         !I need to take into account only the diagonal elements in the phonon space
         if( mat(i,j,2)==mat(i,j,4) ) then
            !the eletronic levels, evaluates even the electronic off-diagonal 
            el1 = mat(i,j,1)
            el2 = mat(i,j,3)
            rhoel(el1,el2) = rhoel(el1,el2) + rho(i,j)

            !for the phonon occupancies, I need the trace over ALL DOF
            if(el1 == el2) then
               evn = evn + ( rho(i,j) *  mat(i,j,4) )
            endif
         endif

      enddo
   enddo
   !write(*,*) counter,lmat

end subroutine traces

!----------------------------------------------------------------

subroutine tracesapp(rho,mat,evn,time,storageBuffer)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: rho
   integer, dimension(:,:,:), INTENT(IN) :: mat
   real(dp), INTENT(IN) :: evn

   complex(dp), dimension(0:(nex-1),0:(nex-1)) :: rhoi1 !ionic density matrix for oscillator 1 and 2
   integer :: i,j,i1,j1
   real(dp) :: time
   complex(dp) :: an1,cr1
   complex(dp), dimension(:) :: storageBuffer 

   rhoi1 = 0.0_dp
   an1 = 0.0_dp
   cr1 = 0.0_dp

   !trace over the electronic DOF to get the ionic DMs
   do i=1,lmat
      do j=1,lmat    
         !DM for oscillator1
         !Trace over the electronic degrees of freedom and oscillator2
         if( mat(i,j,1)==mat(i,j,3) ) then
            i1 = mat(i,j,2)
            j1 = mat(i,j,4)
            rhoi1(i1,j1) = rhoi1(i1,j1) + rho(i,j)
         endif
      enddo
   enddo

   !estimate of the various double creation/destruction terms on the same oscillator

   !careful, the cycle starts off from 2 to avoid the terms with two annihilation operators 
   !which would go to zero
   do i=2,nex-1                
      !annihilation for oscillator1 
      an1 = an1 + sqrt(real(i,dp))*sqrt(i-1.0_dp)*rhoi1(i,i-2)
   enddo
   !careful, the cycle finishes at nex-3 to avoid the terms that with two creation operators 
   !would get too excited for my basis
   do i=0,nex-3
      !creation for oscillator1 
      cr1 = cr1 + sqrt(i+1.0_dp)*sqrt(i+2.0_dp)*rhoi1(i,i+2)
   enddo

   storageBuffer(1) = an1
   storageBuffer(2) = cr1

end subroutine tracesapp

!----------------------------------------------------------------

function energy(eigv,expEt)
   
   use constx3l
   implicit none

   complex(dp), dimension(:), INTENT(IN) :: expEt
   real(dp), dimension(:), INTENT(IN) :: eigv
   real(dp) :: energy
   integer :: A

   energy = 0.0_dp

   do A=1,lmat
      energy = energy + ( eigv(A) * expEt(A) )
   enddo

end function energy

!----------------------------------------------------------------

end program diag

