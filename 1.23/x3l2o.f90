program diag

   use OMP_LIB
   use constx3l  
   implicit none

   !LAPACK VARIABLES   
   complex(dp), dimension(:), allocatable :: work
   real(dp), dimension(:), allocatable :: rwork
   integer, dimension(:), allocatable :: iwork
   integer :: info
  
   !SYSTEM VARIABLES
   complex(dp), dimension(lmat2,lmat2) :: H !hamiltonian of the system
   real(dp) :: eigv(lmat2)    !ordered eigevalue vector
   complex(dp), dimension(lmat2,lmat2) :: rho !time dependent density matrix, and initial DM
   complex(dp), dimension(nlev,nlev) :: rhoel !time dependent ELECTRONIC density matrix
   complex(dp), dimension(lmat2) :: expEt,evo   !temporary matrix to speed up the calculation
   real(dp), dimension(0:nstep,6) :: storageRho !matrix where I store the results, easier to fill it up and print it later with OMP
   complex(dp), dimension(nlev,nlev,8) :: storageBuffer !storage for the local e.v. of the double creation/annihilation operators
   real(dp), dimension(0:nstep,10,3) :: storageDoubleEx !storage for the local e.v. of the double creation/annihilation operators
   !label is the matrix where I store the names of the kets corresponding to a hamiltonian element
   !e.g. <ijk|H|i'j'k'> where i is the electronic label(1:3), 
   !and j and k are the oscillator 1 and 2 labels (1:nex)
   integer, dimension(lmat2,lmat2,6) :: label
   real(dp) :: time , tottr, evn, evm,en
   complex(dp) :: buffer
   integer :: i,k,l,initial,nthreads
   integer :: t1,t2,t3,t4,t5,t6  !NEW INDEXES for testOD
   real(dp), dimension(0:nstep,3) :: storagetestOD !NEW

   
   !$OMP PARALLEL DEFAULT(shared), PRIVATE(i,time,expEt,evo,rho,k,l,rhoel,evn,evm,tottr,storageBuffer)   
 
  !---------------------------------------
  !single thread initialization and diagonalization of the hamiltonian
   !$OMP SINGLE
   call buildlabel(label)
   call buildH(H,label)

   !diagonalize H
   allocate(work(lwork))
   allocate(rwork(lrwork))
   allocate(iwork(liwork))
   call zheevd('V','U',lmat2,H,lmat2,eigv,work,LWORK,rwork,LRWORK,iwork,LIWORK,info)
   deallocate(work)
   deallocate(rwork)
   deallocate(iwork)
  
   !CHECK VARIOUS QUANTITIES
   !write(*,*) info
   !write(*,*) WORK(1), RWORK(1), IWORK(1)
   !do i=1,lmat2
   !   write(*,*) eigv(i)
   !enddo   
   !do i=1,lmat2   
   !   write(*,*) real(H(i,lmat2)),label(i,lmat2,:)
   !enddo
   
   !find the name of the vector in the |ijk> basis from which we start
   initial = findrhozero(H,label,eigv)
   open(33, file='dataX3l_2o.dat')
   open(35, file='double_ex_oscillator_2o.dat')
   open(41, file='double_ex_a1a1.dat')
   open(42, file='double_ex_a2a2.dat')
   open(43, file='double_ex_a1a2.dat')
   open(44, file='double_ex_a1a*2.dat')
   open(45, file='double_ex_select.dat')

   !!!!!
   !NEW
   open(39, file='testOD.dat')
search1: do i=1,lmat2
           if( ( label(i,1,1) .eq. 1 ) .and. ( label(i,1,2) .eq. 1 ) .and. ( label(i,1,3) .eq. 1 )  ) then
             t1 = i
             exit search1
           endif
        end do search1
search2: do i=1,lmat2
           if( ( label(i,1,1) .eq. 3 ) .and. ( label(i,1,2) .eq. 0 ) .and. ( label(i,1,3) .eq. 0 )  ) then
             t2 = i
             exit search2
           endif
        end do search2
search3: do i=1,lmat2
           if( ( label(i,1,1) .eq. 1 ) .and. ( label(i,1,2) .eq. 2 ) .and. ( label(i,1,3) .eq. 0 )  ) then
             t3 = i
             exit search3
           endif
        end do search3
search4: do i=1,lmat2
           if( ( label(i,1,1) .eq. 2 ) .and. ( label(i,1,2) .eq. 0 ) .and. ( label(i,1,3) .eq. 1 )  ) then
             t4 = i
             exit search4
           endif
        end do search4
search5: do i=1,lmat2
           if( ( label(i,1,1) .eq. 2 ) .and. ( label(i,1,2) .eq. 1 ) .and. ( label(i,1,3) .eq. 0 )  ) then
             t5 = i
             exit search5
           endif
        end do search5
search6: do i=1,lmat2
           if( ( label(i,1,1) .eq. 2 ) .and. ( label(i,1,2) .eq. 1 ) .and. ( label(i,1,3) .eq. 1 )  ) then
             t6 = i
             exit search6
           endif
        end do search6
   !!!

   call OMP_SET_NUM_THREADS(numthr)
   write(*,*) 'The number of threads is', omp_get_num_threads()
   nthreads = omp_get_num_threads()
   !$OMP END SINGLE
!--------------------------------------   
   
   !$OMP DO SCHEDULE(STATIC,nthreads)
   do i=0,nstep
      time = dt*i

      !computing once the exponential of the time evolution 
      !times the projection over the initail state
      call buildexpEt(H,expEt,eigv,time,initial)
      !building the old vectors time evoluted
      call buildevo(H,eigv,evo,expEt)

      !the density matrix in the old basis
      do k=1,lmat2
         do l=1,lmat2
            rho(k,l) = evo(k)*conjg(evo(l))
         enddo
      enddo

      !trace over the ionic DOF
      call traces(rho,rhoel,label,evn,evm)
      !off diagonal traces to estimate the magnitude of the terms we ignore in the approximate method
      !it prints the file inside the subroutine
      !comment out for performance
      call tracesapp(rho,label,evn,evm,time,storageBuffer)
      !total trace of rho
      tottr = real(rhoel(1,1),dp)+real(rhoel(2,2),dp)+real(rhoel(3,3),dp)

      !store the data to print in vectors with a thread depending label
      !$OMP CRITICAL
      if(geo2o==24) then
         storageRho(i,1) = real(rhoel(atom-1,atom-1),dp)
         storageRho(i,2) = real(rhoel(atom,atom),dp)
         storageRho(i,3) = real(rhoel(atom+1,atom+1),dp)
      elseif(geo2o==44) then
         storageRho(i,1) = real(rhoel(1,1),dp)
         storageRho(i,2) = real(rhoel(2,2),dp)
         storageRho(i,3) = real(rhoel(3,3),dp)
         storageRho(i,6) = real(rhoel(4,4),dp)
      elseif((geo2o==45).or.(geo2o==46)) then
         storageRho(i,1) = real(rhoel(1,1),dp)
         storageRho(i,2) = real(rhoel(2,2),dp)
         storageRho(i,3) = real(rhoel(nlev-1,nlev-1),dp)
         storageRho(i,6) = real(rhoel(nlev,nlev),dp)
      else
         storageRho(i,1) = real(rhoel(1,1),dp)
         storageRho(i,2) = real(rhoel(2,2),dp)
         storageRho(i,3) = real(rhoel(3,3),dp)
      endif
      storageRho(i,4) = evn
      storageRho(i,5) = evm

      storageDoubleEx(i,1,3) = maxval(abs(storageBuffer(:,:,1)))   !an1
      storageDoubleEx(i,1,1:2) = maxloc(abs(storageBuffer(:,:,1))) !an1

      storageDoubleEx(i,2,3) = maxval(abs(storageBuffer(:,:,2)))   !an2
      storageDoubleEx(i,2,1:2) = maxloc(abs(storageBuffer(:,:,2))) !an2

      storageDoubleEx(i,3,3) = maxval(abs(storageBuffer(:,:,3)))   !cr1
      storageDoubleEx(i,3,1:2) = maxloc(abs(storageBuffer(:,:,3))) !cr1

      storageDoubleEx(i,4,3) = maxval(abs(storageBuffer(:,:,4)))   !cr2
      storageDoubleEx(i,4,1:2) = maxloc(abs(storageBuffer(:,:,4))) !cr2

      storageDoubleEx(i,5,3) = maxval(abs(storageBuffer(:,:,5)))   !an1an2
      storageDoubleEx(i,5,1:2) = maxloc(abs(storageBuffer(:,:,5))) !an1an2

      storageDoubleEx(i,6,3) = maxval(abs(storageBuffer(:,:,6)))   !an1cr2
      storageDoubleEx(i,6,1:2) = maxloc(abs(storageBuffer(:,:,6))) !an1cr2

      storageDoubleEx(i,7,3) = maxval(abs(storageBuffer(:,:,7)))   !cr1an2
      storageDoubleEx(i,7,1:2) = maxloc(abs(storageBuffer(:,:,7))) !cr1an2

      storageDoubleEx(i,8,3) = maxval(abs(storageBuffer(:,:,8)))   !cr1cr2
      storageDoubleEx(i,8,1:2) = maxloc(abs(storageBuffer(:,:,8))) !cr1cr2

      !System dependent choice of variable to store
      storageDoubleEx(i,9,3) = (abs(storageBuffer(1,72,5)))   

      !!!NEW
      storagetestOD(i,1) = abs(rho(t1,t2))
      storagetestOD(i,2) = abs(rho(t1,t3))
      storagetestOD(i,3) = abs(rho(t1,t6))
      !!!
      !$OMP END CRITICAL

      !print on screen (only the thread 0 by convention)
      if(omp_get_thread_num()==0) then
         if((geo2o==44).or.(geo2o==45).or.(geo2o==46)) then
            write(*,'(F9.2,F10.5,F10.5,F10.5,F10.5)') dt*i,storageRho(i,1), &
                                storageRho(i,2),storageRho(i,3),storageRho(i,6)
         else
            write(*,'(F9.2,F10.5,F10.5,F10.5)') dt*i,storageRho(i,1), &
                                storageRho(i,2),storageRho(i,3)
         endif
      endif

   enddo
   !$OMP END DO
   
   !$OMP SINGLE
   !print the result to file
   do i=0,nstep
      if((geo2o==44).or.(geo2o==45).or.(geo2o==46)) then
         write(33,'(F9.3,F10.4,F10.4,F10.4,F10.4,F10.4,F10.4,F10.4)') dt*i,storageRho(i,1),storageRho(i,2),&
                             storageRho(i,3),storageRho(i,6),storageRho(i,4),storageRho(i,5)
      else
         write(33,'(F9.3,F10.4,F10.4,F10.4,F10.4,F10.4,F10.4)') dt*i,storageRho(i,1),storageRho(i,2),&
                             storageRho(i,3),storageRho(i,4),storageRho(i,5)
      endif
      write(35,'(F9.3,F10.6,F10.6,F10.6,F10.6,F10.6,F10.6,F10.6,F10.6)') dt*i,storageDoubleEx(i,1,3),storageDoubleEx(i,2,3),storageDoubleEx(i,3,3), &
                          storageDoubleEx(i,4,3),storageDoubleEx(i,5,3),storageDoubleEx(i,6,3),storageDoubleEx(i,7,3),storageDoubleEx(i,8,3)
      write(39,'(F9.3,E13.3,E13.3,E13.3)') dt*i,storagetestOD(i,1),storagetestOD(i,2),storagetestOD(i,3)
      !printing the maximum of the aa and where it is in the TB basis
      write(41,'(F12.3,i12,i12,F15.7)') dt*i,int(storageDoubleEx(i,1,1)),int(storageDoubleEx(i,1,2)),storageDoubleEx(i,1,3)
      write(42,'(F12.3,i12,i12,F15.7)') dt*i,int(storageDoubleEx(i,2,1)),int(storageDoubleEx(i,2,2)),storageDoubleEx(i,2,3)
      write(43,'(F12.3,i12,i12,F15.7)') dt*i,int(storageDoubleEx(i,5,1)),int(storageDoubleEx(i,5,2)),storageDoubleEx(i,5,3)
      write(44,'(F12.3,i12,i12,F15.7)') dt*i,int(storageDoubleEx(i,6,1)),int(storageDoubleEx(i,6,2)),storageDoubleEx(i,6,3)
      write(45,'(F12.3,F15.7)') dt*i,storageDoubleEx(i,9,3)
   enddo
   !$OMP END SINGLE   
   
   !$OMP END PARALLEL



!-------------------END MAIN PROGRAM-------------------------




contains

!establishes a criterium with which to fill H later on with states |ijk>
!basically it's a map from |p> (with p from 1 to lmat2+nex*nex*nlev) to |ijk>
subroutine buildlabel(mat)

   use constx3l
   implicit none
   
   integer, dimension(:,:,:), INTENT(OUT) :: mat
   integer :: i,j,k,l,m,n,in1,in2

   do in1 = 1,lmat2
      do in2 = 1,lmat2
         !each index moves modulus a different frequency. The third index of both bra and kets
         ! i.e. n and k, moves at the highest frequency, the others move slower

         !l,m,n depend only on the column i.e. in2
         n = mod( in2-1 , nex ) + nmin              !the phonons go from 0 to nex-1
         m = mod( (in2-1)/nex , nex ) + nmin
         l = mod( (in2-1)/(nex*nex) , nlev ) + 1 !the electronic levels go from 1 to nlev
         !i,j,k depend only on the row i.e. in1
         k = mod( in1-1 , nex) + nmin
         j = mod( (in1-1)/nex , nex ) + nmin
         i = mod( (in1-1)/(nex*nex) , nlev ) + 1

         !careful to overwriting of indexes!!!!!!
         mat(in1,in2,1) = i   !kind of electronic level in the bra
         mat(in1,in2,2) = j   !number of phonons in osc 1 in the bra
         mat(in1,in2,3) = k   !number of phonons in osc 2 in the bra
         mat(in1,in2,4) = l   !kind of electronic level in the ket
         mat(in1,in2,5) = m   !number of phonons in osc 1 in the ket
         mat(in1,in2,6) = n   !number of phonons in osc 2 in the ket   
      enddo
   enddo

   !check
   !do i=1,lmat2
   !   do j=1,lmat2
   !      write(*,*) mat(i,j,:)
   !   enddo
   !   write(*,*)
   !enddo
   
end subroutine buildlabel

!----------------------------------------------------------------

subroutine buildH(H,mat)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: H
   integer, dimension(:,:,:), INTENT(IN) :: mat
   complex(dp) :: buff1,buff2,buffe
   real(dp) :: en1,en2
   integer :: i,j

   H = 0.0_dp

   do i = 1,lmat2
      do j = 1,lmat2

         !--------------
         !DIAGONAL TERMS
         !warning: these terms are NOT degenerate in the system they don't act, 
         !i.e. if the electronic coordinate is diagonal, also the oscillators need to be 
         !otherwise <N'M'|NM> may be zero

         if( (mat(i,j,1)==mat(i,j,4)) .and. (mat(i,j,2)==mat(i,j,5)) .and. (mat(i,j,3)==mat(i,j,6)) ) then

            !REDESIGN, this part is not really flexible because of the initial conditions
            !ELECTRONIC PART

            if((geo2o==22) .or. (geo2o==23)) then !3LBond
               if(nlev.ne.3) then
                  write(*,*) 'ERROR, nlev must be 3'
                  STOP
               endif
               !if we are on electronic level 1
               if(mat(i,j,1) == 1) then
                  H(i,j) = H(i,j) + E1
               endif
               !if we are on electronic level 2
               if(mat(i,j,1) == 2) then
                  H(i,j) = H(i,j) + E2
               endif
               !if we are on electronic level 3
               if(mat(i,j,1) == 3) then
                  H(i,j) = H(i,j) + E3
               endif
            endif

            if(geo2o==24) then !3LRing
               !if we are on electronic level 1
               if(mat(i,j,1) == (atom-1)) then
                  H(i,j) = H(i,j) + E1
               endif
               !if we are on electronic level 2
               if(mat(i,j,1) == atom) then
                  H(i,j) = H(i,j) + E2
               endif
               !if we are on electronic level 3
               if(mat(i,j,1) == (atom+1)) then
                  H(i,j) = H(i,j) + E3
               endif
            endif

            if(geo2o==44) then !4L
               if(nlev.ne.4) then
                  write(*,*) 'ERROR, nlev must be 3'
                  STOP
               endif
               !if we are on electronic level 1
               if(mat(i,j,1) == 1) then
                  H(i,j) = H(i,j) + E1
               endif
               !if we are on electronic level 2
               if(mat(i,j,1) == 2) then
                  H(i,j) = H(i,j) + E2
               endif
               !if we are on electronic level 3
               if(mat(i,j,1) == 3) then
                  H(i,j) = H(i,j) + E2
               endif
               !if we are on electronic level 3
               if(mat(i,j,1) == 4) then
                  H(i,j) = H(i,j) + E3
               endif
            endif

            if(geo2o==45) then !4Lelimbo
               !if we are on electronic level 1
               if(mat(i,j,1) == 1) then
                  H(i,j) = H(i,j) + E1
               endif
               !if we are on the last electronic level
               if(mat(i,j,1) == nlev) then
                  H(i,j) = H(i,j) + E3
               endif
            endif

            if(geo2o==46) then !SemicImp
               !if we are on electronic level 1
               if(mat(i,j,1) == 1) then
                  H(i,j) = H(i,j) + E1             
               !if we are on the last electronic level
               elseif(mat(i,j,1) == nlev) then
                  H(i,j) = H(i,j) + E1
               else
                  if(mod(mat(i,j,1),2)==0) then  
                     H(i,j) = H(i,j) + E2
                  elseif(mod(mat(i,j,1),2)==1) then  
                     H(i,j) = H(i,j) + E3
                  endif
               endif
            endif

            !OSCILLATOR1
            H(i,j) = H(i,j) + (real(mat(i,j,2),dp) + 0.5_dp)*hb*omega1

            !OSCILLATOR2
            H(i,j) = H(i,j) + (real(mat(i,j,3),dp) + 0.5_dp)*hb*omega2

         endif


         !------------------
         !OFF-DIAGONAL TERMS

         !ELECTRONIC HOPPING
         !I must be on the oscillator diagonal for instance
         if( (mat(i,j,2)==mat(i,j,5)) .and. (mat(i,j,3)==mat(i,j,6)) ) then

            if(geo2o == 24) then !3LRING
               !electronic hopping of the ring
               if( (mat(i,j,1)==(mat(i,j,4)+1)) .or. (mat(i,j,1)==(mat(i,j,4)-1)) ) then
                  H(i,j) = tchain
                  !zero hoppings near the 3 levels
                  if( (mat(i,j,1)==(atom)) .or. (mat(i,j,1)==(atom+1)) .or. (mat(i,j,1)==(atom-1)) ) then
                     H(i,j) = 0.0_dp
                  endif
               endif

               !electronic coupling of the 3 levels to the ring
               !LEFT
               if( ( mat(i,j,1) .eq. (atom-2) ) .and. ( mat(i,j,4) .eq. (atom-1) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom-2) ) .and. ( mat(i,j,4) .eq. (atom) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom-2) ) .and. ( mat(i,j,4) .eq. (atom+1) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom-1) ) .and. ( mat(i,j,4) .eq. (atom-2) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom) ) .and. ( mat(i,j,4) .eq. (atom-2) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom+1) ) .and. ( mat(i,j,4) .eq. (atom-2) ) ) then  
                  H(i,j) = tchainw
               endif
               !RIGHT
               if( ( mat(i,j,1) .eq. (atom+2) ) .and. ( mat(i,j,4) .eq. (atom-1) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom+2) ) .and. ( mat(i,j,4) .eq. (atom) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom+2) ) .and. ( mat(i,j,4) .eq. (atom+1) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom-1) ) .and. ( mat(i,j,4) .eq. (atom+2) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom) ) .and. ( mat(i,j,4) .eq. (atom+2) ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. (atom+1) ) .and. ( mat(i,j,4) .eq. (atom+2) ) ) then  
                  H(i,j) = tchainw
               endif

               !the last level is conncted to the first
               if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. nlev ) ) then  
                  H(i,j) = tchain
               endif
               if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. nlev ) ) then
                  H(i,j) = tchain
               endif
            endif

            if(geo2o == 44) then !4L
               if( ( mat(i,j,1) .eq. 2 ) .and. ( mat(i,j,4) .eq. 3 ) ) then  
                  H(i,j) = tchainw
               endif
               if( ( mat(i,j,1) .eq. 3 ) .and. ( mat(i,j,4) .eq. 2 ) ) then  
                  H(i,j) = tchainw
               endif
            endif

            if(geo2o == 45) then !4Lelimbo
               !electronic hopping in the central region
               if( (mat(i,j,1)==(mat(i,j,4)+1)) .or. (mat(i,j,1)==(mat(i,j,4)-1)) ) then
                  H(i,j) = tchain
               endif
               !zero hoppings at the extremes
               if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 2 ) ) then  
                  H(i,j) = 0.0_dp
               endif
               if( ( mat(i,j,1) .eq. 2 ) .and. ( mat(i,j,4) .eq. 1 ) ) then  
                  H(i,j) = 0.0_dp
               endif
               if( ( mat(i,j,1) .eq. (nlev-1) ) .and. ( mat(i,j,4) .eq. nlev ) ) then  
                  H(i,j) = 0.0_dp
               endif
               if( ( mat(i,j,1) .eq. nlev ) .and. ( mat(i,j,4) .eq. (nlev-1) ) ) then  
                  H(i,j) = 0.0_dp
               endif
               !hoppings between the 2levels and the chain
               if( ( mat(i,j,1) .eq. 2 ) .and. ( mat(i,j,4) .eq. 3 ) ) then  
                  H(i,j) = tchain
               endif
               if( ( mat(i,j,1) .eq. 3 ) .and. ( mat(i,j,4) .eq. 2 ) ) then  
                  H(i,j) = tchain
               endif
               if( ( mat(i,j,1) .eq. (nlev-1) ) .and. ( mat(i,j,4) .eq. (nlev-2) ) ) then  
                  H(i,j) = tchain
               endif
               if( ( mat(i,j,1) .eq. (nlev-2) ) .and. ( mat(i,j,4) .eq. (nlev-1) ) ) then  
                  H(i,j) = tchain
               endif
            endif

            if(geo2o == 46) then !SemicImp
               !electronic hopping in the central region
               !if( (mat(i,j,1)==(mat(i,j,4)+1)) ) then
               !   if(mod(mat(i,j,1),2)==0) then  !hopping 1
               !      H(i,j) = tchain
               !   elseif(mod(mat(i,j,1),2)==1) then  !hopping 2
               !      H(i,j) = tchain2
               !   endif
               !endif

               !if( ((mat(i,j,1)+1)==(mat(i,j,4))) ) then
               !   if(mod(mat(i,j,1),2)==0) then  !hopping 1
               !      H(i,j) = tchain
               !   elseif(mod(mat(i,j,1),2)==1) then  !hopping 2
               !      H(i,j) = tchain2
               !   endif
               !endif

               if( (mat(i,j,1)==(mat(i,j,4)+1)) .or. (mat(i,j,1)==(mat(i,j,4)-1)) ) then
                  H(i,j) = tchain
               endif

               !zero hoppings at the extremes
               if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 2 ) ) then  
                  H(i,j) = 0.0_dp         
               endif
               if( ( mat(i,j,1) .eq. 2 ) .and. ( mat(i,j,4) .eq. 1 ) ) then  
                  H(i,j) = 0.0_dp         
               endif
               if( ( mat(i,j,1) .eq. (nlev-1) ) .and. ( mat(i,j,4) .eq. nlev ) ) then  
                  H(i,j) = 0.0_dp
               endif
               if( ( mat(i,j,1) .eq. nlev ) .and. ( mat(i,j,4) .eq. (nlev-1) ) ) then  
                  H(i,j) = 0.0_dp
               endif
            endif

         endif

         !OSCILLATOR1 (acts on the right)
         !When the oscillator1 is neither in the ground state or the most excited state (sort of cutoff)
         !both the terms of the annihilation and destruction operator COULD survive, otherwise only one
         buff1 = 0.0_dp
         buffe = 0.0_dp
         en1 = real(mat(i,j,5),dp)
         !for instance we need to be on the diagonal of oscillator 2         
         if(mat(i,j,3) == mat(i,j,6)) then
            if( ( mat(i,j,5) .ne. nmin ) .and. ( mat(i,j,5) .ne. (nmax) )  ) then
               !If the Creation operator acts on the ket AND the bra is correct
               if( mat(i,j,2) .eq. (mat(i,j,5) + 1) ) then
                  buff1 = fact1 * (sqrt(en1+1.0_dp))
               endif
               !If the Annihilation operator acts on the ket AND the bra is correct
               if( mat(i,j,2) .eq. (mat(i,j,5) - 1) ) then
                  buff1 = fact1 * (sqrt(en1))
               endif
            elseif( mat(i,j,5) .eq. nmin ) then
               !ONLY the Creation operator can acts IF the bra is correct
               if( mat(i,j,2) .eq. (mat(i,j,5) + 1) ) then
                  buff1 = fact1 * (sqrt(en1+1.0_dp))
               endif
            elseif( mat(i,j,5) .eq. (nmax) ) then
               !ONLY the Annihilation operator can acts IF the bra is correct
               if( mat(i,j,2) .eq. (mat(i,j,5) - 1) ) then
                  buff1 = fact1 * (sqrt(en1))
               endif
            endif
         endif
         !ELECTRONIC (acts on the left)
         !oscillator 1 is coupled to level 1 and 2
         if(geo2o == 22) then !3L2P
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 2 ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 2 ) ) then
               buffe = - coupling
            endif
         endif
         
         if(geo2o == 23) then !3LBond
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 2 ) ) then
               buffe = - coupling/2.0_dp
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 2 ) ) then
               buffe = - coupling/2.0_dp
            endif
            if( ( mat(i,j,4) .eq. 2 ) .and. ( mat(i,j,1) .eq. 3 ) ) then
               buffe = + coupling/2.0_dp
            endif
            if( ( mat(i,j,1) .eq. 2 ) .and. ( mat(i,j,4) .eq. 3 ) ) then
               buffe = + coupling/2.0_dp
            endif
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 1 ) ) then
               buffe = - coupling/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,1) .eq. 3 ) .and. ( mat(i,j,4) .eq. 3 ) ) then
               buffe = + coupling/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 1 ) ) then
               buffe = - coupling/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,4) .eq. 3 ) .and. ( mat(i,j,1) .eq. 3 ) ) then
               buffe = + coupling/sqrt(2.0_dp)
            endif
         endif

         if(geo2o == 24) then !3LRing
            if( ( mat(i,j,4) .eq. (atom-1) ) .and. ( mat(i,j,1) .eq. (atom) ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,1) .eq. (atom-1) ) .and. ( mat(i,j,4) .eq. (atom) ) ) then
               buffe = - coupling
            endif
            !if( ( mat(i,j,4) .eq. (atom-1) ) .and. ( mat(i,j,1) .eq. (atom) ) ) then
            !   buffe = - coupling/2.0_dp
            !endif
            !if( ( mat(i,j,1) .eq. (atom-1) ) .and. ( mat(i,j,4) .eq. (atom) ) ) then
            !   buffe = - coupling/2.0_dp
            !endif
            !if( ( mat(i,j,4) .eq. (atom) ) .and. ( mat(i,j,1) .eq. (atom+1) ) ) then
            !   buffe = + coupling/2.0_dp
            !endif
            !if( ( mat(i,j,1) .eq. (atom) ) .and. ( mat(i,j,4) .eq. (atom+1) ) ) then
            !   buffe = + coupling/2.0_dp
            !endif
            !if( ( mat(i,j,4) .eq. (atom-1) ) .and. ( mat(i,j,1) .eq. (atom-1) ) ) then
            !   buffe = - coupling/sqrt(2.0_dp)
            !endif
            !if( ( mat(i,j,1) .eq. (atom+1) ) .and. ( mat(i,j,4) .eq. (atom+1) ) ) then
            !   buffe = + coupling/sqrt(2.0_dp)
            !endif
            !if( ( mat(i,j,1) .eq. (atom-1) ) .and. ( mat(i,j,4) .eq. (atom-1) ) ) then
            !   buffe = - coupling/sqrt(2.0_dp)
            !endif
            !if( ( mat(i,j,4) .eq. (atom+1) ) .and. ( mat(i,j,1) .eq. (atom+1) ) ) then
            !   buffe = + coupling/sqrt(2.0_dp)
            !endif
         endif
         if(geo2o == 44) then !4L
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 2 ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 2 ) ) then
               buffe = - coupling
            endif
         endif
         if(geo2o == 45) then !4Lelimbo
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 2 ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 2 ) ) then
               buffe = - coupling
            endif
         endif
         if(geo2o == 46) then !SemicImp
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 2 ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 2 ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 3 ) ) then
               buffe = - coupling
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 3 ) ) then
               buffe = - coupling
            endif
         endif

         H(i,j) = H(i,j) + (buff1*buffe)

         !OSCILLATOR2
         en2 = real(mat(i,j,6),dp)
         buff2 = 0.0_dp
         buffe = 0.0_dp
         !for instance we need to be on the diagonal of oscillator 1  
         if(mat(i,j,2) == mat(i,j,5)) then   
            if( ( mat(i,j,6) .ne. nmin ) .and. ( mat(i,j,6) .ne. (nmax) )  ) then
               !If the Creation operator acts on the ket AND the bra is correct
               if( mat(i,j,3) .eq. (mat(i,j,6) + 1) ) then
                  buff2 = fact2 * (sqrt(en2+1.0_dp))
               endif
               !If the Annihilation operator acts on the ket AND the bra is correct
               if( mat(i,j,3) .eq. (mat(i,j,6) - 1) ) then
                  buff2 = fact2 * (sqrt(en2))
               endif
            elseif( mat(i,j,6) .eq. nmin ) then
               !ONLY the Creation operator can acts IF the bra is correct
               if( mat(i,j,3) .eq. (mat(i,j,6) + 1) ) then
                  buff2 = fact2 * (sqrt(en2+1.0_dp))
               endif
            elseif( mat(i,j,6) .eq. (nmax) ) then
               !ONLY the Annihilation operator can acts IF the bra is correct
               if( mat(i,j,3) .eq. (mat(i,j,6) - 1) ) then
                  buff2 = fact2 * (sqrt(en2))
               endif
            endif
         endif         
         !ELECTRONIC (acts on the left)
         !oscillator 2 is coupled to level 2 and 3
         if(geo2o == 22) then !3L2P
            if( ( mat(i,j,4) .eq. 2 ) .and. ( mat(i,j,1) .eq. 3 ) ) then
               buffe = - coupling2
            endif
            if( ( mat(i,j,1) .eq. 2 ) .and. ( mat(i,j,4) .eq. 3 ) ) then
               buffe = - coupling2
            endif
         endif

         if(geo2o == 23) then !3LBond
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 2 ) ) then   !!!!!!!!!!NEW
               buffe = + coupling2/2.0_dp
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 2 ) ) then
               buffe = + coupling2/2.0_dp
            endif
            if( ( mat(i,j,4) .eq. 2 ) .and. ( mat(i,j,1) .eq. 3 ) ) then
               buffe = - coupling2/2.0_dp
            endif
            if( ( mat(i,j,1) .eq. 2 ) .and. ( mat(i,j,4) .eq. 3 ) ) then
               buffe = - coupling2/2.0_dp
            endif
            if( ( mat(i,j,4) .eq. 1 ) .and. ( mat(i,j,1) .eq. 1 ) ) then
               buffe = - coupling2/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,1) .eq. 3 ) .and. ( mat(i,j,4) .eq. 3 ) ) then
               buffe = + coupling2/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,1) .eq. 1 ) .and. ( mat(i,j,4) .eq. 1 ) ) then
               buffe = - coupling2/sqrt(2.0_dp)
            endif
            if( ( mat(i,j,4) .eq. 3 ) .and. ( mat(i,j,1) .eq. 3 ) ) then
               buffe = + coupling2/sqrt(2.0_dp)
            endif
         endif

         if(geo2o == 24) then !3LRing
            if( ( mat(i,j,4) .eq. (atom+1) ) .and. ( mat(i,j,1) .eq. (atom) ) ) then   !!!!!!!!!!NEW
               buffe = - coupling2
            endif
            if( ( mat(i,j,1) .eq. (atom+1) ) .and. ( mat(i,j,4) .eq. (atom) ) ) then
               buffe = - coupling2
            endif
            !if( ( mat(i,j,4) .eq. (atom-1) ) .and. ( mat(i,j,1) .eq. (atom) ) ) then   !!!!!!!!!!NEW
            !   buffe = + coupling2/2.0_dp
            !endif
            !if( ( mat(i,j,1) .eq. (atom-1) ) .and. ( mat(i,j,4) .eq. (atom) ) ) then
            !   buffe = + coupling2/2.0_dp
            !endif
            !if( ( mat(i,j,4) .eq. (atom) ) .and. ( mat(i,j,1) .eq. (atom+1) ) ) then
            !   buffe = - coupling2/2.0_dp
            !endif
            !if( ( mat(i,j,1) .eq. (atom) ) .and. ( mat(i,j,4) .eq. (atom+1) ) ) then
            !   buffe = - coupling2/2.0_dp
            !endif
            !if( ( mat(i,j,4) .eq. (atom-1) ) .and. ( mat(i,j,1) .eq. (atom-1) ) ) then
            !   buffe = - coupling2/sqrt(2.0_dp)
            !endif
            !if( ( mat(i,j,1) .eq. (atom+1) ) .and. ( mat(i,j,4) .eq. (atom+1) ) ) then
            !   buffe = + coupling2/sqrt(2.0_dp)
            !endif
            !if( ( mat(i,j,1) .eq. (atom-1) ) .and. ( mat(i,j,4) .eq. (atom-1) ) ) then
            !   buffe = - coupling2/sqrt(2.0_dp)
            !endif
            !if( ( mat(i,j,4) .eq. (atom+1) ) .and. ( mat(i,j,1) .eq. (atom+1) ) ) then
            !   buffe = + coupling2/sqrt(2.0_dp)
            !endif
         endif
         if(geo2o == 44) then !4L
            if( ( mat(i,j,4) .eq. 3 ) .and. ( mat(i,j,1) .eq. 4 ) ) then
               buffe = - coupling2
            endif
            if( ( mat(i,j,1) .eq. 3 ) .and. ( mat(i,j,4) .eq. 4 ) ) then
               buffe = - coupling2
            endif
         endif
         if(geo2o == 45) then !4Lelimbo
            if( ( mat(i,j,4) .eq. (nlev-1) ) .and. ( mat(i,j,1) .eq. nlev ) ) then
               buffe = - coupling2
            endif
            if( ( mat(i,j,1) .eq. (nlev-1) ) .and. ( mat(i,j,4) .eq. nlev ) ) then
               buffe = - coupling2
            endif
         endif
         if(geo2o == 46) then !SemicImp
            if( ( mat(i,j,4) .eq. (nlev-1) ) .and. ( mat(i,j,1) .eq. nlev ) ) then
               buffe = - coupling2
            endif
            if( ( mat(i,j,1) .eq. (nlev-1) ) .and. ( mat(i,j,4) .eq. nlev ) ) then
               buffe = - coupling2
            endif
            if( ( mat(i,j,4) .eq. (nlev-2) ) .and. ( mat(i,j,1) .eq. nlev ) ) then
               buffe = - coupling2
            endif
            if( ( mat(i,j,1) .eq. (nlev-2) ) .and. ( mat(i,j,4) .eq. nlev ) ) then
               buffe = - coupling2
            endif
         endif

         H(i,j) = H(i,j) + (buff2*buffe)

      enddo
   enddo

   !check
   !do i=1,lmat2
   !   do j=1,lmat2
   !      write(*,*) real(H(i,j)),mat(i,j,:)
   !   enddo
   !   write(*,*)
   !enddo

end subroutine buildH

!----------------------------------------------------------------

function findrhozero(H,mat,eigv)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: H
   integer, dimension(:,:,:), INTENT(IN) :: mat
   real(dp), dimension(:), INTENT(IN) :: eigv

   integer :: i,j,findrhozero

   !I start from a pure state |elle,enne1,enne2>. Search for the label that characterize it.
   j=0
search: do i=1,lmat2
           if( ( mat(i,1,1) .eq. elle ) .and. ( mat(i,1,2) .eq. enne1 ) .and. ( mat(i,1,3) .eq. enne2 )  ) then
             findrhozero = i
             j=1
             exit search
           endif
        end do search  

   if(j==0) then
      write(*,*)
      write(*,*) 'ERROR!!'
      write(*,*) 'The initial state could not be found'
      write(*,*) 'Check than enne1 and enne2 are within [nmin,nmax]'
      write(*,*)
      STOP
   endif

end function findrhozero

!----------------------------------------------------------------

subroutine buildexpEt(H,expEt,eigv,time,initial)
   
   use constx3l
   implicit none

   complex(dp), dimension(:), INTENT(OUT) :: expEt
   real(dp), dimension(:), INTENT(IN) :: eigv
   complex(dp), dimension(:,:), INTENT(IN) :: H
   real(dp) :: time
   integer :: A,initial

   do A=1,lmat2
      expEt(A) = exp( -(im*time/hb)*eigv(A) ) * conjg(H(initial,A))
      !Convention: I consider <i|A> straight, the opposite I conjugate
   enddo

end subroutine buildexpEt


!----------------------------------------------------------------

subroutine buildevo(H,eigv,evo,expEt)
   
   use constx3l
   implicit none

   complex(dp), dimension(:), INTENT(OUT) :: evo
   complex(dp), dimension(:), INTENT(IN) :: expEt
   complex(dp), dimension(:,:), INTENT(IN) :: H
   real(dp), dimension(:), INTENT(IN) :: eigv
   integer :: A,i

   evo = 0.0_dp

   do i=1,lmat2
      do A=1,lmat2
         !the old vectors written in the eigenvector basis is this H(i,A)
         evo(i) = evo(i) + ( expEt(A) * H(i,A) )
      enddo
   enddo

end subroutine buildevo


!----------------------------------------------------------------
   
subroutine traces(rho,rhoel,mat,evn,evm)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(OUT) :: rhoel
   complex(dp), dimension(:,:), INTENT(IN) :: rho
   integer, dimension(:,:,:), INTENT(IN) :: mat
   real(dp), INTENT(OUT) :: evn, evm

   integer :: i,j,k,el1,el2, counter
   
   rhoel = 0.0_dp
   evn = 0.0_dp
   evm = 0.0_dp
   counter = 0

   !note to self: to speed it up I could store in a vector which elements are part of which trace
   !I'd build this vector at the very beginning with label, before building H

   do i=1,lmat2
      do j=1,lmat2
         
         !Trace over the phonon DOF, but I need a method to know 
         !to which electrons the labels i and j refer
         !I need to take into account only the diagonal elements in the phonon space
         if( (mat(i,j,2)==mat(i,j,5)) .and. (mat(i,j,3)==mat(i,j,6)) ) then
            !the eletronic levels, evaluates even the electronic off-diagonal 
            el1 = mat(i,j,1)
            el2 = mat(i,j,4)
            rhoel(el1,el2) = rhoel(el1,el2) + rho(i,j)

            !for the phonon occupancies, I need the trace over ALL DOF
            if(el1 == el2) then
               evn = evn + ( rho(i,j) *  mat(i,j,5) )
               evm = evm + ( rho(i,j) *  mat(i,j,6) )
            endif
         endif

      enddo
   enddo
   !write(*,*) counter,lmat2

end subroutine traces

!----------------------------------------------------------------

subroutine tracesapp(rho,mat,evn,evm,time,storageBuffer)

   use constx3l
   implicit none

   complex(dp), dimension(:,:), INTENT(IN) :: rho
   integer, dimension(:,:,:), INTENT(IN) :: mat
   real(dp), INTENT(IN) :: evn, evm
   complex(dp), dimension(:,:,:), INTENT(OUT) :: storageBuffer

   complex(dp), dimension(nmin:nmax,nmin:nmax) :: rhoi1,rhoi2 !ionic density matrix for oscillator 1 and 2
   integer :: i,j,i1,j1,i2,j2
   real(dp) :: time
   complex(dp), dimension(nlev,nlev) :: an1,an2,cr1,cr2
   complex(dp), dimension(nlev,nlev) :: an1an2,an1cr2,cr1an2,cr1cr2
   integer :: el1,el2 

   rhoi1 = 0.0_dp
   rhoi2 = 0.0_dp
   an1 = 0.0_dp
   an2 = 0.0_dp
   cr1 = 0.0_dp
   cr2 = 0.0_dp
   an1an2 = 0.0_dp
   cr1cr2 = 0.0_dp
   an1cr2 = 0.0_dp
   cr1an2 = 0.0_dp

   !trace over the electronic DOF to get the ionic DMs
   do i=1,lmat2
      do j=1,lmat2
         
         !DM for oscillator1
         !Trace over the electronic degrees of freedom and oscillator2
         !if( (mat(i,j,1)==mat(i,j,4)) .and. (mat(i,j,3)==mat(i,j,6)) ) then
         !   i1 = mat(i,j,2) 
         !   j1 = mat(i,j,5) 
         !   rhoi1(i1,j1) = rhoi1(i1,j1) + rho(i,j)
         !   !write(*,*) i1,j1,abs(rhoi1(i1,j1))
         !endif

         !DM for oscillator2
         !Trace over the electronic degrees of freedom and oscillator1
         !if( (mat(i,j,1)==mat(i,j,4)) .and. (mat(i,j,2)==mat(i,j,5)) ) then
         !   i2 = mat(i,j,3) 
         !   j2 = mat(i,j,6) 
         !   rhoi2(i2,j2) = rhoi2(i2,j2) + rho(i,j)
         !endif

         el1 = mat(i,j,1)
         el2 = mat(i,j,4)                  

         !2^ APPROXIMATION, same oscillator
         !oscillator 1
         if( mat(i,j,3)==mat(i,j,6) ) then
            !double annihilation
            if( mat(i,j,2).ge.(nmin+2) ) then
               if( mat(i,j,5).eq.(mat(i,j,2)-2)  ) then
                  an1(el1,el2) = an1(el1,el2) + sqrt(real(mat(i,j,2),dp))*sqrt(real((mat(i,j,2)-1),dp))*(rho(i,j))
               endif
            endif
            !double creation
            if( mat(i,j,2).le.(nmax-2) ) then
               if( mat(i,j,5).eq.(mat(i,j,2)+2)  ) then
                  cr1(el1,el2) = cr1(el1,el2) + sqrt(real((mat(i,j,2)+1),dp))*sqrt(real((mat(i,j,2)+2),dp))*(rho(i,j))
               endif
            endif
         endif

         !oscillator 2
         if( mat(i,j,2)==mat(i,j,5) ) then
            !double annihilation
            if( mat(i,j,3).ge.(nmin+2) ) then
               if( mat(i,j,6).eq.(mat(i,j,3)-2)  ) then
                  an2(el1,el2) = an2(el1,el2) + sqrt(real(mat(i,j,3),dp))*sqrt(real((mat(i,j,3)-1),dp))*(rho(i,j))
               endif
            endif
            !double creation
            if( mat(i,j,3).le.(nmax-2) ) then
               if( mat(i,j,6).eq.(mat(i,j,3)+2)  ) then
                  cr2(el1,el2) = cr2(el1,el2) + sqrt(real((mat(i,j,3)+1),dp))*sqrt(real((mat(i,j,3)+2),dp))*(rho(i,j))
               endif
            endif
         endif

         !3^ APPROXIMATION, mixed terms for the oscillators
         !double annihilation for different oscillators            
         if( (mat(i,j,2).ne.nmin) .and. (mat(i,j,3).ne.nmin) ) then
            if( ( mat(i,j,5).eq.(mat(i,j,2)-1) ) .and. ( (mat(i,j,6)).eq.(mat(i,j,3)-1) ) ) then
               an1an2(el1,el2) = an1an2(el1,el2) + sqrt(real(mat(i,j,2),dp))*sqrt(real(mat(i,j,3),dp))*(rho(i,j))
            endif
         endif

         !double creation for different oscillators            
         if( (mat(i,j,2).ne.(nmax)) .and. (mat(i,j,3).ne.(nmax)) ) then
            if( ( (mat(i,j,5)).eq.(mat(i,j,2)+1) ) .and. ( (mat(i,j,6)).eq.(mat(i,j,3)+1) ) ) then
               cr1cr2(el1,el2) = cr1cr2(el1,el2) + sqrt(mat(i,j,2)+1.0_dp)*sqrt(mat(i,j,3)+1.0_dp)*(rho(i,j))
            endif
         endif

         !creation for oscillator1 and annihilation for oscillator2
         if( (mat(i,j,2).ne.(nmax)) .and. (mat(i,j,3).ne.nmin) ) then
            if( ( (mat(i,j,5)).eq.(mat(i,j,2)+1) ) .and. ( (mat(i,j,6)).eq.(mat(i,j,3)-1) ) ) then
               cr1an2(el1,el2) = cr1an2(el1,el2) + sqrt(mat(i,j,2)+1.0_dp)*sqrt(real(mat(i,j,3),dp))*(rho(i,j))
            endif
         endif

         !annihilation for oscillator1 and creation for oscillator2
         if( (mat(i,j,2).ne.nmin) .and. (mat(i,j,3).ne.(nmax)) ) then
            if( ( (mat(i,j,5)).eq.(mat(i,j,2)-1) ) .and. ( (mat(i,j,6)).eq.(mat(i,j,3)+1) ) ) then
               an1cr2(el1,el2) = an1cr2(el1,el2) + sqrt(real(mat(i,j,2),dp))*sqrt(mat(i,j,3)+1.0_dp)*(rho(i,j))
            endif
         endif

      enddo
   enddo

   storageBuffer(:,:,1) = an1
   storageBuffer(:,:,2) = an2
   storageBuffer(:,:,3) = cr1
   storageBuffer(:,:,4) = cr2
   storageBuffer(:,:,5) = an1an2
   storageBuffer(:,:,6) = an1cr2
   storageBuffer(:,:,7) = cr1an2
   storageBuffer(:,:,8) = cr1cr2   

end subroutine tracesapp

!----------------------------------------------------------------

function energy(eigv,expEt)
   
   use constx3l
   implicit none

   complex(dp), dimension(:), INTENT(IN) :: expEt
   real(dp), dimension(:), INTENT(IN) :: eigv
   real(dp) :: energy
   integer :: A

   energy = 0.0_dp

   do A=1,lmat2
      energy = energy + ( eigv(A) * expEt(A) )
   enddo

end function energy

!----------------------------------------------------------------

end program diag

