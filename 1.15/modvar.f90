module modvar
   
   implicit none

   real, parameter :: release = 1.15 !release number
   
   !numerical and computational parameters (don't change them in general on the same machine)
   integer, parameter :: dp = selected_real_kind(12) !precision for reals and complex numbers    
   integer, parameter :: trick = 100  !Euler trick, every trick step an Euler integration is performed instead of a leapfrog 
   integer, parameter :: obsfreq = 200  !it indicates how often the observables are evaluated. Every obsfreq steps, the observables are evaluated
   integer, parameter :: openboundaries = 1 !if 0 the OB are off, if 1 they are on,
                                            !if 2 they are on in Eunan's crippled version (del will be forced to be gam/2)
                                            !if 11 they are on with the ELECTRON GUN MODE (on top of the normal OB, we shoot e- in a certain energy window)
   integer, parameter :: Ehrenfest = 0  !if 1, it turns off the anticommutator which relates to the electronic noise and reproduces a simil Ehrenfest
                                        !situation. hf doesn't mean anything in this case. If 0, the program runs normally. 
                                        !if 2 it turns off BOTH the anticommutator and the commutator in the Ac/As equations.
                                        !this is equivalent to a Born-Oppenheimer-like simulation
   integer, parameter :: hf = 1 !if 0 the Hartree-Fock approximation terms in mu and Ndot are off 
                                !if 1 they are on (they are of capital importance) 
                                !if 2 they are on with Tchavdar's suggestion to ignore their first part, the trace
                                !if 3, NEW!!!!!! the Extendded Hartree-Fock is activated
   integer, parameter :: HplusSwitch = 1 !if 1, Hplus is H0 + SigmaPlus, if 2 it is H0 + i epsE I
   integer, parameter :: tracecentral = 0   !if 1 it traces the electronic energy only over the central region,
                                            !otherwise it traces it over the whole wire
   integer, parameter :: Flipswitch = 0   !if 1 the purification of the DM flips its negative eigenvalues
                                          !if 0 it uses purenonflip, a small positive value instead
   integer, parameter :: specheat = 0   !if 1 it computes the specific heat in diagH and saves it in the cv file
   integer, parameter :: fixedN = 1 !if 1 it turns off Npunto and fixes the average N for the oscillators at their initial value
                                    !it is like an overdamped system that dissipates heat immediately
                                    !if 2 Ndot is 0 ONLY for the first scillator
                                    !if 0 it lets N evolve normally, which means that the oscillators are isolated and don't dissipate at all
   integer, parameter :: fixedRho = 0  !if 1 it turns off rhopunto and fixes rho to its initial condition. It tests the frictionless
                                       !and pure noise energy transfer from hot electrons to the oscillators
   integer, parameter :: Telrho = 23   !if 1, the initial DM is chosen from the eigenstates of H, weighted by the FD distribution at T=inTel
                                      !if 0 the initial DM is just half a band filled from the eigenstates of H, the same as inTel=0K
                                      !if 2, x3l mode, TB basis, the initial electronic Density matrix has 1 electron on 'initTB'
                                      !if 3, x3l mode, H0 basis, the initial electronic Density matrix has 1 electron on state 'initx3l'
                                      !if 15, custom configuration for MOLECULAR mode. 1 electron on every 2s state on the left part of the wire.
                                      !if 16, custom configuration for WELL2L, we start from the lower level of the well
                                      !if 20, WATER, depending on orbh2o the orbitals are filled in the TB basis
                                      !if 21, WATER, depending on orbh2o the orbitals are filled with water electrons 
                                      !if 22, WATER, the orbitals are filled until chempot (which is determined by an unpreturbed system of water without leads)
                                      !if 23, WATER, the orbitals are filled at temperature inTel around chempot
   integer, parameter :: iseed = 33 !if 0, the program uses the system clock as random seed, if not 0 that is the seed and the sequence is reproducible

   !SWITCHES for different systems
   integer, parameter :: nho = 1     !if 0, no oscillators (large speedup), if not, it's the number of independent oscillators
                                     !for nmodes, it better is nmnumber/2 so that there are no double counted modes
   integer, parameter :: ndim = 1   !number of dimensions of the system 
   integer, parameter :: nmodes = 0  !NEW!!!!! if 1 it turns on the normal modes calculation, the parameters are taken from the first osc in input
   integer, parameter :: x3l = 0  !NEW!!!!! in general it means that a comparison with the 1 electron 1 phonon can be performed in this mode
                                  !if 0, we are in the multi electron mode, there is spin in the EOM and the trace of rho is half the number of electronic levels
                                  !if 1, it requires STM or 2L1P, it turns on the x3l comparison mode where 1 oscillator coupled to a 1 electron system
                                  !if 2, it requires WELL/WELLS, 1 electron and oscillators coupled to a well
                                  !if 3, it requires 3L2P or 3LRing or 4L, it turns on the x3l2o comparison mode where 2 oscillators are coupled to 3/4 or many electronic levels
   integer, parameter :: anderson = 0 !if 1 it activates the anderson localization mode
   integer, parameter :: molecular = 0 !if 1 it activates the molecular mode, the geometry is not a single orbital TB anymore, but a multilevel wire
   !!!!WATER
   integer, parameter :: h2o = 4 !NEW!!!!! if not 0, it activates H2O-MODE with h2o molecules of water in the middle of the chain 
   integer, parameter :: orbh2o = 4 !NEW!! number of orbitals per water molecule 
                                    !if 4, 1s1 H1, 1s1 H2, 2p4 O (without py)
                                    !if 5, 1s1 H1, 1s1 H2, 2p4 O
                                    !if 6, 1s H1, 1s1 H2, 2s2 O, 2p4 O
   integer, parameter :: meterlead = 40 !NEW!!!!! length of a buffer part of the leads (L and R) where the current is evaluated
                                       !it needs to be at least 2

   !NEW PARAMETERS, DA SISTEMARE DA QUALCHE PARTE
   !3D
   integer, parameter :: LX = 4 !3D
   integer, parameter :: LY = 4 !3D
   integer, parameter :: LZ = 4 !3D  
   real(dp), parameter :: tcube = -1.0_dp  !3D     
   !2D
   integer, parameter :: geomsurf = 1  !effective only for ndim=2, the surface   
   integer, parameter :: righe = 6   
   integer, parameter :: colonne = 6  
   real(dp), parameter :: tgrid = -1.0_dp  
   !1D
   integer, parameter :: geomwire = 53  !effective only for ndim=1, the wire.
                                       !from 1 to 99, Einstein oscillators, from 100 on Normal Modes geometries
                                       !---------------Einstein oscillators--------------------
                                       !if 1 DIMER geometry
                                       !if 2, ATOM at the center of the wire
                                       !if 3, STM, a wire with a  weak bond in the middle
                                       !if 4, EINSTEIN, several oscillators in the middle of NC, nho must be even
                                       !if 5, OHM, several oscillators uniformly distributed in NC. If nho=0, just a perfect chain
                                       !if 6, LEGION, several Einstein oscillators at random position in the central region excluding the boundaries
                                       !if 7, FULL CIRCLE, like OHM but for a wire with PBC, the last site is connected to the first in the hamiltonian
                                       !if 8, DISSIPION, the first 4 oscillators are placed 2 by 2 in the leads to replace gamma 
                                       !if 9, RANDSEA, the oscillators are coupled to all the sites within the central region 
                                       !      with F = (0,coup_max(in input.dat)
                                       !if 10, TURIN, wire to test the scent theory
                                       !if 11, ANDERRAND, random atomic oscillators in NC, for Anderson mode
                                       !if 12, WELL, Jorge's idea of a single level with less energy in the middle of the wire, NC must be odd, nho =1
                                       !if 13, WELLS, Jorge's idea of several single level with less energy in the middle of the wire, NC and nho odd
                                       !if 14, ANDEREQUAL, Anderson localized system, with equally spaced oscillators, like EINSTEIN
                                       !if 15, MOL2L, molecular mode, 2 levels, coupled by phonons
                                       !if 16, WELL2L, Jorge's idea of a well with 2 levels
                                       !if 17, ATOMS
                                       !if 18, BONDS
                                       !if 19, ALFREDOCV, it must have nlev-2 oscillators. Coupled as in atoms. They are in the leads too
                                       !if 20, 2L1P
                                       !if 21, RINGWELL, like 16 WELL2L but with the end of the wire connected in a ring
                                       !if 22, 3L2P
                                       !if 23, 3LBond, simulation of a 3 molecular orbital system, where F couples atomic orbitals 1-2 and 2-3 
                                       !if 24, 3LRing, 3 levels coupled weakly to a ring
                                       !if 44, 4L, 2 2-levels coupled weakly 
                                       !if 45, 4Lelimbo, 2-levels coupled via a buffer of electrons that help to lose memory of the previous oscillator
                                       !if 46, SemicImp, 2-levels represent impurities in a semiconductor, which is recreated by alternating hoppings in the wire.
                                               !The second 2-level is at the same energy as the first where in (45) it was at a higher energy.
                                       !if 50, WATER, with orbh2o=5
                                       !if 51, WATER, with orbh2o=6
                                       !if 52, WATER, with orbh2o=4 (all of the above work well just for h2o=1)
                                       !if 53, CP2K_WATER, with orbh2o=4 and geometry from CP2K
                                       !---------------Normal Modes--------------------
                                       !if 100, RING, the normal modes are in a ring within the central region
                                       !if 101, LINE, the normal modes are in on a line inside the central region

   integer, parameter :: NL = 80    !number of sites in the Left lead
   integer, parameter :: NR = NL    !number of sites in the Right lead
   integer, parameter :: NC = h2o*orbh2o   !number of sites in the center of the junction, 
                                    !careful that Eunan puts the dimer in the middle so it is equivalent to 20 +2
                                    !DIMER: it must be even
                                    !ATOM: it must be odd
                                    !STM: it must be even 
                                    !EINSTEIN: it must be even
                                    !OHM: it depends, careful to the disposition in diagH.f90 though
                                    !I tend to stay further away from the leads. It must be densityOhm*nho + 2 if density is even
                                    !or densityOhm*nho + 1 if it is odd
   integer, parameter :: oscL = NL + NC/2        !DIMER: position of the LEFT atom in the dimer
   integer, parameter :: oscR = NL + NC/2 + 1    !DIMER: position of the RIGHT atom in the dimer 
   integer, parameter :: atom = NL + NC/2 + 1    !ATOM,WELL,WELLS: position of the single oscillating atom or the center of the well
                                                 !WELL2L, position of the bottom of the well, atom-1 is the upper level in the well
   integer, parameter :: wwidth = 7    !WELLS: atomic width of the well (odd!)
   integer, parameter :: leftend = NL + NC/2     !STM, TURIN: position of the atom on the left of the weak link
   integer, parameter :: initx3l = 59  !X3L MODE: initial state for the electron in the H0 basis
   integer, parameter :: initTB = 1  !X3L MODE: initial state for the electron in the TB basis
   integer, parameter :: eqperiod = 3    !EINSTEIN, ANDERQUAL: periodicity of the oscillators
   integer, parameter :: Lfirst = NL + NC/eqperiod - nho + (eqperiod-1)    !EINSTEIN, ANDERQUAL: position of the first oscillator in the central region, 
                                                                !the last one is on site Lfirst + eqperiod*nho
   integer, parameter :: nminit = NL + 2  !RING,LINE: initial site where thermal modes start
   integer, parameter :: nmnumber = NC - 4  !RING,LINE: maximum number of NM available, number of sites within the movable region. 
                                             !It must be less or equal (recommended) then NC - 4 and EQUAL or BIGGER than nho
   integer, parameter :: siteJ = NL-1 !site where the current evaluated (and the relative +1 site on the right of it), on the right border of NC
   !number of sites, beware!!!!! It depends on the dimensionality of the system, choose only one and comment the others
   integer, parameter :: nlev = NL + NR + NC  !1D
   !integer, parameter :: nlev = righe*colonne  !2D
   !integer, parameter :: nlev = LX*LY*LZ  !3D


   !SIMULATION CONDITIONS 
   !times
   real(dp), parameter :: dt = 0.001_dp !time step in fs
   real(dp), parameter :: T = 200.00_dp !total simulation time in fs
   real(dp), parameter :: TOB = 10.000_dp !time in fs where the OB are turned on linearly, NOT the oscillator 
   real(dp), parameter :: Tcoup = 10.000_dp !time in fs where the coupling F is turned on linearly. Tel is evaluated after Tcoup.
   !energies
   real(dp), parameter :: onsite = 0.0_dp !onsite energy for the TB orbitals
   real(dp), parameter :: onsite1s = 0.0_dp !MOLECULAR mode, onsite energy for the 1s orbitals
   real(dp), parameter :: onsite2s = 1.0_dp !MOLECULAR mode, onsite energy for the 2s orbitals
   real(dp), parameter :: onsiteE1 = -0.2_dp !3L2P, energy level 1
   real(dp), parameter :: onsiteE2 = 0.0_dp    !3L2P, energy level 2
   real(dp), parameter :: onsiteE3 = 0.2_dp  !3L2P, energy level 3
   real(dp), parameter :: tchain = -2.5_dp !hopping parameter for the atoms in the leads L and R
   real(dp), parameter :: tchain1s = -1.0_dp !MOLECULAR mode, hopping energy for the 1s orbitals
   real(dp), parameter :: tchain2s = -2.0_dp !MOLECULAR mode, hopping energy for the 2s orbitals
   real(dp), parameter :: tchainW = -1.0_dp !3LRing weak coupling of the 3 levels to the ring
   real(dp), parameter :: tchain2 = -0.15_dp 
   !real(dp), parameter :: gamFIN = 0.0_dp !imaginary term in the H for the evolution for CS,CC,...    
   !real(dp), parameter :: gamFIN = abs(15.0_dp*tchain/(real(2*(NL-meterlead),dp))) !imaginary term in the H for the evolution for CS,CC,... 
   real(dp), parameter :: gamFIN = abs(15.0_dp*tchain/(real(2*(NL),dp))) !imaginary term in the H for the evolution for CS,CC,... 
   real(dp), parameter :: tcd = -0.5_dp !DIMER: hopping parameter for the atoms in the junction between LC and CR
   real(dp), parameter :: tdimer = -3.88_dp !DIMER: hopping parameter for the atoms in the central region C 
   real(dp), parameter :: onsiteAtom = -0.200_dp !ATOM, WELL: onsite energy of the atom in the middle of the chain
                                               !WELL2L: energy difference between the 2 levels inside the well
   real(dp), parameter :: wellshift = -0.0_dp !WELL2L: onsite energy shift of the levels in the well compared to the onsite in the rest of the chain
   real(dp), parameter :: acar = 3.00 !ATOM: characteristic distance of the chain, in A
   real(dp), parameter :: middle = -0.2_dp !STM,TURIN,X3L MODE: weak coupling of the central bond
                                           !WELL2L: weak coupling of the 2 levels well to the chain
   real(dp), parameter :: anderdis = 0.0_dp*abs(tchain) !ANDERSON: interval (-an,an) over which the onsite energies in NC are chosen
   real(dp), parameter :: anderTbath = 5000 !ANDERSON: fake temperature (K) of the oscillators (N is chosen from this and omega)
                                            !it relates to the energy via kT = (0.5+N)hb omega. It's a definition. The real T is the Bose Einstein
   real(dp), parameter :: epsE = 0.006666_dp  !dissipation parameter in the wire hamiltonian H_0 (in eV)
   real(dp), parameter :: inTel = 10000.0_dp  !initial electronic temperature (in K). It enters the program only if Telrho=1
   real(dp), parameter :: mixTel = 1.0E-2_dp !DESCRIVI MEGLIO e DERIVA MEGLIO IN FUNZIONE DEL TEMPO DA SMOOTHARE, ADIMENSIONALE
   real(dp), parameter :: purenonflip = 1.0E-4_dp !value that the negative eigenvalues of the DM assume is flipswitch = 0
   real(dp), parameter :: violfermi = 1.0E-8_dp !minimum violation of fermionicity so that it is pringted in purity.dat
   real(dp), parameter :: alfredoT = 0.0_dp !ALFREDOCV: temperature (K) of the oscillators (N is chosen from this and omega)
   !!WATER
   real(dp), parameter :: EHs = -13.61_dp
   real(dp), parameter :: EO2s = -29.14_dp    
   real(dp), parameter :: EO2p = -14.13_dp     
   real(dp), parameter :: tsp = 1.84_dp !hopping between H s orbital and O p orbital. NOT IN eV, it must be multiplied by hb^2/m_e R^2
   real(dp), parameter :: tss = -1.41_dp !hopping between H s orbital and O s orbital. NOT IN eV, it must be multiplied by hb^2/m_e R^2
   real(dp), parameter :: ROH = 0.9584_dp !A, distance between O and H at equilibrium
   real(dp), parameter :: Rhydbond = 2.5_dp !A, distance between O and H at equilibrium
   real(dp), parameter :: Rcutoff = 3.0_dp !A, distance between O and H at equilibrium
   real(dp), parameter :: theta = ((180.0_dp - 104.45_dp)/2.0_dp)*3.141592653589793_dp/180.0_dp !equilibrium angle for H2O in radians (it would be 104.45 degrees exactly)
   real(dp), parameter :: massH = 1.007825_dp !mass H (amu)
   real(dp), parameter :: massO = 15.9949_dp !mass O (amu)
   real(dp), parameter :: chempot = -13.87_dp !eV, CHANGE ME depending on the population of the water
   !!CP2K_WATER
   real(dp), parameter :: R_OH1 = 0.97_dp !A, distance between O and H1 (points out of the chain) at equilibrium
   real(dp), parameter :: R_OH2 = 1.00_dp !A, distance between O and H2 (points in of the chain) at equilibrium
   real(dp), parameter :: R_OO = 2.67_dp !A, distance between O and H2 (points in of the chain) at equilibrium
   real(dp), parameter :: beta_cp2k = 105.8_dp !degrees, angle of the H2O molecule in a chain in CP2K
   real(dp), parameter :: theta_cp2k = ((180.0_dp - beta_cp2k)/2.0_dp)*3.141592653589793_dp/180.0_dp !degrees
   real(dp), parameter :: alpha_cp2k = ((180.0_dp - 54.3)/2.0_dp)*3.141592653589793_dp/180.0_dp !radians, angle between moecule 1 and molecule 2


   !The INITIAL CONDITIONS for the oscillators are in input.dat
   !every line corresponds to a different oscillator
   !the listed parameters are coupling, initial N, omega (in eV), mass (adimensional)

   !OB parameters
   real(dp), parameter :: Volt = 0.0_dp !difference of potential betwen the left and the right lead
   integer, parameter :: shapeV = 0   !shape in time of Volt. If 0 it is constant for all times,
                                      !if 1 it is a step of lentgth timeV
                                      !if 2 it is a gaussian (to be used with E-GUN)
   real(dp), parameter :: timeV = 2000.0_dp  !timescale for different shapeV
   !real(dp), parameter :: gamOB = abs(1.0_dp*tchain/(real(NL+NR,dp)))  !wide band approximation parameter
   real(dp), parameter :: gamOB = gamFIN
   real(dp), parameter :: muL = chempot + Volt/2.0_dp !chemical potential in the LEFT lead
   real(dp), parameter :: muR = chempot - Volt/2.0_dp !chemical potential in the RIGHT lead
   real(dp), parameter :: del = 0.00005_dp !probes decoherence approximation parameter 
   real(dp), parameter :: BI = -100000.0_dp !lower boundary of energy integral in the injection term
   real(dp), parameter :: egE = -11.5!chempot + 4.0_dp  !ELECTRON GUN mode, center of the energy level shot (eV)
   real(dp), parameter :: egW = 0.0_dp  !ELECTRON GUN mode, width of the energy level shot, the window is [egE-egW,egE+egW]
   real(dp), parameter :: egunmu = 40.0_dp  !ELECTRON GUN mode, PULSE (shapeV = 2), center of gaussian impulse (fs)
   real(dp), parameter :: egunsigma = 2.0_dp  !ELECTRON GUN mode, PULSE (shapeV = 2), variance sigma of gaussian pulse (fs)

   !STEADY.F90, exact conduction for finite wires and Landauer (used ONLY there)
   integer, parameter :: scanon = 0  !if 1 it turns on the scan of the current for voltages from dV to Vmax
                                     !if 2 it turns on the scan of the current for voltages AND classical displacements from -Xmax to Xmax in steps dX
                                     !if 0 it evaluates one single value of the current for the OB parameters above
   real(dp), parameter :: dV = 0.1_dp  !increment voltage in the scan
   real(dp), parameter :: Vmax = 11.00_dp  !increment voltage in the scan
   real(dp), parameter :: dX = 0.01_dp  !step in classical displacement scan
   real(dp), parameter :: Xmax = 0.5_dp  !maximum displacement in scan
   real(dp), parameter :: Xclass = 0.07_dp  !displacement for DOS and T
   integer, parameter :: integralLand = 0  !if 1 it turns on the Landauer integration of the Trasmission for the dimer system (only that system, analytical)
   real(dp), parameter :: dE = 0.0001_dp
   integer, parameter :: drawDOS = 0 !if 1 it draws a DOS from a file in qbox style, if 2 in a plain list style
                                     !if 3, it draws the DOS from the imaginary part of G-
                                     !if 4, it draws the DOS from the imaginary part of G-, with a classical displacement Xclass (change the code for different modes)
                                     !if 5, it draws the DOS for classical displacements from 0 to Xmax, in steps dX
   integer, parameter :: drawTrasmission = 10 !if 1 it draws a trasmission curve from -enmin to enmax
                                             !if 4, it draws the trasmission curve with a classical displacement Xclass (change the code for different modes)
                                             !if 5, it draws the trasmission curve for classical displacements from 0 to Xmax, in steps dX
                                             !if 10, NEW, ONLY transmission calculations with Tchavdar's trick
   real(dp), parameter :: enmin = chempot - 7.0_dp
   real(dp), parameter :: enmax = chempot + 7.0_dp
   real(dp), parameter :: deltaen = 0.02_dp
   real(dp), parameter :: sigmagauss = 0.1_dp   
 
   !UNIVERSAL CONSTANTS
   real(dp), parameter :: hb = 0.658211942_dp  !hbar in eV*fs
   real(dp), parameter :: mp = 104.3968511_dp  !proton mass in eV*fs^2/A^2
   real(dp), parameter :: amu = 103.642753  !atomic mass unit in eV*fs^2/A^2  (1/12 of the mass of carbon)
   real(dp), parameter :: pi =  3.141592653589793_dp
   real(dp), parameter :: kb = 8.6173324E-05  !Boltzmann constant in eV/K
   complex(dp), parameter :: im = (0.0_dp,1.0_dp) !i
   real(dp), parameter :: quantumConductance = 77.480917 ! muA/eV
   real(dp), parameter :: microampere = 973.6538812 ! muA/eV
   real(dp), parameter :: convevcm = 8065.54! cm^-1/eV
   real(dp), parameter :: masselectron = 1822.88839!=1amu, mass of the electron 
   real(dp), parameter :: zero = 1.0E-20_dp  !when checking for hermiticity, I consider numbers below this as zero
                                            !I also use it for the temperature calculation using Delta entropy, if it is below this, I skip to the next time step
   real(dp), parameter :: eps = epsE/hb   !dissipation parameter in the wire hamiltonian H_0 (in fs^-1), don't touch it
                                          !(I had to put it here, after the definition of hb, otherwise the program wouldn't compile)

   !LAPACK PARAMETERS
   integer, parameter :: LWORK = 10000000 !MAX integer is 2147483647, but I get errors. It must be at least (2 nlev + nlev**2)
   integer, parameter :: LRWORK=20000000
   integer, parameter :: LIWORK=15000

   type global
      complex(dp), allocatable :: rdot(:,:),inj(:,:)       
      real(dp), allocatable :: Hel(:,:)      !electronic Hamiltonian
      complex(dp), allocatable :: Hprojections(:,:),HprojectionsTC(:,:)
      real(dp), allocatable :: E(:)
      complex(dp) :: avTel,avTel2,avkT2,avkTo,avkTel
      real(dp) :: Ein 
      complex(dp), allocatable :: SigmaPlus(:),Hplus(:),HplusTC(:)  
      integer, allocatable :: nonZeroSigma(:,:), nonZeroH(:,:), nonZeroHplus(:,:), nonZeroHplusTC(:,:) !storage of labels created to optimize 
                              !the matrix multiplications since F is very sparse
                              !the 1 index runs 1:numNonZero, the second from 1:2 with 1 for rows and 2 for columns
                              !e.g. nonZeroF(5,1) contains the label of the row of the fifth non zero element of F
      complex(dp) :: tarocrho(nlev,nlev),tarocrhoeig(nlev,nlev)
   end type

   !if the oscillators and these variables are independent
   type oscillator
      complex(dp) :: N, Ndot, c1mu,c2mu   !N is kept here only for the observables, in fact it is updated only every time observables is called
      complex(dp), allocatable :: CCdot(:,:), CSdot(:,:), ACdot(:,:), ASdot(:,:)
      complex(dp), allocatable :: mu(:,:)
      real(dp) :: om,mass,coup
      complex(dp), allocatable :: F(:,:),Fstore(:,:)
      integer, allocatable :: nonZeroF(:,:),nonZeroFrho(:,:)
   end type

   !NEW!!!!!!!!!!!!!!!
   !evolution of the DM in the same style as the oscillators
   type rhoEvo
      complex(dp) :: rho(nlev,nlev)
   end type
   !explicit evolution of the oscillator variables
   type oscillatorEvo
      complex(dp) :: N
      complex(dp) :: CC(nlev,nlev), CS(nlev,nlev), AC(nlev,nlev), AS(nlev,nlev)  !I don't use them allocatable because of the pointer that doesn't work otherwise
   end type

   !NEW!!!!!!!!!!!!!!!
   type pointer
      type(oscillatorEvo), pointer :: ho
   end type 
   type pointerDM
      type(rhoEvo), pointer :: DM
   end type

end module modvar
