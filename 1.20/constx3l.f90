module constx3l
   
   implicit none
   
   integer, parameter :: dp = selected_real_kind(12) 
   integer, parameter :: nlev = 32    !number of electronic levels available, better if it is even now
   !integer, parameter :: nex = 6    !number of excitations to consider for oscillator1 and 2, change freely
   integer, parameter :: nmin = 8  !NEW
   integer, parameter :: nmax = 11  !NEW 
   integer, parameter :: nex = nmax-nmin+1 !NEW !number of excitations to consider for oscillator1 and 2, change freely
   integer, parameter :: lmat = nlev*nex    !dimension of a side of the hamiltonian
   integer, parameter :: lmat2 = nlev*nex*nex    !dimension of a side of the hamiltonian
!the number of oscillators in this program is striclty 1, it needs heavy modifications to increase it
   integer, parameter :: start = 0 !if 0, the initial state is a TB state with a certain electronic level fully occupied and N
                                   !if 1, the initial state is an excited state of the unperturbed electronic hamiltonian
                                   !if 2, NEW, Bose-Einstein: the electronic initial state is a TB state like 0 above, 
                                   !           the oscillator is not a pure state but a BE distribution with T determined by enne1
   integer, parameter :: numthr = 8    !number of threads

   !LAPACK PARAMETERS
   integer, parameter :: LWORK =500000000 !MAX integer is 2147483647, but I get errors
   integer, parameter :: LRWORK=1000000000
   integer, parameter :: LIWORK=1000000
   
   !UNIVERSAL CONSTANTS
   real(dp), parameter :: hb = 0.658211942_dp  !hbar in eV*fs
   real(dp), parameter :: kb = 8.6173324E-05  !Boltzmann constant in eV/K
   real(dp), parameter :: mp = 104.3968511_dp  !proton mass in eV*fs^2/A^2
   real(dp), parameter :: amu = 103.642753  !atomic mass unit in eV*fs^2/A^2  (1/12 of the mass of carbon)
   complex(dp), parameter :: im = (0.0_dp,1.0_dp) 

   !GEOMETRIES
   integer, parameter :: geo = 23  !if 1, STM a wire with a weak bond tchainw and an oscillator there
                                   !if 2, RING a ring with a weak bond tchainw and an oscillator there
                                   !if 16, MOL2L
                                   !if 17, 2L1P 2 levels 1 phonon  
                                   !if 18, WELL2L 
                                   !if 23, 3LBond                
   integer, parameter :: geo2o = 45  !if 22, 3L2P
                                     !if 23, 3LBond
                                     !if 24, 3LRing
                                     !if 44, 4L
                                     !if 45, 4Lelimbo
                                     !if 46, SemicImp
   !SIMULATION CONDITIONS   
   real(dp), parameter :: dt = 1.0_dp !time step in fs
   real(dp), parameter :: T = 1000.0_dp !total simulation time in fs
   integer, parameter :: nstep = int(T/dt)
   integer, parameter :: middle = int(nlev/2) !site in the middle of the electronic chain
   integer, parameter :: atom = int(nlev/2)+1 !site in the middle for 3LRing
   real(dp), parameter :: coupling = 0.05_dp   !coupling strength between the oscillator1 and the electron
   real(dp), parameter :: coupling2 = 0.05_dp  !ONLY x3l2o, coupling strength between the oscillator2 and the electron
   real(dp), parameter :: eps = 0.000_dp !small imaginary part for F, dissipative term
   real(dp), parameter :: onsiteL = 0.0_dp !onsite energy for the TB orbitals on the LEFT
   real(dp), parameter :: onsiteR = 0.0_dp !onsite energy for the TB orbitals on the RIGHT
   real(dp), parameter :: onsiteM = -0.0_dp !onsite energy for the middle atom,in WELL2L middle+1, the level with the lowest energy of the 2L
   real(dp), parameter :: E1 = -0.2_dp  !ONLY x3l2o, energy of the first level in eV
   real(dp), parameter :: E2 = 0.0_dp     !ONLY x3l2o, energy of the second level in eV
   real(dp), parameter :: E3 = 0.2_dp   !ONLY x3l2o, energy of the third levels in eV
   real(dp), parameter :: tchain = -0.1_dp !hopping parameter for the atoms 
   real(dp), parameter :: tchainw = -0.1_dp !weak hopping parameter in the middle of the chain, 3LRing
   real(dp), parameter :: tchain2 = -0.15_dp !hopping between "leads" in WELL2L


   !INITIAL CONDITIONS
   integer, parameter :: elle  = 1  !initial electronic level occupied (1:nlev). Depending on start it is either a TB state or a Hel eigenstate
   integer, parameter :: enne1 = 10 !initial value for the number of phonons in oscillator 1 (0:nex-1)
   integer, parameter :: enne2 = 10  !ONLY x3l2o, initial value for the number of phonons in oscillator 2
   real(dp), parameter :: omega1 = 0.200_dp/hb !characteristic frequency of the oscillator1 in PHz
   real(dp), parameter :: omega2 = 0.200_dp/hb !ONLY x3l2o, characteristic frequency of the oscillator2 in PHz 
   real(dp), parameter :: Mass1 = 0.50_dp*amu !mass of the oscillator1 in mp units  
   real(dp), parameter :: Mass2 = 0.50_dp*amu !ONLY x3l2o, mass of the oscillator2 in mp units 
   real(dp), parameter :: fact1 = sqrt(hb/(2.0_dp*Mass1*omega1)) !factor in front of the ann/creation operator in X for oscillator1
   real(dp), parameter :: fact2 = sqrt(hb/(2.0_dp*Mass2*omega2)) !ONLY x3l2o, factor in front of the ann/creation operator in X for oscillator2

end module constx3l
