program dif

   !This program 

   use modvar
   implicit none

   integer :: i,j,nstep
   integer, parameter :: numlev = 96
   integer, parameter :: numho = 15

   !initializing variables
   real(dp) :: effe1(numlev),effe2(numlev),effe3(numlev),effedot(numlev)
   real(dp) :: consttau,enne,deltat,tottime,time,broadelta,consttaurec
   real(dp) :: mass,coupling,hbaromega
   real(dp) :: enerTB(numlev),sinTB(numlev),sin2TB(numlev),cosTB(numlev)
   real(dp) :: scat(numlev,numlev),broad(numlev,numlev),tauup(numlev,numlev),taudown(numlev,numlev)
   integer :: eulertrick = 10
   integer :: ehr = 0
   integer :: scatmec = 1 !if 1, ATOM; if 2 BOND
   integer :: densos = 1 !if 1, sin(final); if 2, arithmetic average of sin(initial) and sin(final); if 3, geometric average

   enne = 0.1
   mass = 0.5_dp !amu
   coupling = 0.3_dp !eV/A
   hbaromega = 0.2 !eV
   broadelta = hbaromega/2.0_dp !eV

   do j = 1,numlev
      enerTB(j) = -2.0_dp*abs(tchain)*cos(j*pi/(numlev+1))
      sinTB(j) = sqrt( 1.0_dp - (enerTB(j)/2.0_dp)**2 )
      sin2TB(j) = sinTB(j)*sinTB(j)
      cosTB(j) = -enerTB(j)/(2.0_dp*abs(tchain))
   enddo

   !~~~~~~SCATTERING FACTORS~~~~~~ (check the constant factors)

   if (scatmec == 1) then !ATOM
      consttau = (abs(tchain)*mass*amu*(hbaromega/hb)*real(numlev)) / (4.0_dp*pi*coupling*coupling*(real(numho,dp)/real(numlev)))
      do i=1,numlev
         do j=1,numlev
            scat(i,j) = 2.0_dp*(sin2TB(i) + sin2TB(j))
         enddo
      enddo
   elseif (scatmec == 2) then !BOND
      consttau = (abs(tchain)*mass*amu*(hbaromega/hb)) / (coupling*coupling*(real(numho,dp)/real(numlev))) 
      do i=1,numlev
         do j=1,numlev
            scat(i,j) = (1.0_dp + cosTB(i)*cosTB(j))
         enddo
      enddo
   endif

   !~~~~~~BROADENING FACTORS~~~~~~
   do i=1,numlev
      do j=1,numlev
         broad(i,j) = exp( -(enerTB(j) - enerTB(i) - hbaromega)**2 / (broadelta*broadelta) ) / (broadelta*pi)
      enddo
   enddo

   do i=1,numlev
      do j=1,numlev
         tauup(i,j) = consttau/(scat(i,j)*broad(i,j))
         taudown(i,j) = consttau/(scat(i,j)*broad(j,i))
      enddo
   enddo

   !~~~~~~

   tottime = 20000 !fs
   deltat = 20.0 !fs
   open(61, file='diftime.dat')

   effe2 = 0.0_dp   
   do i=(numlev/2)+1,numlev
      effe2(i) = 1.0_dp  !start from an inverted population
   enddo
   call effepunto(effe2,effedot,enne,tauup,taudown,ehr)
   effe1 = effe2 - dt*effedot !euler backward
   effe3 = effe1 + 2.0_dp*dt*effedot

   do j = 1,numlev
      write(61,*) 0.0_dp,j,effe2(j)
   enddo 
   write(61,*)

   !leapfrog
   nstep = int(tottime/deltat)
   do i=1,nstep
      time = i*deltat
      !observables
      !write(*,*) effe2(1),effe2(numlev/2+2),effe2(numlev)
      !if(mod(i,obsfreq*obsfreq)==0) then
         do j = 1,numlev
            write(61,*) time,j,effe2(j)
         enddo 
         write(61,*)
         write(*,'(f9.3,f11.6,f11.6,f11.6)') time,effe2(1),effe2(numlev/2),effe2(numlev)
      !endif

      !renaming
      effe1 = effe2
      effe2 = effe3   

      call effepunto(effe2,effedot,enne,tauup,taudown,ehr)

      if(mod(i+1,eulertrick)==0) then !euler trick
         effe3 = effe2 + deltat*effedot
      else
         effe3 = effe1 + 2.0_dp*deltat*effedot
      endif
   enddo

   consttaurec = (4.0_dp*coupling*coupling*real(numho,dp))/(real(numlev,dp)*mass*amu*(hbaromega/hb))
   write(*,*)
   write(*,*) 'Scattering time', 1.0_dp/consttaurec, 'fs'
   write(*,*) 'Relative energy', hb*consttaurec, 'eV'
   STOP

!-------------------------------------

contains

subroutine effepunto(f,fdot,n,tauup,taudown,ehr)

   use modvar
   implicit none

   real(dp), dimension(:), INTENT(IN) :: f
   real(dp), dimension(:), INTENT(OUT) :: fdot
   real(dp), dimension(:,:), INTENT(IN) :: tauup,taudown
   real(dp), INTENT(IN) :: n
   integer, INTENT(IN) :: ehr

   integer :: i,j
   real(dp) :: buffer

   if(ehr .ne. 1) then

      do i=1,numlev
         fdot(i) = 0.0_dp
         do j=1,numlev
            buffer = -n*f(i)*(1.0_dp-f(j)) + (n+1.0_dp)*f(j)*(1.0_dp-f(i))
            fdot(i) = fdot(i) + (1/tauup(i,j))*buffer

            buffer = n*f(j)*(1.0_dp-f(i)) - (n+1.0_dp)*f(i)*(1.0_dp-f(j))
            fdot(i) = fdot(i) + (1/taudown(i,j))*buffer
         enddo
      enddo

   else  !EHRENFEST

   endif

end subroutine effepunto

end program dif
